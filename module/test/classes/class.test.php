<?php

class TestModelTest extends DbExtent {
	protected $_sTable = "songrequest";
	protected $_sIdField = "idSongrequest";
	public $_aInsertedData = array();
	
	
	public function methodInit() {
		$oMysql = mysqli_connect(Config::$sDbHost, Config::$sDbUser, Config::$sDbPassword, "sirenox");
		
		if( mysqli_connect_errno() ){
			array_push( $this->_aErrors, mysqli_connect_error );
			return false;
		}else
			$this->_oMysql = $oMysql;
		unset($oMysql);
	}
	
	public function getData() {
		$this->methodInit();
				
 		
		$this->_oQueryBuilder->setTable("test");
		$aDataTest = $this->getAll();

		
		parent::__construct();
		$this->_oQueryBuilder->setTable("mixed");
		$this->_sIdField = "user";
		
		
		foreach ($aDataTest AS $aTestVal) {
			if (strpos($aTestVal["name"], "@")) {
				$aTestVal["name"] = substr($aTestVal["name"], 0, strpos($aTestVal["name"], "@"));
			}
			
			$aData = array(
				"user" 			=> $aTestVal["name"],
				"points" 		=> intval((!isset($aTestVal["points"]))?0:$aTestVal["points"]),
				"timeTotal" 	=> intval((!isset($aTestVal["hour"]))?0:$aTestVal["hour"]),
				"lastUpdate" 	=> "2016-08-29 20:00:00"
			);
			
			if (in_array($aTestVal["name"], $this->_aInsertedData)) {
				echo "UPDATE `mixed` SET `points`=`points`+".intval($aTestVal["points"]).", `timeTotal`=`timeTotal`+".intval($aTestVal["hour"]).", `lastUpdate`='2016-08-29 20:00:00' WHERE `user`='".$aTestVal["name"]."';";
			} else {
				echo "INSERT INTO `mixed` (`user`, `points`, `timeTotal`, `lastUpdate`) VALUES ('".$aTestVal["name"]."', '".intval($aTestVal["points"])."', '".intval($aTestVal["hour"])."', '2016-08-29 20:00:00');";
				array_push($this->_aInsertedData, $aTestVal["name"]);
			}
			echo "<br />";
 		}
	}
	
	public function getData2() {
		$this->methodInit();
	
	
		$this->_oQueryBuilder->setTable("stats");
		$aDataStats = $this->getAll();
	
	
		parent::__construct();
		$this->_oQueryBuilder->setTable("mixed");
		$this->_sIdField = "user";
		
		
		foreach ($aDataStats AS $aStatsVal) {
			if (strpos($aStatsVal["username"], "@")) {
				$aStatsVal["username"] = substr($aStatsVal["username"], 0, strpos($aStatsVal["username"], "@"));
			}
			
			$aData = array(
				"postTotal" => intval((!isset($aStatsVal["chat"]))?0:$aStatsVal["chat"])
			);
			
			if (in_array($aStatsVal["username"], $this->_aInsertedData)) {
				echo "UPDATE `mixed` SET `postTotal`=`postTotal`+".intval($aStatsVal["chat"])." WHERE `user`='".$aStatsVal["username"]."';";
			} else {
				echo "INSERT INTO `mixed` (`user`, `postTotal`, `lastUpdate`) VALUES ('".$aStatsVal["username"]."', '".intval($aStatsVal["chat"])."', '2016-08-29 20:00:00');";
				array_push($this->_aInsertedData, $aStatsVal["username"]);
			}
			echo "<br />";
		}
	}
}