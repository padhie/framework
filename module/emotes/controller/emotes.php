<?php
class EmotesControllerEmotes extends eController implements iController {
	public function run() {
		error_reporting(E_ALL & ~E_WARNING);
		
		$sChannel = "xoneris";
		$sWarnings = "";
		$aEmotes = array(
			"Twitch" => array(),
			"Twitch Sub" => array(),
			"FrankenFaceZ" => array(),
			"Better Twitch TV" => array()
		);

		
		// Twitch
		try {
			$sTwitchEmotes = file_get_contents("https://api.twitch.tv/kraken/chat/".$sChannel."/emoticons");
			$aTwitchEmotes = json_decode($sTwitchEmotes, true);
			if (!isset($aTwitchEmotes["error"]) && isset($aTwitchEmotes["emoticons"])) {
				foreach ($aTwitchEmotes["emoticons"] AS $aSingleEmote) {
					if ($aSingleEmote["subscriber_only"] == true) {
						array_push(
							$aEmotes["Twitch Sub"],
							array(
								"tooltip" => $aSingleEmote["regex"],
								"url" => $aSingleEmote["url"]
							)
						);
					} else {
						array_push(
							$aEmotes["Twitch"],
							array(
								"tooltip" => $aSingleEmote["regex"],
								"url" => $aSingleEmote["url"]
							)
						);
					}
				}
			}
			unset($sTwitchEmotes, $aTwitchEmotes);
		} catch(Exception $e) {
			$sWarnings .= Translate::get("TWITCHEMOTES_TEMP_NOT_AVAILABLE")."<br />";
		}
		
		
		// Franker Face Z
		try {
			$sFfzEmotes = file_get_contents("http://api.frankerfacez.com/v1/room/".$sChannel);
			$aFfzEmotes = json_decode($sFfzEmotes, true);
			if (isset($aFfzEmotes["sets"]) && isset($aFfzEmotes["sets"]["2107"]) && isset($aFfzEmotes["sets"]["2107"]["emoticons"])) {
				foreach ($aFfzEmotes["sets"]["2107"]["emoticons"] AS $aSingleEmote) {
					array_push(
						$aEmotes["FrankenFaceZ"],
						array(
							"tooltip" => $aSingleEmote["name"],
							"url" => $aSingleEmote["urls"]["1"]
						)
					);
				}
			}
			unset($sFfzEmotes, $aFfzEmotes);
		} catch(Exception $e) {
			$sWarnings .= Translate::get("FFZEMOTES_TEMP_NOT_AVAILABLE")."<br />";
		}
		
		// Better Twitch TV
		try {
			$sBttvEmotes = file_get_contents("https://api.betterttv.net/2/channels/".$sChannel);
			$aBttvEmotes = json_decode($sBttvEmotes, true);
			if (isset($sBttvEmotes["emotes"])) {
				foreach ($sBttvEmotes["emotes"] AS $aSingleEmote) {
					array_push(
						$aEmotes["Better Twitch TV"],
						array(
							"tooltip" => $aSingleEmote["code"],
							"url" => $aSingleEmote["url"]
						)
					);
				}
			}
			unset($sBttvEmotes, $aBttvEmotes);
		} catch(Exception $e) {
			$sWarnings .= Translate::get("BTTVEMOTES_TEMP_NOT_AVAILABLE");
		}
		
		
		if ($sWarnings != "") {
			View::warning( $sWarnings );
		}
		

		View::emotes($aEmotes);
		
	}

	public function render() {
		View::navi("emotes");
		View::title("Emotes");
		
		parent::render();
	}

}