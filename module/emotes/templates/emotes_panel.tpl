<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{View::currentPlatform()}}</h3>
	</div>
	<div class="panel-body">
		{{foreach from=View::currentEmotes() item=aEmote}}
  			<img src="{{$aEmote.url}}" data-toggle="tooltip" data-placement="top" title="{{$aEmote.tooltip}}" alt="{{$aEmote.tooltip}}"/>
		{{/foreach}}
	</div>
</div>