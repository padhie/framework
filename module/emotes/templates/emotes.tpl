{{View::render("header")}}

<script type="text/javascript">
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	});
</script>

{{foreach from=View::emotes() key=sPlatform item=aEmotes}}
	{{if count($aEmotes) >= 1}}
		{{View::currentPlatform($sPlatform)}}
		{{View::currentEmotes($aEmotes)}}
		<div class="col-md-4">
			{{View::render("emotes_panel", false)}}
		</div>
	{{/if}}
{{/foreach}}

{{View::render("footer")}}