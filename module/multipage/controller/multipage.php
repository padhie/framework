<?php
class MultipageControllerMultipage extends eController implements iController {
	public function run() {
		$aSubMenu = array(
			"index" 		=> Translate::get("MENU_HOME"),
			"commands" 		=> Translate::get("MENU_COMMANDS"),
			"quotes" 		=> Translate::get("MENU_QUOTES"),
			"songrequest" 	=> Translate::get("MENU_SONGREQUEST"),
			"rangs" 		=> Translate::get("MENU_RANGS"),
			"user" 			=> Translate::get("MENU_USERS"),
			"emotes" 		=> Translate::get("MENU_EMOTES"),
		);
		View::subMenu($aSubMenu);
		unset($aSubMenu);
	}
	
	public function render() {
		View::navi("multipage");
		View::title("multipage");
		
		parent::render();
	}
}