{{View::render("header")}}
{{View::loadJqueryUi()}}

<style>
	#panelDiv > div {
		border: 2px solid black;
		background-color: #f5f5f5;
		position: absolute;
		top: 75px;
		left: 20px;
	}
	.topLevel {
		z-index: 10;
	}
</style>
<script type="text/javascript">
	var template = "<div id='{ID}' class='col-md-9' onClick='changeIndex(\"{ID}\");'>"+
						"<div class='col-md-12 head'>"+
							"<div class='col-md-11'>"+
								"<label>{ID}</label>"+
							"</div>"+
							"<div class='col-md-1 text-right'>"+
								"<a href='#' onClick='removePanel(\"{ID}\");'><i class='fa fa-times' aria-hidden='true'></i></a>"+
							"</div>"+
						"</div>"+
						"<div class='col-md-12'>"+
							"{CONTENT}"+
						"</div>"+
					"</div>";

	function openPanel(sView) {
		$.ajax({
			url: "{{Config::getServerHost()}}/"+sView+"?view=single"
		}).done(function(response) {
			var sContent = template.replace(/{CONTENT}/g, response);
			sContent = sContent.replace(/{ID}/g, sView);
			$("#panelDiv").append(sContent);
			$( "#"+sView ).draggable({
				handle: $('.head')
			});
		});
	}

	function removePanel(sView) {
		$("#"+sView).remove();
	}

	function changeIndex(sView) {
		$(".topLevel").removeClass("topLevel");
		$("#"+sView).addClass("topLevel");
	}
</script>

<div class="navi">
	{{foreach from=View::subMenu() key=sKey item=sVal}}
		<a href="#" onClick="openPanel('{{$sKey}}');">{{$sVal}}</a> |
	{{/foreach}}
</div>

<div id="panelDiv" class="row">
</div>

{{View::render("footer")}}