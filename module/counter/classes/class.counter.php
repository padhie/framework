<?php

class CounterModelCounter extends DbExtent {
	protected $_sTable = "counter";
	protected $_sIdField = "idCounter";
	
	public function setGroup($sGroup) {
		$this->_oQueryBuilder->addWhere("group", $sGroup);
	}
	
	public function getNewest() {
		$this->_oQueryBuilder->addWhere("deadline", date("Y-m-d H:i:s"), "", ">");
	}
}