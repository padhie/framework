<?php

class GiveawayModelGiveaway extends DbExtent {
	protected $_sTable = "giveaway";
	protected $_sIdField = "idGiveaway";
	
	public function setRangs($idGiveaway, $aRangIds=array()) {
		$this->_oQueryBuilder->setTable("giveaway_has_rang");
		$this->delete($idGiveaway);
		
		foreach ($aRangIds AS $iIdRang) {
			$this->insert(array(
				"idGiveaway" => $idGiveaway,
				"idRang" => $iIdRang
			));
		}
		
		$this->init();
	}
	
	public function getRangsOfGiveaway($idGiveaway) {
		$this->_oQueryBuilder->setTable("giveaway_has_rang");
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "rang", "idRang");
		$this->_oQueryBuilder->addWhere("idGiveaway", $idGiveaway);
	
		$aData = parent::getAll();
		$this->init();
		return $aData;
	}
}