<?php
class GiveawayControllerGiveaway extends eController implements iController {
	public function start() {
		parent::start();
		parent::loadModulClasses("admin");
		parent::loadModulClasses("user");
		parent::loadModulClasses("rangs");
		parent::loadModulClasses("settings", "admin");
	}
	
	public function run() {
		$oRang = Factory::getModel("Rangs", "Rangs");
		$oForm = new FormHelper(Config::getServerHost()."/giveaway", "post", "_self", "");
		$aTypData = array(
// 			array("key"=>"random", "value"=>Translate::get("RANDOM")),
			array("key"=>"raffle", "value"=>Translate::get("RAFFLE")),
		);
		$oForm->addElement("select",	"giveaway_type",		"", 					Translate::get("GIVEAWAY_TYPE"),			$aTypData, 					array("col-md-3", "col-md-9"));
// 		$oForm->addElement("multiple",	"giveaway_minRang",		"", 					Translate::get("MIN_RANG"), 				$oRang->getFormArray(), 	array("col-md-3", "col-md-9"));
		$oForm->addElement("switch",	"giveaway_anonym",		"", 					Translate::get("STAY_ANONYME"), 			array(), 					array("col-md-3", "col-md-9"));
		$oForm->addElement("number",	"giveaway_duration",	"", 					Translate::get("GIVEAWAY_AUTOENDINGTIME"), 	array(), 					array("col-md-3", "col-md-9"));
		$oForm->addElement("text",		"giveaway_intro",		"", 					Translate::get("INTROTEXT"), 				array(), 					array("col-md-3", "col-md-9"));
		$oForm->addElement("text",		"giveaway_profit",		"", 					Translate::get("PROFIT"), 					array(), 					array("col-md-3", "col-md-9"));
		$oForm->addElement("submit", 	"", 					Translate::get("SAVE"),	"", 										array(), 					"col-md-12");
		$oForm->addElement("hidden", 	"action", 				"save");
		View::formObject($oForm);
		unset($oForm);
		
		
		
		$oTwitch = new TwitchApi();
		View::twitchoAuthLink($oTwitch->authenticate());
		
		if (Params::cookieParams("twitchname") === false) {
			// Daten von Twitch vorhanden
			$sCode = "";
			if (Params::getParams("code")) {
				$sCode = Params::getParams("code");
			} elseif (Params::postParams("code")) {
				$sCode = Params::postParams("code");
			}

			// Wenn auth durch twitch
			View::allow(false);
			if ($sCode != "") {
				$sAccessToken = $oTwitch->get_access_token($sCode);
				$sUsername = $oTwitch->authenticated_user($sAccessToken);
				Params::setCookie("twitchname", $sUsername, (time()+3600));
			}
		}


		// Wenn Daten gefunden oder schon vorhanden
		if (Params::cookieParams("twitchname") !== false) {
			$oUser = Factory::getModel("User", "User");
			$oUser->setUsername(Params::cookieParams("twitchname"));
			$aUserdata = $oUser->getAllFuelData();
				
			// Hat Benutzer den erforderlichen Rang
			$oSettins = Factory::getModel("Settings", "Settings");
			if ($oUser->userHasRang($aUserdata[0]["idUser"], json_decode($oSettins->getSetting("giveaway_minRang"), true))) {
				View::userdata (Params::cookieParams("twitchname"));
				View::allow(true);
			}
			unset($oSettins);
		}

		
		View::rangs($oRang->getAll());
		unset($oRang);
	}

	public function render() {
		View::navi("giveaway");
		View::title("giveaway");
		
		parent::render();
	}
	
	public function action($sAction) {
		$oGiveaway = Factory::getModel("Giveaway");
		switch($sAction) {
			case "save":
				$aData = array(
					"key"			=> sha1(date("Y-m-d H:i:s").Params::postParams("twitch_username")),
					"type" 			=> Params::postParams("giveaway_type"),
// 					"minRangWinner"	=> Params::postParams("giveaway_minRang"),
					"anonym" 		=> 0,
					"intro" 		=> Params::postParams("giveaway_intro"),
					"profit" 		=> Params::postParams("giveaway_profit"),
					"duration" 		=> Params::postParams("giveaway_duration"),
					"status" 		=> "new",
					"creator" 		=> Params::cookieParams("twitchname"),
					"dateCreate" 	=> date("Y-m-d H:i:s")
				);
				if (Params::postParams("giveaway_anonym") !== false) {
					$aData["anonym"] = 1;
				}
				
				$LastId = $oGiveaway->insert($aData);
				
				if (intval($LastId) > 0) {
					View::inserted(true);
					$oGiveaway->setRangs($LastId, Params::postParams("giveaway_minRang", array()));
				} else {
					$aData["error"] = $oGiveaway->getError();
					View::inserted(false);
				}
				View::giveawayData($aData);
				break;
		}
	}
}