{{View::render("header")}}


{{if Params::postParams("action") != ""}}
	{{if View::inserted()}}
		<h2>{{Translate::say("GIVEAWAY_SUCCES")}}</h2>
		{{assign var=aGiveawayData value=View::giveawayData()}}
		{{$aGiveawayData.key}}
	{{else}}
		<h2> {{Translate::say("GIVEAWAY_ERROR")}}</h2>
		<pre>{{View::giveawayData()}}</pre>
	{{/if}}
{{else}}
	{{if View::allow()}}
		{{View::render("giveaway_form")}}
	{{else}}
		<h2>{{Translate::say("NOT_LOGGED_IN")}}</h2><br />
		<a href="{{View::twitchoAuthLink()}}" class="btn btn-info">{{Translate::say("LOGIN")}}</a>
	{{/if}}
{{/if}}


{{View::render("footer")}}