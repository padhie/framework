<?php
class ApiControllerCustom extends eController implements iController {
	protected $oApi;
	
	public function start() {
		if (!Params::getParams("action") && !Params::postParams("action")) {
			if (Params::getParams("x")) {
				$sAction = str_replace(".php", 	"", Params::getParams("x"));
				$sAction = str_replace(".html", "",	$sAction);
				$_GET["action"] = $sAction;
			}
		}
		
		$this->oApi = new ApiCore();
		
		parent::loadModulClasses("admin");
		parent::loadModulClasses("user");
		parent::loadModulClasses("songrequest");
		parent::loadModulClasses("counter");
		parent::loadModulClasses("commands");
		parent::loadModulClasses("carousel", "admin");
		parent::loadModulClasses("templates", "admin");
		parent::loadModulClasses("log", "admin");

		parent::start();
	}
	
	public function users() {
		$oUser = Factory::getModel("User", "User");
		$aUsers = array();
		foreach ($oUser->getAllFuelData() AS $aUser) {
			array_push($aUsers, array(
				"nick"	=> $aUser["user"],
				"rang"	=> $aUser["name"]
			));
		}
		$this->oApi->output($aUsers);
		unset($oUser, $aUsers);
		die();
	}
	
	public function getUserData() {
		$oUser = Factory::getModel("User", "User");
		$oUser->setUsername(Params::getParams("user", ""));
		$aUserData = $oUser->getAll();

		
		if (count($aUserData) > 0) {
			$aUserData = $aUserData[0];
			$aUserData["rangs"] = array();
			$aRangs = $oUser->getRangsOfUser($aUserData["idUser"]);
			
			foreach ($oUser->getRangsOfUser($aUserData["idUser"]) AS $aRang) {
				unset($aRang["idUser"]);
				array_push($aUserData["rangs"], $aRang);
			}
		}
		
		$this->oApi->output($aUserData);
		unset($aUserData);
		die();
	}
	
	public function getRangs() {
		$oRangs = Factory::getModel("Rangs", "Rangs");
		$this->oApi->output($oRangs->getAll());
		unset($oRangs);
		die();
	}

	public function getSongById() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aSong = $oSongrequest->getById(Params::getParams("songId"));
		$this->oApi->output($aSong);
		die;
	}
	
	public function nextSong() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aNextSong = $oSongrequest->getFirst();
		$this->oApi->output($aNextSong);
		unset($oSongrequest, $aNextSong);
		die();
	}
	
	public function nextSongAdmin() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aNextSong = $oSongrequest->getFirst();
		$oSongrequest->delete(intval($aNextSong["idSongrequest"]));		// delete current song
		if ($aNextSong["inPlaylist"] == "1") {
			unset($aNextSong["idSongrequest"]);
			$oSongrequest->insert($aNextSong);
		}
		$aNextSong = $oSongrequest->getFirst();
		$this->oApi->output($aNextSong);
		unset($oSongrequest, $aNextSong);
		die();
	}
	
	public function allSongs() {
		$oSongrequest =Factory::getModel("Songrequest", "Songrequest");
		$oSongrequest->addOrder("inPlaylist");
		$oSongrequest->addOrder("idSongrequest");
		$aSongs = $oSongrequest->getAll();
		$this->oApi->output($aSongs);
		unset($oSongrequest, $aSongs);
		die();
	}
	
	public function deleteSong() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aSongrequest = $oSongrequest->getById(intval(Params::postParams("songId")));
		if ($oSongrequest->delete(intval(Params::postParams("songId")))) {
			$aOutput["status"] = "success";
			$aOutput["message"] = "song deletet";

			if ($aSongrequest["inPlaylist"] == "1") {
				unset($aSongrequest["idSongrequest"]);
				$oSongrequest->insert($aSongrequest);
			}
		} else {
			$this->oApi->error("song not deletet");
		}
		$this->oApi->output($aOutput);
		unset($oSongrequest, $aSongrequest, $aOutput);
		die();
	}
	
	public function addSong() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		if ($oSongrequest->addByYoutubeId(Params::getParams("songId"), Params::getParams("admin"))) {
			$aSongrequest = $oSongrequest->getByYoutubeId(Params::getParams("songId"));			
			LogModelLog::addLog("admin", "songrequest", Params::getParams("admin"), "add data=>".var_export($aSongrequest, true));
			$aOutput["status"] = "success";
			$aOutput["message"] = "song added";
			$this->oApi->output($aOutput);
		} else {
			$this->oApi->error("song not added");
		}
		unset($oSongrequest, $aOutput);
		die();
	}

	public function deleteSongByAdmin() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aSongrequest = $oSongrequest->getById(intval(Params::postParams("songId")));
		if ($oSongrequest->delete(intval(Params::postParams("songId")))) {
			LogModelLog::addLog("admin", "songrequest", Params::postParams("admin"), "Delete data=>".var_export($aSongrequest, true));
			$aOutput["status"] = "success";
			$aOutput["message"] = "song deletet";

			if ($aSongrequest["inPlaylist"] == "1") {
				unset($aSongrequest["idSongrequest"]);
				$oSongrequest->insert($aSongrequest);
			}
		} else {
			$this->oApi->error("song not deletet");
		}
		$this->oApi->output($aOutput);
		unset($oSongrequest, $aSongrequest, $aOutput);
		die();
	}

	public function addToPlaylist() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aSongrequest = array(
			"inPlaylist" => 1,
		);
		$oSongrequest->update($aSongrequest, intval(Params::postParams("songId", Params::getParams("songId"))));
		LogModelLog::addLog("admin", "songrequest", Params::postParams("admin"), "Add to playlist data=>".var_export($aSongrequest, true));
		$aOutput = array(
			"status" => "success",
			"message" => "add song to playlist"
		);
		$this->oApi->output($aOutput);
		unset($oSongrequest, $aSongrequest, $aOutput);
		die();
	}

	public function removeFromPlaylist() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$aSongrequest = array(
			"inPlaylist" => 0,
		);
		$oSongrequest->update($aSongrequest, intval(Params::postParams("songId", Params::getParams("songId"))));
		LogModelLog::addLog("admin", "songrequest", Params::postParams("admin"), "remove from playlist data=>".var_export($aSongrequest, true));
		$aOutput = array(
			"status" => "success",
			"message" => "renive from playlist"
		);
		$this->oApi->output($aOutput);
		unset($oSongrequest, $aSongrequest, $aOutput);
		die();
	}

	public function displaySingleTemplate() {
		if (Params::getParams("t", false) !== false) {
			$oTemplate = Factory::getModel("template", "templates");
			$oTemplate->addWhere("name", Params::getParams("t"));
			$aTemplate = $oTemplate->getAll();
			
			if (count($aTemplate) > 0) {
				echo $aTemplate[0]["content"];
			} else {
				echo "no Template found";
			}				
		} else {
			echo "no Template given";
		}
		die();
	}
	
	public function showEventPage() {
 		View::render("events");
		die();
	}
	
	public function getNextEvent() {
		$oEvent = Factory::getModel("Event");
		$aEvent = $oEvent->getNew();
		
		if (count($aEvent) >= 1) {
			$aEvent[0]["content"] = $aEvent[0]["message"];
			
			$oTemplate = Factory::getModel("Template","Template");
			$aTemplate = $oTemplate->getByName($aEvent[0]["type"]);
			if (count($aTemplate) >= 1) {
				$aEvent[0]["content"] = $aTemplate[0]["content"];
				$aEvent[0]["content"] = str_replace("%ID%", 		$aEvent[0]["idEvent"], 	$aEvent[0]["content"]);
				$aEvent[0]["content"] = str_replace("%TYPE%", 		$aEvent[0]["type"], 	$aEvent[0]["content"]);
				$aEvent[0]["content"] = str_replace("%NEW%", 		$aEvent[0]["new"], 		$aEvent[0]["content"]);
				$aEvent[0]["content"] = str_replace("%USER%", 		$aEvent[0]["user"], 	$aEvent[0]["content"]);
				$aEvent[0]["content"] = str_replace("%MESSAGE%", 	$aEvent[0]["message"], 	$aEvent[0]["content"]);
			}
			
			$this->oApi->output($aEvent[0]);
		}
		unset($oEvent, $aEvent);
		die();
	}
	
	public function getEventTemplate() {
		$oTemplate = Factory::getModel("Template","Templates");
		$aTemplate = $oTemplate->getByName(Params::getParams("event"));
		if (count($aTemplate) >= 1) {
			$this->oApi->output($aTemplate[0]["content"]);
		}
	}
	
	public function getNextEvents() {
		$oEvents = Factory::getModel("events");
		$aEvents = $oEvents->getNew();
		
		$this->oApi->sOutputTyp = Params::getParams("output", "json");
		$this->oApi->output($aEvents);
		
		if (Params::getParams("remove") == 1) {
			foreach ($aEvents AS $aEvent) {
				$oEvents->update(
					array(
						"idEvent" => $aEvent["idEvent"],
						"new" => 0
					),
					$aEvent["idEvent"]
				);
			}
		}
		die();
	}

	public function carousel() {
		$oCarousel = Factory::getModel("Carousel", "Carousel");
		if (Params::getParams("g") !== false) {
			View::images($oCarousel->getByGroup(Params::getParams("g")));
		} else {
			View::images($oCarousel->getAll());
		}
		unset($oCarousel);
		
		View::render("carousel");
	}
	
	public function perlers() {
		$_GET["g"] = "perlers";
		$this->carousel();
		die();
	}

	public function gamewispSub() {
		$oGameWispApi	= new GamewispApi();
		$oGameWispApi->sAuthCode		= Params::getParams("authCode", "");
		$oGameWispApi->sAuthToken		= Params::getParams("authToken", "");
		$oGameWispApi->sRefreshToken	= Params::getParams("refreshToken", "");
		
		if ($oGameWispApi->sAuthToken == "" && Params::getParams("code")) {
			$oGameWispApi->sAuthCode = Params::getParams("code");
			$oGameWispApi->getToken();
		}
		
		
		View::lastSub("");
		if ( $oGameWispApi->getSubList(1) ) {
			if (isset($oGameWispApi->oChannelSubList["data"]) && isset($oGameWispApi->oChannelSubList["data"][0]) 
			&& isset($oGameWispApi->oChannelSubList["data"][0]["user"]) && isset($oGameWispApi->oChannelSubList["data"][0]["user"]["data"])
			 && isset($oGameWispApi->oChannelSubList["data"][0]["user"]["data"]["username"])) {
				View::lastSub($oGameWispApi->oChannelSubList["data"][0]["user"]["data"]["username"]);
			}
		}

		if ($oGameWispApi->sAuthCode != "") {
			View::authCode($oGameWispApi->sAuthCode);
		} else {
			View::authCode(Params::getParams("code",Params::getParams("authCode","")));
		}

		if ($oGameWispApi->sAuthToken != "") {
			View::authToken($oGameWispApi->sAuthToken);
		} elseif ($oGameWispApi->oTokenResponse["access_token"] != "") {
			View::refreshToken($oGameWispApi->oTokenResponse["access_token"]);
		} else {
			View::authToken(Params::getParams("authToken",""));
		}
		
		if ($oGameWispApi->sRefreshToken != "") {
			View::refreshToken($oGameWispApi->sRefreshToken);
		} elseif ($oGameWispApi->oTokenResponse["refresh_token"] != "") {
			View::refreshToken($oGameWispApi->oTokenResponse["refresh_token"]);
		} else {
			View::refreshToken(Params::getParams("refreshToken",""));
		}

		
		View::render("gamewisp");
	}
	
	public function getLastGamewispSub () {
		$oGameWispApi	= new GamewispApi();
		
		$oGameWispApi->sAuthCode		= Params::getParams("authCode", "");
		$oGameWispApi->sAuthToken		= Params::getParams("authToken", "");
		$oGameWispApi->sRefreshToken	= Params::getParams("refreshToken", "");
		
		if (Params::getParams("authToken") !== false) {
			$oGameWispApi->sAuthCode		= Params::getParams("authCode");
			$oGameWispApi->getToken();
		}

		// if (Params::getParams("authCode") == false || Params::getParams("authToken") == false || Params::getParams("refreshToken") === false) {
		// 	if (Params::getParams("authCode") !== false) {
		// 		$oGameWispApi->sAuthCode		= Params::getParams("authCode");
		// 		$oGameWispApi->getToken();
		// 	}
		// } else {
		// 	$oGameWispApi->sAuthCode		= Params::getParams("authCode", "");
		// 	$oGameWispApi->sAuthToken		= Params::getParams("authToken", "");
		// 	$oGameWispApi->sRefreshToken	= Params::getParams("refreshToken", "");
		// }
		

		if ( !$oGameWispApi->getSubList(1) ) {
			$this->oApi->output($oGameWispApi->oChannelSubList["error_description"]);
		} else {
			$oResponse 							= $oGameWispApi->oChannelSubList;
			$oResponse["keys"]					= array();
			$oResponse["keys"]["authCode"]		= $oGameWispApi->sAuthCode;
			$oResponse["keys"]["authToken"]		= $oGameWispApi->sAuthToken;
			$oResponse["keys"]["refreshToken"]	= $oGameWispApi->sRefreshToken;

			$this->oApi->output($oResponse);
		}
		die();
	}

	public function counter() {
		$oCounter = Factory::getModel("Counter", "Counter");
		$oCounter->getNewest();
		
		if (Params::getParams("l")) {
			$oCounter->setLimit( intval(Params::getParams("l")) );
		}
		if (Params::getParams("g")) {
			$oCounter->addGroup( Params::getParams("g") );
		}
		if (Params::getParams("o")) {
			$oCounter->setOrder("deadline", Params::getParams("o", "asc"));
		}
		
		$aCounters = array();
		foreach ($oCounter->getAll() AS $aRow) {
			$oCurrent	= new DateTime( date("Y-m-d H:i:s") );
			$oCount 	= new DateTime($aRow["deadline"]);
			$oDiff		= $oCount->diff($oCurrent);
			
			$aRow["remaining"]		= $oDiff->format('%y-%m-%d %H:%i:%s');
			$aRow["remainingDays"]	= $oDiff->format('%a %H:%i:%s');
			array_push($aCounters, $aRow);
		}
		
		View::counters($aCounters);
		unset($oCounter);
		
		View::title("counter");
		View::render("counter");
	}

	public function speedrunPB() {
		$sChannel = trim(Params::getParams("channel", "xoneris"));
		$sCategory = trim(Params::getParams("cat", "Any%"));
		$aOutput = array();
		
		if ($sChannel == "") {
			$sChannel = trim(Params::getParams("cDefault", "xoneris"));
		}
		
		if ($sChannel != "") {
			try {
				if (Params::getParams("twitch", "0") == "1") {
					// Twitchdaten holen
					$oTwitch = new TwitchApi();
					$aChannelData = $oTwitch->load_channel($sChannel);
					$sChannel = $aChannelData["display_name"];
					if (trim(Params::getParams("game", "")) === "") {
						$sGame = $aChannelData["game"];
					} else {
						$sGame = trim(Params::getParams("game"));
					}
				} else {
					$sGame = trim(Params::getParams("game", ""));
				}
				
				
				// Speedrun Userdaten
				$sSrUser = json_decode(file_get_contents("http://www.speedrun.com/api/v1/users?twitch=".urlencode($sChannel)), true);
				if (isset($sSrUser["data"]) && isset($sSrUser["data"][0]) && isset($sSrUser["data"][0]["id"])) {
					$sSrUserId = $sSrUser["data"][0]["id"];
					
					
					// Speedrun Gamedaten
					$sSrGame = json_decode(file_get_contents("http://www.speedrun.com/api/v1/games?name=".urlencode($sGame)), true);
					if (isset($sSrGame["data"]) && isset($sSrUser["data"][0]) && isset($sSrUser["data"][0])) {
						$sSrGameId = $sSrGame["data"][0]["id"];
						
						
						// Speedrun PB
						$sSrUserPb = json_decode(file_get_contents("http://www.speedrun.com/api/v1/users/".$sSrUserId."/personal-bests?game=".$sSrGameId), true);
			
						
						// Speedrun Category 
						if (count($sSrUserPb["data"]) > 0) {
							$sSrCategoryName = "";
							$sTime = "";
							
							foreach ($sSrUserPb["data"] AS $aDataRow) {
								$sSrCategory = json_decode(file_get_contents("http://www.speedrun.com/api/v1/categories/".$aDataRow["run"]["category"]), true);

								if ($sCategory != "" && $sSrCategory["data"]["name"] == $sCategory) {
									$sSrCategoryName = $sSrCategory["data"]["name"];
									if (isset($aDataRow["run"]["times"]["realtime"]))	$sTime = $aDataRow["run"]["times"]["primary"];
									
								} elseif ($sSrCategoryName == "") {
									$sSrCategoryName = $sSrCategory["data"]["name"];
									if (isset($aDataRow["run"]["times"]["realtime"]))	$sTime = $aDataRow["run"]["times"]["primary"];
								}
							}
							
							// Zeit aufbereiten
							$sTime = str_replace("PT", "", $sTime);
							$sTime = str_replace("H", "H ", $sTime);
							$sTime = str_replace("M", "M ", $sTime);
							$sTime = str_replace("S", "S ", $sTime);
			
							// Ausgabe Vorbereiten
							if ($sGame == "") {
								$sGame = "das aktuelle Spiel";
							}
							$aOutput["message"] = $sChannel."'s PB fuer ".$sGame." (".$sSrCategoryName.") liegt bei ".$sTime."!";
						} else {
							$aOutput["message"] = $sChannel." hat fuer ".$sGame." kein Personal Best!";
						}
						$aOutput["type"] = "success";
						
					} else {
						$aOutput["type"] = "error";
						$aOutput["message"] = "Spiel ".$sGame." nicht gefunden!";
					}
					
				} else {
					$aOutput["type"] = "error";
					$aOutput["message"] = "Kein Speedrun-Channel von ".$sChannel." gefunden!";
				}
				
			} catch(Exception $e) {
				$aOutput["type"] = "error";
				$aOutput["message"] = $e;
			}
			
		} else {
			$aOutput["type"] = "error";
			$aOutput["message"] = "no Channel given!";
		}
		
		$this->oApi->output($aOutput);
		die();
	}

	public function speedrunWR() {
		$sCategory = trim(Params::getParams("cat", "Any%"));
		$aOutput = array(
			"type" => "error",
			"message" => "no data"
		);


		try {
			if (Params::getParams("game", "") != "") {
				$sGame = trim(Params::getParams("game", ""));

				// Speedrun Gamedaten
				$sSrGame = json_decode(file_get_contents("http://www.speedrun.com/api/v1/games?name=".urlencode($sGame)), true);
				if (isset($sSrGame["data"]) && isset($sSrGame["data"][0]) && isset($sSrGame["data"][0]["id"])) {
					$sSrGameId = $sSrGame["data"][0]["id"];
	
	
					// Speedrun WR
					$sSrGameWr = json_decode(file_get_contents("http://www.speedrun.com/api/v1/games/".$sSrGameId."/records?top=1&miscellaneous=no&scope=full-game"), true);

					
					// Speedrun Category
					if (count($sSrGameWr["data"]) > 0) {
						$sSrCategoryName = "unknown";
						$sTime = "unknown";
						$sChannel = "unknown";
						$sChanneId = "";
							
						foreach ($sSrGameWr["data"] AS $aDataRow) {
							$sSrCategory = json_decode(file_get_contents("http://www.speedrun.com/api/v1/categories/".$aDataRow["runs"][0]["run"]["category"]), true);

							if ($sCategory != "" && $sSrCategory["data"]["name"] == $sCategory) {
								$sSrCategoryName = $sSrCategory["data"]["name"];
								if (isset($aDataRow["runs"][0]["run"]["times"]["realtime"]))	$sTime = $aDataRow["runs"][0]["run"]["times"]["primary"];
								if (isset($aDataRow["runs"][0]["run"]["players"][0]["id"]))		$sChanneId = $aDataRow["runs"][0]["run"]["players"][0]["id"];
									
							} elseif ($sSrCategoryName == "") {
								$sSrCategoryName = $sSrCategory["data"]["name"];
								if (isset($aDataRow["runs"][0]["run"]["times"]["realtime"]))	$sTime = $aDataRow["runs"][0]["run"]["times"]["primary"];
								if (isset($aDataRow["runs"][0]["run"]["players"][0]["id"]))		$sChanneId = $aDataRow["runs"][0]["run"]["players"][0]["id"];
							}
						}
						
						if ($sChanneId != "") {
							// Lade Channeldaten
							$sSrUser = json_decode(file_get_contents("http://www.speedrun.com/api/v1/users/".$sChanneId), true);
							if (isset($sSrUser["data"]) && isset($sSrUser["data"]["names"]) && isset($sSrUser["data"]["names"]["international"])) {
								$sChannel = $sSrUser["data"]["names"]["international"];
							}
							
							
							// Zeit aufbereiten
							$sTime = str_replace("PT", "", $sTime);
							$sTime = str_replace("H", "H ", $sTime);
							$sTime = str_replace("M", "M ", $sTime);
							$sTime = str_replace("S", "S ", $sTime);
								
							// Ausgabe Vorbereiten
							if ($sGame == "") {
								$sGame = "das aktuelle Spiel";
							}
							$aOutput["type"] = "success";
							$aOutput["message"] = "WR fuer ".$sGame."(".$sSrCategoryName.") hat ".$sChannel." mit ".$sTime."!";
							
						} else {
							$aOutput["type"] = "error";
							$aOutput["message"] = "Kategorie '".$sCategory."'nicht gefunden!";
						}
						
					} else {
						$aOutput["type"] = "error";
						$aOutput["message"] = "Keine WR Daten fuer ".$sGame."(".$sSrCategoryName.") gefunden!";
					}
	
				} else {
					$aOutput["type"] = "error";
					$aOutput["message"] = "Spiel ".$sGame." nicht gefunden!";
				}
			}

		} catch(Exception $e) {
			$aOutput["type"] = "error";
			$aOutput["message"] = $e;
		}
	
		$this->oApi->output($aOutput);
		die();
	}

	public function currentSong() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		$oSongrequest->addOrder("idSongrequest");
		$oSongrequest->setLimit("1");
		$aSongs = $oSongrequest->getAll();

		$aSong = array();
		if (count($aSongs) > 0) {
			$aSong = $aSongs[0];
		}
		
		$this->oApi->sOutputTyp = Params::getParams("output", "template");
		
		$this->oApi->sTemplate = "currentSong";
		$this->oApi->output($aSong);
		unset($oSongrequest, $aSongs, $aSong);
		die();
	}

	public function getValueOfCounterCommand() {
		$this->oApi->sOutputTyp = "raw";

		if (Params::getParams("command") != false) {
			$oCommand = Factory::getModel("commands", "commands");
			$oCommand->addWhere("type", "counter");
			$oCommand->addWhere("command", Params::getParams("command"));
			$aCommand = $oCommand->getSingleData();
			if ($oCommand != false) {
				$aExtra = explode("|", $aCommand["extra"]);
				$this->oApi->output(
					$aExtra[1]
				);
			} else {
				$this->oApi->output("No Command found");
			}
		} else {
			$this->oApi->output("No Command given");
		}
		die();
	}
}