<?php

class ApiModelEvents extends DbExtent {
	protected $_sTable = "events";
	protected $_sIdField = "idEvent";
	
	public function getNew($sType="all") {
		$this->_oQueryBuilder->addWhere("new", 1);
		if ($sType != "all") {
			$this->_oQueryBuilder->addWhere("type", $sType);
		}
		
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			return $this->getResult();
		}
		return false;
	}
}