<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Events</title>
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	
	<script type="text/javascript">
		var aFollowers 		= [];
		var oQueue = {
			"hosting"		: [],
			"follow"		: [],
			"subscription"	: [],
			"donation"		: []
		};
		
		$(document).ready(function(){
			checkHosting();

			setInterval(function(){
				checkEvents();
			}, {{intval(Params::getParams("ce", 20000))}});
			
			setInterval(function(){
				queueCheck();
			}, {{intval(Params::getParams("qc", 20000))}});
		});

		function checkHosting(blInit) {
			$.ajax({
				url: "https://api.twitch.tv/kraken/channels/padhie/follows?limit=10&direction=asc"
			}).done(function(response) {
				var follows = response.follows;
				
				for (var i=0; i<follows.length; i++ ) {
					var username = follows[i].user.name;
					
					if (blInit) {
						aFollowers[aFollowers.length] = username;
					} else {
						var blFound = false;
						if (aFollowers.indexOf(username) >= 0) {
							blFound = true;
						}
	
						if (blFound == false) {
							var data = {};
							data["USERNAME"] = username;
							//oQueue["hosting"][oQueue["hosting"].length] = data;
						}
					}
				}
			});

			setTimeout(function(){
				checkHosting(false);
			}, {{intval(Params::getParams("ch", 30000))}});
		}

		function checkEvents() {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?action=getNextEvents&remove=1"
			}).done(function(response) {
				data = JSON.parse(response);

				for (var i=0; i<data.length; i++) {
					if (oQueue[data[i].type] == null || oQueue[data[i].type] == undefined) {
						oQueue[data[i].type] = [];
					}
					oQueue[data[i].type].push(data[i]);
				}			
			});
		}
		
		function queueCheck() {
			blCheck = true;
			for (var key in oQueue) {
				if (blCheck == true) {
					if (oQueue[key].length >= 1) {
						showEvent(key, oQueue[key][0]);
						oQueue[key].shift();
						blCheck = false;
					}
				}
			}
		}
		
		function showEvent(sType, oData) {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?action=getEventTemplate&event="+sType
			}).done(function(response){
				// Standardzeichen entfernen
				response = response.substring(1, response.length-1);				// entferne Anführungszeichen
				response = response.replace(/\\r/g, "").replace(/\\n/g, "");		// entferne Umbruch
				response = response.replace(/\\/g, "");								// entferne backslashes
				
				
				// platzhalter entfernen
				for (var key in oData) {
					response = response.replace("%"+key.toUpperCase()+"%", oData[key]);
				}
						

				// show output
				$("#output").html(response)
							.fadeIn()
							.delay({{intval(Params::getParams("d", 5000))}})
							.fadeOut();
								
			});
		}
	</script>
</head>
<body>
	<div id="output" style="display:none;"></div>
</body>
</html>
