<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Carousel</title>
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	{{View::loadCss("perlers")}}
	
	<style type="text/css">
		.item {
			background: #{{Params::getParams("bg", "4b1e30")}};
		}
		.item img {
			max-height: 800px;
			margin-left: auto;
			margin-right: auto;
		}
		
		.carousel-fade .carousel-inner .item {
		  transition-property: opacity;
		}
		
		.carousel-fade .carousel-inner .item,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
		  opacity: 0;
		}
		
		.carousel-fade .carousel-inner .active,
		.carousel-fade .carousel-inner .next.left,
		.carousel-fade .carousel-inner .prev.right {
		  opacity: 1;
		}
		
		.carousel-fade .carousel-inner .next,
		.carousel-fade .carousel-inner .prev,
		.carousel-fade .carousel-inner .active.left,
		.carousel-fade .carousel-inner .active.right {
		  left: 0;
		  transform: translate3d(0, 0, 0);
		}
		
		.carousel-fade .carousel-control {
		  z-index: 2;
		}
		
		html,
		body,
		.carousel,
		.carousel-inner,
		.carousel-inner .item {
		  height: 100%;
		}
	</style>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('.carousel').carousel({
				  interval: {{Params::getParams("t", "10000")}},
				  pause: ""
			});
		});
	</script>
</head>
<body>

	<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
		<!-- Carousel nav-points -->
	    <ol class="carousel-indicators">
	    	{{foreach from=View::images() key=$i item=aRow}}
	    		<li data-target="#carousel" data-slide-to="{{$i}}" class="{{if $i==0}}active{{/if}}"></li>
	    	{{/foreach}}
	    </ol>
	    
	    <!-- Carousel items -->
	    <div class="carousel-inner">
   	    	{{foreach (View::images() AS $i => $aRow) : ?>
		        <div class="{{if $i==0}}active{{/if}} item">
		        	<img src="{{$aRow.image}}" alt="{{$aRow.name}}">
		        </div>
	    	{{/foreach}}
	    </div>

	    <!-- Carousel arrows -->
	    <a class="carousel-control left" href="#carousel" data-slide="prev">&nbsp;</a>
	    <a class="carousel-control right" href="#carousel" data-slide="next">&nbsp;</a>
	</div>

</body>
</html>
