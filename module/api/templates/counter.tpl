<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
       "http://www.w3.org/TR/html4/loose.dtd">
<head lang="de">
<head>
	<meta charset="utf-8"/>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{View::projekt()}} - {{View::title()}}</title>
	{{assign var=sServerHost value=Config::getServerHost()}}
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	{{View::loadFontAwesome()}}
	{{View::loadCss("`$sServerHost`/css/base.css")}}
	<style type="text/css">
		body {
			background-color: transparent !important;
		}
	</style>
</head>
<body>

	{{assign var=$aCounters value=View::counters()}}
	{{if count($aCounters) > 0}}
		{{if Params::getParams("v") == "div"}}
			{{foreach from=$aCounters item=$aCounter}}
				<div class="row">
					<div class="col-md-2 text-right" style="padding-right: 10px;">{{$aCounter.title}}</div>
					<div class="col-md-2 text-left time">{{$aCounter.remainingDays}}</div>
				</div>
			{{/foreach}}
		{{else}}
						<table>
				{{foreach from=$aCounters item=aCounter}}
					<tr>
						<td class="text-right" style="padding-right: 10px;">{{$aCounter.title}}</td>
						<td class="text-left time">{{$aCounter.remainingDays}}</td>
					</tr>
				{{/foreach}}
			</table>
		
		{{/if}}	
	{{/if}}

</body>

	<script type="text/javascript">
		setInterval(function() {
			var aDateTimes = document.getElementsByClassName("time");
			for (var i=0; i<aDateTimes.length; i++) {
				var sDateTime = aDateTimes[i].innerHTML;

				if (sDateTime == "00 Tage 00:00:00") {
					continue;
				}
				
				
				sDateTime = sDateTime.replace(" Tage", "");
				sDateTime = sDateTime.replace(" Tag", "");
				var aDateTime = sDateTime.split(" ");

				if (aDateTime[0].indexOf("-") < 0) {
					aDateTime[0] =  "0000-00-"+aDateTime[0];
				}
				var aDate = aDateTime[0].split("-");
				var aTime = aDateTime[1].split(":");


				// Sekunden
				aTime[2] = (parseInt(aTime[2]))-1;
				if (aTime[2] < 0) {
					aTime[2] = 59;
				
					// Minuten
					aTime[1] = (parseInt(aTime[1]))-1;
					if (aTime[1] < 0) {
						aTime[1] = 59;
				
						// Stunden
						aTime[0] = (parseInt(aTime[0]))-1;
						if (aTime[0] < 0) {
							aDate[0] = 24;
				
							// Tage
							aDate[2] = (parseInt(aDate[2]))-1;
							if (aDate[2] < 0) {
								aDate[2] = 30;

								// Monat
								aDate[1] = (parseInt(aDate[1]))-1;
								if (aDate[1] < 0) {
									aDate[1] = 10;
				
									// Jahr
									aDate[0] = (parseInt(aDate[0]))-1;
									if (aDate[0] < 0) {
										aDate[0] = 0;
									}
								}
							}
						}
					}	
				}

				if (aDate[2] < 10) {	aDate[2] = "0"+parseInt(aDate[2]);	}
				if (aTime[0] < 10) {	aTime[0] = "0"+parseInt(aTime[0]);	}
				if (aTime[1] < 10) {	aTime[1] = "0"+parseInt(aTime[1]);	}
				if (aTime[2] < 10) {	aTime[2] = "0"+parseInt(aTime[2]);	}

				if (parseInt(aDate[2]) == 1) 	{	aDate[2] += " Tag";		}
				else							{	aDate[2] += " Tage";	}

				aDateTimes[i].innerHTML = 
					aDate[2] + " " + aTime[0] + ":" + aTime[1] + ":" + aTime[2]
				;
			}
		}, 1000);
	</script>
</html>