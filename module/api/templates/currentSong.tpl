<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{Translate::say("CURREND_SONG")}}</title>
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}

	<script>
		refreshData();
		var playlistrefresher = setInterval(function() {
			refreshData();
		}, {{intval(Params::getParams("t", 10000))}});
	
		function refreshData() {
			$.ajax({
			  url: "{{Config::getServerHost()}}/api/custom/currentSong?output=json",
			}).done(function(data) {
				var response = JSON.parse(data);
				if (response != null && response != undefined) {
					if (response.title != null && response.title != undefined) {
						var title = response.title;
						$("#songTitle .data").html(title);
					}

					if (response.postedBy != null && response.postedBy != undefined) {
						$("#postedBy .data").html(response.postedBy);
					}

					if (response.duration != null && response.duration != undefined) {
						var duration = convertTime("H:i:s", response.duration);					
						$("#duration .data").html(duration);
					}
				}
			});
		}

		function convertTime(sPattern, sTime) {
			var aTime = sTime.split(":");
			var sNewTime = sPattern;


			for (var i=0; i<aTime.length; i++) {
				if (parseInt(aTime[i]) < 10) {
					aTime[i] = "0"+aTime[i];
				}
			}
			
			
			// Sekunden
			if (aTime.length == 1) {
				sNewTime = sNewTime.replace("s", aTime[0]);

				
			// Minuten : Sekundne
			} else if (aTime.length == 2) {
				sNewTime = sNewTime.replace("i", aTime[0]);
				sNewTime = sNewTime.replace("s", aTime[1]);

				
			// Stunden : Minuten : Sekunden
			} else if (aTime.length == 3) {
				sNewTime = sNewTime.replace("H", aTime[0]);
				sNewTime = sNewTime.replace("i", aTime[1]);
				sNewTime = sNewTime.replace("s", aTime[2]);

			}

			sNewTime = sNewTime.replace("H", "");
			sNewTime = sNewTime.replace("i", "");
			sNewTime = sNewTime.replace("s", "");
		
			return sNewTime;
		}
	</script>
	
	<style type="text/css">
		body {
			background-color: {{Params::getParams("bg", "transparent")}};
			color: {{Params::getParams("c", "black")}}
		}
		#songData {
			margin: 10px;
		}

		.marquee div {
			float: left;
		}
	</style>
</head>
<body>

	{{$aData = View::outputData()}}
	
	{{if Params::getParams("tpl") == 2}}
		<marquee class='marquee'>
			<div id="songTitle"><span class="data">{{$aData["title"]}}</span></div>
			<div id="postedBy">&nbsp;[by <span class="data">{{$aData["postedBy"]}}</span>]</div>
		</marquee>
	{{else}}
		<div id="songData">
			<!--div id="idSongrequest">
				{{Translate::say("ID")}}
				<span class="data">{{$aData["idSongrequest"]}}</span>
			</div-->
			<div id="songTitle">
				{{Translate::say("SONGTITLE")}}:
				<span class="data">{{$aData["title"]}}</span>
			</div>
			<div id="postedBy">
				{{Translate::say("POSTED_BY")}}:
				<span class="data">{{$aData["postedBy"]}}</span>
			</div>
			<div id="duration">
				{{Translate::say("DURATION")}}:
				<span class="data">{{$aData["duration"]}}</span>
			</div>
		</div>
	{{/if}}

</body>
</html>