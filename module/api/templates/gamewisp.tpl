<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Gamewisp - Subs</title>
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	<style type="text/css">
		body {
			margin: 10px;
			background-color: transparent;
		}
	</style>
	<script type="text/javascript">
		var sParams = "";
		 
		$(document).ready(function(){
			if ("{{View::authCode()}}" != "") {
				sParams += "&authCode={{View::authCode()}}";
			}
			if ("{{View::authToken()}}" != "") {
				sParams += "&authToken={{View::authToken()}}";
			}
			if ("{{View::refreshToken()}}" != "") {
				sParams += "&refreshToken={{View::refreshToken()}}";
			}

			
			$("#link").html("{{Config::getServerHost()}}/custom/api/?action=gamewispSub&state=ASKDLFJsisisks23k"+sParams);

			
			if ("{{View::lastSub()}}" != "") {
				$("#output").html("{{View::lastSub()}}");
			}

			
			setInterval( ajax, 20000 );
		});

		function ajax() {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?action=getLastGamewispSub"+sParams,
			}).done(function( response ) {
				var blElse = true;
				
				if (response.indexOf("{") == 0 || response.indexOf("[") == 0) {
					response = JSON.parse(response);
					
					if (response.data != null && response.data != undefined) {
						if (response.data[0] != null && response.data[0] != undefined) {
							if (response.data[0].user != null && response.data[0].user != undefined) {
								if (response.data[0].user.data != null && response.data[0].user.data != undefined) {
									if (response.data[0].user.data.username != null && response.data[0].user.data.username != undefined) {
										sParams = ""+
										"&authCode="+		response.keys.authCode+
										"&authToken="+		response.keys.authToken+
										"&refreshToken="+	response.keys.refreshToken;
									
										$("#output").html(response.data[0].user.data.username);
										blElse=false;
									}
								}
							}
						}
					}
				}

				if (blElse) {
					if (console != null && console != undefined) {
						console.log( response );
					}
				}
			});
		}
	</script>
</head>
<body>
	<div id="link"></div>
	<hr />
	<div id="output"></div>
</body>
</html>
