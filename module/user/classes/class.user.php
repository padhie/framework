<?php

class UserModelUser extends DbExtent {
	protected $_sTable = "user";
	protected $_sIdField = "idUser";

	public function setUsername($sUsername) {
		$this->_oQueryBuilder->addWhere("user", $sUsername);
	}

	public function searchUsername($sUsername) {
		$this->_oQueryBuilder->addWhere("user", "%".$sUsername."%", "", "LIKE");
	}
	
	public function getAllFuelData(){
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "user_has_rang", $this->_sIdField);
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "rang", "idRang");
		return parent::getAll();
	}

	/**
	 * 
	 * @param int $idUser ID des Users welches gepürft werden soll
	 * @param array $aRangList Liste mit Rangindexen(!!!) die mindestesn erreicht sein müssen
	 * @return boolean TRUE wenn den Mindestrang auf in min einer Gruppe vorhanden ist, FALSE wenn nicht
	 */
	public function userHasRang($idUser, $aRangList=array()) {
		$this->_oQueryBuilder->addWhere($this->_sIdField, $idUser);
		$aUserData = $this->getAllFuelData();
		$oRang = Factory::getModel("Rangs", "Rangs");
		$aRangData = $oRang->getAll();
		
		foreach ($aUserData AS $aUserRow) {
			foreach ($aRangData AS $aRangRow) {
				if ($aUserRow["group"] == $aRangRow["group"]) {
					if (intval($aUserRow["index"]) <= intval($aRangRow["index"])) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public function getRangsOfUser($idUser) {
		$this->_oQueryBuilder->setTable("user_has_rang");
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "rang", "idRang");
		$this->_oQueryBuilder->addWhere("idUser", intval($idUser));

		$aData = parent::getAll();
		$this->init();
		return $aData;
	}
	
	public function saveRangsToUser($idUser, $aRangs=array()) {
		$blReturn = false;
		$this->_oQueryBuilder->setTable("user_has_rang");
		$this->_oQueryBuilder->addWhere("idUser", $idUser);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );
		
		if ($this->execute()) {
			$blReturn = true;
			foreach ($aRangs AS $sRang) {
				if (!$this->insert(array("idUser"=>$idUser, "idRang"=>intval($sRang)))) {
					$blReturn = false;
				}
			}
		}
		$this->init();
		
		return $blReturn;
	}

	public function delete($iId) {
		$this->_oQueryBuilder->addWhere($this->_sIdField, $iId);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );
	
		if ($this->execute()) {
			$this->_oQueryBuilder->setTable("user_has_rang");
			$this->_oQueryBuilder->addWhere($this->_sIdField, $iId);
			$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );
				
			if ($this->execute()) {
				return true;
			}
		}
		return false;
	}
}