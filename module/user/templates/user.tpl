{{View::render("header")}}

<div class="row pull-right">
	<form class="form-inline col-md-12" method="get" action="{{LinkHelper::modul("user")}}">
		<div class="form-group">
			<input type="text" class="form-control" name="user" placeholder="{{Translate::say("NICKNAME")}}" value="{{Params::getParams("user", "")}}" />
		</div>
		<button type="submit" class="btn btn-default">{{Translate::say("SEARCH")}}</button>
	</form>
</div>

{{View::tableObject()->render()}}

{{View::render("footer")}}