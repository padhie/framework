<?php
class UserControllerUser extends eController implements iController {
	public function run() {
		$aRangDisplay = View::rangDisplay();
		$aRangIcon = View::rangIcon();

		$oTable = new TableHelper();
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
			"user" 				=> Translate::get("NICKNAME")
									.' <a href="'.Config::getServerHost().'/user?field=user&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
									.' <a href="'.Config::getServerHost().'/user?field=user&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
			"timeTotal" 		=> Translate::get("TIME_TOTAL_MINS")
									.' <a href="'.Config::getServerHost().'/user?field=timeTotal&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
									.' <a href="'.Config::getServerHost().'/user?field=timeTotal&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
			$aRangDisplay[0] 	=> Translate::get("RANG")." 1",
			"postTotal" 		=> Translate::get("POST_TOTAL")
									.' <a href="'.Config::getServerHost().'/user?field=postTotal&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
									.' <a href="'.Config::getServerHost().'/user?field=postTotal&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
			$aRangDisplay[1] 	=> Translate::get("RANG")." 2"
		));

		$oUser = Factory::getModel("User");		
		if (Params::getParams("user") !== false) {
			$oUser->searchUsername(Params::getParams("user"));
		}
		
		$sSort = "asc";
		if (Params::getParams("sort") == "desc") {
			$sSort = "desc";
		}
		switch (Params::getParams("field")) {
			case 'user':			$oUser->setOrder("user", $sSort);				break;
			case 'timeTotal':		$oUser->setOrder("timeTotal", $sSort);			break;
			case 'postTotal':		$oUser->setOrder("postTotal", $sSort);			break;
			default:				$oUser->setOrder("user", $sSort);				break;
		}
		$aUserData = $oUser->getAll();
		
		for ($i=0; $i<count($aUserData); $i++) {		
			$aRangData = $oUser->getRangsOfUser($aUserData[$i]["idUser"]);
			if ($aRangData !== false) {
				foreach ($aRangData AS $aRangRow) {
					if (is_array($aRangDisplay) && in_array($aRangRow["group"], $aRangDisplay)) {
						$aUserData[$i][$aRangRow["group"]] = $aRangRow["name"];
					}
					
					if (is_array($aRangIcon) && isset($aRangIcon[$aRangRow["group"]])) {
						if (isset($aRangIcon[$aRangRow["group"]][$aRangRow["name"]])) {
							$aUserData[$i]["user"] .= " ".$aRangIcon[$aRangRow["group"]][$aRangRow["name"]];
						}
					}
				}
			}
		}

		$oTable->setData($aUserData);
		
		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("user");
		View::title("user");
		
		parent::render();
	}
}