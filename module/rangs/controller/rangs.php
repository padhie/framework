<?php
class RangsControllerRangs extends eController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
			"group" 			=> Translate::get("GROUP"),
			"index" 			=> Translate::get("INDEX"),
			"name" 				=> Translate::get("RANG"),
			// "maxSongrequest"	=> Translate::get("MAX_SONGREQUEST"),
		));
		
		$oRang = Factory::getModel("Rangs");
		$oRang->addOrder("group");
		$oRang->addOrder("index");
		$oTable->setData($oRang->getAll());
		
		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("rangs");
		View::title("rangs");
		
		parent::render();
	}
}