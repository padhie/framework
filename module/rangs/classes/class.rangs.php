<?php

class RangsModelRangs extends DbExtent {
	protected $_sTable = "rang";
	protected $_sIdField = "idRang";
	
	// @TODO auf neues Rangsystem umbauen
	public function getMinRang() {
		$this->_oQueryBuilder->addOrder("index", "ASC");
		$this->_oQueryBuilder->addGroup("group");
		$aRang = $this->createFormArray($this->getAll());

		return $aRang;
	}

	// @TODO auf neues Rangsystem umbauen
	public function getMaxRang() {
		$this->_oQueryBuilder->addOrder("index", "DESC");
		$this->_oQueryBuilder->addGroup("group");
		$aRang = $this->createFormArray($this->getAll());
		
		return $aRang;
	}

	public function getFormArray() {
		$this->_oQueryBuilder->addOrder("group", "DESC");
		$this->_oQueryBuilder->addOrder("index", "ASC");
		$aRang = $this->createFormArray($this->getAll());
		return $aRang;
	}
	
	public function createFormArray($aData) {
		$_aRangs = array();
		foreach ($aData AS $aRow) {
			$_aRangs[$aRow["idRang"]] = $aRow["group"]."|".$aRow["name"];
		}
		return $_aRangs;
	}
	
	public function getByGroupAndRang($sGroup, $sRang) {
		$this->_oQueryBuilder->addWhere("group", $sGroup);
		$this->_oQueryBuilder->addWhere("name", $sRang);
		return $this->getSingleData();
	}
		
	public function convertToArray($sJsonString, $sDelimiter="_") {
		$aReturn = array();
		if (isset($sJsonString)) {
			foreach (json_decode($sJsonString,true) AS $sKey => $sVal) {
				array_push($aReturn, $sKey.$sDelimiter.$sVal);
			}
		}
		
		return $aReturn;
	}
	
	public function convertToString($aData, $sDelimiter="_") {
		$aReturn = array();
		foreach ($aData AS $sRow) {
			$aSplit = explode($sDelimiter, $sRow);
			$aReturn[$aSplit[0]]	= $aSplit[1];
		}
		
		return json_encode($aReturn);
	}
	
	public function createRangTextByData($aRangData=array(), $sSeperator=":", $sDelimiter=" | ") {
		$sRang = "";
		foreach ($aRangData AS $aRangRow) {
			$sRang .= $aRangRow["group"].$sSeperator.$aRangRow["name"].$sDelimiter;
		}
		if (strlen($sRang) > 2) {
			$sRang = substr($sRang, 0, -3);
		}
		return $sRang;
	}
}