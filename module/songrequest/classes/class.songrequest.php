<?php

class SongrequestModelSongrequest extends DbExtent {
	protected $_sTable = "songrequest";
	protected $_sIdField = "idSongrequest";
	
	public function getFirst() {
		$this->_oQueryBuilder->addOrder("inPlaylist");
		$this->_oQueryBuilder->setOrder($this->_sIdField);
		$this->_oQueryBuilder->setLimit(1);
		
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			$aResult = $this->getResult();
			if (count($aResult) >= 1) {
				return $aResult[0];
			}
		}
		return false;
	}

	public function deleteByLink($sLink) {
		$this->_oQueryBuilder->addWhere("link", $sLink);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );
		
		if ($this->execute())
		{
			return true;
		}
		return false;
	}

	public function getByYoutubeId($sYoutubeId) {
		$this->_oQueryBuilder->addWhere("link", $sYoutubeId);
		return $this->getSingleData();
	}

	public function addByYoutubeId($sYoutubeId, $sPostedBy) {
		$oYoutube = new youtube();
		if ($oYoutube->videoExist($sYoutubeId)) {
			$aYoutubeData = $oYoutube->getCompactData();
			
			$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
			$aFirstSong = $oSongrequest->getFirst();
			$iFirstId = intval($aFirstSong["idSongrequest"]);
			$iFirstId--;
			
			
			$aData = array(
				"idSongrequest" => $iFirstId,
				"source" => "youtube",
				"link" => $sYoutubeId,
				"title" => $aYoutubeData["title"],
				"duration" => $aYoutubeData["duration"],
				"postedBy" => $sPostedBy,
				"inPlaylist" => 0,
			);
 			$oSongrequest->insert($aData);
 			return true;
		}
		return false;	
	}
}