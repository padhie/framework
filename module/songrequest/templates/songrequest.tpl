{{View::render("header")}}

<div id="songrequestList" class="col-md-7">

	{{View::tableObject()->render()}}
</div>

<div class="col-md-5">
		<script src="http://www.youtube.com/player_api"></script>
		<div class="row">
			<div id="player" class="col-md-12"></div>
		</div>
		
		<br />
		
		<div id="currendSongData" class="panel panel-default">
			<div class="panel-heading">{{Translate::say("CURREND_SONG")}}</div>
			<div class="panel-body">
			
			{{if View::currentSong() != ""}}
				{{assign var="aSong" value=View::currentSong()}}
			{{else}}
				{{php}}
				$aSong = array(
					"idSongrequest" => "",
					"link" => "",
					"title" => "",
					"postedBy" => "",
					"duration" => ""					
				);
				{{/php}}
			{{/if}}
			{{assign var="aSong" value=View::currentSong()}}
			
			{{assign var="sLink" value=""}}
			{{if strpos($aSong.link,"http://") >= 0 || strpos($aSong.link,"https://") >= 0}}
				{{assign var="sLink" value=$aSong.link}}
			{{else if $aSong["source"] == "youtube"}}
				{{assign var="sLink" value="https://www.youtube.com/watch?v={VIEDOID}"}}
			{{/if}}

			{{Translate::say("ID")}} {{$aSong["idSongrequest"]}}<br />
			{{Translate::say("SONGTITLE")}}: <a href="{{$sLink|replace:'{VIEDOID}':$aSong.link}}" target="_blank">{{$aSong.title}}</a><br />
			{{Translate::say("POSTED_BY")}}: {{$aSong.postedBy}} <br />
			{{Translate::say("DURATION")}}: {{$aSong.duration}}

		</div>
	</div>
	
	<div id="lastSongData" class="panel panel-default">
		<div class="panel-heading">{{Translate::say("LAST_SONG")}}</div>
		<div class="panel-body"></div>
	</div>
</div>

<script>
	refreshPlaylist();
	
	function refreshPlaylist() {
		$.ajax({
		  url: "{{Config::getServerHost()}}/custom/api/songrequest.php?action=allSongs",
		}).done(function(data) {
			var response = JSON.parse(data);
			if (response != null && response != undefined) {
				$("#songrequestList").find("tbody").empty();

				
				var newList = "";
				for (var i=0; i<response.length; i++) {

					var link = "";
					if (response[i].link.indexOf("http://") >= 0 || response[i].link.indexOf("https://") >= 0) {
						link += response[i].link;
					} else if (response[i].source == "youtube") {
						link += "https://www.youtube.com/watch?v={VIEDOID}";
					}

					newList += "<tr>";
						newList += "<td>"+response[i].idSongrequest+"</td>";
						newList += "<td><a href='"+link.replace("{VIEDOID}", response[i].link)+"' target='_blank'>"+response[i].title+"</a>"+"</td>";
						newList += "<td>"+response[i].postedBy+"</td>";
					newList += "</tr>";
				}
				$("#songrequestList").find("tbody").html(newList);
				newList = null;
			}
		});
	}

	var playlistrefresher = setInterval(function() {
		refreshPlaylist();
	}, 10000);

	function nextSong() {
		$.ajax({
		  url: "{{Config::getServerHost()}}/custom/api?action=nextSong",
		}).done(function(data) {
			var response = JSON.parse(data);
			if (response != null && response != undefined) {
				var link = "";
				if (response.link.indexOf("http://") >= 0 || response.link.indexOf("https://") >= 0) {
					$sLink += response.link;
				} else if (response.source == "youtube") {
					link += "https://www.youtube.com/watch?v={VIEDOID}";
				}
				link = link.replace("{VIEDOID}", response.link);
				
				var songData = "";
				songData += "{{Translate::say("ID")}}"+response.idSongrequest+"<br />";
				songData += "{{Translate::say("SONGTITLE")}}: <a href='"+link+"' target='_blank'>"+response.title+"</a><br />";
				songData += "{{Translate::say("POSTED_BY")}}: "+response.postedBy+"<br />";
				songData += "{{Translate::say("DURATION")}}: "+convertTime("H:i:s", response.duration); 
				$("#currendSongData").find(".panel-body").html( songData );
				
				player.loadVideoById({'videoId': response.link});

				refreshPlaylist();
			}
		});
	}

	function lastSong () {
		$.ajax({
		  url: "{{Config::getServerHost()}}/api/custom/currentSong?output=json",
		}).done(function(data) {
			var response = JSON.parse(data);
			if (response != null && response != undefined) {
				var link = "";
				if (response.link.indexOf("http://") >= 0 || response.link.indexOf("https://") >= 0) {
					$sLink += response.link;
				} else if (response.source == "youtube") {
					link += "https://www.youtube.com/watch?v={VIEDOID}";
				}
				link = link.replace("{VIEDOID}", response.link);
				
				var songData = "";
				songData += "{{Translate::say("ID")}}"+response.idSongrequest+"<br />";
				songData += "{{Translate::say("SONGTITLE")}}: <a href='"+link+"' target='_blank'>"+response.title+"</a><br />";
				songData += "{{Translate::say("POSTED_BY")}}: "+response.postedBy+"<br />";
				songData += "{{Translate::say("DURATION")}}: "+convertTime("H:i:s", response.duration); 
				$("#lastSongData").find(".panel-body").html( songData );
				
				player.loadVideoById({'videoId': response.link});

				refreshPlaylist();
			}
		});
	}

	function convertTime(sPattern, sTime) {
		var aTime = sTime.split(":");
		var sNewTime = sPattern;


		for (var i=0; i<aTime.length; i++) {
			if (parseInt(aTime[i]) < 10) {
				aTime[i] = "0"+aTime[i];
			}
		}
		
		
		// Sekunden
		if (aTime.length == 1) {
			sNewTime = sNewTime.replace("s", aTime[0]);

			
		// Minuten : Sekundne
		} else if (aTime.length == 2) {
			sNewTime = sNewTime.replace("i", aTime[0]);
			sNewTime = sNewTime.replace("s", aTime[1]);

			
		// Stunden : Minuten : Sekunden
		} else if (aTime.length == 3) {
			sNewTime = sNewTime.replace("H", aTime[0]);
			sNewTime = sNewTime.replace("i", aTime[1]);
			sNewTime = sNewTime.replace("s", aTime[2]);

		}

		sNewTime = sNewTime.replace("H", "");
		sNewTime = sNewTime.replace("i", "");
		sNewTime = sNewTime.replace("s", "");
	
		return sNewTime;
	}

	// create youtube player
	var player;
	function onYouTubePlayerAPIReady() {
		player = new YT.Player('player', {
			height: '390',
			width: '640',
			videoId: '{{$aSong["link"]}}',
			events: {
				'onReady': function(event) {
					event.target.playVideo();
				},
				'onStateChange': function(event) {
					if (event.data === 0) {
						lastSong();
						nextSong();
					}
				}
			}
		});
	}
</script>

{{View::render("footer")}}