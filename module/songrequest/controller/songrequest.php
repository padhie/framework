<?php
class SongrequestControllerSongrequest extends eController implements iController {
	public function run() {
		$oSongrequest = Factory::getModel("Songrequest");
		$oSongrequest->setOrder("idSongrequest");
		
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPerPage(9999999);
		$oTable->setUsingKeys(array(
				"idSongrequest" => Translate::get("ID"),
				"title" 		=> Translate::get("SONGTITLE"),
				"postedBy" 		=> Translate::get("POSTED_BY")
		));
		
		$oTable->setData($oSongrequest->getAll());
		
		View::tableObject( $oTable );
				
		$aSong = $oSongrequest->getFirst();
		View::currentSong( $aSong );		
	}

	public function render() {
		View::navi("songrequest");
		View::title("songrequest");
		
		parent::render();
	}
}