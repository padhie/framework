{{View::render("header_angular")}}


<div ng-bind-html="content | page"></div>

<script type="text/javascript">
	angular.module('myApp', [])
	.filter('page', ['$sce', function($sce){
	    return function(text) {
	        return $sce.trustAsHtml(text);
	    };
	}])
	
	.controller('customersCtrl', ['$scope', '$http', '$sce', function($scope, $http, $sce) {
	    $scope.changeContent = changeContent;
	
	    function changeContent (content){
	    	$http.get("{{Config::getServerHost()}}/"+content+"/?view=single").success(function (response) {
	    		var navi = document.getElementsByTagName("nav");
	    		var activNavi = navi[0].getElementsByClassName("active")
	    		activNavi[0].className = activNavi[0].className.replace("active", "");
	    		document.getElementById("navi_"+content).className += " active";
	    		navi = null;
	    		activNavi = null;
	    	
		    	
	    		$scope.content = response;
        	});
	    }
	}]);
</script>


{{View::render("footer")}}