<?php
class CronjobControllerStatistic extends eController implements iController {
	public function start() {
		parent::start();
		parent::loadModulClasses("statistic");
	}
	
	public function run() {
		$sChannelName = Params::getParams("streamer", "padhie");
		$oKraken = new twitchKraken();
		$aNewData = array();
		$aNewData["time"] = date("Y-m-d H:i:s");


		// Onlinestatus
		// $oKraken->loadStreamData($sChannelName);
		// $aStreamdata = json_decode($oKraken->getData(), true);
		$aNewData["online"] = 0;


		// Basesdata
		$oKraken->loadChannelData($sChannelName);
		$aChanneldata = json_decode($oKraken->getData(), true);
		$aNewData["game"] = $aChanneldata["game"];
		$aNewData["followers"] = $aChanneldata["followers"];


		// Hostings
		$sHostingdata = file_get_contents("http://tmi.twitch.tv/hosts?include_logins=1&target=".$aChanneldata["_id"]);
		$aHostingdata = json_decode($sHostingdata, true);
		$aNewData["hostings"] = array();
		$aNewData["hostingsCount"] = 0;
		foreach ($aHostingdata["hosts"] AS $aSingleHost) {
			array_push($aNewData["hostings"], $aSingleHost["host_display_name"]);
			$aNewData["hostingsCount"]++;
		}
		$aNewData["hostings"] = serialize($aNewData["hostings"]);


		// Viewer
		$sViewdata = file_get_contents("https://tmi.twitch.tv/group/user/".$sChannelName."/chatters");
		$aViewdata = json_decode($sViewdata, true);
		$aNewData["viewsCount"] = $aViewdata["chatter_count"];



		$oStatistik = Factory::getModel("statistic", "statistic");
		$oStatistik->insert($aNewData);


		unset($oKraken, $aChanneldata, $sHostingdata, $aHostingdata, $oStatistik, $aNewData);
	}

	public function render() {}
}