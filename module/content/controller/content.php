<?php
class ContentControllerContent extends eController implements iController {

	public function run() {
		$sPage = Params::getParams("page", "404");
		$sPage = str_replace(" ", "-", $sPage);
		$sPage = str_replace("/", "", $sPage);
		$sPage = str_replace("\\", "", $sPage);

		$oContentPage = Factory::getModel("page");
		$oContentPage->addWhere("key", $sPage);
		$oContentPage->addWhere("idContentPage", $sPage, "OR");
		$oContentPage->addWhere("active", "1");
		$aContentPage = $oContentPage->getSingleData();
		unset($oContentPage);

		if (count($aContentPage) == 0) {
			$aContentPage = array(
				"idCmsPage"	=> 0,
				"title" 	=> "",
				"content"	=> "",
				"key"		=> ""
			);
		}

		View::navi($aContentPage["key"]);
		View::title($aContentPage["title"]);
		View::content($aContentPage["content"]);
	}

	public function render() {
		View::render("content");
	}
}