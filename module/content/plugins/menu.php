<?php 

class ContentPluginMenu extends PluginCore {
	public function getContent($sParams) {
		$oItems = Factory::getModel("menuitem", "content");
		$oItems->addWhere("idContentMenu", $sParams["idContentMenu"]);
		$oItems->addWhere("active", 1);
		$oItems->addOrder("ordering");
		$aItems = $oItems->getAll();

		View::menuData($aItems);
		$sContent = View::fetch($sParams["template"]);
		return $sContent;
	}
}