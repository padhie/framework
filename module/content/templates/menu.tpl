<nav class="navbar navbar-defailt navbar-fixed-top row">
	<div class="col-md-offset-1 col-md-10 container-fluid">
		<div class="collapse navbar-collapse">
			<ul class="nav nav-pills">
				{{foreach from=$menuData item=menuItem}}
					<li role="presentation" {{if $navi == $menuItem.key}} class="active"{{/if}}>
						<a href="{{if isset($menuItem.link)}} {{$menuItem.link}} {{else}} javascript:void(0); {{/if}}">
							{{$menuItem.title}} 
							{{if isset($menuItem.alphaflag) && $aNavi.alphaflag == true}}
								{{View::render("alphaflag",false)}}
							{{elseif isset($menuItem.betaflag) && $aNavi.betaflag == true}}
								{{View::render("betaflag",false)}}
							{{/if}}
						</a>
					</li>
					{{if isset($menuItem.subnavi)}}
						{{foreach from=$menuItem.subnavi item=submenuItem}}
							<li role="presentation" {{if $subnavi == $submenuItem.key}} class="active"{{/if}}>
								<a href="{{if isset($aSubNavi["link"])}} {{$aSubNavi["link"]}} {{else}} javascript:void(0);}} {{/if}}">
									{{$submenuItem.title}} 
									{{if isset($submenuItem.alphaflag) && $submenuItem.alphaflag == true}}
										{{View::render("alphaflag",false)}}
									{{elseif isset($submenuItem.betaflag) && $submenuItem.betaflag == true}}
										{{View::render("betaflag",false)}}
									{{/if}}
								</a>
							</li>
						{{/foreach}}
					{{/if}}
				{{/foreach}}

				{{if View::navi()=="multipage"}}
					<li role="presentation" {{if $navi == "multipage"}} class="active"{{/if}}><a href="{{Config::getServerHost()}}/multipage">{{Translate::say("MENU_MULTIPAGE")}}</a></li>
				{{/if}}
			</ul>
		</div>
	</div>
</nav>