<?php
class FormmanagerControllerFormmanager extends eController implements iController {

	public function run() {
		$iIdForm = intval(Params::getParams("idForm"));

		$oForm = Factory::getModel("form", "formmanager");
		$oForm->addWhere("active", 1);
		$aFormData = $oForm->getById($iIdForm);
		unset($oForm);

		// Aktives Formular gefunden?
		if (!empty($aFormData)) {

			// Wenn FormID übergeben wurde
			$oField = Factory::getModel("field", "formmanager");
			$oField->addWhere("idForm", $iIdForm);
			$aFields = $oField->getAll();
			
			// Wenn Formularfelder vorhanden sind
			if (count($aFields) > 0) {
				View::aForm($aFormData);
				View::aFields($aFields);

				View::navi("form_".$aFormData["name"]);
				View::title($aFormData["name"]);
				View::render("formpage");
			}
			else {
				$oController = new controller();
				$oController->error();
				unset($oController);
			}
			unset($oField, $aFields);

		}
		else {
			$oController = new controller();
			$oController->error();
			unset($oController);
		}

		unset($aFormData);
	}

	public function render() {}

	public function save() {
		$oEintrag = Factory::getModel("eintrag", "formmanager");
		$iNextId = intval($oEintrag->getLastId())+1;
		$iIdForm = intval(Params::postParams("form"));
		$sDate = date("Y-m-d H:i:s");
		$blCheck = true;

		$oField = Factory::getModel("field", "formmanager");
		$oField->addWhere("idForm", $iIdForm);
		$aFields = $oField->getAll();
		if (count($aFields) > 0) {
			foreach ($aFields AS $aFieldData) {
				$aInsertData = array(
					"idEintrag"	=> $iNextId,
					"idForm"	=> $iIdForm,
					"idField"	=> $aFieldData["idField"],
					"create"	=> $sDate,
					"content"	=> Params::postParams("field_".$iIdForm."_".$aFieldData["idField"])
				);

				if ($oEintrag->insert($aInsertData) === false) {
					$blCheck = false;
				}
			}
		}

		View::completeInsert($blCheck);
	}
}