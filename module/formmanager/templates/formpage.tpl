{{View::render("header")}}

{{if isset($completeInsert)}}
	{{if $completeInsert == true}}
		<div class="alert alert-success" role="alert">
			<a href="#" class="alert-link">Eintragen Erfolgreich!</a>
		</div>
	{{else}}
		<div class="alert alert-danger" role="alert">
			<a href="#" class="alert-link">Daten konnten nicht vollständig gespeichert werden.</a>
		</div>
	{{/if}}
{{/if}}

<form action="" method="post">

	{{foreach from=$aFields item=$aFieldData}}
		<div class="form-group">
			{{if $aFieldData.useLabel == "1"}}
				<label for="field_{{$aForm.idForm}}_{{$aFieldData.idField}}">{{$aFieldData.name}}</label>
			{{/if}}
			<input 
				type="{{$aFieldData.type}}" 
				id="field_{{$aForm.idForm}}_{{$aFieldData.idField}}"
				name="field_{{$aForm.idForm}}_{{$aFieldData.idField}}" 
				placeholder="{{$aFieldData.placeholder}}"
				value="{{$aFieldData.value}}" 
				class="form-control {{$aFieldData.idField}}"
				{{$aFieldData.additionalTag}}
			>
		</div>
	{{/foreach}}

	<div class="form-group text-right">
		<input type="hidden" name="action" value="save">
		<input type="hidden" name="form" value="{{$aForm.idForm}}">
		<input type="submit" value="SEND" class="btn btn-primary">
	</div>
</form>

{{View::render("footer")}}