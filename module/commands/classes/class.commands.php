<?php

class CommandsModelCommands extends DbExtent {
	protected $_sTable = "commands";
	protected $_sIdField = "idCommand";

	public function getAllFuelData(){
		$aData = parent::getAll();
		$oRang = Factory::getModel("Rangs","Rangs");
		
		for ($i=0; $i<count($aData); $i++) {
			$aData[$i]["minRang"] = "";
			$aRangData = $this->getRangsOfCommand($aData[$i]["idCommand"]);
			$aData[$i]["minRang"] = $oRang->createRangTextByData($aRangData);
		}
		return $aData;
	}
	
	public function getRangsOfCommand($idCommand) {
		$this->_oQueryBuilder->setTable("command_has_rang");
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "rang", "idRang");
		$this->_oQueryBuilder->addWhere("idCommand", $idCommand);
		
		$aData = parent::getAll();
		$this->init();
		return $aData;
	}

	public function changeCommandToRang($idCommand, $aRangIds=array()) {
		$this->_oQueryBuilder->setTable("command_has_rang");
		$this->delete($idCommand);
		
		foreach ($aRangIds AS $iIdRang) {
			$this->insert(array(
				"idCommand" => $idCommand,
				"idRang" => $iIdRang
			));
		}
		
		$this->init();
	}
}