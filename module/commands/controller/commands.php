<?php
class CommandsControllerCommands extends eController implements iController {
	public function start() {
		parent::start();
		parent::loadModulClasses("rangs");
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"command" 	=> Translate::get("COMMAND"),
				"text" 		=> Translate::get("COMMANDTEXT"),
				// "minRang"	=> Translate::get("MIN_RANG")
		));
		
		$oCommands = Factory::getModel("commands");
		$oCommands->addWhere("active", "1");
		$oCommands->addWhere("type", "timer", "AND", "!=");
		$oCommands->addWhere("type", "alias", "AND", "!=");
		$oCommands->addOrder("command");
		$oTable->setData($oCommands->getAllFuelData());
		
		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("commands");
		View::title("commands");
		
		parent::render();
	}
}