<?php

class FilesConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"files", 
			"name"=>Translate::get("MENU_ADMIN_FILES"),
			"link"=>LinkHelper::modul("files"),
			"color"=>"danger",
			"icon"=>"fa-file-o"
		);
	}

	public function getRights() {
		return array(
			"files"		=> Translate::get("MENU_ADMIN_FILES"),
		);
	}
}