<?php
class FilesControllerFiles extends eAdminController implements iController {
	public function run() {		
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(100);
		$oTable->setUsingKeys(array(
				"file" 		=> Translate::get("FILE"),
				"link" 		=> Translate::get("LINK"),
				"btns"		=> "",
		));
		$oTable->addExtra("btns",
			'<a href="%link%" target="_blank" class="btn btn-info"><span aria-hidden="true" class="glyphicon glyphicon-new-window"></span></a> '.
			'<button onclick="openModal(\'%file%\')" class="btn btn-danger">'.Translate::get("DELETE").'</button> '
		);
		
		$oFile = new File();
		$oFile->setDir(Config::$sCustomBase.Config::$sCustomFiles);
		foreach($oFile->getListOfDir() AS $sFile) {
			if (!is_dir(Config::getDocumentRoot().$oFile->getDir()."/".$sFile)) {
				$oTable->addData(array(
						"file" => trim($sFile),
						"link" => Config::getServerHost().Config::$sCustomBase.Config::$sCustomFiles."/".$sFile
				));
			}
		}
		View::tableObject( $oTable );
		
		
		$oForm = new FormHelper(Config::getServerHost()."/admin/files/", "post", "_self", "");
		$oForm->addElement("file",		"file",		"",						Translate::get("FILE"), 	array(), 	array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 		Translate::get("SAVE"),	"", 						array(), 	"col-md-12");
		$oForm->addElement("hidden", 	"action", 	"save");
		View::formObject( $oForm );
		unset($oForm);
	}

	public function render() {
		View::navi("files");
		View::title("Files");
		View::render("files");
	}
	
	public function action($action) {
		switch($action) {
			case "save":
				$sFilename = "";
				$sFilename .= Config::$sCustomBase.Config::$sCustomFiles."/";
				$sFilename .= Params::postParams("file");
				
				$oFile = new File();
				$oFile->uploadFile($_FILES["file_file"], $sFilename, true);

				LogModelLog::addLog("admin", "file", Params::cookieParams("username"), "Upload file".Params::postParams("file")." | data=>".var_export($_FILES, true));
				unset($oFile);
				break;
					
			case "delete":
				$oFile = new File();
				$oFile->setDir(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomFiles."/");
				$oFile->setFile(Params::getParams("id"));
				$oFile->deleteFile();
				
				LogModelLog::addLog("admin", "file", Params::cookieParams("username"), "Delete file".Params::getParams("id"));
				break;
		}
		unset($oCarousel);
	}
}