<?php

class BotConfig extends ModulConfig {
	public function getSettingVars($oSettings) {
		return array(
			"basic" => array(
				"title" => Translate::get("BOTSETTINGS"),
				"color" => "info",
				"flag"	=> ""
			),
			"data"	=> array(
				"bot_lastUpdate" => array(
					"type" 	=> "label",
					"label"	=> Translate::get("LAST_UPDATE"),
					"data" 	=> array()
				),
				"bot_intervall" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("UPDATE_INTERVAL"),
					"data" 	=> array()
				),
				"bot_cooldownIntervall" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("COOLDOWN_INTERVAL"),
					"data" 	=> array()
				),
				"bot_message_durable" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("MESSAGE_DURABLE"),
					"data" 	=> $oSettings->getChecked("bot_message_durable")
				),
				"bot_message" => array(
					"type" 	=> "text",
					"label"	=> Translate::get("MESSAGE"),
					"data" 	=> array()
				),
				"bot_restart" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("RESTART"),
					"data" 	=> $oSettings->getChecked("bot_restart")
				),
				"bot_loginWhisper" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("LOGINWHISPER"),
					"data" 	=> $oSettings->getChecked("bot_loginWhisper")
				),
				"bot_spamDetection" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("SPAMDETECTION"),
					"data" 	=> $oSettings->getChecked("bot_spamDetection")
				),
				"bot_settingsChangeWhisper" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("SETTINGSCHANGEWHISPER"),
					"data" 	=> $oSettings->getChecked("bot_settingsChangeWhisper")
				),
			)
		);
	}
}