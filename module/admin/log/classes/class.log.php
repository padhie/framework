<?php
class LogModelLog extends DbExtent {
	protected $_sTable 		= "log";
	protected $_sIdField 	= "idLog";
	
	/**
	 * Speichert einen Logeintrag
	 * @param String $sType Haupttyp der Log
	 * @param String $sName Subtyp des Log
	 * @param String $sUser Username, welcher etwas ausgeführt hat
	 * @param String $sMessage Text, welcher mit gespeichert werden soll
	 */
	public static function addLog($sType, $sName, $sUser, $sMessage) {
		$oLog = new LogModelLog();
		$oLog->insert(array(
			"typ" => $sType,
			"name" => $sName,
			"user" => $sUser,
			"text" => $sMessage
		));
		unset($oLog);
	}
}