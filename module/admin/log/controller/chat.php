<?php
class LogControllerChat extends eAdminController implements iController {
	public function run() {
		
		if (Params::postParams("search", Params::getParams("search", false)) !== false) {
			$oLog = Factory::getModel("Log");
			$oLog->addOrder("time", "DESC");
			$oLog->addWhere("typ", "message");
			$oLog->addWhere("user", Params::postParams("search", Params::getParams("search")), "AND");
			
			$sLogData = "";
			foreach ($oLog->getAll() AS $aRow) {
				$_sTmpLogData = "";
				$_sTmpLogData .= "[".$aRow["time"]."] ";
				if( $aRow["name"] == "whisper") {
					$_sTmpLogData .= "(whisper)";
				}
				$_sTmpLogData .= $aRow["user"].": ";
				$_sTmpLogData .= $aRow["text"];
				$sLogData .= $_sTmpLogData."<br />";
			}
			
			View::result($sLogData);
		}
		
		$oForm = new FormHelper(Config::getServerHost()."/admin/log/chat/", "post", "_self", "");
		$oForm->addElement("text",		"search",	Params::postParams("search", Params::getParams("search", "")),	Translate::get("SEARCH"), 	array(), 	array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 		Translate::get("SEARCH"),										"", 						array(), 	"col-md-12");
		$oForm->addElement("hidden", 	"action", 	"search");
		
		View::search( $oForm );
		unset($oForm);	
	}
	
	public function render() {
		View::navi("log");
		View::subnavi("chatlog");
		View::title("Log");
		View::render("simpleLog");
	}
}