<?php
class LogControllerLog extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(100);
		$oTable->setUsingKeys(array(
				"idLog" 	=> Translate::get("ID"),
				"typ" 		=> Translate::get("TYPE"),
				"name" 		=> Translate::get("NAME"),
				"text" 		=> Translate::get("TEXT"),
				"user"		=> Translate::get("USER"),
				"time"		=> Translate::get("TIME")
		));
		
		$oLog = Factory::getModel("Log");
		$oLog->addOrder("time", "DESC");
		$oTable->setData($oLog->getAll());
		
		View::tableObject( $oTable );		
	}
	
	public function render() {
		View::navi("log");
		View::title("Log");
		View::render("log");
	}
}