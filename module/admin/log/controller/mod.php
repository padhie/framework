<?php
class LogControllerMod extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(20);
		$oTable->setUsingKeys(array(
				"time"		=> Translate::get("TIME"),
				"ziel" 		=> Translate::get("NAME"),
				"dauer" 	=> Translate::get("DURATION"),
				"mod"		=> Translate::get("MODERATOR"),
				"grund"		=> Translate::get("REASON"),
				"btns"		=> "",
		));
		
		$oTable->addExtra("btns",
			'<button onclick="openModal(%idLog%)" class="btn btn-danger">'.Translate::get("DELETE").'</button> '
		);

		$oLog = Factory::getModel("Log");
		$oLog->addOrder("time", "DESC");
		//$oLog->addWhere("typ", "timeout");
		$oLog->addWhere("name", "timeout");
		if (Params::postParams("search", false) !== false) {
			$oLog->addCustomWhere("(user='".Params::postParams("search")."' OR text LIKE '% target > ".Params::postParams("search")."%')", "AND");
			//$oLog->addWhere("user", Params::postParams("search"), "AND");
		}
		
		$aLogData = array();
		foreach ($oLog->getAll() AS $aRow) {
			if (strpos($aRow["text"], "target > ") !== false)	$_sTmpZiel = substr($aRow["text"], strpos($aRow["text"], " target > ")+9);
			else												$_sTmpZiel = "";
				
			if (strpos($aRow["text"], "duration > ") !== false)	$_sTmpDauer = substr($aRow["text"], strpos($aRow["text"], " duration > ")+11);
			else												$_sTmpDauer = "";

			if (strpos($aRow["text"], "mod > ") !== false)		$_sTmpMod = substr($aRow["text"], strpos($aRow["text"], " mod > ")+6);
			else												$_sTmpMod = "";
				
			if (strpos($aRow["text"], "target > ") !== false)	$_sTmpGrund = substr($aRow["text"], strpos($aRow["text"], " reason > ")+9);
			else												$_sTmpGrund = "";
				
			
			if (strpos($_sTmpZiel, " | ") !== false)	$_sTmpZiel	= substr($_sTmpZiel, 0, strpos($_sTmpZiel, "| "));
			else										$_sTmpZiel	= substr($_sTmpZiel, 0);

			if (strpos($_sTmpDauer, " | ") !== false)	$_sTmpDauer	= substr($_sTmpDauer, 0, strpos($_sTmpDauer, "| "));
			else										$_sTmpDauer	= substr($_sTmpDauer, 0);

			if (strpos($_sTmpMod, " | ") !== false)		$_sTmpMod	= substr($_sTmpMod, 0, strpos($_sTmpMod, "| "));
			else										$_sTmpMod	= substr($_sTmpMod, 0);

			if (strpos($_sTmpGrund, " | ") !== false)	$_sTmpGrund	= substr($_sTmpGrund, 0, strpos($_sTmpGrund, "| "));
			else										$_sTmpGrund	= substr($_sTmpGrund, 0);

				
			array_push($aLogData, array(
				"idLog"	=> $aRow["idLog"],
				"time" 	=> $aRow["time"],
				"ziel"	=> "<a href='".LinkHelper::controller("log", "chat")."?search=".trim($_sTmpZiel)."'>".trim($_sTmpZiel)."</a>",
				"dauer"	=> trim($_sTmpDauer),
				"mod"	=> trim($_sTmpMod),
				"grund"	=> trim($_sTmpGrund),
			));
		}
		$oTable->setData($aLogData);

		$oForm = new FormHelper(Config::getServerHost()."/admin/log/mod/", "post", "_self", "");
		$oForm->addElement("text",		"search",	Params::postParams("search", ""),	Translate::get("SEARCH"), 	array(), 	array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 		Translate::get("SEARCH"),			"", 						array(), 	"col-md-12");
		$oForm->addElement("hidden", 	"action", 	"search");
		
		View::search( $oForm );
		unset($oForm);
		
		View::tableObject( $oTable );		
	}
	
	public function render() {
		View::navi("log");
		View::subnavi("modlog");
		View::title("Log");
		View::render("log");
	}
	
	public function action($action) {
		$oLog = Factory::getModel("Log");
		switch($action) {
			case "delete":
				$oLog->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "log:mod", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oTemplate);
	}
}