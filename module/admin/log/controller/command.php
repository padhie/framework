<?php
class LogControllerCommand extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(20);
		$oTable->setUsingKeys(array(
				"idLog" 	=> Translate::get("ID"),
				"typ" 		=> Translate::get("TYPE"),
				"name" 		=> Translate::get("NAME"),
				"text" 		=> Translate::get("TEXT"),
				"user"		=> Translate::get("USER"),
				"time"		=> Translate::get("TIME"),
				"btns"		=> "",
		));
		
		$oTable->addExtra("btns",
			'<button onclick="openModal(%idLog%)" class="btn btn-danger">'.Translate::get("DELETE").'</button> '
		);
		
		$oLog = Factory::getModel("Log");
		$oLog->addOrder("time", "DESC");
		$oLog->addWhere("typ", "command");
		if (Params::postParams("search", false) !== false) {
			$oLog->addWhere("user", Params::postParams("search"), "AND");
		}
		$oTable->setData($oLog->getAll());
		View::tableObject( $oTable );	

		
		$oForm = new FormHelper(Config::getServerHost()."/admin/log/command/", "post", "_self", "");
		$oForm->addElement("text",		"search",	Params::postParams("search", ""),	Translate::get("SEARCH"), 	array(), 	array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 		Translate::get("SEARCH"),			"", 						array(), 	"col-md-12");
		$oForm->addElement("hidden", 	"action", 	"search");
		
		View::search( $oForm );
		unset($oForm);
	}
	
	public function render() {
		View::navi("log");
		View::subnavi("commandlog");
		View::title("Log");
		View::render("log");
	}
	
	public function action($action) {
		$oLog = Factory::getModel("Log");
		switch($action) {
			case "delete":
				$oLog->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "log:mod", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oTemplate);
	}
}