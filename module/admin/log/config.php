<?php

class LogConfig extends ModulConfig {
	public function getMenuConfig() {
		return 
			array("key"=>"log", 		"name"=>Translate::get("MENU_ADMIN_LOG"), 			"color"=>"primary",	"icon"=>"fa-file-text-o",	"subnavi"=>array(
				array("key"=>"commandlog", 	"name"=>Translate::get("MENU_ADMIN_COMMANDLOG"),	"link"=>LinkHelper::controller("log", "command"),	"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"chatlog",		"name"=>Translate::get("MENU_ADMIN_CHATLOG"), 		"link"=>LinkHelper::controller("log", "chat"),		"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"modlog", 		"name"=>Translate::get("MENU_ADMIN_MODLOG"), 		"link"=>LinkHelper::controller("log", "mod"),		"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"adminlog", 	"name"=>Translate::get("MENU_ADMIN_ADMINLOG"), 		"link"=>LinkHelper::controller("log", "admin"),		"color"=>"info",	"icon"=>"fa-circle"),
			));
	}

	public function getRights() {
		return array(
			"log"			=> Translate::get("MENU_ADMIN_LOG"),
			"commandlog"	=> "- ".Translate::get("MENU_ADMIN_COMMANDLOG"),
			"chatlog"		=> "- ".Translate::get("MENU_ADMIN_CHATLOG"),
			"modlog"		=> "- ".Translate::get("MENU_ADMIN_MODLOG"),
			"adminlog"		=> "- ".Translate::get("MENU_ADMIN_ADMINLOG"),
		);
	}
}