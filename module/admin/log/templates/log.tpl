{{View::render("header_admin")}}

<div class="col-md-12">
	{{if View::formObject() !== null}}
		{{View::formObject()->render()}}
	{{elseif View::search() !== null}}
		{{View::formObjectVariable("search")}}
		{{View::search()->render()}}
	{{/if}}
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
</div>

<!-- Modal -->
<script>
	var selectedId 	= 0;
	function openModal(iId) {
		selectedId = iId;
		$('#myModal').modal('show');
	}

	function successModal() {
		window.location = "{{Config::getServerHost()}}/admin/{{if View::subnavi() == "modlog"}}log/mod{{elseif View::subnavi() == "commandlog"}}log/command{{elseif View::subnavi()=="adminlog"}}log/admin{{/if}}/?action=delete&id="+selectedId;
	}
</script>
{{php}}
	View::modalTitle(Translate::get("LOG_MODAL_TITLE"));
	View::modalText(Translate::get("LOG_MODAL_TEXT"));
{{/php}}
{{View::render("modal")}}

{{View::render("footer")}}