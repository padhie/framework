{{View::render("header_admin")}}

<div class="col-md-12">
	{{if View::formObject() !== null}}
		{{View::formObject()->render()}}
	{{elseif View::search() !== null}}
		{{View::formObjectVariable("search")}}
		{{View::search()->render()}}
	{{/if}}
</div>

{{if View::result() !== null}}
	<div class="col-md-12">
		<section class="panel panel-default">
			<div class="panel-body padding-xl">
				{{nl2br(View::result())}}
			</div>
		</section>
	</div>
{{/if}}
{{View::render("footer")}}