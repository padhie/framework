<?php
class LoginControllerLogin extends eController implements iController {
	public function start() {
		parent::start();
		
		parent::loadModulClasses("settings","admin");		
		$oSettings = Factory::getModel("settings", "settings");
		$mStyle = $oSettings->getSetting("style_default");
		unset($oSettings);
		View::addTemplatePath("templates/".$mStyle);
	}
	
	public function run() {
		$oForm = new FormHelper(Config::getServerHost()."/admin/");
		$oForm->addElement("text", 		"admin_user", 	"",							Translate::get("USERNAME"),	array(),	array("", "form-control"));
		$oForm->addElement("password",	"admin_pass", 	"",							Translate::get("PASSWORD"),	array(),	array("", "form-control"));
		$oForm->addElement("submit", 	"", 			Translate::get("LOGIN"),	"",							array(),	"col-md-12");
		$oForm->addElement("hidden", 	"action", 		"login");
		View::formObject($oForm);	
	}
}