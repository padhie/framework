<?php

class AdminsModelAccess {
	public static function adminHasRightById($idAdmin, $sRight) {
		$oAdmin = Factory::getModel("admin", "admins");
		$oAdmin->addJoin();
		$oAdmin->addWhere("idAdmin", $idAdmin);
		$aRights = $oAdmin->getAll();

		$blHasRight = false;
		if (count($aRights) > 0) {
			if ($aRights[0]["master"] == 1) {
				$blHasRight = true;
			} else {
				foreach ($aRights AS $aRightItem) {
					if ($aRightItem["right"] == $sRight) {
						$blHasRight = true;
						break;
					}
				}
			}
		}

		return $blHasRight;
	}

	public static function adminHasRightByUsername($sUsername, $sRight) {
		$oAdmin = Factory::getModel("admin", "admins");
		$oAdmin->addJoin();
		$oAdmin->addWhere("username", $sUsername);
		$aRights = $oAdmin->getAll();

		$blHasRight = false;
		if (count($aRights) > 0) {
			if ($aRights[0]["master"] == 1) {
				$blHasRight = true;
			} else {
				foreach ($aRights AS $aRightItem) {
					if ($aRightItem["right"] == $sRight) {
						$blHasRight = true;
						break;
					}
				}
			}
		}

		return $blHasRight;
	}
}