<?php

class AdminsModelAdmin extends DbExtent {
	protected $_sTable = "admin";
	protected $_sIdField = "idAdmin";
	
	public function addJoin() {
		$this->_oQueryBuilder->addJoin("LEFT OUTER", "admin_rights", "idAdmin");
	}

	public function checkLoginData($sUsername, $sClearPassword) {
		$this->_oQueryBuilder->addWhere("username", $sUsername);
	
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			$aResult = $this->getResult();			
			if ($aResult[0]["password"] == md5($sClearPassword)) {
				return true;
			}
		}
		return false;
	}
	
	public function getTemplate($sUsername) {
		$this->_oQueryBuilder->addWhere("username", $sUsername);
		
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			$aResult = $this->getSingleResult();
			if (isset($aResult["style"]) && $aResult["style"] !== "") {
				return $aResult["style"];
			}
		}
		return false;
	}

	public function saveRights($iIdAdmin, $aRights){
		$this->_oQueryBuilder->setTable("admin_rights");
		$this->delete($iIdAdmin);
		
		foreach ($aRights AS $sRight) {
			$aData = array(
				"idAdmin" 	=> $iIdAdmin,
				"right" 	=> $sRight
			);
			$this->insert($aData);
		}
		$this->init();
	}
	
	public function getRights($iIdAdmin) {
		$this->_oQueryBuilder->setTable("admin_rights");
		$this->addWhere("idAdmin", $iIdAdmin);
				
		$aData = array();
		foreach ($this->getAll() AS $aRow) {
			array_push($aData, $aRow["right"]);
		}
		
		$this->init();
		return $aData;
	}
	
	public function getRightsByUsername($sUsername) {
		$this->addJoin();
		$this->_oQueryBuilder->addWhere("username", $sUsername);
		
		$aData = array();
		foreach ($this->getAll() AS $aRow) {
			array_push($aData, $aRow["right"]);
		}
		
		return $aData;
	}
}