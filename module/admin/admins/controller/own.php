<?php
class AdminsControllerOwn extends eAdminController implements iController {
	public function run() {
		$oAdmin = Factory::getModel("Admin");		
		$oAdmin->addWhere("username", Params::cookieParams("username"));
		$aAdmin = $oAdmin->getAll();
		$oForm = new FormHelper(Config::getServerHost()."/admin/admins/own/", "post");
		$oForm->addElement("label",		"admin_id",		$aAdmin[0]["idAdmin"], 	Translate::get("IDADMIN"),	"",								array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"admin_name", 	$aAdmin[0]["username"],	Translate::get("USERNAME"),	"",								array("col-md-2", "col-md-9"));
		$oForm->addElement("password", 	"admin_pass", 	"",						Translate::get("PASSWORD"),	"",								array("col-md-2", "col-md-9"));
		$oForm->addElement("select", 	"admin_style", 	$aAdmin[0]["style"],	Translate::get("STYLE"),	$this->getAvailableStyles(),	array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 	"id", 			$aAdmin[0]["idAdmin"]);
		$oForm->addElement("hidden", 	"action",		"save");
		$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
		View::formObject($oForm);
		unset($oForm, $oAdmin);
	}
	
	public function render() {
		View::navi("admins");
		View::subnavi("ownadmins");
		View::title("admins");
		View::render("adminsSingle");
	}
	
	public function action($action) {
		$oAdmin = Factory::getModel("Admin");
		switch($action) {
			case "save":
				$aData = array(
					"username"	=> Params::postParams("admin_name", ""),
					"style"		=> Params::postParams("admin_style", "")
				);
				if (Params::postParams("admin_pass")) {
					$aData["password"] = md5(Params::postParams("admin_pass", ""));
				}
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "admin", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oAdmin->update($aData, intval( Params::postParams("id") ));
				}
				break;
		}
		unset($oRangs);
	}
	
	public function getAvailableStyles() {
		$aStyles = array();
		$oSettings = Factory::getModel("settings","settings");
		foreach (explode(",",$oSettings->getSetting("style_available")) AS $sStyle) {
			array_push($aStyles, array("key"=>$sStyle,"value"=>ucfirst(strtolower($sStyle))));
		}
		return $aStyles;
	}
}