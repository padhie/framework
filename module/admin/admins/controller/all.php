<?php
class AdminsControllerAll extends eAdminController implements iController {	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(10);
		$oTable->setUsingKeys(array(
				"idAdmin" 	=> Translate::get("ID"),
				"username" 	=> Translate::get("NAME"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.Config::getServerHost().'/admin/admins/all/?action=load&id=%idAdmin%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idAdmin%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);
		$oAdmin = Factory::getModel("Admin");
		$oTable->setData($oAdmin->getAll());
		View::tableObject( $oTable );
		unset($oAdmin, $oTable);
		
		if (View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/admins", "post");
			$oForm->addElement("label",		"admin_id",		"", Translate::get("IDADMIN"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"admin_name", 	"",	Translate::get("USERNAME"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("password", 	"admin_pass", 	"",	Translate::get("PASSWORD"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("select", 	"admin_style", 	"",	Translate::get("STYLE"),	$this->getAvailableStyles(),	array("col-md-2", "col-md-9"));
			$oForm->addElement("multiple",	"admin_rights",	"",	Translate::get("RIGHTS"),	$this->getRightList(),			array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"id", 			"");
			$oForm->addElement("hidden", 	"action",		"save");
			$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
			View::formObject($oForm);
			unset($oForm);
		}
	}
	
	public function render() {
		View::navi("admins");
		View::subnavi("alladmins");
		View::title("admins");
		View::render("admins");
	}
	
	public function action($action) {
		$oAdmin = Factory::getModel("Admin");
		switch($action) {
			case "load":
				$aAdmin = $oAdmin->getById(intval( Params::getParams("id") ));
				$aRights = $oAdmin->getRights(intval( Params::getParams("id") ));

				$oForm = new FormHelper(Config::getServerHost()."/admin/admins/all/", "post");
				$oForm->addElement("label",		"admin_id",		$aAdmin["idAdmin"], 	Translate::get("IDADMIN"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("text", 		"admin_name", 	$aAdmin["username"],	Translate::get("USERNAME"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("password", 	"admin_pass", 	"",							Translate::get("PASSWORD"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("select", 	"admin_style", 	$aAdmin["style"],		Translate::get("STYLE"),	$this->getAvailableStyles(),	array("col-md-2", "col-md-9"));
				$oForm->addElement("multiple",	"admin_rights",	$aRights,					Translate::get("RIGHTS"),	$this->getRightList(),			array("col-md-2", "col-md-9"));
				$oForm->addElement("hidden", 	"id", 			$aAdmin["idAdmin"]);
				$oForm->addElement("hidden", 	"action",		"save");
				$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
				View::formObject($oForm);
				unset($oForm, $aAdmin);
				break;
					
			case "new":
			case "save":
				$aData = array(
					"username"	=> Params::postParams("admin_name", ""),
					"style"		=> Params::postParams("admin_style", "")
				);
				if (Params::postParams("admin_pass")) {
					$aData["password"] = md5(Params::postParams("admin_pass", ""));
				}
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "admin", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oAdmin->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oAdmin->insert($aData);
					LogModelLog::addLog("admin", "admin", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				
				$oAdmin->saveRights(intval(Params::postParams("id")), Params::postParams("admin_rights"));
				break;


			case "delete":
				LogModelLog::addLog("admin", "admin", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oAdmin->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oRangs);
	}
	
	public function getAvailableStyles() {
		$aStyles = array();
		$oSettings = Factory::getModel("settings","settings");
		foreach (explode(",",$oSettings->getSetting("style_available")) AS $sStyle) {
			array_push($aStyles, array("key"=>$sStyle,"value"=>ucfirst(strtolower($sStyle))));
		}
		return $aStyles;
	}

	public function getRightList() {
		$aRights = array();
		
		if (is_array(Config::$aModules)) {
			foreach (Config::$aModules AS $sModul) {
				$sModulConfig = ModulHelper::getConfigClassname($sModul);
				$oConfigObj = new $sModulConfig();
				if (is_array($oConfigObj->getRights())) {
					$aRights = array_merge($aRights, $oConfigObj->getRights());
				}
			}
		}

		return $aRights;
	}
}