<?php
class AdminsControllerAdmins extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idAdmin" 	=> Translate::get("ID"),
				"username" 	=> Translate::get("NAME"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.Config::getServerHost().'/admin/admins?action=load&id=%idAdmin%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idAdmin%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);
		$oAdmin = Factory::getModel("Admin");
		$oTable->setData($oAdmin->getAll());
		View::tableObject( $oTable );
		unset($oAdmin, $oTable);
		
		
		if (View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/admins", "post");
			$oForm->addElement("label",		"admin_id",		"", Translate::get("IDADMIN"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"admin_name", 	"",	Translate::get("USERNAME"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("password", 	"admin_pass", 	"",	Translate::get("PASSWORD"),	"",								array("col-md-2", "col-md-9"));
			$oForm->addElement("select", 	"admin_style", 	"",	Translate::get("STYLE"),	$this->getAvailableStyles(),	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"id", 			"");
			$oForm->addElement("hidden", 	"action",		"save");
			$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
			View::formObject($oForm);
			unset($oForm);
		}
	}
	
	public function render() {
		View::navi("admins");
		View::title("admins");
		View::render("admins");
	}
	
	public function action($action) {
		$oAdmin = Factory::getModel("Admin");
		switch($action) {
			case "load":
				$aAdmin =  $oAdmin->getById(intval( Params::getParams("id") ));

				$oForm = new FormHelper(Config::getServerHost()."/admin/admins", "post");
				$oForm->addElement("label",		"admin_id",		$aAdmin["idAdmin"], 	Translate::get("IDADMIN"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("text", 		"admin_name", 	$aAdmin["username"],	Translate::get("USERNAME"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("password", 	"admin_pass", 	"",							Translate::get("PASSWORD"),	"",								array("col-md-2", "col-md-9"));
				$oForm->addElement("select", 	"admin_style", 	$aAdmin["style"],		Translate::get("STYLE"),	$this->getAvailableStyles(),	array("col-md-2", "col-md-9"));
				$oForm->addElement("hidden", 	"id", 			$aAdmin["idAdmin"]);
				$oForm->addElement("hidden", 	"action",		"save");
				$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
				View::formObject($oForm);
				unset($oForm, $aAdmin);
				break;
					
			case "new":
			case "save":
				$aData = array(
					"username"	=> Params::postParams("admin_name", ""),
					"style"		=> Params::postParams("admin_style", "")
				);
				if (Params::postParams("admin_pass")) {
					$aData["password"] = md5(Params::postParams("admin_pass", ""));
				}
			
				if (Params::postParams("id") !== false) {
					$oAdmin->update($aData, intval( Params::postParams("id") ));
				} else {
					$oAdmin->insert($aData);
				}
				break;


			case "delete":
				$oAdmin->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oRangs);
	}
	
	public function getAvailableStyles() {
		$aStyles = array();
		$oSettings = Factory::getModel("settings","settings");
		foreach (explode(",",$oSettings->getSetting("style_available")) AS $sStyle) {
			array_push($aStyles, array("key"=>$sStyle,"value"=>ucfirst(strtolower($sStyle))));
		}
		return $aStyles;
	}
}