<?php

class AdminsConfig extends ModulConfig {
	public function getMenuConfig() {
		return 
			array("key"=>"admins", 		"name"=>Translate::get("MENU_ADMIN_ADMINS"), 		"color"=>"info",	"icon"=>"fa-user",			"subnavi"=>array(
				array("key"=>"alladmins", 	"name"=>Translate::get("MENU_ADMIN_ALL"), 		"link"=>LinkHelper::controller("admins", "all"),		"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"ownadmins", 	"name"=>Translate::get("MENU_ADMIN_OWN"), 		"link"=>LinkHelper::controller("admins", "own"),		"color"=>"info",	"icon"=>"fa-circle"),
			))
		;
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "admins")) {
			$oAdmin = Factory::getModel("Admin", "admins");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_ADMINS"),
				"msg"	=> count($oAdmin->getAll())." ".Translate::get("ADMINS"),
				"link" 	=> LinkHelper::_("admins", "all"),
				"color"	=> "info"
			);
		}
	}

	public function getRights() {
		return array(
			"admins"			=> Translate::get("MENU_ADMIN_ADMINS"),
			"alladmins"		=> "- ".Translate::get("MENU_ADMIN_ALL"),
			"ownadmins"	=> "- ".Translate::get("MENU_ADMIN_OWN"),
		);
	}
}