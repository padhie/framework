{{if View::admin()!=""}}
	{{assign var=$aAdmin value=View::admin()}}
{{else}}
	{{php}}
		View::aAdmin(array(
			"idAdmin" => "",
			"username" => ""
		));
	{{/php}}
{{endif}}
	
<form method="post" action="{{Config::getServerHost()}}/admin/admins">

	<div class="form-group row">
		<label for="admin_id" class="control-label col-md-2">{{Translate::say("IDADMIN")}}</label>
		<span>{{$aAdmin.idAdmin}}</span>
	</div>
	<div class="form-group row">
		<label for="admin_name" class="control-label col-md-2">{{Translate::say("USERNAME")}}</label>
		<input type="text" class="form-control-static col-md-9" name="admin_name" id="admin_name" value="{{$aAdmin.username}}">
	</div>
	
	<div class="form-group row">
		<label for="admin_pass" class="control-label col-md-2">{{Translate::say("PASSWORD")}}</label>
		<input type="password" class="form-control-static col-md-9" name="admin_pass" id="admin_pass">
	</div>

	<div class="row">
		<input type="submit" class="btn btn-success col-md-11" value="{{Translate::say("SAVE")}}" />
		<input type="hidden" name="id" value="{{$aAdmin.idAdmin}}" />
		<input type="hidden" name="action" value="save" />
	</div>
</form>	