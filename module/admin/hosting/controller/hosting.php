<?php
class HostingControllerHosting extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("settings", "admin");
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idHost" 	=> Translate::get("ID"),
				"channel" 	=> Translate::get("CHANNEL"),
				"prio" 		=> Translate::get("PRIORITY"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.Config::getServerHost().'/admin/hosting?action=load&id=%idHost%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idHost%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oHost = Factory::getModel("Host");
		$oHost->addOrder("prio", "DESC");
		$oHost->addOrder("idHost", "DESC");
		$oTable->setData($oHost->getAll());
		View::tableObject( $oTable );
		
		
		View::hosts( $oHost->getAll() );
		unset($oHost);
		
		
		if (View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/hosting", "post");
			$oForm->addElement("label",		"host_name",	"", Translate::get("IDHOST"),	array(),				array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"host_channel", "",	Translate::get("CHANNEL"),	array(),				array("col-md-2", "col-md-9"));
			$oForm->addElement("select", 	"host_prio", 	"",	Translate::get("PRIORITY"),	$this->createProList(),	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"action",		"new");
			$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("hosting");
		View::title("hosting");
		View::render("hosting");
	}
	
	public function action($action) {
		$oHost = Factory::getModel("Host", "hosting");
		switch($action) {
			case "load":
				$aHost =  $oHost->getById(intval( Params::getParams("id") ));
				
				$oForm = new FormHelper(Config::getServerHost()."/admin/hosting", "post");
				$oForm->addElement("label",		"host_name",	$aHost["idHost"], 	Translate::get("IDHOST"),	array(),				array("col-md-2", "col-md-9"));
				$oForm->addElement("text", 		"host_channel", $aHost["channel"],	Translate::get("CHANNEL"),	array(),				array("col-md-2", "col-md-9"));
				$oForm->addElement("select", 	"host_prio", 	$aHost["prio"],		Translate::get("PRIORITY"),	$this->createProList(),	array("col-md-2", "col-md-9"));
				$oForm->addElement("hidden", 	"id",			$aHost["idHost"]);
				$oForm->addElement("hidden", 	"action",		"save");
				$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
				View::formObject($oForm);
				unset($oForm);
				
				View::host( $aHost );
				unset($aHost);
				break;
					
			case "new":
				$aData = array(
					"channel"	=> Params::postParams("host_channel", ""),
					"prio"	=> Params::postParams("host_prio", ""),
				);
				$oHost->insert($aData);
				LogModelLog::addLog("admin", "hosting", Params::cookieParams("username"), "Insert id #".$oHost->getLastInsertId());
				break;
				
			case "save":
				$aData = array(
					"channel"	=> Params::postParams("host_channel", ""),				
					"prio"	=> Params::postParams("host_prio", ""),				
				);
				if (Params::postParams("id") !== false) {
					$oHost->update($aData, intval( Params::postParams("id") ));
					LogModelLog::addLog("admin", "hosting", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oHost->insert($aData);
					LogModelLog::addLog("admin", "hosting", Params::cookieParams("username"), "Insert id #".$oHost->getLastInsertId());
				}
				break;
					
			case "delete":
				$oHost->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "hosting", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oHost);
	}
	
	public function createProList() {
		$oSettings = Factory::getModel("Settings", "Settings");
		View::hostingPrioGroup($oSettings->getSetting("hosting_priogroups"));		// Backup für altes layout
		
		$_aData = array();
		for ($i=1; $i<=$oSettings->getSetting("hosting_priogroups"); $i++) {
			array_push($_aData, array(
				"key" 	=> $i,
				"value"	=> Translate::get("PRIORITY")." ".$i
			));
		}
		
		return $_aData;
	}
}