<?php

class HostingConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"hosting", 	
			"name"=>Translate::get("MENU_ADMIN_HOSTING"), 		
			"link"=>LinkHelper::modul("hosting"),		
			"color"=>"violet",	
			"icon"=>"fa-video-camera",	
			"betaflag"=>true
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "hosting")) {
			$oHosts = Factory::getModel("Host", "Hosting");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_HOSTING"),
				"msg"	=> count($oHosts->getAll())." ".Translate::get("HOSTS"),
				"link" 	=> LinkHelper::_("hosting"),
				"color"	=> "violet"
			);
		}
	}

	public function getRights() {
		return array(
			"hosting"		=> Translate::get("MENU_ADMIN_HOSTING"),
		);
	}

	public function getSettingVars($oSettings) {
		return array(
			"basic" => array(
				"title" => Translate::get("HOSTING"),
				"color" => "info",
				"flag"	=> ""
			),
			"data"	=> array(
				"hosting_priogroups" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("PRIORITY"),
					"data" 	=> array()
				),
			)
		);
	}
}