{{View::render("header_admin")}}

<div class="col-md-7">
	{{View::formObject()->render()}}
</div>

<div class="col-md-5">
	{{View::tableObject()->render()}}
	
	{{assign var="sModalLink" value=LinkHelper::_("hosting")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}