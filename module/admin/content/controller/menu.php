<?php
class ContentControllerMenu extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(20);
		$oTable->setUsingKeys(array(
				"idContentMenu" => Translate::get("ID"),
				"name" 			=> Translate::get("NAME"),
				"active" 		=> Translate::get("AKTIV"),
				"btns" 			=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.LinkHelper::_("content", "menu").'?action=load&id=%idContentMenu%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idContentMenu%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oMenu = Factory::getModel("Menu");
		$oTable->setData($oMenu->getAll());
		unset($oMenu);
		View::tableObject( $oTable );

		if (View::formObject() === null) {
			View::formObject($this->getForm());
		}
	}

	public function render() {
		View::navi("content");
		View::title("Content-Menu");
		View::render("menu");
	}

	public function action($action) {
		$oContentMenu = Factory::getModel("menu");
		switch($action) {
			case "load":
				$aContentMenu = $oContentMenu->getById(intval( Params::getParams("id") ));
				$aOptionData=array();
				if ($aContentMenu["active"]=="1") {
					$aOptionData["checked"] = "checked";
				}

				View::formObject($this->getForm($aContentMenu, $aOptionData));
				unset($aContentMenu, $aOptionData);
				break;
					
			case "save":
				$aData = array(
					"idContentMenu"	=> Params::postParams("id", 0),
					"name"			=> Params::postParams("content_name", ""),
					"active"		=> (Params::postParams("content_active",false)!=false?1:0),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "content:menu", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oContentMenu->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oContentMenu->insert($aData);
					LogModelLog::addLog("admin", "content:menu", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;


			case "delete":
				LogModelLog::addLog("admin", "content:menu", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oContentMenu->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oContentMenu);
	}

	public function getForm($aData=array(), $aOptions=array()) {
		$aData = Helper::prepareArray($aData, "idContentMenu,key,name", "");
		$aData = Helper::prepareArray($aData, "active", 0);

		$oForm = new FormHelper(LinkHelper::_("content", "menu"));
		$oForm->addElement("text", 		"content_name", 	$aData["name"],			Translate::get("NAME"),		"",			array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 	"content_active",	$aData["active"],		Translate::get("AKTIV"),	$aOptions,	array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 	"action",			"save");
		$oForm->addElement("hidden", 	"id", 				$aData["idContentMenu"]);
		$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),		"",							"",	"col-md-12");
		return $oForm;
	}
}