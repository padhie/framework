<?php
class ContentControllerMenuitem extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"idContentMenuItem" => Translate::get("ID"),
				"name" 				=> Translate::get("NAME"),
				"title" 			=> Translate::get("TITLE"),
				"menuName" 			=> Translate::get("MENU"),
				"linkType" 			=> Translate::get("TYPE"),
				"ordering"			=> Translate::get("ORDERING"),
				"active" 			=> Translate::get("AKTIV"),
				"btns" 				=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.LinkHelper::_("content", "menuitem").'?action=load&id=%idContentMenuItem%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idContentMenuItem%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oMenuItem = Factory::getModel("MenuItem");
		$oTable->setData($oMenuItem->getAllMenuList());
		unset($oMenuItem);
		View::tableObject( $oTable );

		if (View::formObject() === null) {
			View::formObject($this->getForm());
		}
	}

	public function render() {
		View::navi("content");
		View::title("Content-Menuitem");
		View::render("item");
	}

	public function action($action) {
		$oMenuItem = Factory::getModel("MenuItem");
		switch($action) {
			case "load":
				$aMenuItem = $oMenuItem->getById(intval( Params::getParams("id") ));
				$aOptionData=array();
				if ($aMenuItem["active"]=="1") {
					$aOptionData["checked"] = "checked";
				}
				if (strpos($aMenuItem["settings"], "{") == 0) {
					$aMenuItem["settings"] = json_decode($aMenuItem["settings"], true);
					if (isset($aMenuItem["settings"]["modul"]) && isset($aMenuItem["settings"]["controller"])) {
						$aMenuItem["settings"]["modul_controller"] = $aMenuItem["settings"]["modul"]."_".$aMenuItem["settings"]["controller"];
					}
				}

				View::formObject($this->getForm($aMenuItem, $aOptionData));
				unset($aMenuItem, $aOptionData);
				break;
					
			case "save":
				$_aTmpSplit	= explode("_", Params::postParams("content_controller", "_"));
				$aSettings = array(
					"modul" 		=> $_aTmpSplit[0],
					"controller"	=> $_aTmpSplit[1],
					"extra"			=> Params::postParams("content_extra", "")
				);
				$aData = array(
					"idContentMenu"	=> Params::postParams("content_idContentMenu", 0),
					"name"			=> Params::postParams("content_name", ""),
					"idParent"		=> Params::postParams("content_idParent", ""),
					"key"			=> Params::postParams("content_key", ""),
					"title"			=> Params::postParams("content_title", ""),
					"ordering"		=> Params::postParams("content_ordering", ""),
					"link"			=> Params::postParams("content_link", ""),
					"linkType"		=> Params::postParams("content_linkType", "custom"),
					"settings"		=> json_encode($aSettings),
					"active"		=> (Params::postParams("content_active",false)!=false?1:0),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "content:menuitem", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oMenuItem->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oMenuItem->insert($aData);
					LogModelLog::addLog("admin", "content:menuitem", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;

			case "delete":
				LogModelLog::addLog("admin", "content:menuitem", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oMenuItem->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oMenuItem);
	}

	public function getForm($aData=array(), $aOptions=array()) {
		$aData = Helper::prepareArray($aData, "idContentMenuItem,key,title,link,name", "");
		$aData = Helper::prepareArray($aData, "linkType", "custom");
		$aData = Helper::prepareArray($aData, "idContentMenu,idParent,ordering,active", 0);
		$aData = Helper::prepareArray($aData, "settings", array());
		$aData["settings"] = Helper::prepareArray($aData["settings"], "modul_controller,extra", "");

		// Menüs laden
		$oMenu = Factory::getModel("Menu");
		$aMenu = $oMenu->getAll();
		$aMenu = Helper::createSelectArray($aMenu, "idContentMenu", "name");
		array_unshift($aMenu, array("key"=>"", "value"=>"- Kein Menu -"));
		unset($oMenu);

		// Itemliste für Parent laden
		$oMenuItem = Factory::getModel("MenuItem");
		$aMenuItem = $oMenuItem->getAll();
		$aMenuItem = Helper::createSelectArray($aMenuItem, "idContentMenuItem", "name");
		array_unshift($aMenuItem, array("key"=>0, "value"=>"- Kein Parent -"));
		unset($oMenuItem);

		// LinkTypen
		$aLinkTypes = array(
			array("key"=>"custom", "value"=>"custom"),
			array("key"=>"modul", "value"=>"modul"),
		);

		// Controlleriste
		$aControllerList = array();
		$aModulList = ModulHelper::getModullist();
		foreach ($aModulList AS $sModul) {
			$_aList = ModulHelper::getControllerListOfModul($sModul["name"]);
			foreach ($_aList AS $aItem) {
				array_push($aControllerList, array(
					"key"	=> $sModul["name"]."_".$aItem["name"],
					"value"	=> $aItem["name"],
					"group"	=> $sModul["name"]
				));
			}
		}
		array_unshift($aControllerList, array("key"=>"", "value"=>"- Kein Modul -", "group"=>""));

		$oForm = new FormHelper(LinkHelper::_("content", "menuitem"));
		$oForm->addElement("text", 			"content_name", 				$aData["name"],							Translate::get("NAME"),			"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("select",		"content_idContentMenu", 		$aData["idContentMenu"],				Translate::get("MENU"),			$aMenu,				array("col-md-2", "col-md-9"));
		$oForm->addElement("select",		"content_idParent", 			$aData["idParent"],						Translate::get("PARENT"),		$aMenuItem,			array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 			"content_key", 					$aData["key"],							Translate::get("KEY"),			"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("number",		"content_ordering",				$aData["ordering"],						Translate::get("ORDERING"),		"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("textarea",		"content_title", 				$aData["title"],						Translate::get("TITLE"),		"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 			"content_link", 				$aData["link"],							Translate::get("LINK"),			"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("select", 		"content_linkType",				$aData["linkType"],						Translate::get("TYPE"),			$aLinkTypes,		array("col-md-2", "col-md-9"));
		$oForm->addElement("selectgroup", 	"content_controller",			$aData["settings"]["modul_controller"],	Translate::get("CONTROLLER"),	$aControllerList,	array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 			"content_extra",				$aData["settings"]["extra"],			Translate::get("EXTRA"),		"",					array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 		"content_active",				$aData["active"],						Translate::get("AKTIV"),		$aOptions,			array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 		"action",						"save");
		$oForm->addElement("hidden", 		"id", 							$aData["idContentMenuItem"]);
		$oForm->addElement("submit", 		"", 							Translate::get("SAVE"),				"",								"",					"col-md-12");
		return $oForm;
	}
}