<?php
class ContentControllerPage extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(20);
		$oTable->setUsingKeys(array(
				"idContentPage" => Translate::get("ID"),
				"name" 			=> Translate::get("NAME"),
				"title" 		=> Translate::get("TITLE"),
				"active" 		=> Translate::get("AKTIV"),
				"btns" 			=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.LinkHelper::_("content", "page").'?action=load&id=%idContentPage%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idContentPage%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oPage = Factory::getModel("Page");
		$oTable->setData($oPage->getAll());
		unset($oPage);
		View::tableObject( $oTable );

		if (View::formObject() === null) {
			View::formObject($this->getForm());
		}
	}

	public function render() {
		View::navi("content");
		View::title("Content-Page");
		View::render("page");
	}

	public function action($action) {
		$oPage = Factory::getModel("Page");
		switch($action) {
			case "load":
				$aPage = $oPage->getById(intval( Params::getParams("id") ));
				$aOptionData=array();
				if ($aPage["active"]=="1") {
					$aOptionData["checked"] = "checked";
				}

				View::formObject($this->getForm($aPage, $aOptionData));
				unset($aPage, $aOptionData);
				break;
					
			case "save":
				$aData = array(
					"idContentPage"	=> Params::postParams("id", 0),
					"key"			=> Params::postParams("content_key", ""),
					"title"			=> Params::postParams("content_title", ""),
					"name"			=> Params::postParams("content_name", ""),
					"content"		=> Params::postParams("content_content", ""),
					"active"		=> (Params::postParams("content_active",false)!=false?1:0),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "content:page", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oPage->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oPage->insert($aData);
					LogModelLog::addLog("admin", "content:page", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;


			case "delete":
				LogModelLog::addLog("admin", "content:page", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oPage->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oPage);
	}

	public function getForm($aData=array(), $aOptions=array()) {
		$aData = Helper::prepareArray($aData, "idContentPage,content,key,title,name", "");
		$aData = Helper::prepareArray($aData, "active", 0);

		$oForm = new FormHelper(LinkHelper::_("content", "page"));
		$oForm->addElement("text", 		"content_key", 		$aData["key"],			Translate::get("KEY"),		"",			array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"content_name", 	$aData["name"],			Translate::get("NAME"),		"",			array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"content_title", 	$aData["title"],		Translate::get("TITLE"),	"",			array("col-md-2", "col-md-9"));
		$oForm->addElement("codemirror","content_content", 	$aData["content"],		Translate::get("CONTENT"),	"",			array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 	"content_active",	$aData["active"],		Translate::get("AKTIV"),	$aOptions,	array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 	"action",			"save");
		$oForm->addElement("hidden", 	"id", 				$aData["idContentPage"]);
		$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),		"",							"",	"col-md-12");
		return $oForm;
	}
}