{{View::render("header_admin")}}

<div class="col-md-12">
	{{View::formObject()->render()}}
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
		
	{{assign var="sModalLink" value=LinkHelper::_("content", "menuitem")}}
	{{include file="modalbox.tpl"}}
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#content_linkType").on("change", function() {
			checkType(this);
		});

		checkType($("#content_linkType"));
	});

	function checkType(element) {
		if ($(element).val() == "modul") {
			$("#content_controller").parent().show();
			$("#content_extra").parent().show();
		} else {
			$("#content_controller").parent().hide();
			$("#content_extra").parent().hide();
		}
	}
</script>

{{View::render("footer")}}