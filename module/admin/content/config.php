<?php

class ContentConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"content",
			"name"=>"Content",
			"color"=>"success",	
			"icon"=>"fa-files-o",
			"subnavi" => array(
				array("key"=>"menucontent",		"name"=>"Menu",		"link"=>LinkHelper::controller("content", "menu")),
				array("key"=>"menuitemcontent",	"name"=>"MenuItem",	"link"=>LinkHelper::controller("content", "menuitem")),
				array("key"=>"pagecontent",		"name"=>"Seite",	"link"=>LinkHelper::controller("content", "page")),
			)
		);
	}

	public function getRights() {
		return array(
			"content"			=> Translate::get("MENU_ADMIN_CONTENT"),
			"menucontent"		=> "- ".Translate::get("MENU_ADMIN_CONTENT_MENU"),
			"menuitemcontent"	=> "- ".Translate::get("MENU_ADMIN_CONTENT_MENU_ITEM"),
			"pagecontent"		=> "- ".Translate::get("MENU_ADMIN_CONTENT_PAGE"),
		);
	}

	public function getSettingVars($oSettings) {
		$oContentPage = Factory::getModel("menuitem", "content");
		$aMenu = $oContentPage->getAllMenuList();
		$aMenu = Helper::createSelectArray($aMenu, "idContentMenuItem", "name");
		array_unshift($aMenu, array("key"=>"-", "value"=>"-"));
		return array(
			"basic" => array(
				"title" => Translate::get("CONTENT"),
				"color" => "warning",
				"flag"	=> "alpha"
			),
			"data"	=> array(
				"content_custom404" => array(
					"type" 	=> "select",
					"label"	=> "404 ".Translate::get("PAGE"),
					"data" 	=> $aMenu
				),
			)
		);
	}
}