<?php

class ContentModelMenuitem extends DbExtent {
	protected $_sTable = "content_menu_item";
	protected $_sIdField = "idContentMenuItem";

	public function addJoin() {
		$this->_oQueryBuilder->setSelect("content_menu_item.*");
		$this->_oQueryBuilder->addJoin("LEFT", "content_menu.m", "idContentMenu");
		$this->_oQueryBuilder->addSelect("m.name", "menuName");
	}

	public function getAllMenuList($idParent=0, $sPrefix="") {
		$aReturn = array();

		// Lade Hauptmenüpunkte
		$this->_oQueryBuilder->clear();
		$this->addJoin();
		$this->addWhere("idParent", $idParent);
		$this->addOrder("idContentMenu");
		$this->addOrder("ordering");
		$aItems = parent::getAll();

		// Durchlaufe Resultat
		foreach ($aItems AS $aRow) {
			// Informationen zusammen sammeln
			$aRow["name"] = $sPrefix." ".$aRow["name"];
			array_push($aReturn, $aRow);
			
			// Lade Subnavi
			$aSubItems = $this->getAllMenuList($aRow["idContentMenuItem"], $sPrefix."-");
			if (count($aSubItems) > 0) {
				foreach ($aSubItems AS $aSubItem) {
					array_push($aReturn, $aSubItem);
				}
			}
			unset($aSubItems);
		}
		unset($aItems);


		// Liefer Liste zurück
		return $aReturn;
	}

	public function getAllActivByMenu($idMenu, $idParent=0) {
		// Lade Hauptmenüpunkte
		$this->addWhere("idContentMenu", $idMenu);
		$this->addWhere("active", 1);
		$this->addWhere("idParent", $idParent);
		$this->addOrder("ordering");

		// Durchlaufe Resultat
		$aMenuArray = array();
		foreach ($this->getAll() AS $aRow) {
			// Informationen zusammen sammeln
			$aMenuElement = array(
				"key" 	=> $aRow["key"],
				"name"	=> Translate::get($aRow["name"]),
				"target"	=> $aRow["target"],
			);

			// Linktypen
			$aMenuElement["link"] = $aRow["link"];
			
			// Lade Subnavi
			$aSubMenu = $this->getMenuArray($idMenu, $aRow["idContentMenuItem"]);
			if (count($aSubMenu) > 0) {
				$aMenuElement["subnavi"] = $aSubMenu;
			}

			// Füge Naviinfo in Liste
			array_push($aMenuArray, $aMenuElement);
		}

		// Liefer Liste zurück
		return $aMenuArray;
	}

	public function getDataByLink($sLink) {
		$this->_oQueryBuilder->clear();
		$this->addWhere("active", 1);
		$this->addWhere("link", $sLink);
		return $this->getSingleData();
	}

	public function getArrayBySettings($sSettings) {
		$aJson = json_decode($sSettings, true);
		$aExtras = explode("&", $aJson["extra"]);
		$aNewExtra = array();
		foreach ($aExtras AS $sExtra) {
			$_aTmpSplit = explode("=", $sExtra);
			if (count($_aTmpSplit) == 2) {
				$aNewExtra[$_aTmpSplit[0]] = $_aTmpSplit[1];
			}
		}
		$aJson["extra"] = $aNewExtra;
		return $aJson;
	}

	public function link404() {
		$oSettings = Factory::getModel("settings", "settings");
		$idContentMenuitem = $oSettings->getSetting("content_custom404");

		if ($idContentMenuitem === false) {
			return false;
		}

		$this->addWhere($this->_sIdField, $idContentMenuitem);
		$aItem = $this->getSingleData();
		
		if ($aItem === false) {
			return false;
		}

		header("Location: ".$aItem["link"]);
		return true;
	}
}