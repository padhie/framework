<?php

class ListsConfig extends ModulConfig {
	public function getMenuConfig() {
		return 
			array("key"=>"lists", 		"name"=>Translate::get("MENU_ADMIN_LISTS"), 		"color"=>"violet",	"icon"=>"fa-list",			"betaflag"=>true,	"subnavi"=>array(
				array("key"=>"wordlists", 	"name"=>Translate::get("MENU_ADMIN_WORDLIST"),		"link"=>LinkHelper::controller("lists", "word"),	"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"linklists",	"name"=>Translate::get("MENU_ADMIN_LINKLIST"), 		"link"=>LinkHelper::controller("lists", "link"),	"color"=>"info",	"icon"=>"fa-circle"),
			));
	}

	public function getRights() {
		return array(
			"lists"			=> Translate::get("MENU_ADMIN_LISTS"),
			"wordlists"		=> "- ".Translate::get("MENU_ADMIN_WORDLIST"),
			"linklists"		=> "- ".Translate::get("MENU_ADMIN_LINKLIST"),
		);
	}
}