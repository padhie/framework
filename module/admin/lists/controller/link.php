<?php
class ListsControllerLink extends eAdminController implements iController {	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idList" 	=> Translate::get("ID"),
				"wert" 		=> Translate::get("VALUE"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.Config::getServerHost().'/admin/lists/link?action=load&id=%idList%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idList%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oList = Factory::getModel("lists");
		$oList->addOrder("idList", "DESC");
		$oList->addWhere("type", "blackphrases");
		$oList->addWhere("subtype", "link");
		$oTable->setData($oList->getAll());
		View::tableObject( $oTable );
		
		
		View::hosts( $oList->getAll() );
		unset($oList);
		
		
		if (View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/lists/link", "post");
			$oForm->addElement("text", 		"list_wert", 	"",						Translate::get("VALUE"),	array(),	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"action",		"new");
			$oForm->addElement("submit", 	"", 			Translate::get("SAVE"),	"",							array(),	"col-md-12");
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("lists");
		View::title("lists");
		View::render("lists_link");
	}
	
	public function action($action) {
		$oList = Factory::getModel("lists");
		switch($action) {
			case "load":
				$aHost =  $oList->getById(intval( Params::getParams("id") ));
				
				$oForm = new FormHelper(Config::getServerHost()."/admin/lists/link", "post");
				$oForm->addElement("label",		"list_id",		$aHost["idList"], 	Translate::get("IDHOST"),	array(),	array("col-md-2", "col-md-9"));
				$oForm->addElement("text", 		"list_wert", 	$aHost["wert"],		Translate::get("VALUE"),	array(),	array("col-md-2", "col-md-9"));
				$oForm->addElement("hidden", 	"id",			$aHost["idList"]);
				$oForm->addElement("hidden", 	"action",		"save");
				$oForm->addElement("submit", 	"", 			Translate::get("SAVE"),	"",							array(),	"col-md-12");
				View::formObject($oForm);
				unset($oForm);
				
				View::host( $aHost );
				unset($aHost);
				break;
					
			case "new":
				$aData = array(
					"wert"		=> Params::postParams("list_wert", ""),
					"type"		=> "blackphrases",
					"subtype"	=> "link",
				);
				$oList->insert($aData);
				LogModelLog::addLog("admin", "lists:link", Params::cookieParams("username"), "Insert id #".$oList->getLastInsertId());
				break;
				
			case "save":
				$aData = array(
					"wert"		=> Params::postParams("list_wert", ""),
					"type"		=> "blackphrases",
					"subtype"	=> "link",
				);
				if (Params::postParams("id") !== false) {
					$oList->update($aData, intval( Params::postParams("id") ));
					LogModelLog::addLog("admin", "lists:link", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oList->insert($aData);
					LogModelLog::addLog("admin", "lists:link", Params::cookieParams("username"), "Insert id #".$oList->getLastInsertId());
				}
				break;
					
			case "delete":
				$oList->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "lists:link", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oList);
	}
}