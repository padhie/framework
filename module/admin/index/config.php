<?php

class IndexConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"home", 		
			"name"=>Translate::get("MENU_ADMIN_HOME"), 			
			"link"=>LinkHelper::_(),					
			"color"=>"danger",	
			"icon"=>"fa-dashboard"
		);
	}

	public function getRights() {
		return array(
			"home"		=> Translate::get("MENU_ADMIN_HOME"),
		);
	}
}