<?php
class IndexControllerIndex extends eAdminController implements iController {
	public function run() {
		$aPanels = array();

		$sBaseLink = Config::getDocumentRoot().Config::$sModulFolder."/admin";
		foreach (Config::$aModules AS $sModul) {

			if (file_exists($sBaseLink."/".$sModul."/config.php")) {
				require_once $sBaseLink."/".$sModul."/config.php";
				$sModulConfig = ucfirst($sModul)."Config";
				$oModulConfig = new $sModulConfig();

				$aDashboardConfig = $oModulConfig->getDefaultDashboardConfig();
				if ($aDashboardConfig != false) {
					array_push($aPanels, $aDashboardConfig);
				}
			}
		}
			
		View::adminPanel($aPanels);
		unset($aPanels);
	}

	public function render() {
		View::navi("home");
		View::title("Home");
		View::render("index");
	}
}