<?php
class RangsControllerRangs extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("rangs");
		
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(9999999999999);
		$oTable->setUsingKeys(array(
				"idRang" 			=> Translate::get("ID"),
				"group" 			=> Translate::get("GROUP"),
				"index" 			=> Translate::get("INDEX"),
				"name" 				=> Translate::get("RANG"),
				"maxSongrequest"	=> Translate::get("MAX_SONGREQUEST"),
				"minPost"			=> Translate::get("MIN_POST"),
				"minTime"			=> Translate::get("MIN_TIME"),
				"btns" 				=> ""
		));
		$oTable->addExtra("btns", 
			'<a href="'.Config::getServerHost().'/admin/rangs?action=load&id=%idRang%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idRang%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oRang = Factory::getModel("rangs", "rangs");
		$oRang->addOrder("group");
		$oRang->addOrder("index");
		//$oRang->setOrder("idRang");
		$oTable->setData( $oRang->getAll() );
		View::tableObject( $oTable );
		
		if (View::formObject() === null) {
			View::formObject($this->getForm(array(
					"idRang" => "",
					"group" => "",
					"index" => "",
					"name" => "",
					"maxSongrequest" => "",
					"minPost" => "",
					"minTime" => ""
			)));
		}
	}

	public function render() {
		View::navi("rangs");
		View::title("rangs");
		View::render("rangs");
	}
	
	public function action($action) {
		$oRangs = Factory::getModel("rangs", "rangs");
		switch($action) {
			case "load":
				$aRang =  $oRangs->getById(intval( Params::getParams("id") ));
				if (!empty($aRang)) {
					$oForm = $this->getForm($aRang);
					View::formObject( $oForm );
					unset($oForm);
				}
				unset($aRang);
				break;
					
			case "save":
				$aData = array(
					"idRang"			=> trim(Params::postParams("rang_id",  Params::postParams("id"))),
					"group"				=> trim(Params::postParams("rang_group",  Params::postParams("id"))),
					"index"				=> trim(Params::postParams("rang_index", 1)),	
					"name"				=> trim(Params::postParams("rang_name", "")),	
					"maxSongrequest"	=> intval(trim(Params::postParams("rang_maxSongrequest", 0))),	
					"minPost"			=> intval(trim(Params::postParams("rang_minPost", 0))),	
					"minTime"			=> intval(trim(Params::postParams("rang_minTime", 0))),	
				);
				if (Params::postParams("id") !== false) {
					$oRangs->update($aData, intval( Params::postParams("id") ));
					LogModelLog::addLog("admin", "rangs", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oRangs->insert($aData);
					LogModelLog::addLog("admin", "rangs", Params::cookieParams("username"), "Insert id #".$oRangs->getLastInsertId());
				}
				break;
					
			case "delete":
				$oRangs->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "rangs", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oRangs);
	}
	
	public function getForm($aData=array()) {
		$oForm = new FormHelper(Config::getServerHost()."/admin/rangs?action=save", "post", "_self", "");
		
		$oForm->addElement("text", 		"rang_group",			$aData["group"], 			Translate::get("GROUP"), 			array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("number",	"rang_index",			$aData["index"], 			Translate::get("INDEX"), 			array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"rang_name",			$aData["name"], 			Translate::get("RANG"), 			array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("number", 	"rang_maxSongrequest", 	$aData["maxSongrequest"], 	Translate::get("MAX_SONGREQUEST"), 	array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("number", 	"rang_minPost", 		$aData["minPost"], 			Translate::get("MIN_POST"), 		array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("number", 	"rang_minTime", 		$aData["minTime"], 			Translate::get("MIN_TIME"), 		array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 					Translate::get("SAVE"),		"", 								array(), "col-md-12");
		$oForm->addElement("hidden", 	"id", 					$aData["idRang"]);
		
		return $oForm;
	}
}