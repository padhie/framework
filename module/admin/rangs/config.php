<?php

class RangsConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"rangs", 		
			"name"=>Translate::get("MENU_ADMIN_RANGS"), 		
			"link"=>LinkHelper::modul("rangs"),			
			"color"=>"cyan",	
			"icon"=>"fa-trophy"
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "rangs")) {
			$oRang = Factory::getModel("rangs", "rangs");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_RANGS"),
				"msg"	=> count($oRang->getAll())." ".Translate::get("RANGS"),
				"link" 	=> LinkHelper::_("rangs"),
				"color"	=> "cyan"
			);
		}
	}

	public function getRights() {
		return array(
			"rangs"			=> Translate::get("MENU_ADMIN_RANGS"),
		);
	}

	public function getSettingVars($oSettings) {
		$oRang = Factory::getModel("Rangs", "Rangs");
		$aRangData = array();
		if ($oSettings->getSetting("rang_aktiv") == "1") {
			$aRangData["checked"] = true;
		}
		$aRangMinRangs = json_decode($oSettings->getSetting("rang_startRang"));

		return array(
			"basic" => array(
				"title" => Translate::get("RANGSYSTEM"),
				"color" => "info",
				"flag"	=> "beta"
			),
			"data"	=> array(
				// "rang_aktiv" => array(
				// 	"type" 	=> "switch",
				// 	"label"	=> Translate::get("AKTIVE"),
				// 	"data" 	=> $aRangData
				// ),
				"rang_startRang" => array(
					"type" 	=> "multiple",
					"label"	=> Translate::get("START_RANG"),
					"data" 	=> $oRang->getFormArray()
				),
			)
		);
	}
}