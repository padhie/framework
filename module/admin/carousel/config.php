<?php

class CarouselConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"carousel", 	
			"name"=>Translate::get("MENU_ADMIN_CAROUSEL"), 		
			"link"=>LinkHelper::modul("carousel"),		
			"color"=>"warning",	
			"icon"=>"fa-repeat"
		);
	}

	public function getRights() {
		return array(
			"carousel"	=> Translate::get("MENU_ADMIN_CAROUSEL"),
		);
	}
}