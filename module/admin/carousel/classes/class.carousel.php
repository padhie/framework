<?php

class CarouselModelCarousel extends DbExtent {
	protected $_sTable = "carousel";
	protected $_sIdField = "idCarousel";
	
	public function getByGroup($sGroup="all") {
		if ($sGroup != "all") {
			$this->_oQueryBuilder->addWhere("group", $sGroup);
		}
		
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			return $this->getResult();
		}
		return false;
	}
}