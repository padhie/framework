<?php
class CarouselControllerCarousel extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idCarousel"	=> Translate::get("ID"),
				"imgPreview"	=> "",
				"name" 			=> Translate::get("NAME"),
				"group" 		=> Translate::get("GROUP"),
				"btns" 			=> ""
		));
		$oTable->addExtra("imgPreview", 
			'<img src="%image%" height="50px" width="50px" data-toggle="tooltip" data-placement="right" title="<img src=\'%image%\' style=\'max-height:555px;\' />"/>'
		);
		$oTable->addExtra("btns",
			'<a href="'.Config::getServerHost().'/admin/carousel?action=load&id=%idCarousel%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idCarousel%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
				
		
		$oCarousel = Factory::getModel("Carousel");
		$oTable->setData($oCarousel->getAll());
		View::tableObject( $oTable );
		unset($oCarousel);
		
		if( View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/carousel?action=save");
			$oForm->addElement("text", 		"carousel_name", 	"",						Translate::get("NAME"),		"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"carousel_group", 	"",						Translate::get("GROUP"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("file", 		"carousel_image", 	"",						Translate::get("IMAGE"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"id", 				"");
			$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),	"",							"",	"col-md-12");
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("carousel");
		View::title("carousel");
		View::render("carousel");
	}
	
	public function action($action) {
		$oCarousel = Factory::getModel("Carousel");
		switch($action) {
			case "load":
				$aCarousel =  $oCarousel->getById(intval( Params::getParams("id") ));

				$oForm = new FormHelper(Config::getServerHost()."/admin/carousel?action=save");
				$oForm->addElement("text", 		"carousel_name", 	$aCarousel["name"],			Translate::get("NAME"),		"",	array("col-md-2", "col-md-9"));
				$oForm->addElement("text", 		"carousel_group", 	$aCarousel["group"],		Translate::get("GROUP"),	"",	array("col-md-2", "col-md-9"));
				$oForm->addElement("file", 		"carousel_image", 	$aCarousel["image"],		Translate::get("IMAGE"),	"",	array("col-md-2", "col-md-9"));
				$oForm->addElement("hidden", 	"id", 				$aCarousel["idCarousel"]);	
				$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),			"",							"",	"col-md-12");
				View::formObject($oForm);
				unset($oForm);
				
				View::carousel( $aCarousel );
				unset($aCarousels);
				break;
					
			case "save":
				$aData = array(
					"name"	=> Params::postParams("carousel_name", ""),
					"group"	=> Params::postParams("carousel_group", ""),
					"image"	=> Params::postParams("carousel_image", "")
				);
				
				if (Params::fileParams("carousel_image_file")) {
					$aFile = Params::fileParams("carousel_image_file");
					$sFilename = "";
					if (Params::postParams("carousel_name")) {
						$_sTmpExtend = substr($aFile["name"], strrpos($aFile["name"], ".")+1);
						$_sTmpName = str_replace(" ", "_", Params::postParams("carousel_name"));
						$sFilename = Config::$sCustomBase.Config::$sCustomFiles."/carousel/".$_sTmpName.".".$_sTmpExtend;
						unset($_sTmpExtend, $_sTmpName);
					}
					$oFile = new File();
					if ($oFile->uploadFile($aFile, $sFilename, true)) {
						$aData["image"] = $oFile->getDir()."/".$oFile->getFile();
					}
					unset($oFile);
				}
				
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "carousel", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
 					$oCarousel->update($aData, intval( Params::postParams("id") ));
				} else {
 					$iLastId = $oCarousel->insert($aData);
					LogModelLog::addLog("admin", "carousel", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;
					
			case "delete":
				$aCarousels =  $oCarousel->getById(intval( Params::getParams("id") ));

				$oFile = new File();
				$oFile->setFile($aCarousels["image"]);
				$oFile->deleteFile();	
				unset($oFile);				
				
				LogModelLog::addLog("admin", "carousel", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oCarousel->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oCarousel);
	}
}