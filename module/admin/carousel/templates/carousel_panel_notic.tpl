<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("NOTE")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("LINKS")}}: <br />
		{{Config::getServerHost()}}/custom/api?action=carousel <br />
		<br />
		{{Translate::say("PARAMETERS")}}:<br />
		- {{Translate::say("CAROUSEL_PARAMETERS_G")}}<br />
		- {{Translate::say("CAROUSEL_PARAMETERS_T")}}<br />
		- {{Translate::say("CAROUSEL_PARAMETERS_BG")}}<br /> 
	</div>
</div>