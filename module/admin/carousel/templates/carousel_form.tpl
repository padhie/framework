{{if(View::carousel()!=""}}
	{{php}} $aCarousel = View::carousel(); {{/php}}
{{else}}
	{{php}}
		$aCarousel = array(
			"idCarousel" => "",
			"name" => "",
			"image" => "",
			"group" => ""
		);
	{{/php}}
{{/if}}

<form method="post" action="{{Config::getServerHost()}}/admin/carousel?action=save" enctype="multipart/form-data">

	<div class="form-group row">
		<label for="carousel_name" class="control-label col-md-2">{{Translate::say("NAME")}}</label>
		<input type="text" class="form-control-static col-md-9" name="carousel_name" id="carousel_name" value="{{$aCarousel.name}}">
	</div>
	<div class="form-group row">
		<label for="carousel_group" class="control-label col-md-2">{{Translate::say("GROUP")}}</label>
		<input type="text" class="form-control-static col-md-9" name="carousel_group" id="carousel_group" value="{{$aCarousel.group}}">
	</div>
	<div class="form-group row">
		<label for="carousel_image" class="control-label col-md-2">{{Translate::say("IMAGE")}}</label>
		<input type="text" class="form-control-static col-md-6" name="carousel_image" id="carousel_image" value="{{$aCarousel.image}}">
		<input name="carousel_image_file" class="form-control-static col-md-3" type="file"> 
	</div>
	
	<div class="row">
		<input type="submit" class="btn btn-success col-md-11" value="{{Translate::say("SAVE")}}" />
		<input type="hidden" name="id" value="{{$aCarousel.idCarousel}}" />
	</div>
</form>	