{{View::render("header_admin")}}

<div class="col-md-12">
	<div class="col-md-9">
		{{View::formObject()->render()}}

	</div>
	
	<div class="col-md-3">
		{{View::render("carousel_panel_notic")}}
	</div>
	
	<br /><hr >
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
		
	{{assign var="sModalLink" value=LinkHelper::_("carousel")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}