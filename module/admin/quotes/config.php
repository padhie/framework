<?php

class QuotesConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"quotes",
			"name"=>Translate::get("MENU_ADMIN_QUOTES"),
			"link"=>LinkHelper::modul("quotes"),
			"color"=>"warning",	
			"icon"=>"fa-comment"
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "quotes")) {
			$oQuotes = Factory::getModel("Quote", "Quotes");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_QUOTES"),
				"msg"	=> count($oQuotes->getAll())." ".Translate::get("QUOTES"),
				"link" 	=> LinkHelper::_("quotes"),
				"color"	=> "warning"
			);
		}
	}

	public function getRights() {
		return array(
			"quotes"			=> Translate::get("MENU_ADMIN_QUOTES"),
		);
	}

	public function getSettingVars($oSettings) {
		$oRang = Factory::getModel("Rangs", "Rangs");
		$aQuoteData = array();
		if ($oSettings->getSetting("quote_aktiv") == "1") {
			$aQuoteData["checked"] = true;
		}

		return array(
			"basic" => array(
				"title" => Translate::get("QUOTE"),
				"color" => "info",
				"flag"	=> ""
			),
			"data"	=> array(
				"quote_aktiv" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("AKTIVE"),
					"data" 	=> $aQuoteData
				),
				"quote_postRang" => array(
					"type" 	=> "select",
					"label"	=> Translate::get("POST_RANG"),
					"data" 	=> $oRang->getFormArray()
				),
			)
		);
	}
}