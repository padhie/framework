<?php
class QuotesControllerQuotes extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("quotes");
		parent::loadModulClasses("rangs");
		
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idQuote" 	=> Translate::get("ID"),
				"index" 	=> Translate::get("INDEX"),
				"text" 		=> Translate::get("TEXT"),
				"user" 		=> Translate::get("USER"),
				"minRang"	=> Translate::get("MIN_RANG"),
				"isPublic"	=> Translate::get("ISPUBLIC"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.Config::getServerHost().'/admin/quotes?action=edit&id=%idQuote%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<a href="'.Config::getServerHost().'/admin/quotes?action=release&id=%idQuote%" class="btn btn-success">'.Translate::get("RELEASE").'</a> '.
				'<a href="'.Config::getServerHost().'/admin/quotes?action=unrelease&id=%idQuote%" class="btn btn-warning">'.Translate::get("LOCK").'</a> '.
				'<button onclick="openModal(%idQuote%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oQuote = Factory::getModel("Quote", "Quotes");
		$oTable->setData($oQuote->getAllFuelData());
		
		View::tableObject( $oTable );
		
		
		// Formular für neue Quotes
		if (View::formObject() === null) {
			$oRang = Factory::getModel("Rangs", "Rangs");
			$oForm = new FormHelper(Config::getServerHost()."/admin/quotes?action=new", "post", "_self", "col-md-12");
			$oForm->addElement("label",		"qoute_id", 		"#", 								Translate::get("IDQUERY"),		array(), 				array("col-md-1", "col-md-11"));
			$oForm->addElement("number",	"qoute_index", 		0, 									Translate::get("INDEX"), 		array(), 				array("col-md-1", "col-md-11"));
			$oForm->addElement("text", 		"qoute_user", 		Params::cookieParams("username"), 	Translate::get("USER"), 		array(), 				array("col-md-1", "col-md-11"));
			$oForm->addElement("multiple",	"qoute_minRang",	$oRang->getMinRang(),				Translate::get("MIN_RANG"), 	$oRang->getFormArray(), array("col-md-1", "col-md-11"));
			$oForm->addElement("textarea", 	"qoute_text", 		"", 								Translate::get("TEXT"), 		array(), 				array("col-md-1", "col-md-11"));
			$oForm->addElement("switch", 	"qoute_isPublic",	"", 								Translate::get("ISPUBLIC"),		array(), 				array("col-md-1", "col-md-11"));
			$oForm->addElement("submit",	"", 				Translate::get("SAVE"),				"", 							array(), 				"col-md-12");
			View::formObject($oForm);
			unset($oForm, $oRang);
		} 
	}

	public function render() {
		View::navi("quotes");
		View::title("Quotes");
		View::render("quotes");
	}

	public function action($action) {
		$oQuote = Factory::getModel("Quote", "Quotes");
		switch (Params::getParams("action")) {
			case "edit":
				$oRang = Factory::getModel("Rangs", "Rangs");
				$aData = $oQuote->getById(intval(Params::getParams("id")));
				

				$aMinRangs = array();
				if ($aData !== false) {
					$aQuoteRang = $oQuote->getRangsOfQuote(intval($aData["idQuote"]));
					if ($aQuoteRang !== false) {
						foreach ($aQuoteRang AS $aRow) {
							array_push($aMinRangs, intval($aRow["idRang"]));
						}
					}
				}

				$aOptionData=array();
				if ($aData["isPublic"]=="1") {
					$aOptionData["checked"] = "checked";
				}
				$oForm = new FormHelper(Config::getServerHost()."/admin/quotes?action=save", "post", "_self", "col-md-12");
				$oForm->addElement("label",		"qoute_id", 		$aData["idQuote"], 			Translate::get("IDQUERY"),		array(), 				array("col-md-1", "col-md-11"));
				$oForm->addElement("number",	"qoute_index", 		$aData["index"], 			Translate::get("INDEX"), 		array(), 				array("col-md-1", "col-md-11"));
				$oForm->addElement("text", 		"qoute_user", 		$aData["user"], 			Translate::get("USER"), 		array(), 				array("col-md-1", "col-md-11"));
				$oForm->addElement("multiple",	"qoute_minRang",	$aMinRangs, 				Translate::get("MIN_RANG"), 	$oRang->getFormArray(), array("col-md-1", "col-md-11"));
				$oForm->addElement("textarea", 	"qoute_text", 		$aData["text"], 			Translate::get("TEXT"), 		array(), 				array("col-md-1", "col-md-11"));
				$oForm->addElement("switch", 	"qoute_isPublic",	"", 						Translate::get("ISPUBLIC"),		$aOptionData, 			array("col-md-1", "col-md-11"));
				$oForm->addElement("submit",	"", 				Translate::get("SAVE"),	"", 							array(), 				"col-md-12");
				$oForm->addElement("hidden",	"id", 				$aData["idQuote"]);
				View::formObject($oForm);
				unset($oForm, $oRang);
				break;
				
			case "new":
			case "save":
				$aData = array(
					"index" 	=> Params::postParams("qoute_index"),
					"user" 		=> Params::postParams("qoute_user"),
					"text" 		=> Params::postParams("qoute_text"),
					"isPublic" 	=> (Params::postParams("qoute_isPublic",false)!==false)?"1":"0"
				);
				if (Params::postParams("id", false) !== false) {
					$oQuote->update($aData, intval(Params::postParams("id")));
					LogModelLog::addLog("admin", "quotes", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oQuote->insert($aData);
					LogModelLog::addLog("admin", "quotes", Params::cookieParams("username"), "Insert id #".$oQuote->getLastInsertId());
				}
				
				$oQuote->changeQuoteToRang(
					intval(Params::postParams("id")),
					Params::postParams("qoute_minRang", array())
				);
				break;
				
			case "unrelease":
				$aData = array( "isPublic" => 0 );
				$oQuote->update($aData, intval(Params::getParams("id")));
				LogModelLog::addLog("admin", "quotes", Params::cookieParams("username"), "Unrelease id #".Params::getParams("id"));
				unset($aData);
				break;
				
			case "release":
				$aData = array( "isPublic" => 1 );
				$oQuote->update($aData, intval(Params::getParams("id")));
				LogModelLog::addLog("admin", "quotes", Params::cookieParams("username"), "Release id #".Params::getParams("id"));
				unset($aData);
				break;
					
			case "delete":
				$oQuote->delete(intval(Params::getParams("id")));
				LogModelLog::addLog("admin", "quotes", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oQuote);
	}
}