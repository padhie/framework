{{View::render("header_admin")}}

{{View::formObject()->render()}}

<div class="col-md-12">
	{{View::tableObject()->render()}}
	
	{{assign var="sModalLink" value=LinkHelper::_("quotes")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}