<?php
class GamewispControllerGamewisp extends eAdminController implements iController {
	public function run() {
		$oGameWisp = new GamewispApi();
		View::cliendId( $oGameWisp->getCliendId() );
		View::redirektUri( $oGameWisp->getRedirektUri() );
		View::authUri( $oGameWisp->getAutorisationUrl() );
		
	}
	
	public function render() {
		View::navi("gamewisp");
		View::title("Gamewisp");
		
		parent::render();
	}
}