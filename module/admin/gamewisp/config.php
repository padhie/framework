<?php

class GamewispConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"gamewisp", 	
			"name"=>Translate::get("MENU_ADMIN_GAMEWISP"), 		
			"link"=>LinkHelper::modul("gamewisp"),		
			"color"=>"cyan",	
			"icon"=>"fa-money"
		);
	}

	public function getRights() {
		return array(
			"gamewisp"		=> Translate::get("MENU_ADMIN_GAMEWISP"),
		);
	}
}