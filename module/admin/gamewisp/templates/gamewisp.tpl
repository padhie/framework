{{View::render("header_admin")}}


<div class="col-md-4">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">{{Translate::say("APPLICATION_DATA")}}</h3>
		</div>
		<div class="panel-body">
			<p>Client_ID: {{View::cliendId()}}</p>
			<p>Secret: xxx</p>
			<p>Endpoint: {{View::redirektUri()}}</p>
		</div>
	</div>
</div>


<div class="col-md-4">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3 class="panel-title">{{Translate::say("LINKS")}}</h3>
		</div>
		<div class="panel-body">
			<p>
				<a href="{{View::authUri()}}" target="_blank">{{Translate::say("GAMEWISP_AUTORISATION")}}</a><br />
				<small>{{Translate::say("GAMEWISP_AUTORISATION_TEXT")}}</small>
			</p>
			
			<p>
				<strong>{{Translate::say("GAMEWISP_APIPAGE")}}</strong><br />
				<small>{{Translate::say("GAMEWISP_APIPAGE_TEXT")}}</small><br />
				<code>{{Config::getServerHost()}}/custom/api/?action=gamewispSub&code={CODE}</code>
			</p>
			
		</div>
	</div>
</div>


{{View::render("footer")}}