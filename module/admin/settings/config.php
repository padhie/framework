<?php

class SettingsConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"settings",
			"name"=>Translate::get("MENU_ADMIN_SETTINGS"),
			"link"=>LinkHelper::modul("settings"),
			"color"=>"warning",
			"icon"=>"fa-gear"
		);
	}

	public function getRights() {
		return array(
			"settings"	=> Translate::get("MENU_ADMIN_SETTINGS"),
		);
	}

	public function getSettingVars($oSettings) {
		$aStyles = array();
		foreach (explode(",",$oSettings->getSetting("style_available")) AS $sStyle) {
			array_push($aStyles, array("key"=>$sStyle,"value"=>ucfirst(strtolower($sStyle))));
		}
		
		return array(
			"basic" => array(
				"title" => Translate::get("STYLE"),
				"color" => "warning",
				"flag"	=> ""
			),
			"data"	=> array(
				// "style_default" => array(
				// 	"type" 	=> "select",
				// 	"label"	=> Translate::get("DEFAULT_STYLE"),
				// 	"data" 	=> $aStyles
				// ),
				"style_admin_default" => array(
					"type" 	=> "select",
					"label"	=> Translate::get("DEFAULT_STYLE_ADMIN"),
					"data" 	=> $aStyles
				),
			)
		);
	}
}