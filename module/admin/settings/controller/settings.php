<?php
class SettingsControllerSettings extends eAdminController implements iController {
	public function start() {
		parent::start();
		parent::loadModulClasses("rangs");
	}
	
	public function run() {
		$oSettings = Factory::getModel("Settings");
		$aForms = array();

		$aModules = Config::$aModules;
		foreach ($aModules AS $sModul) {
			$sConfigName = ModulHelper::getConfigClassname($sModul, LinkHelper::$sSubfolder);
			$oConfigObj = new $sConfigName();
			$aVars = $oConfigObj->getSettingVars($oSettings);

			if (is_array($aVars)) {
				$oForm = new FormHelper(LinkHelper::modul("settings"));
				foreach ($aVars["data"] AS $sKey => $aDatas) {
					$sName = $sKey;
					if (isset($aDatas["type"])) {
						$sType = $aDatas["type"];
					} else {
						$sType = "text";
					}
					if (isset($aDatas["data"])) {
						$aFieldData = $aDatas["data"];
					} else {
						$aFieldData = array();
					}
					if (isset($aDatas["label"])) {
						$aLabel = $aDatas["label"];
					} else {
						$aLabel = "";
					}

					$mValue = $oSettings->getSetting($sName);
					if ($sType == "multiple") {
						$mValue = json_decode($mValue);
					}

					$oForm->addElement($sType,	$sName,	$mValue,	$aLabel,	$aFieldData,	array("col-md-6","col-md-5"));

				}
				$oForm->addElement("hidden", 	"action", 		$sModul);
				$oForm->addElement("submit", 	"", 			Translate::get("SAVE"),	"",	"",	"col-md-12");
				array_push($aForms, array(
					"basic" => $aVars["basic"],
					"form"	=> $oForm
				));
				unset($oForm);
			}
			View::formObjects( $aForms );
		}
		View::aForms($aForms);
	}
	
	public function render() {
		View::navi("settings");
		View::title("Settings");
		
		parent::render();
	}

	public function action($sAktion) {
		// Für dynamische Modulsettings
		$oSettings = Factory::getModel("Settings");
		$sConfigName = ModulHelper::getConfigClassname($sAktion, LinkHelper::$sSubfolder);
		$oConfigObj = new $sConfigName();
		$aVars = $oConfigObj->getSettingVars($oSettings);

		foreach ($aVars["data"] AS $sField => $aInfo) {
			switch ($aInfo["type"]) {
				case 'number':
					$oSettings->saveSetting($sField, intval(Params::postParams($sField, 0)));
					break;
				
				case 'switch':
					$oSettings->saveSetting($sField, (Params::postParams($sField, false)!==false)?1:0);
					break;

				case "multiple":
					$aData = json_encode(Params::postParams($sField, array()));
					$oSettings->saveSetting($sField, $aData);
					break;

				default:
					$oSettings->saveSetting($sField, Params::postParams($sField, ""));
					break;
			}			
		}
	}
}