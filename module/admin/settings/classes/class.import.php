<?php

class SettingsModelImport extends DbExtent {
	protected $_sTable = "";
	
	
	public function importByFile($sFile) {
		$_sTmpContent = "";

		// Zeilenweise durchgehen
		foreach (file($sFile) AS $sLine) {

			// Wenn kommentar dann überspringen
			if (substr($sLine, 0, 2) == '--' || $sLine == '') {
				continue;
			}
			
			// Zeile zwischenspeichern
			$_sTmpContent .= $sLine;

			// Query ausführen
			if (substr(trim($sLine), -1, 1) == ';') {
				$this->setQuery( $_sTmpContent );
				if (!$this->execute()) {
					// Wenn Fehler sofort abbrechen!
					return false;
				}

				// Querystring zurücksetzen
				$_sTmpContent = '';
			}
		}
		
		return true;
	}
}