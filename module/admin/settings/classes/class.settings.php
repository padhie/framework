<?php

class SettingsModelSettings extends DbExtent {
	protected $_sTable = "settings";
	protected $_sIdField = "name";
	
	public function saveSetting($sName, $mVal) {
		if (!Validate::isStringNotEmpty($sName)) {
			return false;
		}
		
		if (!Validate::isStringNotEmpty($mVal) && !Validate::isValidInt($mVal)) {
			return false;
		}
		
		
		$this->_oQueryBuilder->addWhere($this->_sIdField, $sName);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );
		if ($this->execute()) {
			
			$this->_oQueryBuilder->setData(array(
				"name" 	=> $sName,
				"value" => "".$mVal
			));
			$this->setQuery( $this->_oQueryBuilder->getInsertStatement() );
			if( $this->execute()) {
				LogModelLog::addLog("admin", "settings", Params::cookieParams("username"), "Update ".$sName);
				return true;
			}

		}
		return false;
	}
	
	public function getSetting($sName) {
		$this->_oQueryBuilder->addWhere($this->_sIdField, $sName);
		$aResult = $this->getAll();
		
		if (count($aResult) > 0) {
			return $aResult[0]["value"];
		}
		return false;
	}
	
	public function getChecked($sNane) {
		$aData = array();
		
		if ($this->getSetting($sNane) == "1") {
			$aData["checked"] = true;
		}
		
		return $aData;
	}
}