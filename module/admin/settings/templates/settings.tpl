{{View::render("header_admin")}}

<style>
	.panel-default {
		border-color: #fff;
	}
	.panel .panel-body.padding-xl {
		padding: 0;
	}
	.row {
		-moz-column-width: 33em;
		-webkit-column-width: 33em;
		-moz-column-gap: .5em;
		-webkit-column-gap: .5em;
	}
	.panel {
		display: inline-block;
		margin:  .5em;
		padding:  0; 
		width:98%;
	}
</style>


<div class="row">
	{{foreach from=$aForms item=aFormData}}
		<div class="panel panel-{{if $aFormData.basic.color != ""}}{{$aFormData.basic.color}}{{else}}default{{/if}}">
			<div class="panel-heading">
				<h3 class="panel-title">
					{{if $aFormData.basic.title != ""}}{{$aFormData.basic.title}}{{/if}}
					{{if isset($aFormData.basic.flag)}}
						{{if $aFormData.basic.flag == "alpha"}} 
							{{View::render("alphaflag", false)}}
						{{elseif $aFormData.basic.flag == "beta"}}
							{{View::render("betaflag", false)}}
						{{/if}}
					{{/if}}
				</h3>
			</div>
			<div class="panel-body">
				{{View::formObject($aFormData.form)}}
				{{$aFormData.form->render()}}
			</div>
		</div>
	{{/foreach}}
</div>

{{View::render("footer")}}