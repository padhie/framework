<?php
class AngluarControllerAngluar extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("admins", "admin");
		parent::loadModulClasses("rangs");
		parent::loadModulClasses("user");
		parent::loadModulClasses("commands");
		parent::loadModulClasses("quotes");
		parent::loadModulClasses("songrequest");
		parent::loadModulClasses("giveaway");
		parent::loadModulClasses("hosting", "admin");
		
		parent::start();
	}
	
	public function run() {
		$aPanels = array();
		if (in_array("commands", View::adminRights())){
			$oCommands = Factory::getModel("Commands", "Commands");
			array_push($aPanels, array(
					"name"	=> Translate::get("MENU_ADMIN_COMMANDS"),
					"msg"	=> count($oCommands->getAll())." ".Translate::get("COMMANDS"),
					"link" 	=> "/admin/commands",
					"color"	=> "orange"
			));
			unset($oCommands);
		}
		
		if (in_array("quotes", View::adminRights())){
			$oQuotes = Factory::getModel("Quote", "Quotes");
			array_push($aPanels, array(
					"name"	=> Translate::get("MENU_ADMIN_QUOTES"),
					"msg"	=> count($oQuotes->getAll())." ".Translate::get("QUOTES"),
					"link" 	=> "/admin/quotes",
					"color"	=> "warning"
			));
			unset($oQuotes);
		}
		
		if (in_array("songrequest", View::adminRights())){
			$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
			array_push($aPanels, array(
					"name"	=> Translate::get("MENU_ADMIN_SONGREQUEST"),
					"msg"	=> count($oSongrequest->getAll())." ".Translate::get("SONGREQUEST"),
					"link" 	=> "/admin/songrequest",
					"color"	=> "success"
			));
			unset($oSongrequest);
		}
		
		if (in_array("rangs", View::adminRights())){
			$oRang = Factory::getModel("rangs", "rangs");
			array_push($aPanels, array(
					"name"	=> Translate::get("MENU_ADMIN_RANGS"),
					"msg"	=> count($oRang->getAll())." ".Translate::get("RANGS"),
					"link" 	=> "/admin/rangs",
					"color"	=> "cyan"
			));
			unset($oRang);
		}
		
		if (in_array("admins", View::adminRights())){
			$oAdmin = Factory::getModel("Admin", "admins");
			array_push($aPanels, array(
				"name"	=> Translate::get("MENU_ADMIN_ADMINS"),
				"msg"	=> count($oAdmin->getAll())." ".Translate::get("ADMINS"),
				"link" 	=> "/admin/admins/all",
				"color"	=> "info"
			));
			unset($oAdmin);
		}
		
		if (in_array("user", View::adminRights())){
			$oUsers = Factory::getModel("User", "User");
			array_push($aPanels, array(
				"name"	=> Translate::get("MENU_ADMIN_USERS"),
				"msg"	=> count($oUsers->getAll())." ".Translate::get("USERS"),
				"link" 	=> "/admin/user",
				"color"	=> "primary"
			));
			unset($oUsers);	
		}
	
		if (in_array("hosting", View::adminRights())){
			$oHosts = Factory::getModel("Host", "Hosting");
			array_push($aPanels, array(
				"name"	=> Translate::get("MENU_ADMIN_HOSTING"),
				"msg"	=> count($oHosts->getAll())." ".Translate::get("HOSTS"),
				"link" 	=> "/admin/hosting",
				"color"	=> "violet"
			));
			unset($oHosts);
		}
	
		if (in_array("giveaway", View::adminRights())){
			$oGiveaway = Factory::getModel("Giveaway", "Giveaway");
			array_push($aPanels, array(
				"name"	=> Translate::get("MENU_ADMIN_GIVEAWAY"),
				"msg"	=> count($oGiveaway->getAll())." ".Translate::get("GIVEAWAYS"),
				"link" 	=> "/admin/giveaway",
				"color"	=> "danger"
			));
			unset($oGiveaway);
		}
		
		View::adminPanel($aPanels);
		unset($aPanels);
	}

	public function render() {
		View::navi("home");
		View::title("Home");
		View::render("index");
	}
}