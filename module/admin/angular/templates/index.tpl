{{View::render("header_angular")}}

{{foreach from=View::adminPanel() item=$aPanel}}
	{{php}}
		$aPanel["title"] = $aPanel["name"];
		$aPanel["body"] = $aPanel["msg"];
		$aPanel["action"] = "<a href='".Config::getServerHost().$aPanel["link"]."'>".Translate::get("GO_TO")."</a>";
	{{/php}}
	{{View::currentPanel($aPanel)}}
	<div class="col-md-4">
		{{View::render("panel", false)}}
	</div>
{{/oreach}}

{{View::render("footer")}}