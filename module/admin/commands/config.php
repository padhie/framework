<?php

class CommandsConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"commands", 	
			"name"=>Translate::get("MENU_ADMIN_COMMANDS"),
			"link"=>LinkHelper::modul("commands"),
			"color"=>"orange",
			"icon"=>"fa-terminal"
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "commands")) {
			$oCommands = Factory::getModel("Commands", "Commands");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_COMMANDS"),
				"msg"	=> count($oCommands->getAll())." ".Translate::get("COMMANDS"),
				"link" 	=> LinkHelper::_("commands"),
				"color"	=> "orange"
			);
		}
	}

	public function getRights() {
		return array(
			"commands"		=> Translate::get("MENU_ADMIN_COMMANDS"),
		);
	}

	public function getSettingVars($oSettings) {
		$oRang = Factory::getModel("Rangs", "Rangs");
		$aCommandsAktivData = array();
		$aCommandsDataMods = array();
		$aCommandsDataRegulars = array();
		if ($oSettings->getSetting("commands_aktiv") == "1") {
			$aCommandsAktivData["checked"] = true;
		}		
		if ($oSettings->getSetting("commands_allowMods") == "1") {
			$aCommandsDataMods["checked"] = true;
		}		
		if ($oSettings->getSetting("commands_allowRegulars") == "1") {
			$aCommandsDataRegulars["checked"] = true;
		}

		return array(
			"basic" => array(
				"title" => Translate::get("COMMANDS_SETTINGS"),
				"color" => "info",
				"flag"	=> "alpha"
			),
			"data"	=> array(
				"commands_aktiv" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("AKTIVE"),
					"data" 	=> $aCommandsAktivData
				),
				"commands_allowRang" => array(
					"type" 	=> "multiple",
					"label"	=> Translate::get("COMMANDS_RANG"),
					"data" 	=> $oRang->getFormArray()
				),
				"commands_allowMods" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("COMMANDS_MODS"),
					"data" 	=> $aCommandsDataMods
				),
				"commands_allowRegulars" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("COMMANDS_REGULARS"),
					"data" 	=> $aCommandsDataRegulars
				),
			)
		);
	}
}