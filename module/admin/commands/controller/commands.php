<?php
class CommandsControllerCommands extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("commands");
		parent::loadModulClasses("rangs");

		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(999999999);
		$oTable->setUsingKeys(array(
				// "idCommand" 	=> Translate::get("ID"),
				"idWithExtra" 	=> Translate::get("ID"),
				"type" 			=> Translate::get("TYPE"),
				"command" 		=> Translate::get("COMMAND"),
				"text" 			=> Translate::get("COMMANDTEXT"),
				"extra" 		=> Translate::get("EXTRA"),
// 				"regularOnly" 	=> Translate::get("REGULARONLY"),
				"minRang" 		=> Translate::get("MIN_RANG"),
				"active" 		=> Translate::get("AKTIV"),
				"btns" 			=> ""
		));
		$oTable->addExtra("idWithExtra", '<a href="'.Config::getServerHost().'/admin/commands?action=load&id=%idCommand%">%idCommand%</a>');
		$oTable->addExtra("btns", 
			'<a href="'.Config::getServerHost().'/admin/commands?action=load&id=%idCommand%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button type="button" onclick="openModal(%idCommand%)" class="btn btn-danger" data-dismiss="modal">'.Translate::get("DELETE").'</button>');
		
		$oCommands = Factory::getModel("Commands", "Commands");
		$oCommands->addOrder("command");
		$oTable->setData($oCommands->getAllFuelData());
		View::tableObject( $oTable );

		
		$oRang = Factory::getModel("Rangs", "Rangs");
		View::rangs( $oRang->getAll() );
				

		$aExtra = array();
		$aCommand = View::command();
		if (isset($aCommand["regularOnly"]) && $aCommand["regularOnly"] == 1) {
			$aExtra["checked"] = true;
		}

		$aMinRangs = array();
		if ($aCommand !== false) {
			$aCommandRang = $oCommands->getRangsOfCommand(intval($aCommand["idCommand"]));
			if ($aCommandRang !== false) {
				foreach ($aCommandRang AS $aRow) {
					array_push($aMinRangs, intval($aRow["idRang"]));
				}
			}
		}
		$aOptionData=array();
		if ($aCommand["active"]=="1") {
			$aOptionData["checked"] = "checked";
		}
		$oForm = new FormHelper(Config::getServerHost()."/admin/commands?action=save", "post", "_self", "col-md-12");
		$oForm->addElement("switch", 	"command_active", 	"", 		Translate::get("AKTIV"),	$aOptionData,	array("col-md-2", "col-md-10"));	
		$oForm->addElement("select", "command_type", $aCommand["type"], Translate::get("TYPE"), 	array(
			array("key"=>"text", 		"value"=>Translate::get("TEXT")),
			array("key"=>"request", 	"value"=>Translate::get("REQUEST")),
			array("key"=>"jsonrequest",	"value"=>Translate::get("REQUESTJSON")),
			array("key"=>"script", 		"value"=>Translate::get("SCRIPT")),
			array("key"=>"alias", 		"value"=>Translate::get("ALIAS")),
			array("key"=>"timer", 		"value"=>Translate::get("TIMER")),
			array("key"=>"quote", 		"value"=>Translate::get("QUOTE")),
			array("key"=>"random", 		"value"=>Translate::get("RANDOM")),
			array("key"=>"stats", 		"value"=>Translate::get("STATS")),
			array("key"=>"spam", 		"value"=>Translate::get("SPAM")),
			array("key"=>"rangOnly", 	"value"=>Translate::get("RANGONLY")),
			array("key"=>"counter", 	"value"=>Translate::get("COUNTER")),
			array("key"=>"lmgtfy", 		"value"=>Translate::get("LMGTFY")),
			array("key"=>"event", 		"value"=>Translate::get("EVENT")),
			array("key"=>"log",			"value"=>Translate::get("LOG")),
			array("key"=>"modlog",		"value"=>Translate::get("MODLOG")),
		), array("col-md-2", "col-md-10"));
		$oForm->addElement("text", 		"command_command", 		$aCommand["command"], 	Translate::get("COMMAND"), 												array("placeholder"=>"!command"), 	array("col-md-2", "col-md-10"));
		$oForm->addElement("text", 		"command_text", 		$aCommand["text"], 		Translate::get("COMMANDTEXT"), 											array(), 							array("col-md-2", "col-md-10"));
		$oForm->addElement("text", 		"command_extra", 		$aCommand["extra"], 	Translate::get("EXTRA"),												array(), 							array("col-md-2", "col-md-10"));
		$oForm->addElement("checkbox", 	"command_regularOnly", 	"", 					Translate::get("REGULARONLY"),											$aExtra, 							array("col-md-2", "col-md-10"));
		$oForm->addElement("multiple", 	"command_minRang", 		$aMinRangs, 			Translate::get("MIN_RANG")."<br />".Translate::get("MULTIPLE_NOTIC"), 	$oRang->getFormArray(),				array("col-md-2", "col-md-10"));
		$oForm->addElement("submit", 	"", 					Translate::get("SAVE"), "", 																	array(),							"col-md-12");
		$oForm->addElement("hidden", 	"id", 					$aCommand["idCommand"]);
		View::formObject( $oForm );
		unset($oRang);
	}

	public function render() {
		View::navi("commands");
		View::title("commands");
		View::render("commands");
	}
	
	public function action($action) {
		$oCommands = Factory::getModel("Commands", "Commands");
		switch($action) {
			case "load":
				$aCommand =  $oCommands->getById(intval( Params::getParams("id") ));
				View::command( $aCommand );
				unset($aCommand);
				break;
					
			case "save":
// 				$aRangData = array();
// 				foreach (Params::postParams("command_minRang", array()) AS $sRow) {
// 					$aSplit = explode("_",$sRow);
// 					$aRangData[$aSplit[0]]	= $aSplit[1];
// 				}

				$aData = array(
					"type" 			=> trim(Params::postParams("command_type", "text")),
					"command" 		=> trim(Params::postParams("command_command", "")),
					"text" 			=> trim(Params::postParams("command_text", "")),
					"extra" 		=> trim(Params::postParams("command_extra", "")),
					"regularOnly" 	=> 0,
					"active"		=> (Params::postParams("command_active",false)!=false?1:0),
// 					"minRang" 		=> json_encode($aRangData),
				);
				if (Params::postParams("command_regularOnly") !== false) {
					$aData["regularOnly"] = 1;
				}
				if (Params::postParams("id") !== false) {
					$oCommands->update($aData, intval( Params::postParams("id") ));
					$LastId = Params::postParams("id");
					LogModelLog::addLog("admin", "commands", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$LastId = $oCommands->insert($aData);
					LogModelLog::addLog("admin", "commands", Params::cookieParams("username"), "Insert id #".$iLastId);
				}

				$oCommands->changeCommandToRang(
					intval($LastId),
					Params::postParams("command_minRang", array())
				);
				break;
					
			case "delete":
				$oCommands->delete(intval( Params::getParams("id") ));
				$oCommands->changeCommandToRang(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "commands", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oCommands);
	}
}