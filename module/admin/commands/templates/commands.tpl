{{View::render("header_admin")}}

<div class="col-md-12">
	<div class="col-md-6">
		{{View::formObject()->render()}}
	</div>
	
	<div class="col-md-3">
		{{View::render("commands_panel_notic")}}
	</div>
	
	<div class="col-md-3">
		{{View::render("commands_panel_placeholder")}}
	</div>
	
	<br /><hr >
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}

	{{assign var="sModalLink" value=LinkHelper::_("commands")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}