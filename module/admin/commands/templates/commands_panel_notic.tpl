<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("NOTE")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("GLOBAL")}}:<br />
		- {{Translate::say("COMMANDS_GLOBAL_TEXT")}}<br />
		<br />
		{{Translate::say("RANDOM")}}:<br />
		- {{Translate::say("COMMANDS_RANDOM_TEXT")}}<br />
		<br />
		{{Translate::say("REQUEST")}}:<br />
		- {{Translate::say("COMMANDS_REQUEST_TEXT")}}<br />
		<br />
		{{Translate::say("SPAM")}}:<br />
		- {{Translate::say("COMMANDS_SPAM_TEXT")}}<br />
		<br />
		{{Translate::say("RANGONLY")}}:<br />
		- {{Translate::say("COMMANDS_RANGONLY_TEXT")}}<br />
		<br />
		{{Translate::say("COUNTER")}}:<br />
		- {{Translate::say("COMMANDS_COUNTER_TEXT")}}<br />
		<br />
		{{Translate::say("ALIAS")}}:<br />
		- {{Translate::say("COMMANDS_ALIAS_TEXT")}}<br />
		<br />
		{{Translate::say("TIMER")}}:<br />
		- {{Translate::say("COMMANDS_TIMER_TEXT")}}<br />
		<br />
		{{Translate::say("EVENT")}}:<br />
		- {{Translate::say("COMMANDS_EVENT_TEXT")}}<br />
		<br />
	</div>
</div>