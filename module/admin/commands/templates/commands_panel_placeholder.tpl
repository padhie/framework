<div class="panel panel-warning">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("PLACEHOLDER")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("GLOBAL")}}:<br />
		%CHANNEL%, %CLEARCHANNEL%, %USER%, %BOT%, %RANDOM_USER%<br />
		%PARAM1% > %PARAM5%, %PARAMALL%<br />
		%TWITCH_TITLE%, %TWITCH_GAME%, %TWITCH_DELAY%, %TWITCH_VIEWS%, %TWITCH_FOLLOWERS%<br />
		<br />
		{{Translate::say("STATS")}}:<br />
		- %RANG_(Gruppenbezeichnung)%<br />
		- %CHECKEDUSER%<br />
		- %TIMETODAY%<br />
		- %TIMETOTAL%<br />
		- %POSTTODAY%<br />
		- %POSTTOTAL%<br />
		- %LASTUPDATE%<br />
		- %POINTS%<br />
		<br />
		{{Translate::say("RANDOM")}}:<br />
		- %RANDOM%<br />
		<br />
		{{Translate::say("REQUEST")}}:<br />
		- %RESULT%<br />
		<br />
		{{Translate::say("COUNTER")}}:<br />
		- %COUNT%<br />
		<br />
		{{Translate::say("LMGTFY")}}:<br />
		- %LINK%<br />
	</div>
</div>