<?php

class UserConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"user", 		
			"name"=>Translate::get("MENU_ADMIN_USERS"), 		
			"link"=>LinkHelper::modul("user"),			
			"color"=>"primary",	
			"icon"=>"fa-users"
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "user")) {
			$oUsers = Factory::getModel("User", "User");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_USERS"),
				"msg"	=> count($oUsers->getAll())." ".Translate::get("USERS"),
				"link" 	=> LinkHelper::_("user"),
				"color"	=> "primary"
			);
		}
	}

	public function getRights() {
		return array(
			"user"			=> Translate::get("MENU_ADMIN_USERS"),
		);
	}

	public function getSettingVars($oSettings) {
		$aPointData = array();
		if ($oSettings->getSetting("points_aktiv") == "1") {
			$aPointData["checked"] = true;
		}

		return array(
			"basic" => array(
				"title" => Translate::get("POINT"),
				"color" => "info",
				"flag"	=> "beta"
			),
			"data"	=> array(
				"points_aktiv" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("AKTIVE"),
					"data" 	=> $aPointData
				),
				"points_time" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("POINT_TIME"),
					"data" 	=> array()
				),
				"points_post" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("POINT_POST"),
					"data" 	=> array()
				),
			)
		);
	}
}