{{View::render("header_admin")}}

<div class="col-md-12">
	{{if View::formObject() !== null}}
		{{View::formObject()->render()}}	
	{{else}}
		{{php}}View::formObjectVariable("search"){{/php}}
		{{View::search()->render()}}
	{{/if}}
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
	
	{{assign var="sModalLink" value=LinkHelper::_("user")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}