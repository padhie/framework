<?php
class UserControllerUser extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("user");
		parent::loadModulClasses("rangs");
		parent::loadModulClasses("settings", "admin");
		parent::start();
	}
	
	public function run() {
		// Tabellenansicht
		$oTable = new TableHelper();
		$oTable->setPerPage(500);
		$oTable->setPage(Params::getParams("p", 1));
		$aKeys = array();
		$aKeys["idUser"] 	= Translate::get("ID")					.' <a href="'.Config::getServerHost().'/admin/user?field=idUser&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=idUser&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["user"] 		= Translate::get("NICKNAME")			.' <a href="'.Config::getServerHost().'/admin/user?field=user&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=user&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["timeToday"] = Translate::get("TIME_TODAY_MINS")		.' <a href="'.Config::getServerHost().'/admin/user?field=timeToday&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=timeToday&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["timeTotal"] = Translate::get("TIME_TOTAL_MINS")		.' <a href="'.Config::getServerHost().'/admin/user?field=timeTotal&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=timeTotal&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["postToday"] = Translate::get("POST_TODAY")			.' <a href="'.Config::getServerHost().'/admin/user?field=postToday&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=postToday&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["postTotal"] = Translate::get("POST_TOTAL")			.' <a href="'.Config::getServerHost().'/admin/user?field=postTotal&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=postTotal&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		$aKeys["points"] 	= Translate::get("POINTS")				.' <a href="'.Config::getServerHost().'/admin/user?field=points&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
																	.' <a href="'.Config::getServerHost().'/admin/user?field=points&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>';
		if (isset(Config::$blUseDiscord) && Config::$blUseDiscord == true) {
			$aKeys["discord"] 	= Translate::get("CONNECT_WITH_DISCORD");
		}
		$aKeys["name"] 		= Translate::get("RANG");
		$aKeys["btns"] 		= "";
		
		$oTable->setUsingKeys($aKeys);
		
		$oTable->addExtra("btns", '<a href="'.Config::getServerHost().'/admin/user?action=load&id=%idUser%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idUser%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oUser = Factory::getModel("User");
		$oUser->addOrder(Params::getParams("field", "user"), Params::getParams("sort", "ASC"));
		
		// Suche
		if (Params::postParams("action", false) == "search") {
			$oUser->searchUsername(Params::postParams("user", ""));
		}
		
		// Fance Rangansicht
		$aUserData = $oUser->getAll();
		for ($i=0; $i<count($aUserData); $i++) {
			$aRangData = $oUser->getRangsOfUser($aUserData[$i]["idUser"]);
			$sRangText = "";
			if ($aRangData !== false) {
				foreach ($aRangData AS $aRangRow) {
					$sRangText .= $aRangRow["group"].":".$aRangRow["name"]."|";
				}	
			}
			$aUserData[$i]["name"] = substr($sRangText, 0, -1);
			$aUserData[$i]["discord"] = "-";
			if ($aUserData[$i]["idDiscordUser"] != NULL) {
				$aUserData[$i]["discord"] = "X";
			}
		}
		$oTable->setData($aUserData);
		View::tableObject( $oTable );
		
		
		$oForm = new FormHelper(Config::getServerHost()."/admin/user", "post", "_self", "");
		$oForm->addElement("text",		"user",		Params::postParams("user", ""),		Translate::get("USER")." ".Translate::get("SEARCH"), 	array(), 	array("col-md-2", "col-md-9"));
		$oForm->addElement("submit", 	"", 		Translate::get("SEARCH"),			"", 													array(), 	"col-md-12");
		$oForm->addElement("hidden", 	"action", 	"search");
		
		View::search( $oForm );
		unset($oForm);
		
	}

	public function render() {
		View::navi("user");
		View::title("user");
		
		parent::render();
	}
	
	public function getForm($aData) {
		$oRang = Factory::getModel("rangs", "rangs");
		$oForm = new FormHelper(Config::getServerHost()."/admin/user?action=save", "post", "_self", "");

		$oForm->addElement("label", 	"user_idUser",			$aData["idUser"], 			Translate::get("ID"), 				array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("label",		"user_user",			$aData["user"], 			Translate::get("NICKNAME"), 		array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("label",		"user_timeToday",		$aData["timeToday"], 		Translate::get("TIME_TODAY_MINS"), 	array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("label",		"user_timeTotal",		$aData["timeTotal"], 		Translate::get("TIME_TOTAL_MINS"), 	array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("label",		"user_postToday",		$aData["postToday"], 		Translate::get("POST_TODAY"), 		array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("label",		"user_postTotal",		$aData["postTotal"], 		Translate::get("POST_TOTAL"), 		array(), 				array("col-md-2", "col-md-9"));
		$oForm->addElement("multiple", 	"user_rang",			$aData["rangs"],			Translate::get("RANG"),				$oRang->getFormArray(),	array("col-md-2", "col-md-9"));
 		$oForm->addElement("submit", 	"", 					Translate::get("SAVE"),		"", 								array(), 				"col-md-12");
		$oForm->addElement("hidden", 	"id", 					$aData["idUser"]);
		
		unset($oRang);
		
		return $oForm;
	}
	
	public function action($action) {
		$oUser = Factory::getModel("user", "user");
		
		switch($action) {
			case "load":
				$aUser =  $oUser->getById(intval( Params::getParams("id") ));
				if (!empty($aUser)) {
					$aUser["rangs"] = array();
					foreach ($oUser->getRangsOfUser($aUser["idUser"]) AS $aRangData) {
						array_push($aUser["rangs"], $aRangData["idRang"]);
					}
					$oForm = $this->getForm($aUser);

					View::formObject( $oForm );
					unset($oForm);
				}
				unset($aUser);
				break;
					
				
			// @TODO: Rangupdaten!!!
			case "update":
				$iIdUser = intval(Params::getParams("id", 0));
				$aUserRangs = $oUser->getRangsOfUser($iIdUser);
				
				$oRang = Factory::getModel("rangs", "rangs");
				$aRang = $oRang->getAll();
				
				$oSettings = Factory::getModel("settings", "settings");
				$aBaseRang = $oSettings->getSetting("rang_startRang");
				$aBaseRang = json_decode($aBaseRang, true);
				

				//var_dump($aUserRangs, $aRang, $aBaseRang);

				//$aNewRangs = array();
				//$oUser->saveRangsToUser($iIdUser, $aNewRangs);
				LogModelLog::addLog("admin", "user", Params::cookieParams("username"), "Update id #".Params::getParams("id", 0));
				break;
				
				
			case "delete":
 				$oUser->delete( intval(Params::postParams("id", Params::getParams("id", 0))) );
 				LogModelLog::addLog("admin", "user", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
				
				
			case "save":
				$aData = Params::postParams("user_rang");
 				$oUser->saveRangsToUser(intval(Params::postParams("id")), $aData);
 				LogModelLog::addLog("admin", "user", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				break;
		}
		unset($oRangs);
	}
}