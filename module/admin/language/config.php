<?php

class LanguageConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"language",
			"name"=>Translate::get("MENU_ADMIN_LANGUAGE"),
			"link"=>LinkHelper::modul("language"),
			"color"=>"info",
			"icon"=>"fa-comment"
		);
	}

	public function getRights() {
		return array(
			"language"		=> Translate::get("MENU_ADMIN_LANGUAGE"),
		);
	}
}