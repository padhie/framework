<?php
class LanguageControllerLanguage extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->setPerPage(9999999999);
		$oTable->blShowHits = false;
 		$oTable->setUsingKeys(array(
 				"group" 		=> Translate::get("GROUP"),
 				"lang" 			=> Translate::get("LANGUAGE"),
 				"key" 			=> Translate::get("KEY"),
 				"inputValue" 	=> Translate::get("VALUE"),
 				"notic" 		=> Translate::get("NOTE"),
 				"btn"			=> ""
 		));
 		$oTable->addExtra("inputValue", "<textarea name='data[%idLanguage%][value]'>%value%</textarea>");
 		$oTable->addExtra("btn", "<button type='submit' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='".Translate::get("SAVE")."' ><i class='fa fa-floppy-o'></i></button>");

		$oLang = new language();
		$oLang->addWhere("group", "bot");
		$oLang->addOrder("key");
 		$oTable->setData($oLang->getAll());
 		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("language");
		View::title("language");
		View::render("language");
	}
	
	public function action($action) {
		$oLang = new language();
		switch($action) {
			case "save":
				$aPostData = Params::postParams("data", array());
				foreach ($aPostData AS $sKey1 => $aVal1) {
					$aData = array();
					foreach ($aVal1 AS $sKey2 => $sVal2) {
						$aData[$sKey2] = trim($sVal2);
					}
					$oLang->update($aData, $sKey1);
					LogModelLog::addLog("admin", "language", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				}
				break;
		}
		unset($oLang);
	}

}