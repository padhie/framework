{{View::render("header_admin")}}

<div class="col-md-12">
	<form action="{{Config::getServerHost()}}/admin/language/" method="post">
		{{View::tableObject()->render()}}		
		<input type="hidden" name="action" value="save" />
	</form>
</div>


<<script type="text/javascript">
	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>

{{View::render("footer")}}