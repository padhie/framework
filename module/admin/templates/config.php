<?php

class TemplatesConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"templates", 	
			"name"=>Translate::get("MENU_ADMIN_TEMPLATES"), 	
			"link"=>LinkHelper::modul("templates"),		
			"color"=>"orange",	
			"icon"=>"fa-table"
		);
	}

	public function getRights() {
		return array(
			"templates"			=> Translate::get("MENU_ADMIN_TEMPLATES"),
		);
	}
}