<div class="panel panel-warning">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("PLACEHOLDER")}}</h3>
	</div>
	<div class="panel-body">
		- %IDEVENT%<br />
		- %TYPE%<br />
		- %NEW%<br />
		- %USER%<br />
		- %MESSAGE%<br />
		<br />
	</div>
</div>