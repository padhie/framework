{{View::render("header_admin")}}

<div class="col-md-12">
	<div class="col-md-8">
		{{View::formObject()->render()}}
	</div>
	
	<div class="col-md-2">
		{{View::render("templates_panel_placeholder")}}
	</div>
	
	<div class="col-md-2">
		{{View::render("templates_panel_notic")}}
		
	</div>
	<br /><hr >
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}

	{{assign var="sModalLink" value=LinkHelper::_("tempaltes")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}