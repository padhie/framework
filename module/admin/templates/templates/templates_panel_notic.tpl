<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("NOTE")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("LINKS")}}:<br />
		- {{Translate::say("LINKS_OBS_EVENT")}}<br />
		- {{Translate::say("LINKS_OBS_SINGLE")}}<br />
		<br />
		
		{{Translate::say("PARAMETERS")}} {{Translate::say("PARAMETERS_EXTRA_TEMPLATES")}}:<br />
		- {{Translate::say("TEMPLATES_PARAMETERS_D")}}<br />
		- {{Translate::say("TEMPLATES_PARAMETERS_CE")}}<br />
		- {{Translate::say("TEMPLATES_PARAMETERS_QE")}}<br />
		<br />
	</div>
</div>