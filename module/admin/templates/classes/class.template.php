<?php

class TemplatesModelTemplate extends DbExtent {
	protected $_sTable = "templates";
	protected $_sIdField = "idTemplate";
	
	public function getByName($sName) {
		if (!Validate::isStringNotEmpty($sName)) {
			return false;
		}
		
		$this->_oQueryBuilder->addWhere("name", $sName);
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			return $this->getResult();
		}
		return false;
	}
}