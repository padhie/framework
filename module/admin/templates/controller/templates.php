<?php
class TemplatesControllerTemplates extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("api");
		
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idTemplate" 	=> Translate::get("ID"),
				"name" 			=> Translate::get("NAME"),
				"btns" 			=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.Config::getServerHost().'/admin/templates?action=load&id=%idTemplate%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idTemplate%)" class="btn btn-danger">'.Translate::get("DELETE").'</button> '.
			'<a href="'.Config::getServerHost().'/admin/templates?action=sendTest&id=%idTemplate%" class="btn btn-warning">'.Translate::get("TEST").'</a>');


		$oTemplate = Factory::getModel("Template");
		$oTable->setData($oTemplate->getAll());
		View::tableObject( $oTable );
		unset($oTemplate);
		
		if (View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/templates?action=save", "post");
			$oForm->addElement("text", 			"template_name", 	"",							Translate::get("NAME"),		array(),	array("col-md-2","col-md-9"));
			$oForm->addElement("codemirror", 	"template_content", "",							Translate::get("CONTENT"),	array(),	array("col-md-2","col-md-9"));
			$oForm->addElement("submit", 		"", 				Translate::get("SAVE"),		"",							array(),	"col-md-12");
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("templates");
		View::title("templates");
		View::render("templates");
	}

	public function action($action) {
		$oTemplate = Factory::getModel("Template");
		switch($action) {
			case "load":
				$aTemplate =  $oTemplate->getById(intval( Params::getParams("id") ));
				$oForm = new FormHelper(Config::getServerHost()."/admin/templates?action=save", "post");
				$oForm->addElement("text", 			"template_name", 	$aTemplate["name"],			Translate::get("NAME"),		array(),	array("col-md-2","col-md-9"));
				$oForm->addElement("codemirror", 	"template_content", $aTemplate["content"],		Translate::get("CONTENT"),	array(),	array("col-md-2","col-md-9"));
				$oForm->addElement("hidden", 		"id", 				$aTemplate["idTemplate"]);
				$oForm->addElement("submit", 		"", 				Translate::get("SAVE"),			"",							array(),	"col-md-12");
				View::formObject($oForm);
				unset($oForm, $aTemplate);
				break;
	
			case "save":
				$aData = array(
				"name" 		=> Params::postParams("template_name", ""),
				"content" 	=> Params::postParams("template_content", "")
				);
				if (Params::postParams("id") !== false) {
					$oTemplate->update($aData, intval( Params::postParams("id") ));
					LogModelLog::addLog("admin", "templates", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oTemplate->insert($aData);
					LogModelLog::addLog("admin", "templates", Params::cookieParams("username"), "Insert id #".$oTemplate->getLastInsertId());
				}
				break;
	
			case "delete":
				$oTemplate->delete(intval( Params::getParams("id") ));
				LogModelLog::addLog("admin", "templates", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
	
			case "sendTest":
				$aTemplate = $oTemplate->getById(intval( Params::getParams("id") ));
				$aData = array(
						"type"		=> $aTemplate["name"],
						"new"		=> 1,
						"user"		=> "admin",
						"message"	=> ""
				);
	
				$oEvent = Factory::getModel("events", "api");
				$oEvent->insert($aData);
				LogModelLog::addLog("admin", "templates", Params::cookieParams("username"), "Testing id #".Params::getParams("id"));
				break;
		}
		unset($oTemplate);
	}
}