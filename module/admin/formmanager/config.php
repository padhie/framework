<?php

class FormmanagerConfig extends ModulConfig {
	public function install() {
		$oInstaller = Faktory::getModel("installer", "installer");
		$oInstaller->executeQuery("
			CREATE TABLE IF NOT EXISTS `formmanager_eintrag` (
				`idEintrag` int(11) NOT NULL,
				`idForm` int(11) NOT NULL,
				`idField` int(11) NOT NULL,
				`create` datetime NOT NULL,
				`content` text NOT NULL,
				PRIMARY KEY (`idEintrag`,`idForm`,`idField`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$oInstaller->executeQuery("
			CREATE TABLE IF NOT EXISTS `formmanager_field` (
				`idField` int(11) NOT NULL AUTO_INCREMENT,
				`idForm` int(11) NOT NULL,
				`type` varchar(255) NOT NULL DEFAULT 'text',
				`name` varchar(255) NOT NULL,
				`placeholder` varchar(255) NOT NULL,
				`value` text NOT NULL,
				`class` varchar(255) NOT NULL,
				`additionalTag` text NOT NULL,
				`useLabel` tinyint(1) NOT NULL,
				`settings` text NOT NULL COMMENT 'zusätzliche Daten',
				PRIMARY KEY (`idField`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$oInstaller->executeQuery("
			CREATE TABLE IF NOT EXISTS `formmanager_formular` (
				`idForm` int(11) NOT NULL AUTO_INCREMENT,
				`name` varchar(255) NOT NULL,
				`active` tinyint(1) NOT NULL,
				PRIMARY KEY (`idForm`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		unset($oInstaller);
	}

	public function delete() {
		$oInstaller = Faktory::getModel("installer", "installer");
		$oInstaller->executeQuery("DROP TABLE IF EXISTS `formmanager_eintrag`;");
		$oInstaller->executeQuery("DROP TABLE IF EXISTS `formmanager_field`;");
		$oInstaller->executeQuery("DROP TABLE IF EXISTS `formmanager_formular`;");
		unset($oInstaller);
	} 
	public function getMenuConfig() {
		return array(
			"key"=>"formmananger",
			"name"=>Translate::get("MENU_ADMIN_FORMMANAGER"),
			"color"=>"warning",	
			"icon"=>"fa-plus",
			"subnavi"=>array(
				array("key"=>"formmanager_form", "name"=>"formular", "link"=>LinkHelper::_("formmanager", "form")),
				array("key"=>"formmanager_field", "name"=>"Felder", "link"=>LinkHelper::_("formmanager", "field")),
				array("key"=>"formmanager_eintrag", "name"=>"Einträge", "link"=>LinkHelper::_("formmanager", "eintrag")),
			)
		);
	}

	public function getRights() {
		return array(
			"regist" => Translate::get("MENU_ADMIN_PLUGINS"),
		);
	}
}