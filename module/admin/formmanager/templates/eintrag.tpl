{{View::render("header_admin")}}

<div class="col-md-12">
	{{if View::formObject() != null}}
		{{View::formObject()->render()}}
	{{/if}}
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
	
	{{assign var="sModalLink" value=LinkHelper::_("formmanager", "eintrag")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}