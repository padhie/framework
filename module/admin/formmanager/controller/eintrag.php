<?php
class FormmanagerControllerEintrag extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"idEintrag"	=> Translate::get("ID"),
				"formname"	=> Translate::get("FORM"),
				"createDe"	=> Translate::get("DATE"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.LinkHelper::_("formmanager", "eintrag").'?action=load&id=%idEintrag%" class="btn btn-info">'.Translate::get("LOAD").'</a> '.
			'<button onclick="openModal(%idEintrag%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);

		$oEintraege = Factory::getModel("eintrag", "formmanager");
		$oEintraege->prepareSelect();
		$oEintraege->addGroup("idEintrag");
		$oTable->setData($oEintraege->getAll());
		View::tableObject( $oTable );
		unset($oEintraege, $oTable);
	}

	public function render() {
		View::navi("formmananger");
		View::title("Einträge");
		View::render("eintrag");
	}

	public function action($action) {
		$oEintraege = Factory::getModel("eintrag", "formmanager");
		switch($action) {
			case "load":
				$aFormData = $oEintraege->getDataByIdEintrag( Params::getParams("id") );

				$oForm = new FormHelper(LinkHelper::_("formmanager", "eintrag"), "post");
				foreach ($aFormData AS $aData) {
					$oForm->addElement("label", "field", htmlentities($aData["content"]), $aData["name"].":", array(), array("col-md-2", "col-md-9"));
				}
				View::formObject($oForm);
				unset($oForm, $aFormData);
				break;
			
			case "delete":
				LogModelLog::addLog("admin", "formmanager:field", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oEintraege->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oEintraege);
	}
}