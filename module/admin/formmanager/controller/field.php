<?php
class FormmanagerControllerField extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"idField"	=> Translate::get("ID"),
				"idForm"	=> Translate::get("FORM"),
				"type"		=> Translate::get("TYPE"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.LinkHelper::_("formmanager", "field").'?action=load&id=%idField%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idField%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);

		$oFormmanager = Factory::getModel("field", "formmanager");
		$oTable->setData($oFormmanager->getAll());
		View::tableObject( $oTable );
		unset($oFormmanager, $oTable);
		

		if (View::formObject() === null) {
			$oForm = $this->getForm(array(), array());
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("formmananger");
		View::title("Felder");
		View::render("field");
	}

	public function action($action) {
		$oFormmanager = Factory::getModel("field", "formmanager");
		switch($action) {
			case "load":
				$aFormData = $oFormmanager->getById(intval( Params::getParams("id") ));
				if ($aFormData["settings"] == "") {
					$aSettings = "";
				} else {
					$aSettings = json_encode($aFormData["settings"]);
				}
				$oForm = $this->getForm($aFormData, $aSettings);
				View::formObject($oForm);
				unset($oForm, $aFormData);
				break;
					
			case "new":
			case "save":
				$aData = array(
					"idForm"			=> Params::postParams("idForm", 0),
					"type"				=> Params::postParams("type", "input"),
					"name"				=> Params::postParams("name", ""),
					"placeholder"		=> Params::postParams("placeholder", ""),
					"value"				=> Params::postParams("value", ""),
					"class"				=> Params::postParams("class", ""),
					"additionalTag"		=> Params::postParams("additionalTag", ""),
					"useLabel"			=> (Params::postParams("useLabel",false)!=false?1:0),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "formmanager:field", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oFormmanager->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oFormmanager->insert($aData);
					LogModelLog::addLog("admin", "formmanager:field", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;


			case "delete":
				LogModelLog::addLog("admin", "formmanager:field", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oFormmanager->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oFormmanager);
	}
 
	public function getForm($aData=array(), $aSettings=array()) {
		$aData = Helper::prepareArray($aData, "idField, idForm, type, name, placeholder, value, class, additionalTag, useLabel");
		$aSettings = Helper::prepareArray($aSettings, "");

		$oForms = Factory::getModel("form", "formmanager");
		$aForms = Helper::createSelectArray($oForms->getAll(), "idForm", "name");
		unset($oForms);

		$aTypes = array("text", "number", "email", "textarea", "select", "checkbox", "radio", "captcha", "hidden");
		$aTypes = Helper::createSelectArrayByNumeric($aTypes);

		$aOptionData=array();
		if ($aData["useLabel"]=="1") {
			$aOptionData["checked"] = "checked";
		}

		$oForm = new FormHelper(LinkHelper::_("formmanager", "field"), "post");
		$oForm->addElement("select", 	"idForm", 			$aData["idForm"],			Translate::get("FORM"),				$aForms,		array("col-md-2", "col-md-9"));
		$oForm->addElement("select", 	"type", 			$aData["type"],				Translate::get("TYPE"),				$aTypes,		array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"name", 			$aData["name"],				Translate::get("NAME"),				"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"placeholder", 		$aData["placeholder"],		Translate::get("PLACEHOLDER"),		"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"value", 			$aData["value"],			Translate::get("VALUE"),			"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"class", 			$aData["class"],			Translate::get("CLASSS"),			"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 		"additionalTag", 	$aData["additionalTag"],	Translate::get("ADDITIONAL_TAG"),	"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 	"useLabel", 		$aData["useLabel"],			Translate::get("USELABEL"),			$aOptionData,	array("col-md-2", "col-md-9"));
		
		$oForm->addElement("hidden", 	"id", 			$aData["idField"]);
		$oForm->addElement("hidden", 	"action",		"save");
		$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
		return $oForm;
	}
}