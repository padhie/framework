<?php
class FormmanagerControllerForm extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"idForm"	=> Translate::get("ID"),
				"name"		=> Translate::get("NAME"),
				"active" 	=> Translate::get("AKTIV"),
				"btns" 		=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.LinkHelper::_("formmanager", "form").'?action=load&id=%idForm%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idForm%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);

		$oFormmanager = Factory::getModel("form", "formmanager");
		$oTable->setData($oFormmanager->getAll());
		View::tableObject( $oTable );
		unset($oFormmanager, $oTable);
		

		if (View::formObject() === null) {
			$oForm = $this->getForm(array(), array());
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("formmananger");
		View::title("Formular");
		View::render("formular");
	}

	public function action($action) {
		$oFormmanager = Factory::getModel("form", "formmanager");
		switch($action) {
			case "load":
				$aFormData = $oFormmanager->getById(intval( Params::getParams("id") ));
				$aOptionData=array();
				if ($aFormData["active"]=="1") {
					$aOptionData["checked"] = "checked";
				}

				$oForm = $this->getForm($aFormData, $aOptionData);
				View::formObject($oForm);
				unset($oForm, $aFormData);
				break;
					
			case "new":
			case "save":
				$aData = array(
					"name"		=> Params::postParams("name", 0),
					"active"	=> (Params::postParams("active",false)!=false?1:0),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "formmanager:form", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oFormmanager->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oFormmanager->insert($aData);
					LogModelLog::addLog("admin", "formmanager:form", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;


			case "delete":
				LogModelLog::addLog("admin", "formmanager:form", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oFormmanager->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oFormmanager);
	}
 
	public function getForm($aData=array(), $aOptions=array()) {
		$aData = Helper::prepareArray($aData, "idForm,name,active");

		$oForm = new FormHelper(LinkHelper::_("formmanager", "form"), "post");
		$oForm->addElement("text",		"name",			$aData["name"], 				Translate::get("NAME"),		"",				array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 	"active", 		$aData["active"],				Translate::get("AKTIV"),	$aOptions,		array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 	"id", 			$aData["idForm"]);
		$oForm->addElement("hidden", 	"action",		"save");
		$oForm->addElement("submit", 	"", 			Translate::get("SAVE"));
		return $oForm;
	}
}