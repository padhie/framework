<?php

class FormmanagerModelEintrag extends DbExtent {
	protected $_sTable = "formmanager_eintrag";
	protected $_sIdField = "";

	public function prepareSelect() {
		$this->_oQueryBuilder->addJoin("LEFT OUTER", "formmanager_formular", "idForm");
		$this->_oQueryBuilder->addRawSelect("DATE_FORMAT(`create`, '%d.%m.%Y %H:%i:%s') AS `createDe`");
		$this->_oQueryBuilder->addRawSelect("CONCAT(formmanager_formular.idForm, ':', formmanager_formular.name) AS `formname`");
	}

	public function addGroup($sField) {
		$this->_oQueryBuilder->addGroup($sField);
	}

	public function getDataByIdEintrag($idEintrag) {
		$this->_oQueryBuilder->addJoin("LEFT OUTER", "formmanager_field", "idField");
		$this->_oQueryBuilder->addWhere("idEintrag", $idEintrag);
		return $this->getAll();
	}

	public function delete($idEintrag) {
		$this->_oQueryBuilder->addWhere("idEintrag", $idEintrag);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );

		if ($this->execute()) {
			return true;
		}
		return false;
	}

	public function getLastId() {
		$this->setOrder("idEintrag", "DESC");
		$aLast = $this->getSingleData();
		return $aLast["idEintrag"];
	}
}