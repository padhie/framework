<?php
class GiveawayControllerGiveaway extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("giveaway");
		parent::loadModulClasses("rangs");	
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		
		$oTable->addExtra("btns",
				'<a href="'.Config::getServerHost().'/admin/giveaway?action=lock&id=%idGiveaway%" class="btn btn-warning">'.Translate::get("LOCK").'</a> '.
				'<button onclick="openModal(%idGiveaway%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oRang 		= Factory::getModel("Rangs","Rangs");
		$oGiveaway 	= Factory::getModel("Giveaway");
		$oGiveaway->addOrder("idGiveaway", "DESC");
		$aData 		= $oGiveaway->getAll();
		if (count($aData) >= 1) {
			foreach ($aData[0] AS $sKey => $sVal) {
				$oTable->addUsingKeys($sKey, Translate::get(strtoupper($sKey)));
			}
			$oTable->addUsingKeys("minRang", Translate::get(strtoupper("minRang")));
			
			for ($i=0; $i<count($aData); $i++) {
				$aRangRowData = $oGiveaway->getRangsOfGiveaway($aData[$i]["idGiveaway"]);
				$aData[$i]["minRang"] = $oRang->createRangTextByData($aRangRowData);
			}
				
		}
		$oTable->addUsingKeys("btns", "");
		$oTable->setData($aData);
		unset($oGiveaway);
		
		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("giveaway");
		View::title("Giveaway");
		View::render("giveaway");
	}

	public function action($action) {
		$oGiveaway = Factory::getModel("Giveaway");
		switch (Params::getParams("action")) {
			case "load":
				$aGiveaway =  $oGiveaway->getById(intval( Params::getParams("id") ));
				View::host( $aGiveaway );
				unset($aGiveaway);
				break;
					
			case "save":
				$aData = array(
				"channel"	=> Params::postParams("host_channel", ""),
				"prio"	=> Params::postParams("host_prio", ""),
				);
				if (Params::postParams("id") !== false) {
					$oGiveaway->update($aData, intval( Params::postParams("id") ));
					LogModelLog::addLog("admin", "giveaway", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
				} else {
					$oGiveaway->insert($aData);
					LogModelLog::addLog("admin", "giveaway", Params::cookieParams("username"), "Insert id #".$oGiveaway->getLastInsertId());
				}
				break;
					
			case "lock":
				$aData = array( "status" => "lock" );
				$oGiveaway->update($aData, intval(Params::getParams("id")));
				LogModelLog::addLog("admin", "giveaway", Params::cookieParams("username"), "Lock id #".Params::getParams("id"));
				unset($aData);
				break;
					
			case "delete":
				$oGiveaway->delete(intval(Params::getParams("id")));
				LogModelLog::addLog("admin", "giveaway", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				break;
		}
		unset($oGiveaway);
	}
}