{{View::render("header_admin")}}

<style>
	table.table td {
		text-align: center;
	}
</style>

<div class="col-md-12">
	{{View::tableObject()->render()}}
		
	{{assign var="sModalLink" value=LinkHelper::_("giveaway")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}
