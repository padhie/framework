<?php

class GiveawayConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"giveaway", 	
			"name"=>Translate::get("MENU_ADMIN_GIVEAWAY"), 		
			"link"=>LinkHelper::modul("giveaway"),		
			"color"=>"danger",	
			"icon"=>"fa-gift",			
			"betaflag"=>true
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "giveaway")) {
			$oGiveaway = Factory::getModel("Giveaway", "Giveaway");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_GIVEAWAY"),
				"msg"	=> count($oGiveaway->getAll())." ".Translate::get("GIVEAWAYS"),
				"link" 	=> LinkHelper::_("giveaway"),
				"color"	=> "danger"
			);
		}
	}

	public function getRights() {
		return array(
			"giveaway"		=> Translate::get("MENU_ADMIN_GIVEAWAY"),
		);
	}

	public function getSettingVars($oSettings) {
		$oRang = Factory::getModel("Rangs", "Rangs");
		$aGiveawayData = array();
		$aGiveawayMinRangs = json_decode($oSettings->getSetting("giveaway_minRang"));
		if ($oSettings->getSetting("giveaway_displayIntoText") == "1") {
			$aGiveawayData["checked"] = true;
		}

		return array(
			"basic" => array(
				"title" => Translate::get("GIVEAWAYS"),
				"color" => "warning",
				"flag"	=> ""
			),
			"data"	=> array(
				"giveaway_minRang" => array(
					"type" 	=> "multiple",
					"label"	=> Translate::get("MIN_RANG"),
					"data" 	=> $oRang->getFormArray()
				),
				"giveaway_displayIntoText" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("GIVEAWAY_DISPLAY_INTROTEXT"),
					"data" 	=> $aGiveawayData
				),
				"giveaway_autoEndingTime" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("GIVEAWAY_AUTOENDINGTIME"),
					"data" 	=> array()
				),
				"giveaway_rememberTime" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("GIVEAWAY_REMEMBERTIME"),
					"data" 	=> array()
				),
				"giveaway_template" => array(
					"type" 	=> "text",
					"label"	=> Translate::get("GIVEAWAY_TEMPLATE"),
					"data" 	=> array()
				),
			)
		);
	}
}