<?php

class SongrequestConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"songrequest", 
			"name"=>Translate::get("MENU_ADMIN_SONGREQUEST"), 	
			"link"=>LinkHelper::modul("songrequest"),	
			"color"=>"success",	
			"icon"=>"fa-play"
		);
	}

	public function getDefaultDashboardConfig() {
		if (AdminsModelAccess::adminHasRightByUsername(Params::cookieParams("username", ""), "songrequest")) {
			$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
			return array(
				"name"	=> Translate::get("MENU_ADMIN_SONGREQUEST"),
				"msg"	=> count($oSongrequest->getAll())." ".Translate::get("SONGREQUEST"),
				"link" 	=> LinkHelper::_("songrequest"),
				"color"	=> "success"
			);
		}
	}

	public function getRights() {
		return array(
			"songrequest" => Translate::get("MENU_ADMIN_SONGREQUEST"),
		);
	}

	public function getSettingVars($oSettings) {
		$aSongData = array();
		if ($oSettings->getSetting("songrequest_aktiv") == "1") {
			$aSongData["checked"] = true;
		}
		$aSongTypes = array(
			array("key"=>"high", 		"value"=>Translate::get("HIGHEST")),
			array("key"=>"low", 		"value"=>Translate::get("LOWEST")),
			array("key"=>"addition", 	"value"=>Translate::get("ADDITION"))
		);

		return array(
			"basic" => array(
				"title" => Translate::get("SONGREQUEST"),
				"color" => "info",
				"flag"	=> ""
			),
			"data"	=> array(
				"songrequest_aktiv" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("AKTIVE"),
					"data" 	=> $aSongData
				),
				"songrequest_maxSongs" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("MAX_SONGS"),
					"data" 	=> array()
				),
				"songrequest_maxDuration" => array(
					"type" 	=> "number",
					"label"	=> Translate::get("MAX_DURATION"),
					"data" 	=> array()
				),
				"songrequest_requestType" => array(
					"type" 	=> "select",
					"label"	=> Translate::get("MAXSONGTYPE"),
					"data" 	=> $aSongTypes
				),
				"songrequest_ignoreCurrentSong" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("IGNORECURRENTSONG"),
					"data" 	=> $oSettings->getChecked("songrequest_ignoreCurrentSong")
				),
				"songrequest_duplicateSongInList" => array(
					"type" 	=> "switch",
					"label"	=> Translate::get("DUPLICATESONGINLIST"),
					"data" 	=> $oSettings->getChecked("songrequest_duplicateSongInList")
				),
			)
		);
	}
}