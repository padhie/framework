<?php
class SongrequestControllerSongrequest extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("songrequest");
		
		parent::start();
	}
	
	public function run() {
		$oSongrequest = Factory::getModel("Songrequest", "Songrequest");
		View::currentSong( $oSongrequest->getFirst() );
		
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idSongrequest" => Translate::get("ID"),
				"title2" 		=> Translate::get("SONGTITLE"),
				"duration" 		=> Translate::get("DURATION"),
				"postedBy" 		=> Translate::get("POSTED_BY"),
				"btns" 			=> ""
		));
		$oTable->addExtra("title2", '<a href="https://youtube.com/watch?v=%link%" target="_blank">%title%</a>');
		$oTable->addExtra("btns", 
			'<button onclick="addToPlaylist(%idSongrequest%)" class="btn btn-success" title="Zur Playlist hinzufügen"><i aria-hidden="true" class="fa fa-plus-circle"></i></button> '.
			'<button onclick="removeFromPlaylist(%idSongrequest%)" class="btn btn-success" title="Von Playlist entfernen"><i aria-hidden="true" class="fa fa-minus-circle"></i></button> '.
			'<a href="https://youtube.com/watch?v=%link%" target="_blank" class="btn btn-info"><span aria-hidden="true" class="glyphicon glyphicon-new-window"></span></a> '.
			'<button onclick="deleteSongQuestion(%idSongrequest%)" class="btn btn-danger">X</button>'
		);
		
		$oSongrequest->addOrder("inPlaylist");
		$oSongrequest->addOrder("idSongrequest");
		$aData = $oSongrequest->getAll();
		$oTable->setData($aData);
		
		View::tableObject( $oTable );
		
		$iHour = 0;
		$iMinutes = 0;
		$iSeconds = 0;
		foreach ($aData AS $aSong) {
			$aTimes = explode(":", $aSong["duration"]);
			if (count($aTimes) == 3) {
				$iHour 		= $iHour+intval($aTimes[0]);
				$iMinutes 	= $iMinutes+intval($aTimes[1]);
				$iSeconds 	= $iSeconds+intval($aTimes[2]);
				
			} elseif (count($aTimes) == 2) {
				$iMinutes 	= $iMinutes+intval($aTimes[0]);
				$iSeconds 	= $iSeconds+intval($aTimes[1]);
				
			} elseif (count($aTimes) == 1) {
				$iSeconds 	= $iSeconds+intval($aTimes[0]);
				
			}
			
			if ($iSeconds >= 60) {
				$iMinutes++;
				$iSeconds = $iSeconds-60;
			}
			if ($iMinutes >= 60) {
				$iHour++;
				$iMinutes = $iMinutes-60;
			}
		}
		
		View::totalDuration($iHour.":".$iMinutes.":".$iSeconds);
		
		

		// $oForm = new FormHelper(LinkHelper::modul("songrequest"), "post", "_self", "");
		$oForm = new FormHelper("#", "", "", "");
		$oForm->addElement("text", 		"id",		"", 						Translate::get("YOUTUBE")." ID",	array(), array("col-md-2", "col-md-9"));
		$oForm->addElement("button", 	"addBtn", 	Translate::get("ADD"),		"", 								array(), "col-md-12");
		// $oForm->addElement("hidden", 	"action", 	"add");
		
		View::formObject($oForm);
	}

	public function render() {
		View::navi("songrequest");
		View::title("songrequest");
		View::render("songrequest");
	}
	
	public function add() {
		$oSongrequest = Factory::getModel("Songrequest");
		if ($oSongrequest->addByYoutubeId(Params::postParams("id"), Params::cookieParams("username"))) {
 			LogModelLog::addLog("admin", "songrequest", Params::cookieParams("username"), "Insert id #".$oSongrequest->getLastInsertId());
		}	
	}
}