{{if View::currentSong() != ""}}
	{{assign var="aSong" value=View::currentSong()}}
	<script src="http://www.youtube.com/player_api"></script>
	<script>
		function playNextSong() {
			lastSong(function() {
				deleteSongById(currentSongId, function(data) {
					nextSong();
				});
			});
		}

		function nextSong() {
			$.ajax({
			  url: "{{Config::getServerHost()}}/custom/api?output=json&action=nextSong",
			  // url: "{{Config::getServerHost()}}/custom/api?action=nextSongAdmin&admin={{Params::cookieParams("username")}}",
			}).done(function(data) {
				var response = JSON.parse(data);
				if (response != null && response != undefined) {
					var link = "";
					if (response.source == "youtube") {
						link += "https://www.youtube.com/watch?v={VIEDOID}";
					}
					link = link.replace("{VIEDOID}", response.link);
					
					var songData = "";
					songData += "{{Translate::say("ID")}}"+response.idSongrequest+"<br />";
					songData += "{{Translate::say("SONGTITLE")}}: <a href='"+link+"' target='_blank'>"+response.title+"</a><br />";
					songData += "{{Translate::say("POSTED_BY")}}: "+response.postedBy+"<br />";
					songData += "{{Translate::say("DURATION")}}: "+convertTime("H:i:s", response.duration); 

					$("#currendSongData").find(".panel-body").html( songData );
					
					player.loadVideoById({'videoId': response.link});
					currentSongId = response.idSongrequest;

					refreshPlaylist();
				}
			});
		}

		function lastSong (successFuction) {
			$.ajax({
			  // url: "{{Config::getServerHost()}}/api/custom/currentSong?output=json",
			  url: "{{Config::getServerHost()}}/api/custom/getSongById?output=json&songId="+currentSongId,
			}).done(function(data) {
				var response = JSON.parse(data);
				if (response != null && response != undefined) {
					var link = "";
					if (response.source == "youtube") {
						link += "https://www.youtube.com/watch?v={VIEDOID}";
					}
					link = link.replace("{VIEDOID}", response.link);
					
					var songData = "";
					songData += "{{Translate::say("ID")}}"+response.idSongrequest+"<br />";
					songData += "{{Translate::say("SONGTITLE")}}: <a href='"+link+"' target='_blank'>"+response.title+"</a><br />";
					songData += "{{Translate::say("POSTED_BY")}}: "+response.postedBy+"<br />";
					songData += "{{Translate::say("DURATION")}}: "+convertTime("H:i:s", response.duration); 
					$("#lastSongData").find(".panel-body").html( songData );
					
					refreshPlaylist();
					if (typeof successFuction == "function") {
						successFuction();
					}				
				}
			});
		}
	
		// create youtube player
		var player;
		function onYouTubePlayerAPIReady() {
			currentSongId = {{$aSong["idSongrequest"]}};
			player = new YT.Player('player', {
				height: '390',
				width: '640',
				videoId: '{{$aSong["link"]}}',
				events: {
					'onReady': function(event) {
						event.target.playVideo();
					},
					'onStateChange': function(event) {
						if (event.data === 0) {
							playNextSong();							
						}
					}
				}
			});
		}
	</script>

	
	<div class="row">
		<div id="player" class="col-md-12"></div>
	</div>
	
	<br />
	
	<div id="currendSongData" class="panel panel-default">
		<div class="panel-heading">{{Translate::say("CURREND_SONG")}}</div>
		<div class="panel-body">
			{{if View::currentSong() != ""}}
				{{assign var="aSong" value=View::currentSong()}}
			{{else}}
				{{php}}
					View::aSong(array(
						"idSongrequest" => "",
						"link" => "",
						"title" => "",
						"postedBy" => "",
						"duration" => ""
					));
				{{/php}}
			{{/if}}
			
			
			{{assign var="sLink" value=""}}
			{{if $aSong.source == "youtube"}}
				{{assign var="sLink" value=$sLink|cat:"https://www.youtube.com/watch?v={VIEDOID}"}}
			{{/if}}
			{{assign var="sLink" value=$sLink|replace:"{VIEDOID}":$aSong.link}}
			{{Translate::say("ID")}}{{$aSong.idSongrequest}}<br />
			{{Translate::say("SONGTITLE")}}: <a href="{{$sLink}}" target="_blank">{{$aSong.title}}</a><br />
			{{Translate::say("POSTED_BY")}}: {{$aSong.postedBy}} <br />
			{{Translate::say("DURATION")}}: {{$aSong.duration}}
		</div>
		
		<div class="panel-footer">
			<button class="btn btn-warning" onClick="playNextSong()">{{Translate::say("NEXT")}}</button>
		</div>
		
	</div>
	
	<div id="lastSongData" class="panel panel-default">
		<div class="panel-heading">{{Translate::say("LAST_SONG")}}</div>
		<div class="panel-body"></div>
	</div>
{{/if}}