{{View::render("header_admin")}}

<div class="col-md-7" id="songrequestList">
	{{View::formObject()->render()}}

	<script>
		var selectedSongId 	= "";
		var refreshVideo 	= 0;
		var currentSongId	= "";
		refreshPlaylist();
		
		function deleteSongQuestion(iSongId, blRefreshVideo) {
			selectedSongId = iSongId;
			refreshVideo = blRefreshVideo;
			$('#myModal').modal('show');
		}

		function successModal() {
			deleteSongByAdmin();
		}
		
		function deleteSong() {
			$('#myModal').modal('hide');
			if (typeof nextSong == 'function' && refreshVideo == 1) {
				nextSong();
			} else {
				deleteSongById(selectedSongId, function(data) {
					var response = JSON.parse(data);
					if (response != null && response != undefined && response.status == "success") {
						refreshPlaylist();
					}

					selectedSongId 	= "";
					refreshVideo 	= 0;
				})
			}
		}

		function deleteSongByAdmin() {
			$('#myModal').modal('hide');
			if (typeof nextSong == 'function' && refreshVideo == 1) {
				nextSong();
			} else {
				$.ajax({
					method: "POST",
					url: "{{Config::getServerHost()}}/custom/api/songrequest.php?output=json&action=deleteSongByAdmin",
					data: { songId: selectedSongId, admin: "{{Params::cookieParams("username")}}"}
				}).done(function(data) {
					var response = JSON.parse(data);
					if (response != null && response != undefined && response.status == "success") {
						refreshPlaylist();
					}

					selectedSongId 	= "";
					refreshVideo 	= 0;
				});
			}
		}

		function deleteSongById(id, successFunction) {
			$.ajax({
				method: "POST",
				url: "{{Config::getServerHost()}}/custom/api/songrequest.php?output=json&action=deleteSong",
				data: {songId: id}
			}).done(function(data) {
				if (typeof successFunction == "function") {
					successFunction(data);
				}
			});
		}
	
		function addSong(sYoutubeId) {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?output=json&action=addSong",
				data: { songId: sYoutubeId, admin: "{{Params::cookieParams("username")}}"}
			}).done(function(data) {
				refreshPlaylist();
			});
		}
	
		function addToPlaylist(iSongId) {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?output=json&action=addToPlaylist",
				data: { songId: iSongId, admin: "{{Params::cookieParams("username")}}"}
			}).done(function(data) {
				refreshPlaylist();
			});
		}

		function removeFromPlaylist(iSongId) {
			$.ajax({
				url: "{{Config::getServerHost()}}/custom/api/?output=json&action=removeFromPlaylist",
				data: { songId: iSongId, admin: "{{Params::cookieParams("username")}}"}
			}).done(function(data) {
				refreshPlaylist();
			});
		}

		function refreshPlaylist() {
			$.ajax({
			  	url: "{{Config::getServerHost()}}/custom/api/songrequest.php?output=json&action=allSongs",
			}).done(function(data) {
				console.log(data);
				var response = JSON.parse(data);
				if (response != null && response != undefined) {
					$("#songrequestList").find("table").find("tbody").empty();

					var newList = "";
					var blFirst = 1;
					var iHour = 0;
					var iMinutes = 0;
					var iSeconds = 0;
					for (var i=0; i<response.length; i++) {
						newList += "<tr>";
							newList += "<td>"+response[i].idSongrequest+"</td>";
							newList += "<td><a href='https://youtube.com/watch?v="+response[i].link+"' target='_blank'>"+response[i].title+"</a></td>";
							newList += "<td>"+convertTime("H:i:s",response[i].duration)+"</td>";
							newList += "<td>"+response[i].postedBy+"</td>";
							newList += "<td>";
								if (response[i].inPlaylist == "0") {
									newList += "<button onclick='addToPlaylist("+response[i].idSongrequest+")' class='btn btn-success' title='Zur Playlist hinzufügen'><i aria-hidden='true' class='fa fa-plus-circle'></i></button> ";
								} else {
									newList += "<button onclick='removeFromPlaylist("+response[i].idSongrequest+")' class='btn btn-danger' title='Von Playlist entfernen'><i aria-hidden='true' class='fa fa-minus-circle'></i></button> ";
								}
								newList += "<a href='https://youtube.com/watch?v="+response[i].link+"' target='_blank' class='btn btn-info' title='Video öffnen'><span aria-hidden='true' class='glyphicon glyphicon-new-window'></span></a> ";
								newList += "<button onclick='deleteSongQuestion("+response[i].idSongrequest+", "+blFirst+")' class='btn btn-danger' title='entfernen'>X</button>";
							newList += "</td>";
						newList += "</tr>";

						var aTimes = response[i].duration.split(":");
						
						// Wenn leerer String bug
						for(var o=0; o<aTimes.length; o++) {
							if (aTimes[o] == "") {
								aTimes[o] = 0;
							}
						}
						
						if (aTimes.length == 3) {
							iHour 		= iHour+parseInt(aTimes[0]);
							iMinutes 	= iMinutes+parseInt(aTimes[1]);
							iSeconds 	= iSeconds+parseInt(aTimes[2]);

						} else if (aTimes.length == 2) {
							iMinutes 	= iMinutes+parseInt(aTimes[0]);
							iSeconds 	= iSeconds+parseInt(aTimes[1]);

						} else if (aTimes.length == 1) {
							iSeconds 	= iSeconds+parseInt(aTimes[0]);

						}
						
						if (iSeconds >= 60) {
							iMinutes++;
							iSeconds = iSeconds-60;
						}
						if (iMinutes >= 60) {
							iHour++;
							iMinutes = iMinutes-60;
						}
						blFirst = 0;
					}

					newList += "<tr>";
						newList += "<td colspan='2'></td>";
						newList += "<td id='totalDuration' colspan='3'></td>";
					newList += "</tr>";
					
					$("#songrequestList").find("table").find("tbody").html(newList);
					$("#totalDuration").html(convertTime("H:i:s", iHour+":"+iMinutes+":"+iSeconds));
					
					newList = null;
				}
			});
		}
	
		var playlistrefresher = setInterval(function() {
			refreshPlaylist();
		}, 10000);

		function convertTime(sPattern, sTime) {
			var aTime = sTime.split(":");
			var sNewTime = sPattern;


			for (var i=0; i<aTime.length; i++) {
				if (parseInt(aTime[i]) < 10) {
					aTime[i] = "0"+aTime[i];
				}
			}
			
			
			// Sekunden
			if (aTime.length == 1) {
				sNewTime = sNewTime.replace("s", aTime[0]);

				
			// Minuten : Sekundne
			} else if (aTime.length == 2) {
				sNewTime = sNewTime.replace("i", aTime[0]);
				sNewTime = sNewTime.replace("s", aTime[1]);

				
			// Stunden : Minuten : Sekunden
			} else if (aTime.length == 3) {
				sNewTime = sNewTime.replace("H", aTime[0]);
				sNewTime = sNewTime.replace("i", aTime[1]);
				sNewTime = sNewTime.replace("s", aTime[2]);

			}

			sNewTime = sNewTime.replace("H", "");
			sNewTime = sNewTime.replace("i", "");
			sNewTime = sNewTime.replace("s", "");
		
			return sNewTime;
		}


		$(document).ready(function() {
			$("#addBtn").on("click", function() {
				var sYoutubeId = $("#id").val();
				if (sYoutubeId != "") {
					addSong(sYoutubeId);
					$("#id").val("")
				}
			});
		});
	</script>
	{{View::tableObject()->render()}}
	
	<!-- Modal -->
	{{php}}
		View::modalTitle(Translate::get("SONGREQUEST_MODAL_TITLE"));
		View::modalText(Translate::get("SONGREQUEST_MODAL_TEXT"));
	{{/php}}
	{{View::render("modal")}}
</div>

<div class="col-md-5">
	{{View::render("songrequest_videoblock_admin")}}
	{{View::render("songrequest_panel_notic")}}
</div>

{{View::render("footer")}}