{{if count(View::tableObject()->getData()) >= 1}}
	<!-- Table -->
	<table class="table table-striped table-hover table-responsive">
		{{assign var="aKeys" value=View::tableObject()->getUseingKeys()}}
		
		<!-- Head -->
		<thead>
			<tr>
				{{foreach from=$aKeys item=$sName}}
					<td>{{$sName}}</td>
				{{/foreach}}
			</tr>
		</thead>

		<!-- Body -->
		<tbody>
			{{foreach from=View::tableObject()->getData() item=$aRow}}
				<tr>
					{{foreach from=$aKeys key=$sKey item=$sName}}
						<td>
							{{if isset($aRow.$sKey)}}
								{{$aRow.$sKey}}
							{{else: ?>
								{{View::tableObject()->getSingleExtra($sKey, $aRow)}}
							{{/if}}
						</td>
					{{/foreach}}
				</tr>
			{{/foreach}}
		</tbody>

		<!-- Footer -->
		<tfoot>
			<tr>
				<td colspan="2"></td>
				<td id="totalDuration" colspan="3">{{View::totalDuration()}}</td>
			</tr>
		</tfoot>
	</table>
{{/if}}