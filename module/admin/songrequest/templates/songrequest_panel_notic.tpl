<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("NOTE")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("LINKS")}}: <br />
		{{Config::getServerHost()}}/api/custom/currentSong <br />
		<br />
		{{Translate::say("PARAMETERS")}}:<br />
		- {{Translate::say("SONGREQUEST_PARAMETERS_T")}}<br />
		- {{Translate::say("SONGREQUEST_PARAMETERS_C")}}<br />
		- {{Translate::say("SONGREQUEST_PARAMETERS_BG")}}<br /> 
		- {{Translate::say("SONGREQUEST_PARAMETERS_TPL")}}<br /> 
	</div>
</div>