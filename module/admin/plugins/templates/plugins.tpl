{{View::render("header_admin")}}

<div class="col-md-12">
	{{View::formObject()->render()}}
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}
	
	{{assign var="sModalLink" value=LinkHelper::_("plugins", "regist")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}