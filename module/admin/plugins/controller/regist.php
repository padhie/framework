<?php
class PluginsControllerRegist extends eAdminController implements iController {
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"idPluginsRegist"	=> Translate::get("ID"),
				"modul"				=> Translate::get("MODUL"),
				"plugin" 			=> Translate::get("PLUGIN"),
				"settings" 			=> Translate::get("SORT"),
				"active" 			=> Translate::get("AKTIV"),
				"btns" 				=> ""
		));
		$oTable->addExtra("btns",
			'<a href="'.LinkHelper::_("plugins", "regist").'?action=load&id=%idPluginsRegist%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
			'<button onclick="openModal(%idPluginsRegist%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>'
		);

		$oPluginsRegist = Factory::getModel("regist");
		$aData = $oPluginsRegist->getAll();
		for ($i=0; $i<count($oPluginsRegist->getAll()); $i++) {
			$sSettings = "";
			foreach (json_decode($aData[$i]["settings"], true) AS $sKey => $sRow) {
				$sSettings .= $sKey."=".$sRow."&";
			}
			$aData[$i]["settings"] = substr($sSettings, 0, -1);
		}
		$oTable->setData($aData);
		View::tableObject( $oTable );
		unset($oPluginsRegist, $oTable);
		

		if (View::formObject() === null) {
			$oForm = $this->getForm(array(), array());
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("plugins");
		View::title("Plugins");
		View::render("plugins");
	}

	public function action($action) {
		$oRegist = Factory::getModel("regist", "plugins");
		switch($action) {
			case "load":
				$aRegistData = $oRegist->getById(intval( Params::getParams("id") ));
				$aOptionData=array();
				if ($aRegistData["active"]=="1") {
					$aOptionData["checked"] = "checked";
				}

				$aRegistData["plugin"] = $aRegistData["modul"]."_".$aRegistData["plugin"];
				$sSettings = "";
				foreach (json_decode($aRegistData["settings"], true) AS $sKey => $sRow) {
					$sSettings .= $sKey."=".$sRow."&";
				}
				$aRegistData["settings"] = substr($sSettings, 0, -1);

				$oForm = $this->getForm($aRegistData, $aOptionData);
				View::formObject($oForm);
				unset($oForm, $aRegistData);
				break;
					
			case "new":
			case "save":
				list($sModul, $sPlugin) = explode("_", Params::postParams("plugin","_"));
				$aSingleSetting = explode("&", Params::postParams("settings",""));
				$aSettings = "";
				foreach ($aSingleSetting AS $sRow) {
					$aRowData = explode("=", $sRow);
					if (isset($aRowData[0]) && isset($aRowData[1])) {
						$aSettings[$aRowData[0]] = $aRowData[1];
					}
				}

				$aData = array(
					"idPluginsRegist"	=> Params::postParams("id", 0),
					"idPluginsPosition"	=> Params::postParams("idPluginsPosition", 0),
					"modul"				=> $sModul,
					"plugin"			=> $sPlugin,
					"active"			=> (Params::postParams("active",false)!=false?1:0),
					"settings"			=> json_encode($aSettings),
				);
			
				if (Params::postParams("id") !== false) {
					LogModelLog::addLog("admin", "plugins:regist", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
					$oRegist->update($aData, intval( Params::postParams("id") ));
				} else {
					$iLastId = $oRegist->insert($aData);
					LogModelLog::addLog("admin", "plugins:regist", Params::cookieParams("username"), "Insert id #".$iLastId);
				}
				break;


			case "delete":
				LogModelLog::addLog("admin", "plugins:regist", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
				$oRegist->delete(intval( Params::getParams("id") ));
				break;
		}
		unset($oRegist);
	}
 
	public function getForm($aData=array(), $aOptions=array()) {
		$aData = Helper::prepareArray($aData, "idPluginsPosition,plugin,active,idPluginsRegist,settings");

		$oPosition = Factory::getModel("position", "plugins");
		$aPositions = $oPosition->getAll();
		$aPositions = Helper::createSelectArray($aPositions, "idPluginsPosition", "name");
		unset($oPosition);

		$aPlugins = array();
		foreach (Config::$aModules AS $sModul) {
			$_aTmpPluginData = ModulHelper::getPluginListOfModul($sModul);
			if (count($_aTmpPluginData) > 0) {
				foreach ($_aTmpPluginData AS $aPlugin) {
					array_push($aPlugins, array(
						"value"	=> $aPlugin["name"],
						"key"	=> $sModul."_".$aPlugin["name"],
						"group"	=> $sModul
					));
				}
			}
		}

		$oForm = new FormHelper(Config::getServerHost()."/admin/plugins/regist", "post");
		$oForm->addElement("select",		"idPluginsPosition",	$aData["idPluginsPosition"], 	Translate::get("POSITION"),		$aPositions,	array("col-md-2", "col-md-9"));
		$oForm->addElement("selectgroup", 	"plugin", 				$aData["plugin"],				Translate::get("NAME"),			$aPlugins,		array("col-md-2", "col-md-9"));
		$oForm->addElement("switch", 		"active", 				$aData["active"],				Translate::get("AKTIV"),		$aOptions,		array("col-md-2", "col-md-9"));
		$oForm->addElement("text", 			"settings",				$aData["settings"],				Translate::get("SETTINGS"),		array(),		array("col-md-2", "col-md-9"));
		$oForm->addElement("hidden", 		"id", 					$aData["idPluginsRegist"]);
		$oForm->addElement("hidden", 		"action",				"save");
		$oForm->addElement("submit", 		"", 					Translate::get("SAVE"));
		return $oForm;
	}
}