<?php

class PluginsModelRegist extends DbExtent {
	protected $_sTable = "plugins_regist";
	protected $_sIdField = "idPluginsRegist";

	public function addJoin() {
		$this->_oQueryBuilder->addJoin("LEFT OUTER", "plugins_positionen.p", "idPluginsPosition");
	}

	public function getAllData() {
		$this->addJoin();
		return parent::getAll();
	}
}