<?php

class PluginsConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"plugins",
			"name"=>Translate::get("MENU_ADMIN_PLUGINS"),
			"link"=>LinkHelper::_("plugins", "regist"),
			"color"=>"warning",	
			"icon"=>"fa-plus"
		);
	}

	// public function getDefaultDashboardConfig() {
	// 	if (in_array("quotes", View::adminRights())){
	// 		$oQuotes = Factory::getModel("Quote", "Quotes");
	// 		return array(
	// 			"name"	=> Translate::get("MENU_ADMIN_QUOTES"),
	// 			"msg"	=> count($oQuotes->getAll())." ".Translate::get("QUOTES"),
	// 			"link" 	=> LinkHelper::_("quotes"),
	// 			"color"	=> "warning"
	// 		);
	// 	}
	// }

	public function getRights() {
		return array(
			"regist" => Translate::get("MENU_ADMIN_PLUGINS"),
		);
	}
}