<?php

class CounterConfig extends ModulConfig {
	public function getMenuConfig() {
		return array(
			"key"=>"counter", 	
			"name"=>Translate::get("MENU_ADMIN_COUNTER"), 		
			"link"=>LinkHelper::modul("counter"),		
			"color"=>"success",	
			"icon"=>"fa-calendar"
		);
	}

	public function getRights() {
		return array(
			"counter"		=> Translate::get("MENU_ADMIN_COUNTER"),
		);
	}
}