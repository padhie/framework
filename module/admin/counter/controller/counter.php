<?php
class CounterControllerCounter extends eAdminController implements iController {
	public function start() {
		parent::loadModulClasses("counter");
		
		parent::start();
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(18);
		$oTable->setUsingKeys(array(
				"idCounter" 	=> Translate::get("ID"),
				"title" 		=> Translate::get("TITLE"),
				"group" 		=> Translate::get("GROUP"),
				"deadline" 		=> Translate::get("DEADLINE"),
				"btns" 			=> ""
		));
		$oTable->addExtra("btns",
				'<a href="'.Config::getServerHost().'/admin/counter?action=load&id=%idCounter%" class="btn btn-info">'.Translate::get("EDIT").'</a> '.
				'<button onclick="openModal(%idCounter%)" class="btn btn-danger">'.Translate::get("DELETE").'</button>');
		
		$oCounter = Factory::getModel("Counter");
		$oTable->setData($oCounter->getAll());
		unset($oCounter);
		View::tableObject( $oTable );
		
		
		$oCounter = Factory::getModel("Counter");
		View::counters($oCounter->getAll());
		unset($oCounter);
		
		
		if( View::formObject() === null) {
			$oForm = new FormHelper(Config::getServerHost()."/admin/counter?action=save");
			$oForm->addElement("text", 		"counter_title", 	"",						Translate::get("TITLE"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"counter_group", 	"",						Translate::get("GROUP"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"counter_deadline", "",						Translate::get("DEADLINE"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"id", 				"");
			$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),	"",							"",	"col-md-12");
			View::formObject($oForm);
			unset($oForm);
		}
	}

	public function render() {
		View::navi("counter");
		View::title("counter");
		View::render("counter");
	}
	
	public function save() {
		$oCounter = Factory::getModel("Counter");
		$aData = array(
			"title" 	=> Params::postParams("counter_title", "unknown"),
			"group" 	=> Params::postParams("counter_group", ""),
			"deadline"	=> Params::postParams("counter_deadline", date("Y-m-d H:i:s"))
		);
		if (Params::postParams("id") !== false) {
			if ($oCounter->update($aData, intval( Params::postParams("id") ))) {
				View::success("Update success");
				LogModelLog::addLog("admin", "counter", Params::cookieParams("username"), "Update id #".Params::postParams("id"));
			} else {
				View::error("Update failed: <br />".$oCounter->getError());
			}
		} else {
			if ($oCounter->insert($aData)) {
				View::success("Insert success");
				LogModelLog::addLog("admin", "counter", Params::cookieParams("username"), "Insert id #".$oCounter->getLastInsertId());
			} else {
				View::error("Insert failed: <br />".$oCounter->getError());
			}
		}
		unset($oCounter);
	}
	public function load() {
		if (Params::getParams("id")) {
			$oCounter = Factory::getModel("Counter");
			$aCounter = $oCounter->getById(intval(Params::getParams("id")));
			
			$oForm = new FormHelper(Config::getServerHost()."/admin/counter?action=save");
			$oForm->addElement("text", 		"counter_title", 	$aCounter["title"],		Translate::get("TITLE"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"counter_group", 	$aCounter["group"],		Translate::get("GROUP"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("text", 		"counter_deadline", $aCounter["deadline"],	Translate::get("DEADLINE"),	"",	array("col-md-2", "col-md-9"));
			$oForm->addElement("hidden", 	"id", 				$aCounter["idCounter"]);
			$oForm->addElement("submit", 	"", 				Translate::get("SAVE"),	"",							"",	"col-md-12");
			View::formObject($oForm);
			unset($oForm, $oCounter);
		} else {
			View::error("Missing ID!");
		}
	}
	public function delete() {
		$oCounter = Factory::getModel("Counter");
		$oCounter->delete( intval( Params::getParams("id") ) );
		LogModelLog::addLog("admin", "counter", Params::cookieParams("username"), "Delete id #".Params::getParams("id"));
		unset($oCounter);
	}
}