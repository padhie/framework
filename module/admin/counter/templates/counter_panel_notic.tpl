<div class="panel panel-info">
	<div class="panel-heading">
		<h3 class="panel-title">{{Translate::say("NITE")}}</h3>
	</div>
	<div class="panel-body">
		{{Translate::say("LINKS")}}: <br />
		{{Config::getServerHost()}}/custom/api?action=counter <br />
		<br />
		{{Translate::say("PARAMETERS")}}:<br />
		- {{Translate::say("COUNTER_PARAMETERS_G")}}<br />
		- {{Translate::say("COUNTER_PARAMETERS_L")}}<br />
		- {{Translate::say("COUNTER_PARAMETERS_O")}}<br />
	</div>
</div>