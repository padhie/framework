{{View::render("header_admin")}}

<div class="col-md-12">
	<div class="row">
		<div class="col-md-9">
			{{View::formObject()->render()}}
		</div>
		
		<div class="col-md-3">
			{{View::render("counter_panel_notic")}}
		</div>
	</div>
	
	<br /><hr />
</div>

<div class="col-md-12">
	{{View::tableObject()->render()}}

	{{assign var="sModalLink" value=LinkHelper::_("counter")}}
	{{include file="modalbox.tpl"}}
</div>

{{View::render("footer")}}