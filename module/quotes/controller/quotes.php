<?php
class QuotesControllerQuotes extends eController implements iController {
	public function start() {
		parent::start();
		parent::loadModulClasses("rangs");
	}
	
	public function run() {
		$oTable = new TableHelper();
		$oTable->blShowHits = false;
		$oTable->setPage(Params::getParams("p", 1));
		$oTable->setPerPage(30);
		$oTable->setUsingKeys(array(
				"index" 	=> Translate::get("ID")
								.' <a href="'.Config::getServerHost().'/quotes?field=index&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
								.' <a href="'.Config::getServerHost().'/quotes?field=index&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
				"text" 		=> Translate::get("QUOTETEXT"),
				"user" 		=> Translate::get("FROM")
								.' <a href="'.Config::getServerHost().'/quotes?field=user&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
								.' <a href="'.Config::getServerHost().'/quotes?field=user&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
				"time"		=> Translate::get("DATE")
								.' <a href="'.Config::getServerHost().'/quotes?field=time&sort=desc"><i class="fa fa-angle-down" aria-hidden="true"></i></a>'
								.' <a href="'.Config::getServerHost().'/quotes?field=time&sort=asc"><i class="fa fa-angle-up" aria-hidden="true"></i></a>',
				// "minRang" 	=> Translate::get("MIN_RANG")
		));
		
		$oQuote = Factory::getModel("Quote");
		$sSort = "asc";
		if (Params::getParams("sort") == "desc") {
			$sSort = "desc";
		}
		switch (Params::getParams("field")) {
			case 'index':		$oQuote->setOrder("index", $sSort);			break;
			case 'user':		$oQuote->setOrder("user", $sSort);			break;
			case 'time':		$oQuote->setOrder("time", $sSort);			break;
			default:			$oQuote->setOrder("index", $sSort);			break;
		}

		$aData = $oQuote->getAllPublic();
		for ($i=0; $i<count($aData); $i++) {
			$aData[$i]["time"] = date("d.m.Y", strtotime($aData[$i]["time"]));
		}
		$oTable->setData($aData);
		unset($aData);
		
		View::tableObject( $oTable );
	}

	public function render() {
		View::navi("quotes");
		View::title("Quotes");
		
		parent::render();
	}
}