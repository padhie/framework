<?php

class QuotesModelQuote extends DbExtent {
	protected $_sTable = "quotes";
	protected $_sIdField = "idQuote";
	
	public function getMinRang($sMinRang) {
		$this->_oQueryBuilder->addWhere("minRang", $sMinRang);
		return $this->getAll();
	}
	
	public function getAllFuelData() {
		$oRang = Factory::getModel("Rangs","Rangs");
		$aData = $this->getAll();
		
		
		for ($i=0; $i<count($aData); $i++) {
			$aData[$i]["minRang"] = "";
			$aQuoteData = $this->getRangsOfQuote($aData[$i]["idQuote"]);
			$aData[$i]["minRang"] = $oRang->createRangTextByData($aQuoteData);
		}
		
		return $aData;
	}
	
	public function getAllPublic() {
		$this->_oQueryBuilder->addWhere("isPublic", 1);
		return $this->getAllFuelData();
	}
	
	public function getRangsOfQuote($idQuote) {
		$this->_oQueryBuilder->setTable("quote_has_rang");
		$this->_oQueryBuilder->addJoin("LEFT JOIN", "rang", "idRang");
		$this->_oQueryBuilder->addWhere("idQuote", $idQuote);
	
		$aData = parent::getAll();
		$this->init();
		return $aData;
	}
	
	public function changeQuoteToRang($idQuote, $aRangIds=array()) {
		$this->_oQueryBuilder->setTable("quote_has_rang");
		$this->delete($idQuote);
	
		foreach ($aRangIds AS $iIdRang) {
			$this->insert(array(
					"idQuote" => $idQuote,
					"idRang" => $iIdRang
			));
		}
	
		$this->init();
	}
}