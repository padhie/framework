<?php
include __DIR__."/config.php";


// ####################
// INTRO
echo "###########################".NL;
echo "#####                 #####".NL;
echo "#####  MODULE Sample  #####".NL;
echo "#####                 #####".NL;
echo "###########################".NL;

$aData = $aConfig;
// ####################
// PREPARE CONTENT
echo "Prepare Template starting!".NL;
$aReplaceVars 	= array(
	"%DATABASE%"	=> strtoupper(DATABASE),		"%Database%"	=> ucfirst(strtolower(DATABASE)),		"%database%"	=> strtolower(DATABASE),
	"%FILENAME%"	=> strtoupper(FILENAME),		"%Filename%"	=> ucfirst(strtolower(FILENAME)),		"%filename%"	=> strtolower(FILENAME),
	"%MODULENAME%"	=> strtoupper(MODULENAME),		"%Modulename%"	=> ucfirst(strtolower(MODULENAME)),		"%modulename%"	=> strtolower(MODULENAME),
);
foreach ($aData["templates"] AS $sKey => $sVal) {
	$aData["templates"][$sKey] = replace($sVal, $aReplaceVars);
	echo "- prepare '".$sKey."'".NL;
}
echo "Prepare Template finish!".NL.NL;


// ####################
// CREATE CURRENTDIRECTORY
echo "Create Directorys starting!".NL;
foreach ($aData["directorys"] AS $sKey => $sVal) {
	$sVal = str_replace("\\", "/", $sVal);
	dirCreate($sVal);
	echo "- create '".$sVal."'".NL;
}
foreach ($aData["files"] AS $sKey => $sVal) {
	$sVal = str_replace("\\", "/", $sVal);
	dirCreate(substr($sVal, 0, strrpos($sVal, "/")));
	echo "- create '".substr($sVal, 0, strrpos($sVal, "/"))."'".NL;
}
echo "Create Directorys finish!".NL.NL;


// ####################
// FILL FILE
echo "Fill Files starting!".NL;
foreach ($aData["template_in_file"] AS $sKey => $sVal) {
	dirCreate($aData["template_in_file"][$i]);
	file_put_contents($aData["files"][$sVal], $aData["templates"][$sKey], FILE_APPEND);
	echo "- fill '".$aData["files"][$sVal]."'".NL;
}
echo "Fill Files finish!".NL.NL;