<?php
// ####################
// ### CLI || BROWSER
if (php_sapi_name() === 'cli') {
	define("NL", "\r\n");
	
	if (count($argv) <= 1) {
		die( "Missing Parameter!" );
	}
	
	if ($argv[1] == "help") {
		die( "-mModulname".NL."[-fFilenames]".NL."[-dbDatabaseTablename]".NL."[-tTemplatename]" );
	}
		
	for ($i=0; $i<count($argv); $i++) {		
		if (strpos($argv[$i], "-m") === 0) {
			$MODULENAME = substr($argv[$i], 2);
			
			if (!isset($FILENAME)) {
				$FILENAME = $MODULENAME;
			}
			
			if (!isset($TEMPLATE)) {
				$TEMPLATE = $MODULENAME;
			}
			
			if (!isset($DATABASE)) {
				$DATABASE = $MODULENAME;
			}
		}
		
		if (strpos($argv[$i], "-f") === 0) {
			$FILENAME = substr($argv[$i], 2);
		}
		
		if (strpos($argv[$i], "-t") === 0) {
			$TEMPLATE = substr($argv[$i], 2);
		}
		
		if (strpos($argv[$i], "-db") === 0) {
			$DATABASE = substr($argv[$i], 3);
		}
		
	}
	
	if (!isset($MODULENAME)) {
		die( "Missing Parameter!" );
	}
} else {
	define("NL", "<br />");
	
	if (!isset($_GET["m"]) || !isset($_GET["f"]) || !isset($_GET["t"]) || !isset($_GET["db"])) {
		die( "Missing Parameter! ".NL."m=Modulname f=Filenames db=DatabaseTablename t=Templatename" );
	}
	
	if (isset($_GET["m"])) {
		$MODULENAME = $_GET["m"];
			
		if (isset($_GET["f"])) {
			$FILENAME = $_GET["f"];
		} else {
			$FILENAME = $_GET["m"];
		}
			
		if (isset($_GET["t"])) {
			$TEMPLATE = $_GET["t"];
		} else {
			$TEMPLATE = $_GET["m"];
		}
			
		if (isset($_GET["db"])) {
			$DATABASE = $_GET["db"];
		} else {
			$DATABASE = $_GET["m"];
		}
	} else {
		die( "Missing Parameter! ".NL."m=Modulname [f=Filenames] [db=DatabaseTablename] [t=Templatename]" );
	}
}


// ####################
// ### CONSTANTS
define("MODULENAME", 	$MODULENAME);
define("FILENAME", 		$FILENAME);
define("TEMPLATE", 		$TEMPLATE);
define("DATABASE", 		$DATABASE);
unset($MODULENAME, $FILENAME, $TEMPLATE, $DATABASE);


// ####################
// ### FUNCTIONS
function replace($sContent, $aReplaces) {
	foreach ($aReplaces AS $sKey => $sValue) {
		$sContent = str_replace($sKey, $sValue, $sContent);
	}
	return $sContent;
}

function dirCreate($sPath) {
	$sPath = str_replace("\\", "/", $sPath);
	$sCurrentPath = "";
	$aParts = explode("/", $sPath);
	for ($i=0; $i<count($aParts); $i++) {
		$sCurrentPath .= $aParts[$i]."/";
		if (!file_exists($sCurrentPath)) {
			mkdir($sCurrentPath);
		}
	}
}