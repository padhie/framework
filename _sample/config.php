<?php
include __DIR__."/init.php";

$aConfig = array(
	"directorys" => array(
		__DIR__."/../module/".MODULENAME."/classes",
		__DIR__."/../module/".MODULENAME."/controller",
		__DIR__."/../module/".MODULENAME."/templates",
	),
	"files" => array(
		__DIR__."/../module/".MODULENAME."/classes/class.".strtolower(DATABASE).".php",
		__DIR__."/../module/".MODULENAME."/controller/".strtolower(FILENAME).".php",
		__DIR__."/../module/".MODULENAME."/templates/".strtolower(TEMPLATE).".php",
	),
	"templates" => array('
<?php
class %Modulename%Model%Database% extends DbExtent {
	protected $_sTable 		= "%database%";
	protected $_sIdField 	= "id%Database%";
}', 
	'<?php
class %Modulename%Controller%Modulename% extends eController implements iController {}',
	'<?php View::render("header"); ?>

<?php View::render("footer"); ?>',
	),
	"template_in_file" => array(
		0 => 0,
		1 => 1,
		2 => 2,
	)
);