<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.pluginByPosition.php
 * Type:     function
 * Name:     pluginByPosition
 * Purpose:  outputs a random magic answer
 * -------------------------------------------------------------
 */
function smarty_function_pluginByPosition($params, &$smarty) {
	$sPosition 		= (isset($params["position"]))?$params["position"]:"";

	$oPlugins = Factory::getModel("regist", "plugins");
	$oPlugins->addWhere("key", $sPosition);
	$oPlugins->addWhere("active", 1);
	$aPlugins = $oPlugins->getAllData();
	unset($oPlugins);

	$sOutput = "";
	foreach ($aPlugins AS $aPluginItem) {
		$sPluginClassname = ModulHelper::getPluginsClassname($aPluginItem["modul"], $aPluginItem["plugin"]);
		$oPluginObj = new $sPluginClassname();

		if ($oPluginObj instanceof PluginCore) {
			$oPluginObj->init();
			$sOutput .= $oPluginObj->getContent(json_decode($aPluginItem["settings"], true));
		}
		unset($oPluginObj);
	}

	echo $sOutput;
}