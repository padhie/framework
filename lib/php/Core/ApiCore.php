<?php
class ApiCore {
	public $sOutputTyp = "";
	public $sTemplate = "";
	
	public function output($mOutput) {
		if (trim($this->sOutputTyp) == "") {
			$this->sOutputTyp = Params::getParams("output", "json");
		}

		switch(strtolower(trim($this->sOutputTyp))) {
			case "raw":
				echo $mOutput;
				break;
			case "template":
				View::outputData($mOutput);
				View::render($this->sTemplate);
				break;
			case "json":
			default:
				$this->generateJsonOutput($mOutput);
				break;
		}
	}
	
	public function generateJsonOutput($mOutput) {
		echo json_encode($mOutput);
	}
	
	public static function error($sError) {
		echo json_encode(array(
			"status" => "error",
			"message" => $sError
		));
	}
}