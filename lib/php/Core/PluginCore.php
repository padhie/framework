<?php

class PluginCore {
	public function init() {
		$sCalledClass = get_called_class();
		$aPluginData = ModulHelper::getInformationsOfClass($sCalledClass);

		View::addTemplatePath($aPluginData["path"]."/../templates");
	}
}