<?php
require_once "../autoloader.php";

$sAction = Params::getParams("action");
if( Validate::isStringNotEmpty($sAction) !== false){
	if (class_exists("Api")) {
		if( method_exists("Api", $sAction) && is_callable(array("Api", $sAction)) ){
			$oApi = new Api();
			echo $oApi->$sAction();
		}else{
			ApiBase::error("Invalid Method!");
		}
	} else {
		ApiBase::error("Api not available!");
	}
} else {
	ApiBase::error("Invalid Method!");
}
die();