<?php

class ConfigCore {
	// DATABASE CONNECTION INFORMATIONS
	public static $sDbHost 			= "localhost";
	public static $sDbUser 			= "";
	public static $sDbPassword 		= "";
	public static $sDbDatabase 		= "";
	public static $sDbCollation 	= "UTF-8";
	
	// MODUL-CONFIGS
	public static $aModules 		= array();

	// PATH
	public static $sModulFolder			= "/module";
	public static $sCustomBase 			= "/custom";
	public static $sCustomClasses 		= "/classes";
	public static $sCustomVendorClasses = "/vendor";
	public static $sCustomFiles 		= "/data";
	public static $sTemplateFolder		= "/templates";

	
	// METHODES
	public static function bootstrap() {}
	
	public static function getDocumentRoot () {
		return $_SERVER["DOCUMENT_ROOT"];
	}
	
	public static function getServerHost ()	{
		return LinkHelper::getServerHost();
	}
}