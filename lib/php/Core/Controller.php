<?php

class Controller {
	public $sModul = "";
	
	public $sController = "";
	
	public $sSubfolder = "";
	
	private function includeing() {
		include_once Config::getDocumentRoot()."/lib/php/pattern/extend.controller.php";
		include_once Config::getDocumentRoot()."/lib/php/pattern/interface.controller.php";
	}
	
	public function run() {
		if ($this->sController === "") {
			$this->sController = $this->sModul;
		}

		if (is_string($this->sController) && !empty($this->sController)) {
			$this->includeing();
				
			$_sControllerFile = Config::getDocumentRoot()."/module/";
			if ($this->sSubfolder !== "") {
				$_sControllerFile .= $this->sSubfolder."/";
			}
			$_sControllerFile .= $this->sModul."/controller/".$this->sController.".php";
			
			
			if (file_exists($_sControllerFile)) {
				include ($_sControllerFile);

				Factory::$sModul 	= $this->sModul;
				$oController 		= Factory::getController($this->sController);
				$sControllername	= get_class($oController);
				$aInterfaces		= class_implements($sControllername);
				if (in_array("iController", $aInterfaces)) {
					if ($oController instanceof eController) {
						$oController->sModul = $this->sModul;
						$oController->sSubfolder = $this->sSubfolder;
						$oController->start();
						$oController->run();
						$oController->render();
						
					} else {
						$sOutput = "Class '";
						if (isset($sControllername))	$sOutput .= $sControllername;
						else							$sOutput .= "index";				
						$sOutput .= "' is not instance of 'eController'!";
						$this->error($sOutput);
					}
					
				} else {
					$sOutput = "Missing interface 'iController' at class ";
					if (isset($sControllername))	$sOutput .= $sControllername;
					else							$sOutput .= "index";
					$sOutput .= "'!";
					$this->error($sOutput);
				}
				unset($oController, $aInterfaces);
				
			} else {
				$sOutput = "Missing Modul '". $this->sSubfolder."\\".$this->sModul."::";
				if (isset($sControllername))	$sOutput .= $sControllername;
				else							$sOutput .= "index";
				$sOutput .= "'!";
				$this->error($sOutput);
			}
		}
	}

	public function error($sMessage) {
		if (class_exists("ContentModelMenuitem")) {
			$oContent = Factory::getModel("menuitem", "content");
			if (!$oContent->link404()) {
				die($sMessage);
			}
			unset($oContent);
		} else {
			die($sMessage);
		}
	}
}