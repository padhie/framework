<?php
/**
 * Filename: Validate.php
 * Codierung: UTF-8
 * Author: Sven Lindenau <sl(at)alphabytes.de>
 * Date: 23.03.2015
 */

class Validate {
	/**
	 * Prueft ob die Variable ein Integer-Wert ist
	 * @param integer $iValue
	 * @return boolean Wenn die Variable ein Integer ist liefert es boolean TRUE zurueck, sonnst boolean FALSCH
	 */
    public static function isValidInt($iValue) {
		if (!is_integer($iValue)) {
			return false;
		}
		
		if (intval($iValue) !== $iValue) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Prueft ob die Variable ein String ist
	 * @param string $sValue
	 * @return boolean Wenn die Variable ein String ist liefert es boolean TRUE zurueck, sonnst boolean FALSCH
	 */
    public static function isStringNotEmpty ($sValue)
    {
    	if (is_string($sValue) && !empty($sValue))
    	{
    		return true;
    	}
    	return false;
    }

	/**
	 * Prueft ob die Variable ein positiver Integer-Wert ist
	 * @param integer $iValue
	 * @return boolean Wenn die Variable ein Integer ist liefert es boolean TRUE zurueck, sonnst boolean FALSCH
	 */
    public static function isPositiveInt ($iValue) {
    	if (self::isValidInt($iValue) && !empty($iValue))
    	{
    		return true;
    	}
    	return false;
    }

   	/**
	 * Prueft ob die Variable ein positiver Double-Wert ist
	 * @param string $dValue
	 * @return boolean Wenn die Variable ein positiver Double-Wert ist liefert es boolean TRUE zurueck, sonnst boolean FALSCH
	 */
    public static function isPositiveDouble ($dValue)
    {
    	if (is_double($sValue) && !empty($sValue))
    	{
    		return true;
    	}
    	return false;
    }

   	/**
	 * Prueft ob die Variable ein Array ist
	 * @param string $aValue
	 * @return boolean Wenn die Variable ein Array ist liefert es boolean TRUE zurueck, sonnst boolean FALSCH
	 */
    public static function isArrayNotEmpty ($aValue)
    {
    	if (is_array($aValue) && !empty($aValue))
    	{
    		return true;
    	}
    	return false;
    }
}