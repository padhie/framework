<?php

/**
 * Diese Klasse hilft dabei, Übersetungen zu verwalten.
 * Als Input gibt es 'single', 'array', 'object', 'json', 'csv' und 'dbklasse'
 * @author Padhie
 *
 */
class Translate {
	/**
	 * Hier stehen alle Übersetzungen in folgender Struktur drin:
	 * 1. Sprache
	 * 2. Sprachkey
	 * Value = Übersetzung
	 * @var array
	 */
	static protected $aData = array();
	
	/**
	 * Hier steht die Sprach drin. Der wer hier drin entspricht des 1. Level von $aData
	 * @var string
	 */
	static protected $sLang = "";
	
	/**
	 * Hier steht die Defaultsprache. Diese wird verwendet, wenn Sprachkey nicht in aktueller Sprache vorhanden ist
	 * @var string
	 */
	static protected $sDefaultLang = "";
	
	/**
	 * Ist diese Flag TRUE wird nicht die Sprachfrase sondern ###Sprachkey### ausgegeben
	 * @var boolean
	 */
	static protected $blDebug = false;
	
	/**
	 * Dieses Pattern wird für die Debugausgabe verwendet
	 * %KEY% wird durch den Sprachkey ersetzt
	 * @var string
	 */
	static protected $sDebugPattern = "###%KEY%###";
	
	
	/**
	 * Diese Funktion setzt die zu verwendenden Sprache
	 * @param string $sLang Kürzel der Sprache (entsprecht $sLang bei 'setSingle()', 'setArray()', 'setObject()', 'setJson()')
	 * @param string $sLang [optional] Kürzel für die Defaultsprache (wird verwendet, wenn Sprachkey in aktueller Sprache nicht verwendet wird). Wenn leer, wird $sLang verwendet
	 */
	public static function setLang($sLang, $sDefault="") {
		self::$sLang = $sLang;
		if ($sDefault == "") {
			self::$sDefaultLang = $sLang;
		} else {
			self::$sDefaultLang = $sDefault;
		}
	}
	
	/**
	 * Diese Methode liefert den hinterlegten Sprachschlüssel zurück
	 * @return string Sprachschlüssel
	 */
	public static function getLang() {
		return self::$sLang;
	}
	
	/**
	 * Aktiviert / Deaktiviert Debugmodus. Jedes 'ge()' liefert den Sprachkey zurück.
	 * @param boolean $blState TRUE aktiviert Debugmodus, FALSE deaktiviert diesen
	 * @param string $sPattern [optional] Gibt das Pattern an, welches bei der Debugausgabe verwendet werden soll (%KEY% wird durch Sprachkey ersetzt)
	 */
	public static function debug($blState, $sPattern="") {
		self::$blDebug = $blState;
		if ($sPattern != "") {
			self::$sDebugPattern = $sPattern;
		}
	}
	
	/**
	 * Diese Methode leer den Speicher
	 */
	public static function clean() {
		self::$aData = array();
	}
	
	/**
	 * Diese Methode liefert die Sprachphrase zu dem übergebenen Key. Ist der Debugmodus an, wird der Debugpattern zurück geliefert.
	 * @param string $sKey Sprachkey, welcher ausgegeben werden soll
	 * @return string 
	 */
	public static function get($sKey) {
		if (self::$blDebug) {
			return str_replace("%KEY%", $sKey, self::$sDebugPattern);
		}
		
		if (isset(self::$aData[self::$sLang]) && is_array(self::$aData[self::$sLang])) {
			if (isset(self::$aData[self::$sLang][$sKey])) {
				return self::$aData[self::$sLang][$sKey];
			}
		}
		
		if (isset(self::$aData[self::$sDefaultLang]) && is_array(self::$aData[self::$sDefaultLang])) {
			if (isset(self::$aData[self::$sDefaultLang][$sKey])) {
				return self::$aData[self::$sDefaultLang][$sKey];
			}
		}
		
		
		return $sKey;
	}
	
	/**
	 * Diese Methode gibt das Ergebnis von 'get()' mit 'echo' aus
	 * @param string $sKey Sprachkey, welcher ausgegeben werden soll
	 */
	public static function say($sKey) {
		echo self::get($sKey);
	}
	
	/**
	 * Setzt eine einzelne Sprachphrase
	 * @param string $sKey Sprachkey
	 * @param string $sVal Sprachphrase
	 * @param string $sLang Sprachkürzel, unter welchem das gespeichert werden soll
	 */
	public static function setSingle($sKey, $sVal, $sLang="") {
		if ($sLang == "") {
			$sLang = self::$sLang;
		}
		self::$aData[$sLang][$sKey] = $sVal;
	}
	
	/**
	 * Fügt das übergebene Array dem Speicher hinzu. Die Vorhandenen Daten werden erweitert/überschrieben.
	 * @param array $aData Array mit folgendem Aufbau: Sprachkey => Sprachphrase
	 * @param string $sLang [optional] Kürzel, worunter die Daten gespeichert werden sollen. Wenn leer wird aktuelle Sprache verwendet
	 */
	public static function setArray($aData, $sLang="") {
		foreach ($aData AS $sKey => $sVal) {
			self::setSingle($sKey, $sVal, $sLang);
		}
	}
	
	/**
	 * Fügt das übergebene stdObject dem Spreicher hinzu. Die vorhanden Daten werden erweitert/überschrieben.
	 * @param object $oData Object mit folgendem Aufbau: Sprachkey => Sprachphrase
	 * @param string $sLang [optional] Kürzel, worunter die Daten gespeichert werden sollen. Wenn leer wird aktuelle Sprache verwendet
	 */
	public static function setObject($oData, $sLang="") {
		self::setArray($oData, $sLang);
	}
	
	/**
	 * Fügt das übergebene json dem Speicher hinzu. Die vorhanden Daten werden erweitert/überschrieben.
	 * @param unknown $sJson jsonstring mit folgendem Aufbau: Sprachkey => Sprachphrase
	 * @param string $sLang [optional] Kürzel, worunter die Daten gespeichert werden sollen. Wenn leer wird aktuelle Sprache verwendet
	 */
	public static function setJson($sJson, $sLang="") {
		$aData = json_decode($sJson, true);
		self::setArray($aData, $sLang);
	}
	
	/**
	 * Fügt die Daten aus einer Datenbank dem Speicher hinzu. Die vorhandenen Daten werden erweitert/überschrieben
	 * @param object $oDbClass Diese Klasse muss die Methode getAll() enthalten um die Daten zu laden
	 * @param string $sKey [optional|default=key] Index, worunter der Sprachkey zu finden ist
	 * @param string $sVal [optional|default=value] Index, worunter die Sprachphrase zu finden ist
	 * @param string $sLang [optional|default=lang] Index, worunter das Sprachkürzel zu finden ist
	 */
	public static function setClass($oDbClass, $sKey="key", $sVal="value", $sLang="lang") {
		$aData = $oDbClass->getAll();
		foreach ($aData AS $aRow) {
			if (isset($aRow[$sKey]) && isset($aRow[$sVal]) && isset($aRow[$sLang])) {
				self::setSingle($aRow[$sKey], $aRow[$sVal], $aRow[$sLang]);
			}
		}
	}
	
	/**
	 * Fügt die Daten aus einer CSV-Datei dem speicher hinzu. Die vorhandenen Daten werden erweitert/überschrieben
	 * @param string $sFile Absoluter Dateipfad zur Sprachdatei
	 * @param number $sKey [optional|default=0] Gibt an, in welcher Spalte der Sprachkey steht
	 * @param number $sVal [optional|default=1] Gibt an, in welcher Spalte die Sprachphrase steht
	 * @param number $sLang [optional|default=2] Gibt an, in welcher Spalte das Sprachkürzel steht
	 * @param number $iStart [optional|default=0] Gibt an, ab welche Zeile gestartet werden soll
	 * @param string $sDelemiter [optional|default=;] Gibt den Delimiter der Datei an
	 */
	public static function setCsv($sFile, $sKey=0, $sVal=1, $sLang=2, $iStart=0, $sDelemiter=";") {
		if (file_exists($sFile)) {
			$rHandle = fopen($sFile, "r");
			if ($rHandle !== false) {
				$iCount=0;
				while(!feof($rHandle)) {
					if ($iCount >= $iStart) {
						$aData = fgetcsv($rHandle, 0, $sDelemiter);
						self::setSingle($aData[$sKey], $aData[$sVal], $aData[$sLang]);
					}
					$iCount++;
				}
				fclose($rHandle);
			}
		}
	}
}