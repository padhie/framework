<?php
include_once Config::getDocumentRoot()."/lib/php/pattern/extend.view.php";

class View extends eView {
	// ##################
	// ### PROPERTIES ###
	// ##################
	private static $_oSmarty;
	public static $sTemplateFileExtension = "tpl";
	private static $_sVersion = "3.1.30";
	
	
	// ###############
	// ### METHODS ###
	// ###############
	public static function init($blInitAgain=false) {
		if (!self::$_blInitialisiert || (self::$_blInitialisiert && $blInitAgain)) {
			parent::init();

			require_once Config::getDocumentRoot()."/lib/vendor/smarty/".self::$_sVersion."/SmartyBC.class.php";
			self::$_oSmarty = new SmartyBC();
			self::$_oSmarty->left_delimiter = "{{";
			self::$_oSmarty->right_delimiter = "}}";

			self::$_oSmarty->setCompileDir(Config::getDocumentRoot().Config::$sTemplateFolder.'/templates_c');
			self::$_oSmarty->setCacheDir(Config::getDocumentRoot().Config::$sTemplateFolder.'/cache');
			self::$_oSmarty->addPluginsDir(Config::getDocumentRoot()."/lib/vendor/smarty/plugins");
			self::$_oSmarty->addPluginsDir(Config::getDocumentRoot().Config::$sTemplateFolder.'/plugins');
		}
		self::$_blInitialisiert = true;
	}
	
	public static function render($sViewPage, $blOnce=true) {
		if (!Validate::isStringNotEmpty($sViewPage)) {
			return false;
		}
			
		if (!strpos($sViewPage, ".".self::$sTemplateFileExtension)) {
			$sViewPage = $sViewPage.".".self::$sTemplateFileExtension;
		}

		self::$_oSmarty->display($sViewPage);
	}
	
	public static function fetch($sViewPage) {
		if (!Validate::isStringNotEmpty($sViewPage)) {
			return false;
		}
			
		if (!strpos($sViewPage, ".".self::$sTemplateFileExtension)) {
			$sViewPage = $sViewPage.".".self::$sTemplateFileExtension;
		}
		
		return self::$_oSmarty->fetch($sViewPage);
	}
	
	
	// #######################
	// ### SETTER / GETTER ###
	// #######################
	public static function setTemplatePath($sTemplatePath) {
		self::$_aTemplatePathes = array();
		self::addTemplatePath($sTemplatePath);
	}
	
	public static function addTemplatePath($sTemplatePath) {
		array_push(self::$_aTemplatePathes, $sTemplatePath);
		if (file_exists($sTemplatePath)) {
			self::$_oSmarty->addTemplateDir($sTemplatePath);
		}
		if (file_exists(Config::getDocumentRoot()."/".$sTemplatePath)) {
			self::$_oSmarty->addTemplateDir(Config::getDocumentRoot()."/".$sTemplatePath);
		}
		if (file_exists(Config::getDocumentRoot().Config::$sTemplateFolder."/".$sTemplatePath)) {
			self::$_oSmarty->addTemplateDir(Config::getDocumentRoot().Config::$sTemplateFolder."/".$sTemplatePath);
		}
	}

	public static function popTemplatePath($sTemplatePath) {
		self::$_oSmarty->setTemplateDir(Config::getDocumentRoot()."/".$sTemplatePath);
		foreach (self::$_aTemplatePathes AS $_sTemplatePath) {
			self::addTemplatePath($_sTemplatePath);
		}
		array_unshift(self::$_aTemplatePathes, $sTemplatePath);
	}
	
	public static function getTemplatePath() {
		return self::$_oSmarty->getTemplateDir();
	}
	
	/**
	 * @deprecated
	 */
	public static function setHomeTemplatePath($sTemplatePath) {}

	public static function __callStatic($key, $val) {
		if (count($val) >= 1) {
			self::$_oSmarty->assign($key, $val[0]);
		} else {
			$mValue = self::$_oSmarty->getTemplateVars($key);
			if ($mValue != NULL ) {
				return $mValue;
			}
			unset($mValue);
		}
	}
	
	public static function getVar($key) {
		$mValue = self::$_oSmarty->getTemplateVars($key);
		if ($mValue != NULL) {
			$mSubvalue = self::$_oSmarty->getTemplateVars($mValue);
			if ($mSubvalue != NULL) {
				return $mSubvalue;
			}
			unset($mSubvalue);
		}
		unset($mValue);
		return false;
	}
}