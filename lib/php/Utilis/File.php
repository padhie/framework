<?php

class File {
	protected $_sDirectory;
	protected $_sFile;
	
	public function __construct($sFile="", $sDir="") {
		$this->init();
		if ($sFile !== "") {
			$this->setFile($sFile);
		}
		
		if ($sDir !== "") {
			$this->setDir($sDir);
		}
	}
	
	public function init() {
		$this->setDir(Config::$sCustomBase.Config::$sCustomFiles);
	}
	
	public function setDir($sDirectory) {
		if (!Validate::isStringNotEmpty($sDirectory)) {
			return false;
		}
		
		$this->_sDirectory = $sDirectory;
		return $this;
	}
	
	public function setFile($sFile) {
		if (!Validate::isStringNotEmpty($sFile)) {
			return false;
		}
		
		$this->_sFile = $sFile;
		return $this;
	}
	
	public function getDir() {
		return $this->_sDirectory;
	}
	
	public function getFile() {
		return $this->_sFile;
	}
	
	public function fileExist($sFile="") {
		if ($sFile==="") {
			$sFile = $this->_sDirectory.$this->_sFile;
		}
		
		if (file_exists($sFile)) {
			return true;
		}
		return false;
	}
	
	public function getListOfDir() {
		$_sDir = Config::getDocumentRoot().$this->_sDirectory;
		if (($handler=opendir($_sDir))!==false) {
			$aFiles = array();
			while (($file = readdir($handler)) !== false) {
				if ($this->fileExist($_sDir."/".$file) && $file !== "." && $file !== "..") {
					array_push($aFiles, $file);
				}
			}
			return $aFiles;
		}
		return false;
	}
	
	public function getFileContent() {
		if ($this->fileExist()) {
			return file_get_contents ( $this->_sDirectory.$this->_sFile, FILE_USE_INCLUDE_PATH );
		}
		return false;
	}
	
	public function moveFile($sToDir, $sNewFile="", $blOverwrite=false) {
		if ($sNewFile === "") {
			$sNewFile = $this->_sFile;
		}
		
		$blCopy=true;
		if ($this->fileExist()) {
			$blCopy=false;
			if ($blOverwrite) {
				$blCopy=true;
			}
		}

		if ($blCopy)
		{
			return rename($this->_sDirectory.$this->_sFile, $sToDir.$sNewFile);
		}
		return false;
	}
	
	public function copyFile($sToDir, $sNewFile="", $blOverwrite=false) {
		if ($sNewFile === "") {
			$sNewFile = $this->_sFile;
		}
		
		$blCopy=true;
		if ($this->fileExist()) {
			$blCopy=false;
			if ($blOverwrite) {
				$blCopy=true;
			}
		}

		if ($blCopy) {
			return copy($this->_sDirectory.$this->_sFile, $sToDir.$sNewFile);
		}
		return false;
		
	}
	
	public function uploadFile($aTmpFile, $sFile="", $blOverwrite=false) {
		if (!$this->fileExist($aTmpFile["tmp_name"])) {
			return false;
		}
		
		if ($sFile === "") {
			$sFile = $aTmpFile["name"];
		}
		
		$blWrite=true;
		if ($this->fileExist($sFile)) {
			if (!$blOverweite) {
				$blWrite=false;
			}
		}
		
		if ($blWrite) {
			$mResult = move_uploaded_file($aTmpFile["tmp_name"], Config::getDocumentRoot().$sFile);
				
			$sDir = "";
			$aFile = explode("/", $sFile);
			for ($i=0; $i<count($aFile)-1; $i++) {
				if (trim($aFile[$i]) !== "") {
					$sDir .= "/".$aFile[$i];
				}
			}
			
			$this->setDir($sDir);
			$this->setFile($aFile[count($aFile)-1]);
			
			return $mResult;
		}
		return false;
	}
	
	public function writeIntoFile($sContent) {
		if (!Validate::isStringNotEmpty($sContent)) {
			return false;
		}
		
		if ($this->fileExist()) {
			return file_put_contents($this->_sDirectory.$this->_sFile, $sContent);
		}
		return false;
		
	}
	
	public function appendIntoFile($sContent) {
		if (!Validate::isStringNotEmpty($sContent)) {
			return false;
		}
		
		if ($this->fileExist()) {
			return file_put_contents($this->_sDirectory.$this->_sFile, $sContent, FILE_APPEND);
		}
		return false;
	}

	public function deleteFile() {
		if ($this->fileExist()) {
			return unlink( $this->_sDirectory.$this->_sFile );
		}
		return false;
	}
}