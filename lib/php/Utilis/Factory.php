<?php

class Factory {
	static private $aObjects = array();
	static public $sModul = "";
	
	
	static private function getLocalObject($sControllerName) {
		if (!in_array($sControllerName, self::$aObjects)) {
			$oRefObject = new $sControllerName();
			self::$aObjects[$sControllerName] = &$oRefObject;
		}
		$oObject = &self::$aObjects[$sControllerName];
		
		return $oObject;
	} 
	
	static public function getController($sController, $sModul="") {
		if ($sModul == "") {
			$sModul = self::$sModul;
		}
		$sControllerName = ucfirst(strtolower($sModul))."Controller".ucfirst(strtolower($sController));
		return self::getLocalObject($sControllerName);
	}
	
	static public function getModel($sModel, $sModul="") {
		if ($sModul == "") {
			$sModul = self::$sModul;
		}
		$sModelName = ucfirst(strtolower($sModul))."Model".ucfirst(strtolower($sModel));
		return self::getLocalObject($sModelName);
	}

	static public function getConfig($sModul="") {
		if ($sModul == "") {
			$sModul = self::$sModul;
		}
		$aConfigName = ucfirst(strtolower($sModul))."Config";
		return self::getLocalObject($aConfigName);
	}
	
	static public function getClass($sClass) {
		return self::getLocalObject($sClass);
	}
}