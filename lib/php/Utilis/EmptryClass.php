<?php

class EmptryClass {
	private $_aData = array();
	private static $_aStaticData = array();

	public function setData($aData) {
		$this->_aData = $aData;
	}
	
	public function getData() {
		return $this->_aData;
	}

	public function clearData() {
		$this->_aData = array();
	}

	public function __get($sKey) {
		if ($sKey == "_aData") {
			return $this->_aData;
		} elseif (isset($this->_aData[$sKey])) {
			return $this->_aData[$sKey];
		}
		return "";
	}

	public function __set($sKey, $sValue) {
		$this->_aData[$sKey] = $sValue;
	}

	public function __call($sKey, $mValue) {
		if (count($mValue) >= 1) {
			$this->_aData[$sKey] = $mValue[0];
			return $this;
		} else {
			if (isset($this->_aData[$sKey]) ) {
				return $this->_aData[$sKey];
			}
		}
	}

	public static function __callStatic($key, $val) {
		if (count($val) >= 1) {
			self::$_aStaticData[$key] = $val[0];
		} else {
			if (isset(self::$_aStaticData[$key]) ) {
				return self::$_aStaticData[$key];
			}
			return "";
		}
	}
}