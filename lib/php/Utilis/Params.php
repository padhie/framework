<?php

class Params {
	/**
	 * Liefert den Inhalt von $_GET zurueck
	 * @param string $sParam Key aus dem $_GET-Array
	 * @param mixed $mDefault (optional) [default=false] Dieser Wert wird zurueck geliefert, wenn kein Inhalt zu dem Key vorhanden ist
	 * @return mixed Liefert den Inhalt des $_GET, welches mit dem Key vorliegt oder die Variable $mDefault zurueck
	 */
	public static function getParams ($sParam, $mDefault=false) {
		if (isset($_GET[$sParam]) && !empty($_GET[$sParam])) {
			return $_GET[$sParam];
		}
		return $mDefault;
	}

	/**
	 * Liefert den Inhalt von $_POST zurueck
	 * @param string $sParam Key aus dem $_POST-Array
	 * @param mixed $mDefault (optional) Dieser Wert wird zurueck geliefert, wenn kein Inhalt zu dem Key vorhanden ist
	 * @return mixed Liefert den Inhalt des $_POST, welches mit dem Key vorliegt oder die Variable $mDefault zurueck
	 */
	public static function postParams ($sParam, $mDefault=false)
	{
		if (isset($_POST[$sParam]) && !empty($_POST[$sParam]))
		{
			return $_POST[$sParam];
		}
		return $mDefault;
	}
	
	/**
	 * Liefert den Inhalt von $_SESSION zurueck
	 * @param string $sParam Key aus dem $_SESSION-Array
	 * @param mixed $mDefault (optional) Dieser Wert wird zurueck geliefert, wenn kein Inhalt zu dem Key vorhanden ist
	 * @return mixed Liefert den Inhalt des $_SESSION, welches mit dem Key vorliegt oder die Variable $mDefault zurueck
	 */
	public static function sessionParams ($sParam, $mDefault=false) {
		if (function_exists("session_status") && session_status() == PHP_SESSION_NONE) {
			session_start();
		} elseif (!isset($_COOKIE["PHPSESSID"])) {
			session_start();
		}
		
		if (isset($_SESSION[$sParam]) && !empty($_SESSION[$sParam])) {
			return $_SESSION[$sParam];
		}
		return $mDefault;
	}
	
	/**
	 * Liefert den Inhalt von $_COOKIE zurück
	 * @param unknown $sParam 	Key aus dem $_COOKIE-Array
	 * @param string $mDefault	(optional) Dieser Wert wird zurueck geliefert, wenn kein Inhalt zu dem Key vorhanden ist
	 * @return mixed Liefert den Inhalt des $_COOKIE, welcher mit dem Key vorliegt oder die Variable $mDefault zurueck
	 */
	public static function cookieParams ($sParam, $mDefault=false) {
		if (isset($_COOKIE[$sParam]) && !empty($_COOKIE[$sParam])) {
			return $_COOKIE[$sParam];
		}
		return $mDefault;
	}
	
	/**
	 * Liefert den Inhalt von $_FILES zurueck
	 * @param string $sParam Key aus dem $_FILES-Array
	 * @param mixed $mDefault (optional) Dieser Wert wird zurueck geliefert, wenn kein Inhalt zu dem Key vorhanden ist
	 * @return mixed Liefert den Inhalt des $_FILES, welches mit dem Key vorliegt oder die Variable $mDefault zurueck
	 */
	public static function fileParams ($sParam, $mDefault=false)
	{
		if (isset($_FILES[$sParam]) && !empty($_FILES[$sParam]))
		{
			return $_FILES[$sParam];
		}
		return $mDefault;
	}
	
	/**
	 * Setzt den Wert in eine Session
	 * @param string $sParam Key, welches als erkennung verwendet wird
	 * @param mixed $mValue Wert, welches gesetzt werden soll
	 * @return boolean Liefert bei erfolgreichem setzen boolean TRUE sonnst boolean FALSCH
	 */
	public static function setSession ($sParam, $mValue) {
		if (function_exists("session_status") && session_status() == PHP_SESSION_NONE) {
			session_start();
		} elseif (!isset($_COOKIE["PHPSESSID"])) {
			session_start();
		}
		
		if (gettype($sParam) !== "string") {
			return false;
		}

		$_SESSION[$sParam] = $mValue;
		return true;
	}
	
	/**
	 * Setzt den Wert in eine Cookie
	 * @param string $sParam Key, welches als erkennung verwendet wird
	 * @param mixed $mValue Wert, welches gesetzt werden soll
	 * @return boolean Liefert bei erfolgreichem setzen boolean TRUE sonnst boolean FALSCH
	 */
	public static function setCookie ($sParam, $mValue, $iExpore=0) {
		if (gettype($sParam) !== "string") {
			return false;
		}
		$iExpore = intval($iExpore);
	
		$_COOKIE[$sParam] = $mValue;
		return setcookie($sParam, $mValue, $iExpore);
	}
	
	/**
	 * Löscht einen Cookie
	 * @param string $sParam Key, welches als erkennung verwendet wird
	 * @return boolean Liefert bei erfolgreichem löschen boolean TRUE sonnst boolean FALSCH
	 */
	public static function removeCookie ($sParam) {
		if (gettype($sParam) !== "string") {
			return false;
		}
		
		unset($_COOKIE[$sParam]);
		return setcookie($sParam, null, time()-36000, "/");
	}
}