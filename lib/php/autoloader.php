<?php
// Alle Libary Klassen laden
$aClasses = array(
	"Core" 		=> array("Controller", "ApiCore", "ModulConfig", "ConfigCore", "PluginCore"),
	"Utilis" 	=> array("EmptryClass", "Validate", "Params", "View", "File", "Translate", "Factory"),
	"Database" 	=> array("DbBase", "DbExtent", "QueryBuilder"),
	"Helper" 	=> array("TableHelper", "FormHelper", "FormHelperElement", "LinkHelper", "StringHelper", "ModulHelper", "Helper", "HtmlHelper")
);

// Lade Libery Klassen
foreach ($aClasses as $sKey => $mVal) {	
	if (is_array($mVal) && !empty($mVal)) 	{
		for ($i=0; $i<count($mVal); $i++) {
			if (file_exists(dirname(__FILE__)."/".$sKey."/".$mVal[$i].".php")) {
				require_once dirname(__FILE__)."/".$sKey."/".$mVal[$i].".php";
			}
		}
		unset($i);
	} elseif (is_string($mVal) && !empty($mVal)) 	{
		if (file_exists(dirname(__FILE__)."/".$mVal.".php")) {
			require_once dirname(__FILE__)."/".$mVal.".php";
		}
	}
}
unset($aClasses, $sKey, $mVal);

// Autoloader für default Framework
spl_autoload_register(function ($sClassName) {
	// Custom Klassen
	if (file_exists(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomClasses."/class.".$sClassName.".php")) {
		require_once Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomClasses."/class.".$sClassName.".php";
	} elseif (file_exists(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomClasses."/".$sClassName.".php")) {
		require_once Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomClasses."/".$sClassName.".php";
	}

	// Vendor Klassen
	elseif (file_exists(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomVendorClasses."/class.".$sClassName.".php")) {
		require_once Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomVendorClasses."/class.".$sClassName.".php";
	}
	elseif (file_exists(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomVendorClasses."/".$sClassName.".php")) {
		require_once Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomVendorClasses."/".$sClassName.".php";
	}

	// Modul Klassen
	else {
		// Splitte Klassennamen auf, um Verzeichnissstruktur zu erkennen/bauen
		$aSplit = preg_split('/(?=[A-Z])/', $sClassName, -1, PREG_SPLIT_NO_EMPTY);
		if (isset($aSplit[1])) {
			$sBaseLink = Config::getDocumentRoot().Config::$sModulFolder."/";
			$sFile = "";

			// Art der Klasse Ermitteln
			switch($aSplit[1]) {
				case "Model":			$sFile = strtolower($aSplit[0])."/classes/class.".strtolower($aSplit[2]).".php";	break;
				case "Controller":		$sFile = strtolower($aSplit[0])."/controller/".strtolower($aSplit[2]).".php";		break;
				case "Plugin":			$sFile = strtolower($aSplit[0])."/plugins/".strtolower($aSplit[2]).".php";			break;
				case "Config":			$sFile = strtolower($aSplit[0])."/config.php";										break;
			}

			// Datei includieren, wenn vorhanden
			if ($sFile != "") {
				if (file_exists($sBaseLink.$sFile)) {
					require_once $sBaseLink.$sFile;
				} elseif (file_exists($sBaseLink."admin/".$sFile)) {
					require_once $sBaseLink."admin/".$sFile;
				}
			}
		}
	}
});

// Smarty (immer laden)
require_once Config::getDocumentRoot()."/lib/vendor/smarty/3.1.30/Autoloader.php";
Smarty_Autoloader::register();
// spl_autoload_register('Smarty_Autoloader'); 


/*
// Alle Customer Klassen laden
function requireFileInFolder ($sFolder) {
	if (file_exists($sFolder)) {
		if ($hCustomFolder = opendir($sFolder)) {
			while (($sFile=readDir($hCustomFolder)) !== false) {
				if ($sFile !== "." && $sFile !== "..") {
					if (is_file($sFolder."/".$sFile) && is_readable($sFolder."/".$sFile)) {
						require_once $sFolder."/".$sFile;
					} elseif (is_dir($sFolder."/".$sFile)) {
						requireFileInFolder($sFolder."/".$sFile);
					}
				}
			}
		}
	}
}

// Alle Modul Klassen laden
function requirerModulClasses($sFolder) {
	if (file_exists($sFolder)) {
		if ($hCustomFolder = opendir($sFolder)) {
			while (($sFile=readDir($hCustomFolder)) !== false) {
				if ($sFile !== "." && $sFile !== "..") {
					if (is_file($sFolder."/".$sFile) && is_readable($sFolder."/".$sFile) && strpos($sFolder, "class") !== false) {
						require_once $sFolder."/".$sFile;
					} elseif (is_dir($sFolder."/".$sFile)) {
						requirerModulClasses($sFolder."/".$sFile);
					}
				}
			}
		}
	}	
}

requireFileInFolder(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomClasses);
requireFileInFolder(Config::getDocumentRoot().Config::$sCustomBase.Config::$sCustomVendorClasses);

requirerModulClasses(Config::getDocumentRoot()."/module");
*/