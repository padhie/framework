<?php

/**
 * Klasse zum Ausführen von Datenbankabfragen.
 */
class QueryBuilder {
	/**
	 * Datenbankobject
	 * @var object
	 */
	private $_oDbo;
	
	/**
	 * SELECT-Statement
	 * @var string
	 */
	private $_sSelect;

	/**
	 * Zu verarbeitende Tabelle
	 * @var string
	 */
	private $_sTable;

	/**
	 * Daten des Datensatzes
	 * @var array
	 */
	private $_aData;

	/**
	 * Optionale WHERE-Klausel
	 * @var string
	 */
	private $_sWhere;

	/**
	 * Optionale ORDER BY-Klausel
	 * @var string
	 */
	private $_sOrder;
	
	/**
	 * Optionales JOIN
	 * @var string
	 */
	private $_sJoin;
	
	/**
	 * Optionale LIMIT-Klausel
	 * @var string
	 */
	private $_sLimit;
	
	/**
	 * Optionale GROUP-Klausel
	 * @var string
	 */
	private $_sGroup;
	
	
	public function __construct(){
		$this->clear(true);
	}
	
	public function setDatabaseObject($oDbo) {
		if ($oDbo !== null) {
			$this->_oDbo = $oDbo;
		}
	}
	
	/**
	 * Setzt alle Propertiers wieder auf zurück
	 * 
	 * @param boolean $blTable (optional) Soll die Tabellen auch zurückgesetzt werden? (default) false
	 */
	public function clear($blTable=false){
		$this->_sSelect = "*";
		$this->_aData = array();
		$this->_sWhere = "";
		$this->_sOrder = "";
		$this->_sJoin = "";
		$this->_sLimit = "";
		$this->_sGroup = "";
		if( $blTable === true ){
			$this->_sTable = "";
		}
	}

	/**
	 * Legt den SELECT-Teil einer Abfrage fest.
	 *
	 * @param string $sSelect SELECT-Anweisung
	 *
	 * @return $this|bool
	 */
	public function setSelect($sSelect, $sAlias="")
	{
		if (!Validate::isStringNotEmpty($sSelect)) {
			return false;
		}

		if (strpos($sSelect, ".") !== false) {
			list($_sTable, $_sSelect) = explode(".", $sSelect);
			$sSelect = "`" . $_sTable . "`";
			if ($_sSelect == "*") {
				$sSelect .= ".".$_sSelect;
			} else {
				$sSelect .= ".`".$_sSelect."`";
			}
		} else {
			$sSelect = "`" . $sSelect . "`";
		}
		$this->_sSelect = $sSelect;
		
		if( Validate::isStringNotEmpty($sAlias) ){
			$this->_sSelect .= " AS `" . $sAlias . "`";
		}
		unset($sSelect);

		return $this;
	}//end setSelect()

	/**
	 * Fügt dem SELECT-Teil einer Abfrage hinzu.
	 *
	 * @param string $sSelect SELECT-Anweisung
	 *
	 * @return $this|bool
	 */
	public function addSelect($sSelect, $sAlias="")
	{
		if (!Validate::isStringNotEmpty($sSelect)) {
			return false;
		}

		if (strpos($sSelect, ".") !== false) {
			list($_sTable, $_sSelect) = explode(".", $sSelect);
			$sSelect = "`" . $_sTable . "`";
			if ($_sSelect == "*") {
				$sSelect .= ".".$_sSelect;
			} else {
				$sSelect .= ".`".$_sSelect."`";
			}
		} else {
			$sSelect = "`" . $sSelect . "`";
		}

		if( $this->_sSelect !== ""){
			$this->_sSelect .= ", ".$sSelect;
		}
		
		if( Validate::isStringNotEmpty($sAlias) ){
			$this->_sSelect .= " AS `" . $sAlias . "`";
		}
		unset($sSelect);

		return $this;
	}//end setSelect()

	/**
	 * Fügt 1:! dem SELECT-Teil einer Abfrage hinzu.
	 *
	 * @param string $sSelect SELECT-Anweisung
	 *
	 * @return $this|bool
	 */
	public function addRawSelect($sSelect)
	{
		if (!Validate::isStringNotEmpty($sSelect)) {
			return false;
		}

		if( $this->_sSelect !== ""){
			$this->_sSelect .= ", ".$sSelect;
		} else {
			$this->_sSelect = $sSelect;
		}
		unset($sSelect);

		return $this;
	}//end addRawSelect()

	/**
	 * Legt die zu verarbeitende Tabelle fest.
	 *
	 * @param string $sTable Name der zu verarbeitenden Tabelle
	 *
	 * @return $this|bool
	 */
	public function setTable($sTable, $sAlias="")
	{
		if (!Validate::isStringNotEmpty($sTable)) {
			return false;
		}

		$this->_sTable = $sTable;
		if( Validate::isStringNotEmpty($sAlias) ){
			$this->_sTable .= " AS `" . $sAlias . "`";
		}
		unset($sTable);

		return $this;
	}//end setTable()

	/**
	 * Legt die Daten des Datensatzes fest.
	 *
	 * @param array $aData Die Daten des Datensatzes als Array
	 *
	 * @return $this|bool
	 */
	public function setData($aData)
	{
		if (!Validate::isArrayNotEmpty($aData)) {
			return false;
		}

		foreach ($aData AS $key => $val) {
			$aData[$key] = $this->_oDbo->real_escape_string($val);
		}
		
		$this->_aData = $aData;
		unset($aData);

		return $this;
	}//end setData()

	/**
	 * Fügt der Abfrage-Klausel ein ORDER BY-Statement hinzu.
	 *
	 * @param string $sOrder    Order Angabe (ohne "ORDER BY")
	 * @param string $sSort     (optional) Welche Sortierung (ASC/DESC)
	 * 
	 * @return $this|bool
	 */
	public function addOrder($sOrder, $sSort="ASC") {
		if (!Validate::isStringNotEmpty($sOrder)) {
			return false;
		}
		 
		if( $this->_sOrder !== "" ){
			$this->_sOrder .= ", `".$sOrder . "` " . $sSort;
		}else{
			$this->_sOrder = "`" . $sOrder . "` " . $sSort;
		}
		return $this;
	}
	
	/**
	 * Fügt der Abfrage-Klausel ein ORDER BY-Statement hinzu.
	 * 
	 * @deprecated              setOrder verwenden
	 * @see                     QueryBuilder::setOrder()
	 * 
	 * @param string $sOrder    Order Angabe (ohne "ORDER BY")
	 * @param string $sSort     (optional) Welche Sortierung (ASC/DESC)
	 * 
	 * @return $this|bool
	 */
	public function setOrder($sOrder, $sSort="ASC"){
		return $this->addOrder($sOrder, $sSort);
	}//end setOrder()
	
	/**
	* Fügt der Abfrage-Klausel ein LIMIT-Statement hinzu.
	*
	* @param int $iAmount       Anzahl, wie viele ausgegeben werden sollen
	* @param string $iStart     (optional) Ab welchen Eintrag soll ausgegeben werden
	*
	* @return $this|bool
	*/
	public function setLimit($iAmount, $iStart=0){
		if (!Validate::isPositiveInt($iAmount)) {
			return false;
		}
		
		$this->_sLimit = $iAmount;
		if (Validate::isPositiveInt($iStart) && $iStart>0){
			$this->_sLimit = $iStart . ", ".$iAmount;
		}
		return $this;
	}//end setLimit()

	/**
	 * Fügt der Abfrage-Klausel ein JOIN-Statement hinzu
	 * 
	 * @param string $sType         Art des Join (left join, right join...)
	 * @param string $sJoinTable    Zu Joinende Tabelle
	 * @param string $sFieldBase    Spaltenname der Ausgangstabelle
	 * @param string $sFieldJoin    Spaltenname der Jointabelle
	 * 
	 * @return $this|bool
	 */
	public function addJoin($sType, $sJoinTable, $sFieldBase, $sFieldJoin=""){
		if (!Validate::isStringNotEmpty($sType) 
		|| !Validate::isStringNotEmpty($sJoinTable) 
		|| !Validate::isStringNotEmpty($sFieldBase) ) {
			return false;
		}
		
		if (strpos($sType, "JOIN") === false) {
			$sType .= " JOIN";
		}

		$sTableAlias = "";
		if (strpos($sJoinTable, ".") !== false) {
			list($sJoinTable, $sTableAlias) = explode(".", $sJoinTable);
		}
		
		$this->_sJoin .= " " . $sType . " `" . $sJoinTable . "`";
		if ($sTableAlias != "") {
			$this->_sJoin .= " AS $sTableAlias";
		}

		if ($sFieldBase === $sFieldJoin || $sFieldJoin === "")
		{
			$this->_sJoin .= " USING(`" . $sFieldBase. "`)";
		}
		else
		{
			if (strstr($sFieldBase, ".") === false)
			{
				$sFieldBase = "`" . $this->_sTable . "`.`" . $sFieldBase. "`";
			}
			$this->_sJoin .= " ON " . $sFieldBase . " = `" . $sJoinTable . "`.`" . $sFieldJoin . "`";
		}
		
		return $this;
	}//end setJoin()
	
	/**
	 * Fügt der Abfrage-Klausel ein WHERE-Statement hinzu.
	 *
	 * @param string $sField    Zu testendes Feld
	 * @param mixed  $mValue    Gesuchter Wert
	 * @param string $sLinkage  Verknüpfungsoperator (AND/OR)
	 * @param string $sOperator Vergleichoperator (z.B. =)
	 *
	 * @return $this|bool
	 */
	public function addWhere($sField, $mValue, $sLinkage = "AND", $sOperator = "=")
	{
		if (!Validate::isStringNotEmpty($sField)) {
			return false;
		}

		if (is_string($mValue)) {
			$mValue = "'" . $mValue . "'";
		}

		$_sTmpWhere = "`" . $sField . "` " . $sOperator . " " . $mValue;

		if (strlen($this->_sWhere) == 0) {
			$this->_sWhere = $_sTmpWhere;
		} elseif (Validate::isStringNotEmpty($sLinkage)) {
			$this->_sWhere .= " " . $sLinkage . " " . $_sTmpWhere;
		}

		unset($_sTmpWhere, $sField, $mValue, $sLinkage, $sOperator);

		return $this;
	}//end addWhere()
	
	/**
	 * Fügt der Abfrage-Klausel eine eigenes WHERE-Statement hinzu.
	 *  
	 * @param string $sClausl   eigenes WHERE-Statement
	 * @param string $sLinkage  Verknüpfungsoperator (AND/OR)
	 * 
	 * @return $this|bool
	 */
	public function addCustomWhere($sClausl, $sLinkage = "") {
		if (!Validate::isStringNotEmpty($sLinkage)) {
			$this->_sWhere = $sClausl;
		} else {
			$this->_sWhere .= " " . $sLinkage . " " . $sClausl;
		}
		
		unset($sClausl, $sLinkage);
		
		return $this;
	}//end addCustomWhere()
	
	/**
	 * Fügt der Abfrage-Klausel ein GROUP BY-Statement hinzu.
	 * @param string $sField    Liste mit zu grupierenden Feldern (komma separiert)
	 * 
	 * @return $this|boolean
	 */
	public function addGroup($sField)
	{
		if (!Validate::isStringNotEmpty($sField)) {
			return false;
		}

		$_sTmpGroup = "";
		if (strpos($sField, ",")) {
			foreach (explode(",", $sField) AS $sSingleField) {
				$_sTmpGroup .= "`".$sSingleField."`,";
			}
			$_sTmpGroup = substr($_sTmpGroup, 0, -1);
		} else {
			$_sTmpGroup .= "`".$sField."`";
		}

		if ($this->_sGroup == "") {
			$this->_sGroup = $_sTmpGroup;
		} else {
			$this->_sGroup .= ",".$_sTmpGroup;
		}
		unset($_sTmpGroup);

		return $this;
	}//end addGroup()
	
	/**
	 * Nimmt das aktuelle WHERE-Statement und klammert dieses
	 * Alle weiteren 
	 * 
	 * @return $this|bool
	 */
	public function combineWhere(){
		
		$this->_sWhere = "(".$this->_sWhere.")";

		return $this;
	}//end combineWhere()

	/**
	 * Erstellt die INSERT-Klausel aus den zuvor hinzugefügten Daten des Datensatzes.
	 *
	 * @return string
	 */
	protected function generateInsertClause()
	{
		$_sTmpFields = "";
		$_sTmpValue = "";

		foreach ($this->_aData as $sKey => $sVal) {
			if (is_string($sVal)) {
				$sVal = "'" . $sVal . "'";
			}
			$_sTmpFields .= "`" . $sKey . "`, ";
			$_sTmpValue .= $sVal . ", ";
		}
		$_sTmpFields = substr($_sTmpFields, 0, -2);
		$_sTmpValue = substr($_sTmpValue, 0, -2);

		return "(" . $_sTmpFields . ") VALUES (" . $_sTmpValue . ")";
	}//end generateInsertClause()

	/**
	 * Generiert eine UPDATE-Klausel aus den zuvor festgelegten Daten des Datensatzes
	 *
	 * @return string
	 */
	protected function generateUpdateClause()
	{
		$_sSet = "";

		foreach ($this->_aData as $sKey => $sVal) {
			if (is_string($sVal)) {
				$sVal = "'" . $sVal . "'";
			}
			$_sSet .= "`" . $sKey . "`=" . $sVal . ", ";
		}
		return substr($_sSet, 0, -2);
	}//end generateUpdateClause()

	/**
	 * Liefert das fertig konstruierte SELECT-Statement zurück.
	 *
	 * @return string
	 */
	public function getSelectStatement()
	{
		$_sTmp = "SELECT " . $this->_sSelect . " FROM `" . $this->_sTable . "`";
		if( $this->_sJoin !== "" )
			$_sTmp .= $this->_sJoin;

		if( $this->_sWhere !== "" )
			$_sTmp .= " WHERE " . $this->_sWhere;

		if( $this->_sGroup !== "" )
			$_sTmp .= " GROUP BY " . $this->_sGroup;
		
		if( $this->_sOrder !== "" )
			$_sTmp .= " ORDER BY " . $this->_sOrder;
		
		if( $this->_sLimit !== "" )
			$_sTmp .= " LIMIT " . $this->_sLimit;

		$this->clear();
		return $_sTmp;
	}//end getSelectStatement()

	/**
	 * Liefert das fertig konstruierte UPDATE-Statement zurück.
	 *
	 * @return string
	 */
	public function getUpdateStatement()
	{
		$_sTmp = "UPDATE `" . $this->_sTable . "` SET " . $this->generateUpdateClause() . " WHERE " . $this->_sWhere;
		$this->clear();
		return $_sTmp;
	}//end getUpdateStatement()

	/**
	 * Liefert das fertig konstruierte INSERT-Statement zurück.
	 * @return string
	 */
	public function getInsertStatement()
	{
		$_sTmp =  "INSERT INTO `" . $this->_sTable . "` " . $this->generateInsertClause();
		$this->clear();
		return $_sTmp;
	}//end getInsertStatement()

	/**
	 * Liefert das fertig konstruierte DELETE-Statement zurück.
	 * @return string
	 */
	public function getDeleteStatement()
	{
		$_sTmp = "DELETE FROM `" . $this->_sTable . "` WHERE " . $this->_sWhere;
		$this->clear();
		return $_sTmp;
	}//end getDeleteStatement()
}//end class QueryBuilder{}