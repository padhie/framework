<?php

abstract class DbExtent extends DbBase {
	protected $_oQueryBuilder;
	protected $_aData = array();
	

 	public function __construct() {
		parent::__construct();
		
		$this->init();
	}
	
	protected function init() {
		$this->_oQueryBuilder = new QueryBuilder();
		$this->_oQueryBuilder->setDatabaseObject($this->_oMysql);
		$this->_oQueryBuilder->setTable($this->_sTable);
	}
	
	public function addOrder($sField, $sOrdering="ASC")
	{
		$this->_oQueryBuilder->addOrder($sField, $sOrdering);
	}
	
	public function setOrder($sField, $sOrdering="ASC")	{
		$this->_oQueryBuilder->setOrder($sField, $sOrdering);
	}
	
	public function setLimit($iCount, $iStart=0)
	{
		$this->_oQueryBuilder->setLimit($iCount, $iStart);
	}	
	
	public function addWhere($sKey, $sVal, $sLinkage = "AND", $sOperator = "=") {
		$this->_oQueryBuilder->addWhere($sKey, $sVal, $sLinkage, $sOperator);
	}

	public function addCustomWhere($sClausl, $sLinkage = "") {
		$this->_oQueryBuilder->addCustomWhere($sClausl, $sLinkage = "");
	}
	
	public function getById($iId) {
		$this->_oQueryBuilder->addWhere($this->_sIdField, $iId);
		$this->setLimit(1);
		return $this->getSingleData();
	}
	
	public function getAll(){
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			$this->_aData = $this->getResult();
			return $this->_aData;
		}
		return false;
	}
	
	public function getSingleData() {
		$this->setLimit(1);
		$this->setQuery( $this->_oQueryBuilder->getSelectStatement() );
		if( $this->execute() ){
			$this->_aData = $this->getResult();
			if (count($this->_aData) > 0) {
				$this->_aData = $this->_aData[0];
				return $this->_aData;
			}
		}
		return false;
	}
	
	public function insert($aData){
		if( !Validate::isArrayNotEmpty($aData) )
			return false;
		
		$this->_oQueryBuilder->setData($aData);
		$this->setQuery( $this->_oQueryBuilder->getInsertStatement() );
		if( $this->execute() ){
			return $this->getLastInsertId();
		}
		return false;
	}
	
	public function update($aData, $iId) {
		if( !Validate::isArrayNotEmpty($aData)){
			return false;
		}
		
		$this->_oQueryBuilder->setData($aData)
							->addWhere($this->_sIdField, $iId);
		$this->setQuery( $this->_oQueryBuilder->getUpdateStatement() );
		
		
		if( $this->execute() ){
			return true;
		}
		return false;
	}
	
	public function delete($iId) {
		$this->_oQueryBuilder->addWhere($this->_sIdField, $iId);
		$this->setQuery( $this->_oQueryBuilder->getDeleteStatement() );

		if ($this->execute()) {
			return true;
		}
		return false;
	}
}