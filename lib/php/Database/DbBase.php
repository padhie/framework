<?php

# namespace Alphabytes\Database;

abstract class DbBase {
	// ######################
	// ##### PROPERTIES #####
	// ######################

	protected $_oMysql;
	private $_oStmt;

	private $_sQuery;

	private $_aErrors=array();
	private $_aLastQuerys=array();
	private $_mResult=array();


	// ########################
	// ##### MAGIC METHOD #####
	// ########################
	public function __construct(){
		if( $this->connect() ){
			$this->_sQuery = "SET CHARSET UTF8;";
			$this->execute();
		}
	}

	public function __destruct(){
		$this->_oMysql->close();
	}


	// ##################
	// ##### GETTER #####
	// ##################

	public function getResult(){
		return $this->_mResult;
	}
	
	public function getSingleResult() {
		if (isset($this->_mResult[0])) {
			return $this->_mResult[0];
		}
		return array();
	}
	
	public function getError(){
		$mReturn=NULL;
		switch(count($this->_aErrors)){
			case 0:
				$mReturn=false;
				break;
			case 1:
				$mReturn=$this->_aErrors[0];
				break;
			default;
				$mreturn=$this->_aErrors;
		}
		return $mReturn;
	}
	
	public function getLastQuerys(){
		return $this->_aLastQuerys;
	}

	public function getLastInsertId(){
		return mysqli_insert_id($this->_oMysql);
	}


	// ##################
	// ##### SETTER #####
	// ##################
	public function setQuery($sQuery){
		if( gettype($sQuery) !== "string" )
			return false;
		
		$this->_sQuery = $sQuery;
		unset($sQuery);
		return true;
	}


	// ##################
	// ##### METHOD #####
	// ##################
	private function connect(){
		$oMysql = mysqli_connect(Config::$sDbHost, Config::$sDbUser, Config::$sDbPassword, Config::$sDbDatabase);
		
		if( mysqli_connect_errno() ){
			array_push( $this->_aErrors, mysqli_connect_error() );
			$this->onError();
			return false;
		}else
			$this->_oMysql = $oMysql;
		
		unset($oMysql);
		return true;
	}
	
	protected function execute(){
		if( count($this->_aErrors) >= 1 )
			return false;

		$result = mysqli_query($this->_oMysql, $this->_sQuery);
		$this->setExecutetQuery($this->_sQuery);

		if ( mysqli_errno($this->_oMysql) ) {
			array_push( $this->_aErrors, mysqli_error($this->_oMysql) );
			$this->onError();
			return false;
		}

		if( !is_object($result) ){
			$this->_mResult = $result;
			return true;
		}
		
		$this->_mResult=array();
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC) ){
			array_push($this->_mResult, $row);			
		}
		return true;
	}
	
	private function setExecutetQuery($sQuery){
		array_push($this->_aLastQuerys, $sQuery);
		unset($sQuery);
	}

	protected function showColumns($sTable){
		$sQuery = "SHOW COLUMNS FROM `".$sTable."`";
		$this->setQuery($sQuery);
		$this->execute();
		return $this->getResult();
	}

	public function onError() {
		$sContent = var_export($this->_aLastQuerys, false);
		$sContent .= var_export($this->getError(), false);
		mail("padhiez@hotmail.de", "MySQL-Error on ".Config::getServerHost(), $sContent);
	}
}