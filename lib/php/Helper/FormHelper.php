<?php
class FormHelper {
	protected $_aData = array();
	protected $_aMainData = array();
	
	public function __construct($sAction="", $sMethod="post", $sTarget="_self", $sClass="") {
		$this->_aMainData["action"]		= $sAction;
		$this->_aMainData["method"]		= $sMethod;
		$this->_aMainData["target"]		= $sTarget;
		$this->_aMainData["class"]		= $sClass;
		$this->_aMainData["enctype"]	= "";
	}
	
	public function setData($aData) {
		$this->_aData = array();
		foreach ($aData AS $aRow) {
			// $sLabel 	= "";
			// $aDataArray = array();
			// $aClass		= array();
			
			// if (isset($aRow["label"]))		$sLabel 	= $aRow["label"]; 
			// if (isset($aRow["data"]))		$aDataArray = $aRow["data"]; 
			// if (isset($aRow["class"]))		$aClass 	= $aRow["class"]; 
			$this->addElement($aRow);
		}
	}
	
	public function addElementSingleParams($sType, $sName, $mValue, $sLabel="", $aData=array(), $mClass=array(), $aAdditionalContent=array()) {
		if ($sType=="file") {
			$this->_aMainData["enctype"] = "multipart/form-data";
		}
		
		$oFormHelperElement = new FormHelperElement();
		$oFormHelperElement
			->type($sType)
			->name($sName)
			->value($mValue)
			->label($sLabel)
			->data($aData)
			->class($mClass)
			->additional($aAdditionalContent);
		// $aData = array(
		// 	"type" 	=> $sType,
		// 	"name"	=> $sName,
		// 	"value"	=> $mValue,
		// 	"label"	=> $sLabel,
		// 	"data"	=> $aData,
		// 	"class"	=> $mClass
		// );
		
		if (!is_array($mClass)) {
			$oFormHelperElement->class(array(
				0 => "",
				1 => $mClass
			));
			// $aData["class"] = array(
			// 	0 => "",
			// 	1 => $mClass
			// );
		}
		
		// array_push($this->_aData, $aData);
		$this->addElement($oFormHelperElement);
		unset($oFormHelperElement);
	}

	/**
	 * Diese Methode mit mehreren Parametern ist deprecated
	 * (deprecated) addElement($sType, $sName, $mValue, $sLabel="", $aData=array(), $mClass=array())
	 * addElement(FormHelperElement)
	 */
	public function addElement() {
		$aArgs = func_get_args();
		if (count($aArgs) == 1 && $aArgs[0] instanceof FormHelperElement) {
			array_push($this->_aData, $aArgs[0]->getAllData());
		} elseif (count($aArgs) == 1 && is_array($aArgs[0])) {
			array_push($this->_aData, $aArgs[0]);
		} elseif (count($aArgs) == 3) {
			$this->addElementSingleParams($aArgs[0], $aArgs[1], $aArgs[2]);
		} elseif (count($aArgs) == 4) {
			$this->addElementSingleParams($aArgs[0], $aArgs[1], $aArgs[2], $aArgs[3]);
		} elseif (count($aArgs) == 5) {
			$this->addElementSingleParams($aArgs[0], $aArgs[1], $aArgs[2], $aArgs[3], $aArgs[4]);
		} elseif (count($aArgs) == 6) {
			$this->addElementSingleParams($aArgs[0], $aArgs[1], $aArgs[2], $aArgs[3], $aArgs[4], $aArgs[5]);
		} elseif (count($aArgs) == 7) {
			$this->addElementSingleParams($aArgs[0], $aArgs[1], $aArgs[2], $aArgs[3], $aArgs[4], $aArgs[5], $aArgs[6]);
		}
	}
	
	public function getMainData() {
		return $this->_aMainData;
	}
	
	public function getData() {
		return $this->_aData;
	}
	
	public function render($sView="form") {
		View::render($sView, false);
	}
}