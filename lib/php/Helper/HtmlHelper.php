<?php

class HtmlHelper {
	public static function label($aData) {
		$sOutput = "<label ";

		// For
		if (isset($aData["name"])) {
			$sOutput .= "for='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='control-label ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][0])) {
				$sOutput .= $aData["class"][0];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'>";
		
		// Value
		if (isset($aData["label"])) {
			$sOutput .= $aData["label"];
		}


		$sOutput .= "</label>";
		return $sOutput;
	}

	public static function label2($aData) {
		$sOutput = "<label ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'>";
		
		// Value
		if (isset($aData["value"])) {
			$sOutput .= $aData["value"];
		}


		$sOutput .= "</label>";
		return $sOutput;
	}

	public static function input($aData) {
		$sOutput = "<input ";

		// Type
		if (isset($aData["type"])) {
			$sOutput .= "type='".$aData["type"]."' ";
		} else {
			$sOutput .= "type='text' ";
		}

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "' ";

		// Value
		if (isset($aData["value"])) {
			$sOutput .= "value='".$aData["value"]."' ";
		}

		// Placeholder
		if (isset($aData["placeholder"])) {
			$sOutput .= "placeholder='".$aData["placeholder"]."' ";
		}
		
		$sOutput .= ">";
		return $sOutput;
	}

	public static function textarea($aData) {
		$sOutput = "<textarea ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'>";

		// Value
		if (isset($aData["value"])) {
			$sOutput .= $aData["value"];
		}
		
		$sOutput .= "</textarea>";
		return $sOutput;
	}

	public static function codemirror($aData) {
		$sOutput = "<div ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='codemirrorBox form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'>";
		$sOutput .= "<textarea data-type='codemirror' ";

		// Name
		if (isset($aData["name"])) {
			$sOutput .= "name='".$aData["name"]."' ";
		}
		$sOutput .= ">";

		// Value
		if (isset($aData["value"])) {
			$sOutput .= $aData["value"];
		}
		
		$sOutput .= "</textarea>";
		$sOutput .= "</div>";
		return $sOutput;
	}

	public static function select($aData) {
		$sOutput = "<select ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'> ";


		// Option
		foreach ($aData["data"] AS $sSubKey => $mSubData) {
			$sUsingKey = "";
			$sUsingVal = "";
			// Wenn Asoc-Array als Daten übergeben wurde
			if (isset($mSubData["key"])) {
				$sUsingKey = $mSubData["key"];
				$sUsingVal = $mSubData["value"];
			} 
			// Wenn Numeric-Array als Daten übergeben wurde
			else {
				$sUsingKey = $sSubKey;
				$sUsingVal = $mSubData;
			}

			$sOutput .= "<option ";
			$sOutput .= "value='".$sUsingKey."' ";
			if ($aData["value"] == $sUsingKey) {
				$sOutput .= "selected ";
			}
			$sOutput .= ">";
			$sOutput .= $sUsingVal;
			$sOutput .= "</option>";

		}

		$sOutput .= "</select>";
		return $sOutput;
	}	

	public static function multiple($aData) {
		$sOutput = "<select ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."[]' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "' multiple> ";


		// Option
		foreach ($aData["data"] AS $sSubKey => $mSubData) {


			$sOutput .= "<option ";
			$sOutput .= "value='".$sSubKey."' ";
			if (is_array($aData["value"]) && in_array($sSubKey,  $aData["value"])) {
				$sOutput .= "selected ";
			}
			$sOutput .= ">";
			$sOutput .= $mSubData;
			$sOutput .= "</option>";

		}

		$sOutput .= "</select>";
		return $sOutput;
	}

	public static function selectgroup($aData) {
		$sOutput = "<select ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'> ";

		// Optiongroup
		$_aTmpData = array();
		foreach ($aData["data"] AS $sSubKey => $mSubData) {
			if (!isset($_aTmpData[$mSubData["group"]])) {
				$_aTmpData[$mSubData["group"]] = array();
			}
			array_push($_aTmpData[$mSubData["group"]], $mSubData);
		}

		// Option
		$sLastGroup = "";
		foreach ($_aTmpData AS $sGroup => $mSubData) {
			if ($sLastGroup != $sGroup) {
				if ($sLastGroup != "") {
					$sOutput .= "</optgroup>";
				}
				$sOutput .= "<optgroup label='".$sGroup."'>";
				$sLastGroup == $sGroup;
			}

			foreach ($mSubData AS $mSubSubData) {
				$sOutput .= "<option ";
				$sOutput .= "value='".$mSubSubData["key"]."' ";
				if ($aData["value"] == $mSubSubData["key"]) {
					$sOutput .= "selected ";
				}
				$sOutput .= ">";
				$sOutput .= $mSubSubData["value"];
				$sOutput .= "</option>";
			}
		}
		$sOutput .= "</optgroup>";

		$sOutput .= "</select>";
		return $sOutput;
	}

	public static function checkbox($aData) {
		$sOutput = "<input type='checkbox' ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='form-control-static ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "' ";

		// Value
		if (isset($aData["data"]["checked"])) {
			$sOutput .= "checked ";
		}		

		$sOutput .= ">";
		return $sOutput;
	}

	public static function radio($aData) {
		$sOutput = "";


		foreach ($aData["data"] AS $aSubData) {
			$sOutput .= "<input type='radio' ";

			// Name
			if (isset($aData["name"])) {
				$sOutput .= "name='".$aData["name"]."' ";
			}

			// Value
			if (isset($aSubData["value"])) {
				$sOutput .= "value='".$aSubData["value"]."'> ";
			}

			// Key / Text
			if (isset($aSubData["key"])) {
				$sOutput .= $aSubData["key"];
			}
		}

		return $sOutput;
	}

	public static function file($aData) {
		$sOutput = "<div ";

		// Class 1
		$sOutput .= "class='row ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} else {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'> ";

		// Input
		$sOutput .= self::input(array(
			"class"	=> array("", "col-md-10"),
			"name"	=> $aData["name"],
			"value"	=> $aData["value"]
		));

		// Input 2
		$sOutput .= "<input type='file' ";

		// Name
		if (isset($aData["name"])) {
			$sOutput .= "name='".$aData["name"]."_file' ";
		}

		// Class 2
		$sOutput .= "class='form-control-static col-md-2'";
		

		$sOutput .= "data-ui-file-upload /> ";
		$sOutput .= "</div>";
		return $sOutput;
	}

	public static function switcher($aData) {
		$sOutput = "<label class='ui-switch'>";

		// Checkbox
		$sOutput .= self::checkbox(array(
			"name"	=> $aData["name"],
			"class"	=> $aData["class"],
			"data"	=> $aData["data"]
		));

		$sOutput .= "<i></i>";
		$sOutput .= "</label>";
		return $sOutput;
	}

	public static function hidden($aData) {
		$sOutput = "<input type='hidden' ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "' ";
	
		// Value
		if (isset($aData["value"])) {
			$sOutput .= "value='".$aData["value"]."' ";
		}
		
		$sOutput .= ">";
		return $sOutput;
	}

	public static function submit($aData) {
		$sOutput = "<input type='submit' ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='btn btn-success ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "' ";
	
		// Value
		if (isset($aData["value"])) {
			$sOutput .= "value='".$aData["value"]."' ";
		}
		
		$sOutput .= ">";
		return $sOutput;
	}

	public static function button($aData) {
		$sOutput = "<button type='button' ";

		// ID / Name
		if (isset($aData["name"])) {
			$sOutput .= "id='".$aData["name"]."' ";
			$sOutput .= "name='".$aData["name"]."' ";
		}

		// Class
		$sOutput .= "class='btn btn-success ";
		if (isset($aData["class"])) {
			if (isset($aData["class"][1])) {
				$sOutput .= $aData["class"][1];
			} elseif (!is_array($aData["class"])) {
				$sOutput .= $aData["class"];
			}
		}
		$sOutput .= "'> ";
	
		// Value
		if (isset($aData["value"])) {
			$sOutput .= $aData["value"];
		}
		
		$sOutput .= "</button>";
		return $sOutput;
	}
}