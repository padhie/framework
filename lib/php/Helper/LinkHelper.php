<?php

class LinkHelper {
	public static $sPattern = "{BASE}/{LANGUAGE}/{MODUL}/{CONTROLLER}/{CUSTOM}/";
	public static $sTmpPattern = "";
	public static $sSubfolder = "";
	public static $sHost = "";
	public static $sDomain = "";
	

	public static function getDocumentRoot () {
		return $_SERVER["DOCUMENT_ROOT"];
	}
	
	public static function getServerHost ()	{
		$_sProtokoll = "http";
		if (strstr($_SERVER["SERVER_PROTOCOL"], "HTTPS") !== false) {
			$_sProtokoll = "https";
		}
		if (self::$sHost != "") {
			$_sProtokoll = self::$sHost;
		}
		$_sProtokoll .= "://";

		$_sDomain = $_SERVER["SERVER_NAME"];
		if (self::$sDomain != "") {
			$_sDomain = self::$sDomain;
		}

		return $_sProtokoll.$_sDomain;
	}

	public static function setTmpPattern($sPattern) {
		self::$sTmpPattern = self::$sPattern;
		self::$sPattern = $sPattern;
	}
	
	public static function restorePattern() {
		self::$sPattern = self::$sTmpPattern;
		self::$sTmpPattern = "";
	}
	
	public static function _($sModel="", $sController="", $sCustom="") {
		$sFuelLink	= self::$sPattern;
		
		// Platzhalter Ersetzen
		$sFuelLink	= str_replace("{LANGUAGE}", 	Translate::getLang(), 	$sFuelLink);
		$sFuelLink	= str_replace("{SUBFOLDER}",	self::$sSubfolder,		$sFuelLink);
		$sFuelLink	= str_replace("{MODUL}", 		$sModel, 				$sFuelLink);
		$sFuelLink	= str_replace("{CONTROLLER}", 	$sController, 			$sFuelLink);
		$sFuelLink	= str_replace("{CUSTOM}", 		$sCustom, 				$sFuelLink);

		// Doppelte SLashes entfernen
		while (strpos($sFuelLink, "//") !== false) {
			$sFuelLink	= str_replace("//", "/", $sFuelLink);
		}
		
		// Basisroot setzen (Nach Doppelslas ersetzung, weil >>> http:// <<<)
		$sFuelLink	= str_replace("{BASE}", Config::getServerHost(), $sFuelLink);

		return $sFuelLink;
	}
	
	public static function modul($sModul) {
		return self::_($sModul);
	}
	
	public static function controller($sModul, $sController) {
		return self::_($sModul, $sController);
	}
	
	public static function clear($sClearUrl) {
		return self::_("", "", $sClearUrl);
	}

	public static function getVarsFromUrl($sUrl) {
		$aVariablen = array(
			"{LANGUAGE}" 	=> Translate::getLang(),
			"{SUBFOLDER}" 	=> "",
			"{MODUL}" 		=> "",
			"{CONTROLLER}" 	=> "",
			"{CUSTOM}" 		=> ""
		);
		$aPlatzhalter = array(
			"{LANGUAGE}",
			"{SUBFOLDER}",
			"{MODUL}",
			"{CONTROLLER}",
			"{CUSTOM}"
		);
		$_sTmpPattern = self::$sPattern;
		$_sTmpPattern = str_replace("{BASE}", "", $_sTmpPattern);


		// Ermittel Position vom ersten Platzhalter
		$_iPos = StringHelper::getPostOfNextString($_sTmpPattern, $aPlatzhalter);
		

		// Url und Pattern auf gleichen Anfangsstand bringen
		$sUrl = substr($sUrl, $_iPos);
		$_sTmpPattern = substr($_sTmpPattern, $_iPos);


		// So lange durchlaufen bis kein Platzhalter mehr übrig ist
		$iLoopTry=0;
		while(strpos($_sTmpPattern, "{LANGUAGE}") !== false 
		|| strpos($_sTmpPattern, "{SUBFOLDER}") !== false 
		|| strpos($_sTmpPattern, "{MODUL}") !== false 
		|| strpos($_sTmpPattern, "{CONTROLLER}") !== false 
		|| strpos($_sTmpPattern, "{CUSTOM}") !== false) {

			$sKey = "";
			$sPlaceholderPos = 0;

			// Nächster Platzhalter LANGUAGE?
			if (strpos($_sTmpPattern, "{LANGUAGE}") === 0) {
				$sKey = "{LANGUAGE}";
				$sPlaceholderPos = 10;
			}

			// Nächster Platzhalter MODUL?
			elseif (strpos($_sTmpPattern, "{SUBFOLDER}") === 0) {
				$sKey = "{SUBFOLDER}";
				$sPlaceholderPos = 11;
			}

			// Nächster Platzhalter MODUL?
			elseif (strpos($_sTmpPattern, "{MODUL}") === 0) {
				$sKey = "{MODUL}";
				$sPlaceholderPos = 7;
			}

			// Nächster Platzhalter CONTROLLER?
			elseif (strpos($_sTmpPattern, "{CONTROLLER}") === 0) {
				$sKey = "{CONTROLLER}";
				$sPlaceholderPos = 12;
			}

			// Nächster Platzhalter CUSTOM?
			elseif (strpos($_sTmpPattern, "{CUSTOM}") === 0) {
				$sKey = "{CUSTOM}";
				$sPlaceholderPos = 8;
			}


			// Wenn Platzhalter und Key gefunden wurde
			if ($sKey != "" && $sPlaceholderPos >= 0) {
				// Ermittel nächster Delimiter
				$_sTmpPattern = substr($_sTmpPattern, $sPlaceholderPos);
				$_iPos = StringHelper::getPostOfNextString($_sTmpPattern, $aPlatzhalter);
				$_sDelimiter = substr($_sTmpPattern, 0, $_iPos);

				// Ermittel Positionen des Delimiter in URL und Pattern
				$_iPosInUrl = strlen($sUrl);
				$_iPosInPattern = strlen($_sTmpPattern);
				if (strpos($sUrl, $_sDelimiter) !== false)			$_iPosInUrl = strpos($sUrl, $_sDelimiter);
				if (strpos($_sTmpPattern, $_sDelimiter) !== false)	$_iPosInPattern = strpos($_sTmpPattern, $_sDelimiter);

				// Ermittel Speicher PatternVariable und entferne Platzhalter + Delimiter aus Pattern und URL
				$aVariablen[$sKey] = substr($sUrl, 0, $_iPosInUrl);
				if ($aVariablen[$sKey] === false)	$aVariablen[$sKey] = "";
				$_sTmpPattern = substr($_sTmpPattern, $_iPosInPattern+strlen($_sDelimiter));
				$sUrl = substr($sUrl, $_iPosInUrl+strlen($_sDelimiter));
			}


			// Wenn URL nicht weiter aufgedrüselt werden kann
			$iLoopTry++;
			if ($iLoopTry >= 30) {
				break;
			}
		}
		

		// Gefunden Variablen zurück geben
		return $aVariablen;
	}
	
}