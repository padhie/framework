<?php

class ModulHelper {

	/**
	 * Funktion liefert den Basispfad zurück
	 * @param  string $sModul     Modulverzeichnis, wenn Pfad in das Modulverzeichnis gehen soll
	 * @param  string $sSubfolder Unterverzeichnis, wenn Pfad in das Unterverzeichnis gehen soll
	 * @return string             Basislink mit Angegebenen Modul- und Unterverzeichnis
	 */
	private static function getBaseLink($sModul="", $sSubfolder="") {
		// Pfad zusammen bauen
		$sBaseLink = Config::getDocumentRoot().Config::$sModulFolder;

		if ($sSubfolder != "") {
			$sBaseLink .= "/".$sSubfolder;
		}

		if ($sModul != "") {
			$sBaseLink .= "/".$sModul;
		}
		
		return $sBaseLink;
	}

	public static function getInformationsOfClass($sClassname, $sSubfolder="") {
		$aReturnData = array(
			"class"	=> $sClassname,
			"modul"	=> "",
			"type"	=> "",
			"name"	=> "",
			"path"	=> "",
			"file"	=> "",
		);

		$aSplit = array();
		$sFile = "";
		$sPath = self::getBaseLink();
		if (strpos($sClassname, "Controller") !== false) {
			$aReturnData["type"] = "controller";
			$aSplit = explode("Controller", $sClassname);
			$sFile = $aSplit[1].".php";
			$sPath .= "/".strtolower($aSplit[0])."/controller";

		} elseif (strpos($sClassname, "Model") !== false) { 
			$aReturnData["type"] = "model";
			$aSplit = explode("Model", $sClassname);
			$sFile = "class.".strtolower($aSplit[1]).".php";
			$sPath .= "/".strtolower($aSplit[0])."/classes";

		} elseif (strpos($sClassname, "Plugin") !== false) { 
			$aReturnData["type"] = "plugin";
			$aSplit = explode("Plugin", $sClassname);
			$sFile = strtolower($aSplit[1]).".php";
			$sPath .= "/".strtolower($aSplit[0])."/plugins";

		} elseif (strpos($sClassname, "Config") !== false) { 
			$aReturnData["type"] = "config";
			$aSplit = explode("Config", $sClassname);
			$aSplit[1] = "";
			$sFile = "config.php";
			$sPath .= "/".strtolower($aSplit[0]);
		}

		$aReturnData["modul"]	= strtolower($aSplit[0]);
		$aReturnData["name"]	= strtolower($aSplit[1]);
		$aReturnData["file"]	= strtolower($sFile);
		$aReturnData["path"]	= $sPath;


		return $aReturnData;
	}

	/**
	 * Liefert eine Liste von Modulen Zurück
	 * Es wird das, in der Config angegebene, Modulverzeichnis ($sModulFolder) durchsucht
	 * @param  string $sSubfolder 	[optional] Soll ein Unterverzeichnis durchsucht werden?
	 * @return array 				Liste mit gefundenen Ordnern in dem Verzeichnis
	 */
	public static function getModullist($sSubfolder="") {
		$sBaseLink = self::getBaseLink("", $sSubfolder);

		// Modulliste vorbereiten
		$aModulList = array();

		// Alle Adminmodule laden und durchlaufen (um Config auszuführen)
		$aModuleFolderList = scandir($sBaseLink);	
		foreach ($aModuleFolderList as $sModulFolder) {

			// nur richtige Ordner
			if ($sModulFolder != "." && $sModulFolder != ".." && is_dir($sBaseLink."/".$sModulFolder)) {

				// Moduleinstellungen speichern
				$_aTmpModulSettings = array(
					"name"		=> $sModulFolder,
					"path"		=> $sBaseLink."/".$sModulFolder,
					"settings"	=> false
				);

				// Prüfen ob ModulConfig vorhanden ist
				if (file_exists($sBaseLink."/".$sModulFolder."/config.php")) {

					// Flag für das Modul setzen
					$_aTmpModulSettings["settings"] = true;
				}

				// Aktuelle Modulsettings in Liste speichern
				array_push($aModulList, $_aTmpModulSettings);
				unset($_aTmpModulSettings);
			}
		}

		// Modulliste zurück liefern
		return $aModulList;
	}

	/**
	 * Liefert den Namen der Configklasse zurück
	 * @param  string $sModul 	Modulname
	 * @return string
	 */
	public static function getConfigClassname($sModul) {
		return ucfirst($sModul)."Config";
	}

	/**
	 * Liefert den Namen der Controllerklasse zurück
	 * @param  string $sModul      Modulanme
	 * @param  string $sController [optional] Angabe, ob ein bestimmter Controller verwedet werden soll
	 * @return string
	 */
	public static function getControllerClassname($sModul, $sController="") {
		if ($sController == "") {
			$sController = $sModul;
		}

		return ucfirst($sModul)."Controller".ucfirst($sController);
	}

	/**
	 * Liefert eine Liste aller Controllerdateien und dessen Namen zurück
	 * @param  string $sModul     	Modulname
	 * @param  string $sSubfolder 	[optional] Angabe, ob ein Unterverzeichnis verwendet werden soll
	 * @return array             	Assoziatives Array in einem Numerischem Array
	 */
	public static function getControllerListOfModul($sModul, $sSubfolder="") {
		$sBaseLink = self::getBaseLink($sModul, $sSubfolder)."/controller";

		// Modulliste vorbereiten
		$aControllerList = array();

		// Alle Module laden und durchlaufen (um Config auszuführen)
		if (is_dir($sBaseLink)) {
			$aContentList = scandir($sBaseLink);
			foreach ($aContentList as $sContent) {

				// nur richtige Dateien
				if ($sContent != "." && $sContent != ".." && !is_dir($sBaseLink.$sContent)) {

					// Ist es eine PHP Datei?
					if (strpos($sContent, ".php") == strlen($sContent)-4) {

						// Name des Controllers
						$sName = substr($sContent, 0, -4);

						// Datei in Controllerliste hinzufügen
						array_push($aControllerList, array(
							"name"	=> $sName,
							"file"	=> $sContent,
							"path"	=> $sBaseLink,
							"class"	=> self::getControllerClassname($sModul, $sName)
						));
					}
				}
			}
		}

		return $aControllerList;
	}

	public static function getPluginsClassname($sModul, $sPlugin) {
		return ucfirst($sModul)."Plugin".ucfirst($sPlugin);
	}

	public static function getPluginListOfModul($sModul, $sSubfolder="") {
		$sBaseLink = self::getBaseLink($sModul, $sSubfolder)."/plugins";

		// Modulliste vorbereiten
		$aPluginList = array();

		// Alle Module laden und durchlaufen (um Config auszuführen)
		if (is_dir($sBaseLink)) {
			$aContentList = scandir($sBaseLink);
			foreach ($aContentList as $sContent) {

				// nur richtige Dateien
				if ($sContent != "." && $sContent != ".." && !is_dir($sBaseLink.$sContent)) {

					// Ist es eine PHP Datei?
					if (strpos($sContent, ".php") == strlen($sContent)-4) {

						// Name des Controllers
						$sName = substr($sContent, 0, -4);

						// Datei in Controllerliste hinzufügen
						array_push($aPluginList, array(
							"name"	=> $sName,
							"file"	=> $sContent,
							"path"	=> $sBaseLink,
							"class"	=> self::getPluginsClassname($sModul, $sName)
						));
					}
				}
			}
		}

		return $aPluginList;
	}
}