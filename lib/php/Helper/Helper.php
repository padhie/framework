<?php

class Helper {

	/**
	 * Diese Funktion prüft ob die angegebenen Keys in Dataarray vorhanden sind. Falls nicht wieder dieser Eintrag mit dem Defaultwert werstellt
	 * @param  array 	$aData    	Array welches die Keys enthalten soll
	 * @param  string 	$sKeys    	Liste mit den Keys, (durch Komma (,) getrennt)
	 * @param  mixed 	$mDefault 	[optional null] Wert, welcher beim neue Eintragen des Keys gesetzt werden soll
	 * @return array           		Übergebenes Array mit den nich hinzugefügten Keys
	 */
	public static function prepareArray($aData, $sKeys, $mDefault=null) {
		$aKeylist = explode(",", $sKeys);

		foreach ($aKeylist AS $sKey) {
			if (!isset($aData[trim($sKey)])) {
				$aData[trim($sKey)] = $mDefault;
			}
		}

		unset($sKeys, $aKeylist, $mDefault);
		return $aData;
	}

	public static function createSelectArray($aData, $sKey, $sVal) {
		$aReturn = array();

		foreach ($aData AS $aItem) {
			array_push($aReturn, array("key"=>$aItem[$sKey], "value"=>$aItem[$sVal]));
		}

		return $aReturn;
	}

	public static function createSelectArrayByNumeric($aData, $blUseNumAsKey=false) {
		$aReturn = array();

		foreach ($aData AS $iKey => $sVal) {
			$mUsingKey = $sVal;
			if ($blUseNumAsKey===true) {
				$mUsingKey = $iKey;
			}
			array_push($aReturn, array("key"=>$mUsingKey, "value"=>$sVal));
		}

		return $aReturn;
	}
}