<?php
class TableHelper {
	protected $_aData 		= array();
	protected $_aViewData	= array();
	protected $_aExtra 		= array();
	protected $_aKeys 		= array();
	protected $_iPage 		= 1;
	protected $_iPerPage 	= 10;
	public $blShowHits		= true;
	
	public function setData($aData) {
		$this->_aData = $aData;
		
		if (empty($this->_aKeys) && count($aData) >= 1) {
			foreach ($aData[0] AS $sKey => $sVal) {
				$this->addUsingKeys($sKey, $sKey);
			}
		}
	}
	
	public function addData($aData) {
		array_push($this->_aData, $aData);
	}
	
	public function getData() {
		return $this->_aViewData;
	}
	
	public function setExtra($aExtra) {
		$this->_aExtra = $aExtra;
	}
	
	public function addExtra($sName, $sExtra) {
		$this->_aExtra[$sName] = $sExtra;
	}
	
	public function getSingleExtra($sName, $aData=array()) {
		$_sExtra = "";
		if (isset($this->_aExtra[$sName])) {
			$_sExtra = $this->_aExtra[$sName];
		}

		if (count($aData) >= 1) {
			foreach ($aData AS $sKey => $sVal) {
				$_sExtra = str_replace("%".$sKey."%", $sVal, $_sExtra);
			}
		}
		
		return $_sExtra;
	}
	
	public function setUsingKeys($aKeys) {
		$this->_aKeys = $aKeys;
	}
	
	public function addUsingKeys($sKey, $sName) {
		$this->_aKeys[$sKey] = $sName;
	}
	
	public function getUseingKeys() {
		return $this->getUsingKeys();
	}
	public function getUsingKeys() {
		return $this->_aKeys;
	}
	
	public function setPage($iPage) {
		$this->_iPage = $iPage;
	}
	
	public function getPage() {
		return $this->_iPage;
	}
	
	public function getMaxPage() {
		$_iMaxPage = ($this->getHits() / $this->_iPerPage);
		if (intval($_iMaxPage) < $_iMaxPage) {
			$_iMaxPage = intval( ceil($_iMaxPage) );
		} 
		return $_iMaxPage;
	}
	
	public function setPerPage($iPerPage) {
		$this->_iPerPage = $iPerPage;
	}
	
	public function getHits() {
		return count($this->_aData);
	}
	
	public function render($sView="table") {
		$_iStart 	= ($this->_iPage * $this->_iPerPage)-$this->_iPerPage;
		$_iEnd		= $_iStart + $this->_iPerPage;
		$_iCurrent 	= 0;
		
		foreach ($this->_aData AS $sKey => $sVal) {
			$_iCurrent++;

			if ($_iCurrent >= $_iStart && $_iCurrent < $_iEnd) {
				$this->_aViewData[$sKey] = $sVal;
			}
		}
		
		View::render($sView, false);
	}
}