<?php

class StringHelper  {
	public static function getPostOfNextString($sHaystack, $aNeedles, $blEndpos=false) {
		$_iPos = strlen($sHaystack);
		$_sFirstNeedle = "";

		foreach ($aNeedles AS $sNeedle) {
			if (strpos($sHaystack, $sNeedle) !== false && strpos($sHaystack, $sNeedle) < $_iPos) {
				$_iPos = strpos($sHaystack, $sNeedle);
				$_sFirstNeedle = $sNeedle;
			}
		}

		if ($blEndpos) {
			$_iPos += strlen($_sFirstNeedle);
		}

		return $_iPos;
	}
}