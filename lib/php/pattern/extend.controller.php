<?php

class eController {
	public $sModul;
	public $sSubfolder = "";
	public $sTemplate = "templates";
	protected $blExecuteMainBootstrap=true;
	
	/**
	 * Läd alle Classen dieses Modules
	 * @deprecated 		1.0 		Wird durch Autoloader ersetzt
	 * @param  string 	$sModul     Name des Models
	 * @param  string 	$sSubfolder Subfolder im Modulverzeichniss (wenn vorhanden)
	 */
	protected function loadModulClasses($sModul, $sSubfolder="") {
		// $sPath = Config::getDocumentRoot()."/module/";
		// if ($sSubfolder !== "") {
		// 	$sPath .= $sSubfolder."/";
		// }
		// requireFileInFolder($sPath.$sModul."/classes/");
	}
	
	public function start() {
		// Lade Modulklassen
		$this->loadModulClasses($this->sModul, $this->sSubfolder);
		
		
		// Bootstrap Methode ausführen (für Projekt pre-Funktionalitäten)
		if ($this->blExecuteMainBootstrap === true) {
			Config::bootstrap();			
		}


		// Initialisiere Templateklasse
		$sPath = "/module/";
		if ($this->sSubfolder !== "") {
			$sPath .= $this->sSubfolder."/";
		}
 		View::setTemplatePath($sPath.$this->sModul."/".$this->sTemplate);
 		// View::setTemplatePath($sPath.$this->sModul."/".$this->sTemplate."Smarty");

		
		// Führe aktionen aus
		if (Params::getParams("action") !== false || Params::postParams("action") !== false) {
			if (Params::getParams("action") !== false) {
				$this->action(Params::getParams("action"));
			} else {
				$this->action(Params::postParams("action"));
			}
		}
	}
	
	public function action($action) {
		if (method_exists($this, $action)) {
			$this->$action();
		}
	}
	
	public function run() {}
	
	public function render() {
		View::render($this->sModul);
	}
}