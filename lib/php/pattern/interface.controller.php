<?php
interface iController {
	public function start();
	public function action($action);
	public function run();
	public function render();
}