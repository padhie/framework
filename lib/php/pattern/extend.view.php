<?php
class eView {
	// ##################
	// ### PROPERTIES ###
	// ##################
	protected static $_sTemplatePath;
	protected static $_aTemplatePathes;
	public static $_blInitialisiert = false;


	// ###############
	// ### METHODS ###
	// ###############
	public static function init() {
		self::$_aTemplatePathes = array();
	}

	public static function debug($blDump=false) {
		echo "<pre>";
		if ($blDump) {
			var_dump(self::$_aVariables);
		} else {
			foreach (self::$_aVariables AS $mKey => $mVal) {
				echo var_export($mKey. " => ") . var_export($mVal) . "<br />";
			}
		}		
		echo "</pre>";
	}


	// ##############
	// ### LOADER ###
	// ##############	
	public static function loadCss($sCssFile) {
		if (!Validate::isStringNotEmpty($sCssFile)) {
			return false;
		}
		
		if (!strpos($sCssFile, ".css")) {
			$sCssFile = $sCssFile.".css";
		}
		
		$sFilePath = "";
		$blFound = false;
		foreach (self::$_aTemplatePathes AS $sTemplatePath) {
			if (file_exists(Config::getDocumentRoot()."/".$sTemplatePath."/css/".$sCssFile)) {
				$sFilePath = Config::getServerHost()."/".$sTemplatePath."/css/".$sCssFile;
				$blFound = true;
			} elseif (file_exists(Config::getDocumentRoot().Config::$sTemplateFolder.$sTemplatePath."/css/".$sCssFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder.$sTemplatePath."/css/".$sCssFile;
				$blFound = true;
			}
		}
		if ($blFound !== true) {
			if (file_exists(Config::getServerHost().Config::$sTemplateFolder."/base/css/".$sCssFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder."/base/css/".$sCssFile;
			} elseif (file_exists(Config::getServerHost().Config::$sTemplateFolder."/css/".$sCssFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder."/css/".$sCssFile;
			} elseif (file_exists(Config::getServerHost().Config::$sTemplateFolder."/".$sCssFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder."/".$sCssFile;
			} elseif (file_exists(Config::getServerHost()."/css/".$sCssFile)) {
				$sFilePath = Config::getServerHost()."/css/".$sCssFile;
			} elseif (file_exists(Config::getServerHost()."/".$sCssFile)) {
				$sFilePath = Config::getServerHost()."/".$sCssFile;
			} else {
				$sFilePath = $sCssFile;
			}
		}
		
		echo "<link href='".$sFilePath."' rel='stylesheet' />";		
	}
	
	public static function loadJs($sJsFile) {
		if (!Validate::isStringNotEmpty($sJsFile)) {
			return false;
		}
		
		if (!strpos($sJsFile, ".js")) {
			$sJsFile = $sJsFile.".js";
		}
		
		$sFilePath = "";
		$blFound = false;
		foreach (self::$_aTemplatePathes AS $sTemplatePath) {
			if (file_exists(Config::getDocumentRoot()."/".$sTemplatePath."/js/".$sJsFile)) {
				$sFilePath = Config::getServerHost()."/".$sTemplatePath."/js/".$sJsFile;
				$blFound = true;
			}
		}
		if ($blFound !== true) {
			if (file_exists(Config::getServerHost().Config::$sTemplateFolder."/base/js/".$sJsFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder."/base/js/".$sJsFile;
			} elseif (file_exists(Config::getServerHost().Config::$sTemplateFolder."/js/".$sJsFile)) {
				$sFilePath = Config::getServerHost().Config::$sTemplateFolder."/js/".$sJsFile;
			} elseif (file_exists(Config::getServerHost()."/js/".$sJsFile)) {
				$sFilePath = Config::getServerHost()."/js/".$sJsFile;
			} elseif (file_exists(Config::getServerHost()."/".$sJsFile)) {
				$sFilePath = Config::getServerHost()."/".$sJsFile;
			} else {
				$sFilePath = $sJsFile;
			}
		}
		
		echo "<script src='".$sFilePath."'></script>";
	}

	public static function loadLibJs($sJsFile) {
		self::loadJs(Config::getServerHost()."/lib/js/".$sJsFile);
	}
	
	public static function loadJquery($sVersion="1.10.2") {
		self::loadJs(Config::getServerHost()."/lib/vendor/jquery/".$sVersion."/jquery.min.js");
	}
	
	public static function loadBootstrap($sVersion="3.3.5", $blWithJs=true) {
		self::loadCss("/lib/vendor/bootstrap/".$sVersion."/css/bootstrap.min.css");
		self::loadCss("/lib/vendor/bootstrap/".$sVersion."/css/bootstrap-theme.min.css");
		
		if ($blWithJs == true) {			
			self::loadJs("/lib/vendor/bootstrap/".$sVersion."/js/bootstrap.min.js");
		}
	}
	
	public static function loadJqueryUi($sVersion="1.11.4") {
		self::loadCss("/lib/vendor/jquery-ui/".$sVersion."/jquery-ui.min.css");
		self::loadCss("/lib/vendor/jquery-ui/".$sVersion."/jquery-ui.theme.min.css");

		self::loadJs("/lib/vendor/jquery-ui/".$sVersion."/jquery-ui.min.js");
	}
	
	public static function loadFontAwesome($sVersion="4.4.0") {
		self::loadCss("/lib/vendor/font-awesome/".$sVersion."/css/font-awesome.min.css");
	}

	public static function loadCodeMirror($sVersion="5.20.2", $sMode="htmlmixed") {
		self::loadJs("/lib/vendor/codemirror/".$sVersion."/lib/codemirror.js");
		self::loadCss("/lib/vendor/codemirror/".$sVersion."/lib/codemirror.css");
		self::loadJs("/lib/vendor/codemirror/".$sVersion."/mode/".$sMode."/".$sMode.".js");

		// wird extra benötigt
		if ($sMode == "htmlmixed") {
			self::loadJs("/lib/vendor/codemirror/".$sVersion."/mode/xml/xml.js");
			self::loadJs("/lib/vendor/codemirror/".$sVersion."/mode/javascript/javascript.js");
			self::loadJs("/lib/vendor/codemirror/".$sVersion."/mode/css/css.js");
		}
	}
}