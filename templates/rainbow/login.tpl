{{View::render("header_login")}}

<div class="body-inner">
	<ul class="rainbow-border">
		<li class="bg-danger"></li>
		<li class="bg-warning"></li>
		<li class="bg-success"></li>
		<li class="bg-info"></li>
		<li class="bg-violet"></li>
	</ul>

	<section class="logo text-center">
		<h1>
			<a href="#/" class="ng-binding">Rainbow</a>
		</h1>
	</section>
	
	<div class="form-container">
		{{assign var=aMainData value=View::formObject()->getMainData()}}
		<form class="form-horizontal ng-pristine ng-valid" action="{{$aMainData.action}}" method="{{$aMainData.method}}">
			<fieldset>
			
			{{foreach from=View::formObject()->getData() item=$aData}}
				{{if($aData.type == "text"}}
					<div class="form-group">
						<div class="btn-icon-lined btn-icon-round btn-icon-sm btn-default-light">
							<span class="glyphicon glyphicon-user"></span>
						</div>
						<input type="{{$aData.type}}" name="{{$aData.name}}" placeholder="{{$aData.label}}" class="form-control input-lg input-round text-center">
					</div>
				
				
				{{elseif $aData.type == "password"}}
					<div class="form-group">
						<div class="btn-icon-lined btn-icon-round btn-icon-sm btn-default-light">
							<span class="glyphicon glyphicon-lock"></span>
						</div>
						<input type="{{$aData.type}}" name="{{$aData.name}}" placeholder="{{$aData.label}}" class="form-control input-lg input-round text-center">
					</div>
					
				
				
				{{elseif $aData.type == "submit"}}
					<div class="divider divider-lg"></div>
					<div class="form-group">
						<input type="submit" btn-text="{{$aData.value}}" class="btn btn-primary btn-lg btn-round btn-block btn-gradient" value="{{$aData.value}}" />
					</div>
					
					
				
				{{/if}}
			{{/foreach}}
			</fieldset>
		</form>
	</div>
                                    
	<footer>
		<a href="#/page/signup">Sign up</a>
		<span class="divider-h"></span>
		<a href="#/page/forgot-password">Forgot your password?</a>
	</footer>
	
	{{View::formObject()->render()}}
</div>

{{View::render("footer_login")}}