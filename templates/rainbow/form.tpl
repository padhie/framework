<section class="panel panel-default">
	{{assign var="blSwitch" value=false}}
	{{assign var="blCodemirror" value=false}}
	{{assign var="oFormObject" value=View::getVar("formObjectVariable")}}

	{{if $oFormObject === false}}
		{{assign var="oFormObject" value=View::formObject()}}
	{{/if}}
	
	{{assign var="aMainData" value=$oFormObject->getMainData()}}

	<div class="panel-body padding-xl">
		<div class="row">
			<div class="col-md-12">

				<form role="form" method="{{$aMainData.method}}" action="{{$aMainData.action}}" target="{{$aMainData.target}}" {{if $aMainData.enctype != ""}}enctype='{{$aMainData.enctype}}'{{/if}} class="ng-pristine ng-valid {{$aMainData.class}}">
					{{foreach from=$oFormObject->getData() item=aData}}
						<div class="form-group {{if $aData.type != "hidden" && $aData.type != "submit" && $aData.type != "button"}}row{{/if}}">
					
							{{if $aData.type == "text" || $aData.type == "password" || $aData.type == "number" || $aData.type == "email" || $aData.type == "number"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::input($aData)}}
								
								
							{{elseif $aData.type == "textarea"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::textarea($aData)}}
								

							{{elseif $aData.type == "codemirror"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::codemirror($aData)}}
								{{$blCodemirror = true}}
								

							{{elseif $aData.type == "select"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::select($aData)}}


							{{elseif $aData.type == "selectgroup"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::selectgroup($aData)}}
								
								
							{{elseif $aData.type == "multiple"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::multiple($aData)}}
								

							{{elseif $aData.type == "checkbox"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::checkbox($aData)}}
								
								
							{{elseif $aData.type == "radio"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::radio($aData)}}

								
							{{elseif $aData.type == "label"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::label2($aData)}}
								
							
							{{elseif $aData.type == "file"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::file($aData)}}
								
				
							{{elseif $aData.type == "switch"}}
								{{HtmlHelper::label($aData)}}
								{{HtmlHelper::switcher($aData)}}
								{{$blSwitch=true}}
							
													
							{{elseif $aData.type == "hidden"}}
								{{HtmlHelper::hidden($aData)}}
																
								
							{{elseif $aData.type == "submit"}}
								{{HtmlHelper::submit($aData)}}
								
								
							{{elseif $aData.type == "button"}}
								{{HtmlHelper::button($aData)}}								

							{{/if}}
						</div>	
					{{/foreach}}
				</form>

			</div>
		</div>
	</div>
	{{if $blSwitch==true}}
		<script type="text/javascript">
			$(".ui-switch i").on("click", function(e) {
				$(e).parent().find("input[type=checkbox]").click();
			});
		</script>
	{{/if}}
	{{if $blCodemirror == true}}
		{{View::loadCodeMirror()}}
		<script>
			var mode = "htmlmixed";
			var elements = document.querySelectorAll("[data-type='codemirror']");
			for (var i=0; i<elements.length; i++) {
				var editor = CodeMirror.fromTextArea(elements[i], {
					lineNumbers: true,
					mode: "htmlmixed"
				});
			}
		</script>
	{{/if}}
</section>