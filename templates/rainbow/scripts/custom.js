$(document).ready(function(){
	// Toggle Menu
	$(".toggle-menu").on("click", function() {
		$("#app").toggleClass("nav-collapsed-min");
		if (getCookie("toggle-menu") == "1") {
			setCookie("toggle-menu", 0);
		} else {
			setCookie("toggle-menu", 1);
		}
	});

	// Toggle color-menu
	$(".change-color").on("click", function() {
		$(this).parent().toggleClass("open");
	});

	// Change color
	$(".skin-check").on("click", function() {
		var colorVariante = $(this).find("[name=skin]").val();
		var value = colorVariante.split("_");

		changeColor(value[0], value[1]);

		removeCookie("backgroundcolor");
		setCookie("bg-color", colorVariante);
	});

	// Change color by loading
	var colorCookie = getCookie("bg-color");
	if (colorCookie != "") {
		var value = colorCookie.split("_");
		changeColor(value[0], value[1]);

		var inputs = $("[name=skin]");
		for (var i=0; i<inputs.length; i++) {
			if ($(inputs[i]).val() == colorCookie) {
				$($(inputs[i])).prop("checked", true);
			}
		}
	}

	// Toggle Menu by loading
	if (getCookie("toggle-menu") == "1") {
		$("#app").addClass("nav-collapsed-min");
	}
});

function changeColor(color, variant) {
	// remove old color
	$(".logo")[0].className = "logo";
	var headerClasses = $("#header")[0].className.split(" ");
	$("#header")[0].className = "";
	for (var i=0; i<headerClasses.length; i++) {
		if (headerClasses[i].indexOf("bg-") != 0) {
			$("#header").addClass(headerClasses[i]);
		}
	} 

	var naviClasses = $("#nav-container")[0].className.split(" ");
	$("#nav-container")[0].className = "";
	for (var i=0; i<naviClasses.length; i++) {
		if (naviClasses[i].indexOf("bg-") != 0) {
			$("#nav-container").addClass(naviClasses[i]);
		}
	} 


	// set new logo-color
	$(".logo").addClass("bg-"+color);

	if (variant == "1") {
		$("#header").addClass("bg-light");
		$("#nav-container").addClass("bg-dark");
	} else if(variant == "2") {
		$("#nav-container").addClass("bg-dark");
		$("#header").addClass("bg-"+color);
	} else if(variant == "3") {
		$("#nav-container").addClass("bg-light");
		$("#header").addClass("bg-"+color);
	}
}