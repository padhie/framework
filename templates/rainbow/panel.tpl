{{assign var=aCurrentPanel value=View::currentPanel()}}
<div class="card bg-{{$aCurrentPanel.color}}">
	<div class="card-content">
		<span class="card-title">{{$aCurrentPanel.title}}</span>
		<p>{{$aCurrentPanel.body}}</p>
	</div>
	<div class="card-action">
		{{$aCurrentPanel.action}}
	</div>
</div>