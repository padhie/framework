{{$oTableObject = View::getVar("tableObjectVariable")}}
{{if $oTableObject === false}}
	{{$oTableObject = View::tableObject()}}
{{/if}}

{{if count($oTableObject->getData()) >= 1}}
	<section class="panel panel-default">
		<div class="panel-body padding-xl">
			<div class="row">
				{{assign var=sNextCol value="col-md-12"}}
			
				{{if $oTableObject->blShowHits}}
					{{assign var=sNextCol value="col-md-9"}}
					<div class="col-md-3">
						<!-- Hits -->
						<a href="#" class="btn btn-default disabled" role="button" style="margin:20px 0;">{{Translate::get("HITS")}}: {{$oTableObject->getHits()}}</a>
					</div>
				{{/if}}
			
				<div class="{{$sNextCol}} text-right">
					<!-- Pagination -->
					{{assign var=iMaxPages value=$oTableObject->getMaxPage()}}

					{{if $iMaxPages > 1}}
						<nav>
							<ul class="pagination">
							
								{{if $oTableObject->getPage() > 1}}
									<!-- Back BTN -->
									<li>
										<a href="?p={{math equation="x - 1" x=$oTableObject->getPage()}}" aria-label="Previous">
											<span aria-hidden="true">&laquo;</span>
										</a>
									</li>
								{{/if}}
								
								<!-- Pages -->
								
								{{for $i=1 to $iMaxPages}}
									<li {{if $i==$oTableObject->getPage()}} "class="active"{{/if}}>
										<a href="?p={{$i}}">{{$i}}</a>
									</li>
								{{/for}}
								
								
								{{if $oTableObject->getPage() < $iMaxPages}}
									<!-- Forward BTN -->
									<li >
										<a href="?p={{$oTableObject->getPage()+1}}" aria-label="Next">
											<span aria-hidden="true">&raquo;</span>
										</a>
									</li>
								{{/if}}
							</ul>
						</nav>
					{{/if}}
						
				</div>
			</div>

			<!-- Table -->
			<div class="tableScrollTopWrapper">
				<div class="tableScrollTop"></div>
			</div>
			<div class="table-responsive">
				<table class="table table-striped no-margin">
					{{assign var=aKeys value=$oTableObject->getUseingKeys()}}
					
					<!-- Head -->
					<thead>
						<tr>
							{{foreach from=$aKeys item=$sName}}
								<th><strong>{{$sName}}</strong></th>
							{{/foreach}}
						</tr>
					</thead>
			
					<!-- Body -->
					<tbody>
						{{foreach from=$oTableObject->getData() item=$aRow}}
							<tr>
								{{foreach from=$aKeys key=$sKey item=$sName}}
									<td>
										{{if isset($aRow.$sKey)}}
											{{$aRow.$sKey}}
										{{else}}
											{{$oTableObject->getSingleExtra($sKey, $aRow)}}
										{{/if}}
									</td>
								{{/foreach}}
							</tr>
						{{/foreach}}
					</tbody>
			
				</table>
			</div>
			
		</div>
	</section>

	<script>
		$(document).ready(function() {
			$(".tableScrollTopWrapper").width( $(".table-responsive").width() );
			$(".tableScrollTop").width( $(".table").width() );

			$(".tableScrollTopWrapper").scroll(function(){
				$(".table-responsive").scrollLeft($(".tableScrollTopWrapper").scrollLeft());
			});
			$(".table-responsive").scroll(function(){
				$(".tableScrollTopWrapper").scrollLeft($(".table-responsive").scrollLeft());
			});
		});
	</script>
	<style>
		.tableScrollTopWrapper {
			height: 18px;
			overflow-x: auto;
		}
		.tableScrollTop {
			height: 1px;
		}
	</style>
{{/if}}