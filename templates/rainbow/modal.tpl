<section class="panel panel-default">
	<style>
		.modal{
			z-index:1050;
		}
	</style>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog ">
			<div uib-modal-transclude="" class="modal-content">
				<div class="modal-header ng-scope">
					<h3>{{View::modalTitle()}}</h3>
				</div>
				<div class="modal-body ng-scope">
					{{View::modalText()}}
				</div>
				<div class="modal-footer ng-scope">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{Translate::get("NO")}}</button>
					<button type="button" onclick="successModal()" class="btn btn-danger">{{Translate::get("YES")}}</button>
				</div>
			</div>
		</div>
	</div>
</section>