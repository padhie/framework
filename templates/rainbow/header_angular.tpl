{{if Params::getParams("view") != "single"}}
<!DOCTYPE html>
<html class="no-js wf-lato-n4-active wf-lato-n7-active wf-lato-n3-active wf-lato-i4-active wf-lato-i3-active wf-active">
	<head lang="de">
		<meta charset="utf-8"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		{{View::loadBootstrap("3.3.5", true)}}
		
		<style type="text/css">[uib-typeahead-popup].dropdown-menu{display:block;}</style>
		<style type="text/css">.uib-time input{width:50px;}</style>
		<style type="text/css">[uib-tooltip-popup].tooltip.top-left &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.top-right &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.bottom-left &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.bottom-right &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.left-top &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.left-bottom &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.right-top &gt; .tooltip-arrow,[uib-tooltip-popup].tooltip.right-bottom &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.top-left &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.top-right &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.bottom-left &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.bottom-right &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.left-top &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.left-bottom &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.right-top &gt; .tooltip-arrow,[uib-tooltip-html-popup].tooltip.right-bottom &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.top-left &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.top-right &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.bottom-left &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.bottom-right &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.left-top &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.left-bottom &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.right-top &gt; .tooltip-arrow,[uib-tooltip-template-popup].tooltip.right-bottom &gt; .tooltip-arrow,[uib-popover-popup].popover.top-left &gt; .arrow,[uib-popover-popup].popover.top-right &gt; .arrow,[uib-popover-popup].popover.bottom-left &gt; .arrow,[uib-popover-popup].popover.bottom-right &gt; .arrow,[uib-popover-popup].popover.left-top &gt; .arrow,[uib-popover-popup].popover.left-bottom &gt; .arrow,[uib-popover-popup].popover.right-top &gt; .arrow,[uib-popover-popup].popover.right-bottom &gt; .arrow,[uib-popover-html-popup].popover.top-left &gt; .arrow,[uib-popover-html-popup].popover.top-right &gt; .arrow,[uib-popover-html-popup].popover.bottom-left &gt; .arrow,[uib-popover-html-popup].popover.bottom-right &gt; .arrow,[uib-popover-html-popup].popover.left-top &gt; .arrow,[uib-popover-html-popup].popover.left-bottom &gt; .arrow,[uib-popover-html-popup].popover.right-top &gt; .arrow,[uib-popover-html-popup].popover.right-bottom &gt; .arrow,[uib-popover-template-popup].popover.top-left &gt; .arrow,[uib-popover-template-popup].popover.top-right &gt; .arrow,[uib-popover-template-popup].popover.bottom-left &gt; .arrow,[uib-popover-template-popup].popover.bottom-right &gt; .arrow,[uib-popover-template-popup].popover.left-top &gt; .arrow,[uib-popover-template-popup].popover.left-bottom &gt; .arrow,[uib-popover-template-popup].popover.right-top &gt; .arrow,[uib-popover-template-popup].popover.right-bottom &gt; .arrow{top:auto;bottom:auto;left:auto;right:auto;margin:0;}[uib-popover-popup].popover,[uib-popover-html-popup].popover,[uib-popover-template-popup].popover{display:block !important;}</style>
		<style type="text/css">.uib-datepicker-popup.dropdown-menu{display:block;float:none;margin:0;}.uib-button-bar{padding:10px 9px 2px;}</style>
		<style type="text/css">.uib-position-measure{display:block !important;visibility:hidden !important;position:absolute !important;top:-9999px !important;left:-9999px !important;}.uib-position-scrollbar-measure{position:absolute !important;top:-9999px !important;width:50px !important;height:50px !important;overflow:scroll !important;}.uib-position-body-scrollbar-measure{overflow:scroll !important;}</style>
		<style type="text/css">.uib-datepicker .uib-title{width:100%;}.uib-day button,.uib-month button,.uib-year button{min-width:100%;}.uib-left,.uib-right{width:100%}</style>
		<style type="text/css">.ng-animate.item:not(.left):not(.right){-webkit-transition:0s ease-in-out left;transition:0s ease-in-out left}</style>
		<style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}.ng-animate-shim{visibility:hidden;}.ng-anchor{position:absolute;}
		</style>
			
		<title>{{View::project()}} - {{View::title()}}</title>
		<link rel="icon" type="image/png" href="{{Config::getServerHost()}}/custom/data/favicon.png" />
		<meta content="Responsive Admin Web App with Bootstrap and AngularJS" name="description">
		<meta content="angularjs admin, admin templates, admin themes, bootstrap admin" name="keywords">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">

		<style>.file-input-wrapper { overflow: hidden; position: relative; cursor: pointer; z-index: 1; }.file-input-wrapper input[type=file], .file-input-wrapper input[type=file]:focus, .file-input-wrapper input[type=file]:hover { position: absolute; top: 0; left: 0; cursor: pointer; opacity: 0; filter: alpha(opacity=0); z-index: 99; outline: 0; }.file-input-name { margin-left: 8px; }</style>

		{{assign var="sServerHost" value=Config::getServerHost()}}
		{{View::loadCss("`$sServerHost`/templates/rainbow/styles/loader.css")}}
		{{View::loadCss("`$sServerHost`/templates/rainbow/bower_components/font-awesome/css/font-awesome.min.css")}}
		{{View::loadCss("`$sServerHost`/templates/rainbow/styles/main.css")}}
		{{View::loadCss("`$sServerHost`/templates/rainbow/styles/custom.css")}}
		{{View::loadJs("`$sServerHost`/templates/rainbow/scripts/custom.js")}}
		{{//View::loadLibJs("functions.js")}}

		<script type="text/javascript">
			function openSubnavi(parentmenu) {
				var subnavi = parentmenu.parentElement.getElementsByTagName("ul");
				for (var i=0; i<subnavi.length; i++) {
					if (subnavi[i].style.display == "block") {
						parentmenu.parentElement.getElementsByTagName("ul")[i].style.display = "none";
					} else {
						parentmenu.parentElement.getElementsByTagName("ul")[i].style.display = "block";
					}
				}
			}
		</script>
	</head>
	<body id="body">
		<!--[if lt IE 9]>
			<div class="lt-ie9-bg">
				<p class="browsehappy">You are using an <strong>outdated</strong> browser.</p>
				<p>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			</div>
		<![endif]-->

		<div id="loader-container" style="display: none;"></div>

		<section id="app-container">
			<section class="app" id="app" style="">
				<header class="header-container  ng-scope header-fixed bg-light" id="header" style="">
					<header class="top-header clearfix ng-scope">
						<div ui-preloader="" class="preloaderbar hide">
							<span class="bar"></span>
						</div>

						<!-- Logo -->
						<div class="logo bg-info-alt" style="">
						<a href="#/">
							<span class="logo-icon fa fa-diamond"></span><span class="logo-text ng-binding">{{View::project()}}</span>
						</a>
						</div>

						<!-- needs to be put after logo to make it work -->
						<div toggle-off-canvas="" class="menu-button">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</div>

						<div class="top-nav">
							<ul class="nav-left list-unstyled">
								<li>
									<a href="#/" data-toggle-nav-collapsed-min="" class="toggle-menu">
										<i class="fa fa-bars"></i>
									</a>
								</li>
								<li class="dropdown hidden-xs" is-open="status.isopenPalette" style="">
									<a href="javascript:void(0);" class="dropdown-toggle change-color" aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-tint"></i>
									</a>
									<div class="dropdown-menu with-arrow panel panel-default skin-options dropdown-menu-scaleIn">
										<div class="panel-heading">
											<span data-translate="COLOR_OPTIONS" class="ng-scope">Color Options</span>
										</div>
										<div class="panel-body">
											{php}
  												$this->assign("aColors", array("dark", "primary", "success", "info-alt", "warning", "danger"));
											{/php}
											{{foreach from=$aColors item=$sColor}}
												<div class="row">
													<label class="col-xs-4 skin-check">
														<input name="skin" value="{{$sColor}}_1" ng-model="main.skin" class="ng-untouched ng-valid ng-not-empty ng-dirty" aria-invalid="false" style="" type="radio">
														<span class="skin-item bg-body">
															<span class="overlay"><span class="glyphicon glyphicon-ok"></span></span>
															<span class="bg-{{$sColor}} item-header"></span>
															<span class="bg-light item-header"></span>
															<span class="bg-dark"></span>
														</span>
													</label>
													<label class="col-xs-4 skin-check">
														<input name="skin" value="{{$sColor}}_2" ng-model="main.skin" class="ng-pristine ng-untouched ng-valid ng-not-empty" aria-invalid="false" type="radio">
														<span class="skin-item bg-body">
															<span class="overlay"><span class="glyphicon glyphicon-ok"></span></span>
															<span class="bg-{{$sColor}} item-header"></span>
															<span class="bg-{{$sColor}} item-header"></span>
															<span class="bg-dark"></span> 
														</span>
													</label>
													<label class="col-xs-4 skin-check">
														<input name="skin" value="{{$sColor}}_3" ng-model="main.skin" class="ng-pristine ng-untouched ng-valid ng-not-empty" aria-invalid="false" type="radio">
														<span class="skin-item bg-body">
															<span class="overlay"><span class="glyphicon glyphicon-ok"></span></span>
															<span class="bg-{{$sColor}} item-header"></span>
															<span class="bg-{{$sColor}} item-header"></span>
															<span class="bg-light"></span> 
														</span>
													</label> 
												</div>
											{{/foreach}}
										</div>
									</div>
								</li>
							</ul>
							<ul class="nav-right pull-right list-unstyled">
								<li is-open="status.isopenLang" uib-dropdown="" class="dropdown langs text-normal ng-scope">
									<a href="{{Config::getServerHost()}}/admin/login?action=logout"><i class="fa fa-sign-out text-danger"></i></a>
								</li> 
								<li is-open="status.isopenProfile" uib-dropdown="" class="dropdown text-normal nav-profile">
									<a ng-disabled="disabled" uib-dropdown-toggle="" href="javascript:;" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">
										<span class="hidden-xs">
											<span class="ng-scope">{{View::username()}}</span>
										</span>
									</a>
								</li>
							</ul>
						</div>

					</header>
				</header>

				<div class="main-container">
					<aside class="nav-container nav-vertical ng-scope bg-dark" id="nav-container" style="">        
						<div class="nav-wrapper ng-scope">
							<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
								<ul data-highlight-active="" data-accordion-nav="" data-slim-scroll="" class="nav" id="nav" style="overflow: hidden; width: auto; height: 100%;">
									{{foreach from=View::adminNaviMenu() item=$aNavi)}}
										{{if in_array($aNavi.key, View::adminRights())}}
											<li {{if View::navi() == $aNavi.key}}class="active"{{/if}}>
												<a href="{{if isset($aNavi.link)) {{$aNavi.link}} {{else}} javascript:void(0) {{/if}}" 
												{{if isset($aNavi.subnavi)}} onclick='openSubnavi(this);'{{/if}}
												title="{{$aNavi.name}}">
													<i class="fa {{$aNavi.icon}}">
														<span class="icon-bg bg-{{$aNavi.color}}"></span>
													</i>
													<span>{{$aNavi.name}}</span> 
												</a>
												
												{{if isset($aNavi.subnavi))}}
													<ul style="display:{{if View::navi() == $aNavi.key}}block{{else}}none{{/if}}">
														{{foreach from=$aNavi.subnavi item=$aSubNavi}}
															{{if in_array($aSubNavi.key, View::adminRights()}}
																<li>
																	<a href="{{if isset($aSubNavi.link}}  {{$aSubNavi.link}} {{else}} javascript:void(0); {{/if}}">
																		<i class="fa {{$aSubNavi.icon}}">
																			<span class="icon-bg bg-{{$aSubNavi.color}}"></span>
																		</i>
																		<span>{{$aSubNavi.name}} </span> 
																	</a>
																</li>
															{{/if}}
														{{/foreach}}
													</ul>
													<i class="fa fa-caret-right icon-has-ul"></i>
												{{/if}}
											</li>
										{{/if}}
									{{/foreach}}
							
								</ul>
								<div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 856px;"></div>
								<div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div>
							</div>
						</div>
					</aside>

					<section class="content-container ainmate-slide-in-right" id="content" style="">
						<div class="page page-table ng-scope">
{{/if}}
