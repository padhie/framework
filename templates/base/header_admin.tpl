{{if Params::getParams("view")!="single"}}

<!DOCTYPE HTML>
<head lang="de">
<head>
	<meta charset="utf-8"/>
 	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="icon" type="image/png" href="{{Config::getServerHost()}}/custom/data/favicon.png" />
 	<title>{{$project}} - {{$title}}</title>
	
	{{assign var="sServerHost" value=Config::getServerHost()}}
	{{View::loadJquery()}}
	{{View::loadJqueryUi()}}
	{{View::loadBootstrap()}}
	{{View::loadFontAwesome()}}
	{{View::loadCss("`$sServerHost`/lib/vendor/bootstrap-switch/3.3.4/dist/css/bootstrap3/bootstrap-switch.min.css")}}
	{{View::loadCss("`$sServerHost`/templates/base/css/base.css")}}

	{{View::loadJs("`$sServerHost`/lib/vendor/bootstrap-switch/3.3.4/dist/js/bootstrap-switch.min.js")}}
	{{View::loadLibJs("functions.js")}}
</head>
<body>
	
	<nav class="navbar navbar-defailt navbar-fixed-top row">
		<div class="col-md-offset-1 col-md-9 container-fluid">
			<div class="collapse navbar-collapse">
				<ul class="nav nav-pills">
					{{foreach from=$adminNaviMenu item=aNavi}}
						{{if AdminsModelAccess::adminHasRightByUsername($username, $aNavi.key)}}
							{{if isset($aNavi.link)}}
								<li role="presentation" {{if View::navi() == $aNavi.key}}class="active"{{/if}}>
									<a href="{{if isset($aNavi.link)}} {{$aNavi.link}} {{else}} javascript:void(0); {{/if}}">
										{{$aNavi.name}} 
										{{if isset($aNavi.alphaflag) && $aNavi.betaflag == true}}
											{{View::render("alphaflag",false)}}
										{{elseif isset($aNavi.betaflag) && $aNavi.betaflag == true}}
											{{View::render("betaflag",false)}}
										{{/if}}
									</a>
								</li>
							{{/if}}

							{{if isset($aNavi.subnavi)}}
								{{foreach from=$aNavi.subnavi item=aSubNavi}}
									{{if AdminsModelAccess::adminHasRightByUsername($username, $aNavi.key)}}
										{{if isset($aSubNavi.link)}}
											<li role="presentation" {{if View::subnavi() == $aSubNavi.key}} class="active" {{/if}}>
												<a href="{{if isset($aSubNavi.link)}} {{$aSubNavi.link}} {{else}} javascript:void(0); {{/if}}">
													{{$aSubNavi.name}} 
													{{if isset($aSubNavi.alphaflag) && $aSubNavi.betaflag == true}}
														{{View::render("alphaflag",false)}}
													{{elseif isset($aSubNavi.betaflag) && $aSubNavi.betaflag == true}}
														{{View::render("betaflag",false)}}
													{{/if}}
												</a>
											</li>
										{{/if}}
									{{/if}}
								{{/foreach}}
							{{/if}}
						{{/if}}
					{{/foreach}}
				
					{{if $navi == "multipage"}}
						<li role="presentation" {{if $navi == "multipage"}}class="active"{{/if}}><a href="{{Config::getServerHost()}}/admin/multipage">{{Translate::say("MENU_ADMIN_MULTIPAGE")}}</a></li>
					{{/if}}
				</ul>
			</div>
		</div>
		<div class="col-md-1 text-right">
			<span>{{$username}}</span>
			<a href="{{Config::getServerHost()}}/admin/login?action=logout" class="btn btn-danger">{{Translate::say("LOGOUT")}}</a>
		</div>
	</nav>

	<div class="content row">
		<div class="col-md-offset-1 col-md-10">

{{/if}}
			{{php}} View::render("messages"); {{/php}}