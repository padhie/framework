{{assign var="blSwitch" value=false}}
{{assign var="blCodemirror" value=false}}
{{assign var="oFormObject" value=View::getVar("formObjectVariable")}}
{{if $oFormObject === false}}
	{{assign var="oFormObject" value=View::formObject()}}
{{/if}}

{{assign var="aMainData" value=$oFormObject->getMainData()}}
<form method="{{$aMainData.method}}" action="{{$aMainData.action}}" target="{{$aMainData.target}}" {{if $aMainData.enctype != ""}} "enctype='{{$aMainData.enctype}}" {{/if}} class="{{$aMainData.class}}">

	{{foreach from=$oFormObject->getData() item=aData}}
		<div class="form-group row">

			{{if $aData.type == "text" || $aData.type == "password" || $aData.type == "number" || $aData.type == "email" ||  $aData.type == "number"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<input 
					type="{{if isset($aData.type)}}{{$aData.type}}{{/if}}" 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					value="{{if isset($aData.value)}}{{$aData.value}}{{/if}}"
					placeholder="{{if isset($aData.data.placeholder)}}{{$aData.data.placeholder}}{{/if}}"
				/>
				
				
			{{elseif $aData.type == "textarea"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<textarea
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}"
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" >
					{{if isset($aData.value)}}{{$aData.value}}{{/if}}
				</textarea>					


			{{elseif $aData.type == "codemirror"}}
				{{assign var="blCodemirror" value=true}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<div 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="codemirrorBox form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}">
					<textarea 
						data-type="codemirror" 
						name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}">
						{{if isset($aData.value)}}{{$aData.value}}{{/if}}
					</textarea>
				</div>				
				

			{{elseif $aData.type == "select"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<select 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}">
					{{foreach from=$aData.data item=aSubData}}
						{{if isset($aSubData.key)}}
							<option value="{{$aSubData.key}}" {{if $aData.value==$aSubData.key}} selected="selected" {{/if}}>{{$aSubData.value}}</option>
						{{else}}
							<option value="{{$sSubKey}}" {{if $aData.value==$sSubKey}} selected="selected" {{/if}}>{{$aSubData}}</option>
						{{/if}}
					{{/foreach}}
				</select>

				
			{{elseif $aData.type == "multiple"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<select 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" multiple>
					{{foreach from=$aData.data key=mSubKey item=mSubVal}}
						<option value="{{$mSubKey}}" {{if is_array($aData.value) && in_array($mSubKey, $aData.value)}} selected='selected' {{/if}}>{{$mSubVal}}</option>
					{{/foreach}}
				</select>
				
				
			{{elseif $aData.type == "checkbox" || $aData.type == "switch"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">{{$aData.label}}</label>
				<input 
					type="checkbox" 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" {{if isset($aData.data.checked)}}checked{{/if}}/> 
				
				
			{{elseif $aData.type == "radio"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				{{foreach from=$aData.data item=aSubData}}
					<input 
						type="radio" 
						name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
						value="{{$aSubData.value}}"> {{$aSubData.key}}
				{{/foreach}}
				

			{{elseif $aData.type == "label"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">{{$aData.label}}</label>
				<label 
					class="form-control-static {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}"
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}">
					{{if isset($aData.value)}}{{$aData.value}}{{/if}}
				</label>

				
			{{elseif $aData.type == "file"}}
				<label 
					for="{{if isset($aData.name)}}{{$aData.name}}{{/if}}"
					class="control-label {{if isset($aData.class[0])}}{{$aData.class[0]}}{{/if}}">
					{{$aData.label}}
				</label>
				<div 
					class="row {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}">
					<input 
						type="text" 
						class="form-control-static col-md-10" 
						name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
						id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
						value="{{if isset($aData.value)}}{{$aData.value}}{{/if}}">
					<input 
						type="file" 
						name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}_file" 
						class="form-control-static col-md-2" />
				</div> 

				
			{{elseif $aData.type == "hidden"}}
				<input 
					type="hidden" 
					class="{{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					value="{{if isset($aData.value)}}{{$aData.value}}{{/if}}" />
				
				
			{{elseif $aData.type == "submit"}}
				<input 
					type="submit" 
					class="btn btn-success {{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}" 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					name="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					value="{{if isset($aData.value)}}{{$aData.value}}{{/if}}" />
				
				
			{{elseif $aData.type == "button"}}
				<button 
					id="{{if isset($aData.name)}}{{$aData.name}}{{/if}}" 
					class="{{if isset($aData.class[1])}}{{$aData.class[1]}}{{/if}}">
					{{if isset($aData.value)}}{{$aData.value}}{{/if}}
				</button>
			{{/if}}
			
		</div>	
	{{/foreach}}

</form>

{{if $blSwitch == true}}
	<script>
		$(".btswitch input[type=checkbox]").bootstrapSwitch();
	</script> 
{{/if}}

{{if $blCodemirror == true}}
	{{View::loadCodeMirror()}}
	<script>
		var mode = "htmlmixed";
		var elements = document.querySelectorAll("[data-type='codemirror']");
		for (var i=0; i<elements.length; i++) {
			var editor = CodeMirror.fromTextArea(elements[i], {
				lineNumbers: true,
				mode: "htmlmixed"
			});
		}
	</script>
{{/if}}