{{if Params::getParams("view")!="single"}}
		
		<!DOCTYPE html>
		<head lang="de">
		<head>
			<meta charset="utf-8"/>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<title>{{$project}} - {{$title}}</title>
		 	<link rel="icon" type="image/png" href="{{Config::getServerHost()}}/custom/data/favicon.png" />

			<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
			<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>
		
			{{View::loadBootstrap("3.3.5", false)}}
			{{View::loadFontAwesome()}}
			{{View::loadCss(Config::getServerHost()."/templates/base/css/base.css")}}
		</head>
		<body >
			<div ng-app="myApp" ng-controller="customersCtrl">
			
				<nav class="navbar navbar-defailt navbar-fixed-top row">
					<div class="col-md-offset-1 col-md-10 container-fluid">
						<div class="collapse navbar-collapse">
							<ul class="nav nav-pills">
								{{foreach from=$naviMenu item=aNavi}}
									<li role="presentation" {{if $navi == $aNavi.key}}class="active"{{/if}} id="navi_{{$aNavi.key}}" >
										<a href="#{{$aNavi.key}}" ng-click="changeContent('{{if $aNavi.key != "home"}} {{$aNavi.key}}')">
											{{$aNavi.name}} 
											{{if $aNavi["key"]=="songrequest"}}
												 {{View::render("betaflag",false)}}
											{{elseif $aNavi["key"]=="giveaway"}}
												 {{View::render("alphaflag",false)}}
											{{if}}
										</a>
									</li>
								{{/foreach}}
							
								{{if View::navi()=="multipage"}}
									<li role="presentation" {{if $navi == "multipage"}}class="active"{{/if}} id="navi_multipage">
										<a href="{{Config::getServerHost()}}/multipage">{{Translate::say("MENU_MULTIPAGE")}}</a>
									</li>
								{{elseif View::navi()=="angular"}}
									<li role="presentation" {{if $navi == "angular"}}class="active"{{/if}} id="navi_angular">
										<a href="{{Config::getServerHost()}}/angular">
											{{Translate::say("MENU_ANGULAR")}}
											{{View::render("alphaflag",false)}}
										</a>
									</li>
								{{if}}
							</ul>
						</div>
					</div>
				</nav>
			
				<div class="content row">
					<div class="col-md-offset-1 col-md-10">

{{/if}}
						{{View::render("messages")}}