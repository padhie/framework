{{assign var="aCurrentPanel" value=View::currentPanel()}}
<div class="panel panel-{{$aCurrentPanel.color}}">
	<div class="panel-heading">
		<h3 class="panel-title">{{$aCurrentPanel.name}}</h3>
	</div>
	<div class="panel-body">
		{{$aCurrentPanel.msg}}
	</div>
	<div class="panel-footer">
		<a href="{{Config::getServerHost()}}{{$aCurrentPanel.link}}">{{Translate::say("GO_TO")}}</a>
	</div>
</div>