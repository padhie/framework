{{if Params::getParams("view") !== "single"}}
					{{pluginByPosition position="content3"}}
				</div>
			</div>
			
			<div class="cookiebox"></div>

			{{pluginByPosition position="preFooter"}}

			<footer class="row footer">
				<div class="col-md-offset-1 col-md-7">
					{{pluginByPosition position="footerLeft"}}
					{{Translate::say("CREATET_BY")}} <a href="https://twitter.com/PuddyPadhie" target="_blank">@PuddyPadhie</a> | <a href="http://padhie.de/impressum.html" target="_blank">{{Translate::say("IMPRINT")}}</a> | <a href="{{LinkHelper::_("changelog")}}">{{Translate::say("CHANGELOG")}}</a>
				</div>
				<div class="col-md-2 text-right">
					{{pluginByPosition position="footerRight"}}
					<a href="https://www.twitch.tv/xoneris" target="_blank" title="{{Translate::say("TWITCH")}}"><i class="fa fa-twitch fa-2x" aria-hidden="true"></i></a>
					<a href="https://twitter.com/Xoneris" target="_blank" title="{{Translate::say("TWITTER")}}"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
					<a href="http://steamcommunity.com/groups/xoneris" target="_blank" title="{{Translate::say("STEAM")}}"><i class="fa fa-steam fa-2x" aria-hidden="true"></i></a>
					<a href="https://www.youtube.com/Xoneris" target="_blank" title="{{Translate::say("YOUTUBE")}}"><i class="fa fa-youtube fa-2x" aria-hidden="true"></i></a>
					<a href="https://www.facebook.com/xoneris" target="_blank" title="{{Translate::say("FACEBOOK")}}"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
				</div>
			</footer>

			{{pluginByPosition position="postFooter"}}

			<!-- Google Analytics -->
			{{View::render('analytict')}}
		</div>
	</body>
</html>

{{/if}}