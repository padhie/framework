{{if isset($success) && $success != ""}}
	<div class="col-md-12">
		<div class="alert alert-success" role="alert">
			<strong>{{ranslate::get("Erfolgreich")}}</strong> | {{$success}} <span aria-hidden="true" class="glyphicon glyphicon-ok"></span>
		</div>
	</div>
{{/if}}

{{if isset($warning) && $warning != ""}}
	<div class="col-md-12">
		<div class="alert alert-warning" role="alert">
			<strong>{{Translate::get("Achtung")}}</strong> | {{$warning}} <span aria-hidden="true" class="glyphicon glyphicon-warning-sign"></span>
		</div>
	</div>
{{/if}}

{{if isset($error) && $error != ""}}
	<div class="col-md-12">
		<div class="alert alert-danger" role="alert">
			<strong>{{Translate::get("Fehler")}}</strong> | {{$error}} <span aria-hidden="true" class="glyphicon glyphicon-remove"></span>
		</div>
	</div>
{{/if}}