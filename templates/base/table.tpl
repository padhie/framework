{{assign var="oTableObject" value=View::getVar("tableObjectVariable")}}
{{if $oTableObject == false}}
	{{assign var="oTableObject" value=View::tableObject()}}
{{/if}}

{{if count($oTableObject->getData()) >= 1}}
	<div class="row">
		{{assign var="sNextCol" value="col-md-12"}}
		
		{{if $oTableObject->blShowHits}}
			{{assign var="sNextCol" value="col-md-9"}}
			<div class="col-md-3">
				<!-- Hits -->
				<a href="#" class="btn btn-default disabled" role="button" style="margin:20px 0;">{{Translate::get("HITS")}}: {{$oTableObject->getHits()}}</a>
			</div>
		{{/if}}
		
		<div class="{{$sNextCol}} text-right">
			<!-- Pagination -->
			{{assign var="iMaxPages" value=$oTableObject->getMaxPage()}}
			{{if $iMaxPages > 1}}
				{{assign var="iStartPages" value=$oTableObject->getPage()-3}}
				{{if $iStartPages <= 0}}
					{{assign var="iStartPages" value=1}}
				{{/if}}

				{{assign var="iEndPages" value=$oTableObject->getPage()+3}}
				{{if $iEndPages > $oTableObject->getMaxPage()}}
					{{assign var="iEndPages" value=$oTableObject->getMaxPage()}}
				{{/if}}
			
				<nav>
					<ul class="pagination">
					
						{{if $oTableObject->getPage() > 1}}
							<!-- Back BTNs -->
							<li>
								<a href="?p=1" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							<li>
								<a href="?p={{$oTableObject->getPage()-1}}" aria-label="Previous">
									<span aria-hidden="true">&lsaquo;</span>
								</a>
							</li>
						{{/if}}
						
						<!-- Pages -->
						{{for $i=0 to $iEndPages}}
							<li {{if $i==$oTableObject->getPage()}} class='active' {{/if}}>
								<a href="?p={{$i}}">{{$i}}</a>
							</li>
						{{/for}}
						
						
						{{if $oTableObject->getPage() < $iMaxPages}}
							<!-- Forward BTN -->
							<li >
								<a href="?p={{$oTableObject->getPage()+1}}" aria-label="Next">
									<span aria-hidden="true">&rsaquo;</span>
								</a>
							</li>
							<li >
								<a href="?p={{$oTableObject->getMaxPage()}}" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
						{{/if}}
					</ul>
				</nav>
			{{/if}}
				
		</div>
	</div>

	<!-- Table -->
	<table class="table table-striped table-hover table-responsive">
		{{assign var="aKeys" value=$oTableObject->getUseingKeys()}}
		
		<!-- Head -->
		<thead>
			<tr>
				{{foreach from=$aKeys item=sName}}
					<th><strong>{{$sName}}</strong></th>
				{{/foreach}}
			</tr>
		</thead>

		<!-- Body -->
		<tbody>
			{{foreach from=$oTableObject->getData() item=aRow}}
				<tr>
					{{foreach from=$aKeys  item=sKey key=sName}}
						<td>
							{{if isset($aRow.$sName)}}
								{{$aRow.$sName}}
							{{else}}
								{{$oTableObject->getSingleExtra($sKey, $aRow)}}
							{{/if}}
						</td>
					{{/foreach}}
				</tr>
			{{/foreach}}
		</tbody>

	</table>
{{/if}}