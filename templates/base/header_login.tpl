<!DOCTYPE HTML>
<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{$project}} - {{$title}}></title>
 	<link rel="icon" type="image/png" href="{{Config::getServerHost()}}/custom/data/favicon.png" />
		 	
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	{{View::loadLibJs("functions.js")}}
	<script type="text/javascript">
		setCookie("backendLogin", "invalid");
		setCookie("username", "invalid");
	</script>
</head>
<body>
	<div class="col-md-offset-3 col-md-6" style="padding-top: 50px;">