function viewCookieBox(sElement, sMessage) {
	if (getCookie("cookie") != "1") {
		var sTemplate = ""+
			"<div class='close'>X</div>"+
			"<p>"+sMessage+"</p>"
			"";
	
		$(sElement).html(sTemplate);
		
		$(sElement).show();
		
		$(sElement+" .close").show();
		
		$(sElement+" .close").click(function(){
			closeCookieBox(sElement);
		});	
	} else {
		$( sElement ).remove();
	}
};

function closeCookieBox(sElement) {
	$(sElement).fadeOut();

	setCookie("cookie", "1");
};