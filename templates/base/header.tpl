{{if Params::getParams("view") != "single"}}

<!DOCTYPE HTML>
<head lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 	<link rel="icon" type="image/png" href="{{Config::getServerHost()}}/custom/data/favicon.png" />
 	<title>{{$project}} - {{$title}}</title>
	
	{{assign var="sServerHost" value=Config::getServerHost()}}
	{{View::loadJquery()}}
	{{View::loadBootstrap()}}
	{{View::loadFontAwesome()}}
	{{View::loadCss("`$sServerHost`/templates/base/css/base.css")}}
	{{View::loadLibJs("functions.js")}}
	{{View::loadJs("`$sServerHost`/templates/base/js/cookieMsg.js")}}
	<script type="text/javascript">
		$(document).ready(function(){
			viewCookieBox(".cookiebox", "{{Translate::say("COOKIEMSG")}}");
		});
	</script>
	{{pluginByPosition position="head"}}
</head>
<body>
	<div>
		{{pluginByPosition position="navihead"}}
		
		<div class="content row">
			<div class="col-md-offset-1 col-md-10">

{{/if}}
				{{pluginByPosition position="content1"}}
				{{View::render("messages")}}
				{{pluginByPosition position="content1"}}