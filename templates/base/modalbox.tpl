<!-- Modal -->
<script type="text/javascript">
	var selectedId 	= 0;
	function openModal(iId) {
		selectedId = iId;
		$('#myModal').modal('show');
	}

	function successModal() {
		window.location = "{{$sModalLink}}?action=delete&id="+selectedId;
	}
</script>
{{php}}
	View::modalTitle(Translate::get("MODAL_TITLE"));
	View::modalText(Translate::get("MODAL_TEXT"));
{{/php}}
{{View::render("modal")}}