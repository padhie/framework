<?php
include_once __DIR__."/Config.php";
include_once __DIR__."/lib/php/autoloader.php";

// ###################################
// # VORBEREITUNG
// ###################################
// Benötigte Variablen initialisieren
$sRequestUri = $_SERVER["REQUEST_URI"];
$aParams = array();

// GET-Parameter aus Variable entfernen
if (strpos($sRequestUri, "?") !== false) {
	$sRequestUri = substr($sRequestUri, 0, strpos($sRequestUri, "?"));
}

// Prefunktionalitäten Initialisieren
Config::bootstrap();
if (strpos($sRequestUri, "/admin") !== false) {
	Config::adminBootstrap();
}


// ###################################
// # NAVI PRÜFUNG
// ###################################
// Prüfen ob Navi für den Link existiert
$oMenuItem = Factory::getModel("menuitem", "content");
$aMenuItem = $oMenuItem->getDataByLink($sRequestUri);
// unset($oMenuItem);
if ($aMenuItem !== false) {
	$aSettings = $oMenuItem->getArrayBySettings($aMenuItem["settings"]);

	$aParams["{SUBFOLDER}"]		= "";
	$aParams["{MODUL}"]			= $aSettings["modul"];
	$aParams["{CONTROLLER}"]	= $aSettings["controller"];

	if (!is_array($_GET))				$_GET = array();
	if (!is_array($_POST))				$_POST = array();
	if (!is_array($_REQUEST))			$_REQUEST = array();
	if (!is_array($aSettings["extra"]))	$aSettings["extra"] = array();

	$_GET 		= array_merge($aSettings["extra"], $_GET);
	$_POST 		= array_merge($aSettings["extra"], $_POST);
	$_REQUEST 	= array_merge($aSettings["extra"], $_REQUEST);
	unset($aSettings);
}
unset($oMenuItem, $aMenuItem);


// ###################################
// # LINKPATTERN PRÜFUNG
// ###################################
// Wenn keine Navi für Link existiert, Anhand von Link-Pattern information Ermitteln
if (count($aParams) == 0) {
	// Ermittel Parameter anhand vom Pattern
	$aParams = LinkHelper::getVarsFromUrl($sRequestUri);


	// Parameter überschreiben, wenn welche Übergeben worden sind
	$aParams["{SUBFOLDER}"]		= Params::getParams("subfolder", $aParams["{SUBFOLDER}"]);
	$aParams["{MODUL}"]			= Params::getParams("modul", $aParams["{MODUL}"]);
	$aParams["{CONTROLLER}"]	= Params::getParams("controller", $aParams["{CONTROLLER}"]);
}


// ###################################
// # VARIABLEN AUFBEREITUNG
// ###################################
if ($aParams["{MODUL}"] === "") {
	$aParams["{MODUL}"] = "index";
}
if ($aParams["{MODUL}"] !== "" && $aParams["{CONTROLLER}"] === "") {
	$aParams["{CONTROLLER}"] = $aParams["{MODUL}"];
}


// Fix für Subfolder ohne Modulübergabe
if ($aParams["{SUBFOLDER}"] !== "" && $aParams["{MODUL}"] === "") {
	$aParams["{MODUL}"] = "index";
}


// ###################################
// # CONTROLLER AUFRUFEN
// ###################################
// Maincontroller Initialisieren
$oController = new controller();

// Informationen setzen
$oController->sSubfolder 	= $aParams["{SUBFOLDER}"];
$oController->sModul 		= $aParams["{MODUL}"];
$oController->sController 	= $aParams["{CONTROLLER}"];


// Controller aufrufen
$oController->run();