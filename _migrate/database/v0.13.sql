INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`)
VALUES ('de', 'web', 'SEARCH', 'Suche', '');


INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`)
VALUES ('de', 'bot', 'BOT_LEVELUP', '%USER% ist nun Rang %RANG%', '%USER% %RANG% %GROUP%');


INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`)
VALUES ('de', 'web', 'COMMANDS_TIMER_TEXT', 'Zeit, bis zum nächsten timekommand, in Extra', '');


INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`)
VALUES ('de', 'web', 'MENU_ANGULAR', 'Angular', '');


INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`) VALUES 
('de', 'web', 'SONGREQUEST_PARAMETERS_T', 'Intervall für das aktualisieren der Daten (in ms)', ''),
('de', 'web', 'SONGREQUEST_PARAMETERS_C', 'Schriftfarbe (default=black)', ''),
('de', 'web', 'SONGREQUEST_PARAMETERS_BG', 'Hintergrundfarbe MIT # (default=transparent)', '');


ALTER TABLE `language` ADD UNIQUE (`lang`, `key`);


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(239,	'de',	'web',	'LAST_SONG',	'letzter Song',	'');


INSERT INTO `settings` (`name`, `value`) VALUES ('points_time', '5');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(240,	'de',	'web',	'COOLDOWN_INTERVAL',	'Cooldown intervall (s)',	'');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(241,	'de',	'web',	'POINT',		'Punktesystem',			''),
(242,	'de',	'web',	'POINT_TIME',	'Punkte pro Minute',	''),
(243,	'de',	'web',	'POINT_POST',	'Punkte pro Post',		'');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(244, 'de', 'web', 'EVENT', 'Event', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(245, 'de', 'bot', 'USER_NOT_FOUND', 'User not found!', '');


ALTER TABLE `events` CHANGE `type` `type` VARCHAR(32) NOT NULL DEFAULT 'other';

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(246, 'de', 'web', 'TEMPLATES_PARAMETERS_D', 'd;wie lange soll der inhalt angezeigt werden (in ms) [default=5000]', ''),
(247, 'de', 'web', 'TEMPLATES_PARAMETERS_CE', 'ce;in welchem intervall soll nach neuen events geprüft werden (in ms) [default=20000]', ''),
(248, 'de', 'web', 'TEMPLATES_PARAMETERS_QE', 'qe;in welchem intervall soll nach neuen inhalt geprüft werden (in ms) [default=20000]', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(249, 'de', 'web', 'USER_MODAL_TITLE', 'User löschen?', ''),
(250, 'de', 'web', 'USER_MODAL_TEXT', 'Soll der User wirklich gelöscht werden?', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(251, 'de', 'web', 'RESTART', 'Restart', ''),
(252, 'de', 'bot', 'BOT_RESTART', 'Bot wird neugestartet!', ''),
(253, 'de', 'bot', 'BOT_DIE', 'Bot wird heruntergefahren!', '');
