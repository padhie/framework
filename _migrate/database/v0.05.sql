CREATE TABLE `counter` (
  `idCounter` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `deadline` datetime NOT NULL,
  PRIMARY KEY (`idCounter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;