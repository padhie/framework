ALTER TABLE `giveaway` 
	ADD `duration` INT(3) NULL DEFAULT NULL AFTER `dateExecuted`;

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(292, 'de', 'bot', 'RAFFLE_REMEMBER', "Ihr könnt immernoch am aktuellen Gewinnspiel teilnehmen. Einfach !raffle in den Chat schreiben", ''),
(293, 'de', 'bot', 'RAFFLE_START_AUTOENDING', "Giveaway gestartet! Benutz !raffle zum joinen (Raffle endet in %DAUER% Minuten)", '%DAUER%'),
(294, 'de', 'bot', 'RAFFLE_CREATED_BY', "erstellt von", ''),
(295, 'de', 'web', 'GIVEAWAY_AUTOENDINGTIME', "Dauer für automatischen beenden (Angabe in Minuten)", ''),
(296, 'de', 'web', 'GIVEAWAY_REMEMBERTIME', "Intervall für die Erinneringsnachricht (Angabe in Minuten)", '');


ALTER TABLE `quotes` ADD `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `minRang`;


CREATE TABLE `statistic` ( 
	`idStatistic` INT(11) NOT NULL AUTO_INCREMENT,
	`online` TINYINT(1) NOT NULL DEFAULT 0,
	`game` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`followers` INT(11) NOT NULL, 
	`hostings` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
	`hostingsCount` INT(20) NOT NULL, 
	`viewsCount` INT(20) NOT NULL, 
	`time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
	PRIMARY KEY (`idStatistic`)
) ENGINE = InnoDB;


ALTER TABLE `user` CHANGE `idDiscordUser` `idDiscordUser` VARCHAR(32) NULL DEFAULT NULL COMMENT 'ID der Discord-Account (eindeutig)';

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(297, 'de', 'web', 'CONNECT_WITH_DISCORD', "Mit Discord verknüpft", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(298, 'de', 'web', 'GIVEAWAY_TEMPLATE', "Template für Streamalert (leer lassen um nicht zu verwenden)", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(299, 'de', 'bot', 'SUBSCRIPTION_NORMAL', "%USER% HAT SUBSCRIPT!", '%USER%'),
(300, 'de', 'bot', 'SUBSCRIPTION_PRIME', "%USER% HAT MIT PRIME SUBSCRIPT!", '%USER%'),
(301, 'de', 'bot', 'CHEER', "%USER% hat mit %AMOUNT_CHEER% cheers gespendet!", '%USER% %AMOUNT_CHEER%'),
(302, 'de', 'bot', 'STREAMSTATS', "Titel=%TITEL% | Spiel=%GAME% | %AMOUNBT_FOLLOWER% followers | %AMOUNBT_SUBSCRIPTIONS% subscriptions", '%TITEL% %GAME% %AMOUNBT_FOLLOWER% %AMOUNBT_SUBSCRIPTIONS%');

