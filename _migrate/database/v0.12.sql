CREATE TABLE `language` (
  `idLanguage` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `lang` char(2) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Sprachkey 2stellig',
  `group` varchar(255) COLLATE 'utf8_general_ci' NULL COMMENT 'guppe (bot oder webgiu)',
  `key` varchar(255) COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Key, womit die phrase identifiziert werden kann',
  `value` text COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Sprachphrase',
  `notic` text COLLATE 'utf8_general_ci' NOT NULL COMMENT 'Hinweise'
) COLLATE 'utf8_general_ci';