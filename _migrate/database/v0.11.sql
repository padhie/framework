ALTER TABLE `commands` 
ADD `minRangOld` INT(11) DEFAULT 0,
ADD `active` TINYINT(1) DEFAULT 0, 
MODIFY `minRang` `minRang` TEXT;

UPDATE `commands` SET `minRangOld` = `minRang`;

ALTER TABLE `giveaway`
CHANGE `minRangWinner` `minRangWinner` text COLLATE 'utf8_general_ci' NOT NULL AFTER `type`;

ALTER TABLE `commands` 
CHANGE `minRang` `minRangOld` INT( 11 ) NOT NULL
ADD `minRang` TEXT NOT NULL AFTER `minRangOld`;

ALTER TABLE `rang`
ADD `group` varchar(255),
ADD `index` INT(11),
ADD `minPost` INT(11),
ADD `minTime` INT(11);

ALTER TABLE `songrequest`
DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci,
ADD `source` ENUM( 'youtube' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
ADD `duration` VARCHAR( 45 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

CREATE TABLE `settings` (
	`name` VARCHAR(255),
	`value` TEXT
);

ALTER TABLE `settings` 
CHANGE `name` `name` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
CHANGE `postetBy` `postedBy` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
CHANGE `value` `value` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' ;

ALTER TABLE `user`
DROP COLUMN `idRang`,
ADD `points` BIGINT(20) DEFAULT 0 AFTER `lastUpdate`,
CHANGE `lastUpdate` `lastUpdate` DATETIME NOT NULL DEFAULT '1970-01-01 00:00:00';

INSERT INTO `settings` (`name`, `value`) VALUES
('style_admin_default',	'rainbow'),
('style_available',	'base,rainbow'),
('style_default',	'base');