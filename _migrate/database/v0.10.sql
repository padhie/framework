CREATE TABLE `command_has_rang` (
	`idCommand` int(11) NOT NULL,
	`idRang` int(11) NOT NULL,
	PRIMARY KEY (`idCommand`,`idRang`)
);

CREATE TABLE `user_has_rang` (
	`idUser` int(11) NOT NULL,
	`idRang` int(11) NOT NULL,
	PRIMARY KEY (`idUser`,`idRang`)
);

CREATE TABLE `quote_has_rang` (
	`idQuote` int(11) NOT NULL,
	`idRang` int(11) NOT NULL,
	PRIMARY KEY (`idQuote`,`idRang`)
);

ALTER TABLE `quotes`
ADD `isPublic` tinyint(1) DEFAULT 0;

ALTER TABLE `user`
ADD UNIQUE `user` (`user`);