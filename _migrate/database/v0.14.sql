CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `typ` varchar(64) DEFAULT NULL COMMENT "z.B. command, löschen, ändern",
  `name` varchar(255) DEFAULT NULL COMMENT "den command, das Modul...",
  `text` text,
  `user` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL
);



INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(254, 'de', 'web', 'TIME', 'Zeit', ''),
(255, 'de', 'web', 'DATE', 'Datum', ''),
(256, 'de', 'web', 'LOG', 'Log', ''),
(257, 'de', 'web', 'MENU_ADMIN_LOG', 'Log', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(258, 'de', 'web', 'LOGINWHISPER', 'Loginwhisper', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(259, 'de', 'web', 'IGNORECURRENTSONG', "Aktuellen Song bei 'allowPostSong' ignorieren", ''),
(260, 'de', 'web', 'DUPLICATESONGINLIST', 'Darf der gleiche Song mehrmals gepostet werden?', ''),
(261, 'de', 'bot', 'SONGREQUEST_DUPLICATE_SONG', 'Song ist schon in der Liste vorhanden', '');


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(262, 'de', 'bot', 'QUOTE_OUTPUT_MSG', "/me Quote #%INDEX%:'%TEXT%' von %FROM%", '%INDEX% %TEXT% %FROM%');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(263, 'de', 'bot', 'RAFFLE_NOTALLOW', "You are not allow to join this raffle!", '%USER%');



INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(264, 'de', 'web', 'COMMANDS_EVENT_TEXT', "Name des Templates (welches verwendet werden soll) in 'Extra' schreiben", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(265, 'de', 'web', 'TEST', "Testen", '');

ALTER TABLE `quotes` 
CHANGE `minRangOld` `minRangOld` INT( 11 ) NULL DEFAULT NULL,
CHANGE `minRang` `minRang` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;


INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(266, 'de', 'web', 'SPAMDETECTION', "SpamDetection verwenden", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(267, 'de', 'web', 'SETTINGSCHANGEWHISPER', "SettingsChangeWhisper", '');