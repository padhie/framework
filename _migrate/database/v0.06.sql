ALTER TABLE `songrequest` 
ADD `source` VARCHAR(45) NOT NULL AFTER `source`,
ADD `duration` VARCHAR(45) NOT NULL AFTER `title`,
ADD `postedBy` VARCHAR(45) NOT NULL AFTER `duration`;

CREATE TABLE IF NOT EXISTS `settings` (
  `name` varchar(255) NOT NULL PRIMARY KEY,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;