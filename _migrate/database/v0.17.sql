CREATE TABLE IF NOT EXISTS `gamesettings` ( 
	`idGameSetting` INT(11) NOT NULL AUTO_INCREMENT,
	`game` VARCHAR(255) NOT NULL,
	`type` VARCHAR(255) NOT NULL,
	`settings` LONGTEXT NOT NULL,
	PRIMARY KEY (`idGameSetting`)
);

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(303, 'de', 'web', 'COMMANDS_SETTINGS', "Commands im Chat verwalten", ''),
(304, 'de', 'web', 'COMMANDS_RANG', "Ränge die editieren dürfen", ''),
(305, 'de', 'web', 'COMMANDS_MODS', "Mods dürfen editieren", ''),
(306, 'de', 'web', 'COMMANDS_REGULARS', "Regulars dürfen editieren", ''),
(307, 'de', 'bot', 'COMMANDS_NOTALLOW', "Du hast keine Bereichtigung um einen Command bearbeiten", ''),
(308, 'de', 'bot', 'COMMANDS_UNKNOWN', "Unbekannter Fehler, versuche es später erneut", ''),
(309, 'de', 'bot', 'COMMANDS_ADDET', "Command wurde erstellt", ''),
(310, 'de', 'bot', 'COMMANDS_EDIT', "Command wurde bearbeitet", ''),
(311, 'de', 'bot', 'COMMANDS_DELETED', "Command wurde entfernt", '');

CREATE TABLE IF NOT EXISTS `cms_menu` (
	`idCmsMenu` INT(11) NOT NULL AUTO_INCREMENT,
	`idParentMenu` INT(11) NOT NULL DEFAULT 0,
	`name` VARCHAR(255) NOT NULL DEFAULT '',
	`key` VARCHAR(255) NOT NULL DEFAULT '',
	`link` TEXT NOT NULL,
	`linkType` ENUM('custom', 'modul') NOT NULL DEFAULT 'custom',
	`active` TINYINT(11) NOT NULL DEFAULT 0,
	`target` ENUM('_self', '_blank') NOT NULL DEFAULT '_self',
	`ordering` INT(11) NOT NULL DEFAULT 0,
	PRIMARY KEY(`idCmsMenu`)
);

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(312, 'de', 'web', 'MENU_ADMIN_CMS', "CMS", ''),
(313, 'de', 'web', 'MENU_ADMIN_CMS_MENU', "Menü", ''),
(314, 'de', 'web', 'MENU_ADMIN_CMS_PAGES', "Seiten", ''),
(315, 'de', 'web', 'CMS_MENU_MODAL_TITLE', "Menu l&ouml;schen?", ''),
(316, 'de', 'web', 'CMS_MENU_MODAL_TEXT', "Soll der Menupunkt wirklich gel&ouml;scht werden?", ''),
(317, 'de', 'web', 'CMS_PAGE_MODAL_TITLE', "Seite l&ouml;schen?", ''),
(318, 'de', 'web', 'CMS_PAGE_MODAL_TEXT', "Soll die Seite wirklich gel&ouml;scht werden?", ''),
(319, 'de', 'web', 'PARENTID', "ParentId", ''),
(320, 'de', 'web', 'ORDERING', "Sortierung", ''),
(321, 'de', 'web', 'TOP_LEVEL', "oberste Ebene", '');

ALTER TABLE `cms_menu`
	ADD `group` VARCHAR(255) NOT NULL DEFAULT '' AFTER `idParentMenu`,
	ADD `color` VARCHAR(255) NOT NULL DEFAULT '',
	ADD `icon` VARCHAR(255) NOT NULL DEFAULT '';

CREATE TABLE IF NOT EXISTS `cms_page` (
	`idCmsPage` INT(11) NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`key` VARCHAR(255) NOT NULL DEFAULT '',
	`link` VARCHAR(255) NOT NULL DEFAULT '',
	`content` TEXT NOT NULL,
	`active` TINYINT(11) NOT NULL DEFAULT 0,
	PRIMARY KEY(`idCmsPage`)
);

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(322, 'de', 'web', 'TARGET', "Ziel / zu öffnen", ''),
(323, 'de', 'web', '_SELF', "Im gleichen Fenster", ''),
(324, 'de', 'web', '_BLANK', "Im neuen Fenster", '');

ALTER TABLE `commands` 
CHANGE `extra` `extra` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;


UPDATE `language`
SET
	`notic` = CONCAT(`notic`, ' %DATE%')
WHERE 
	`key` = 'QUOTE_OUTPUT_MSG' AND
	`group` = 'bot'
;


INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`) VALUES
('de', 'web', 'MENU_MENU_MODAL_TITLE', "Soll der Menupunkt wirklich gel&ouml;scht werden?", ''),
('de', 'web', 'MENU_MENU_MODAL_TEXT', "Seite l&ouml;schen?", '');

RENAME TABLE `cms_menu` TO `menu`;