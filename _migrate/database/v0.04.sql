CREATE TABLE IF NOT EXISTS `events` (
  `idEvent` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('host','follow','sub','donation','giveaway','other') NOT NULL DEFAULT 'other',
  `new` tinyint(1) NOT NULL,
  `user` varchar(255) NOT NULL,
  `message` text,
  PRIMARY KEY (`idEvent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------
  
CREATE TABLE IF NOT EXISTS `templates` (
  `idTemplate` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`idTemplate`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------
  
CREATE TABLE `carousel` (
  `idCarousel` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  PRIMARY KEY (`idCarousel`),
  KEY `group` (`group`);
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -------------------------

CREATE TABLE IF NOT EXISTS `host` (
  `idHost` int(11) NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) NOT NULL,
  `prio` int(1) NOT NULL,
  `lastHost` datetime NOT NULL,
  PRIMARY KEY (`idHost`),
  UNIQUE KEY `channel` (`channel`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;