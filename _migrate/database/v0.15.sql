INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES 
(268, 'de', 'web', 'MENU_ADMIN_COMMANDLOG', "Commandlog", ''),
(269, 'de', 'web', 'MENU_ADMIN_CHATLOG', "Chatlog", ''),
(270, 'de', 'web', 'MENU_ADMIN_MODLOG', "Modlog", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(271, 'de', 'web', 'REASON', "Grund", ''),
(272, 'de', 'web', 'MODERATOR', "Mod", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(273, 'de', 'web', 'MODLOG', "ModLog", '');

CREATE TABLE `admin_rights` (
  `idAdmin` int(11) NOT NULL COMMENT 'Eindeutiger Key von Tabelle admin',
  `right` varchar(64) NOT NULL COMMENT 'key des recht'
);

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(274, 'de', 'web', 'RIGHTS', "Rechte", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(275, 'de', 'web', 'PARAMETERS_EXTRA_TEMPLATES', "(nur für Eventseite verfügbar)", ''),
(276, 'de', 'web', 'LINKS_OBS_SINGLE', "<a href='http://xoneris.local/custom/api/?action=displaySingleTemplate&t=%TEMPLATENAME%'>Templatelink für OBS</a>", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(277, 'de', 'web', 'LOG_MODAL_TITLE', "Logeintrag l&ouml;schen", ''),
(278, 'de', 'web', 'LOG_MODAL_TEXT', "Soll der Logeintrag wirklich gel&ouml;scht werden?", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(279, 'de', 'web', 'MENU_ADMIN_ALL', "Alle Admins", ''),
(280, 'de', 'web', 'MENU_ADMIN_OWN', "Eigene Admindaten", '');

ALTER TABLE `user` 
ADD `idDiscordUser` INT(32) NULL DEFAULT NULL COMMENT 'ID der Discord-Account (eindeutig)' AFTER `idUser`, 
ADD INDEX (`idDiscordUser`);

ALTER TABLE `lists` 
ADD `subtype` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `type`;

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(281, 'de', 'web', 'LISTS_MODAL_TITLE', "Soll der Eintrag wirklich gelöscht werden?", ''),
(282, 'de', 'web', 'LISTS_MODAL_TEXT', "Eintrag löschen?", ''),
(283, 'de', 'web', 'MENU_ADMIN_LISTS', "Listen", ''),
(284, 'de', 'web', 'MENU_ADMIN_WORDLIST', "Wortliste", ''),
(285, 'de', 'web', 'MENU_ADMIN_LINKLIST', "Linkliste", '');

ALTER TABLE `log` CHANGE `time` `time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(286, 'de', 'web', 'MENU_ADMIN_ADMINLOG', "Adminlog", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(287, 'de', 'web', 'LINK', "Link", ''),
(288, 'de', 'web', 'MENU_ADMIN_FILES', "Files", ''),
(289, 'de', 'web', 'FILES_MODAL_TITLE', "Datei löschen", ''),
(290, 'de', 'web', 'FILES_MODAL_TEXT', "Soll die Datei wirklich gelöscht werden?", '');

INSERT INTO `language` (`idLanguage`, `lang`, `group`, `key`, `value`, `notic`) VALUES
(291, 'de', 'web', 'GIVEAWAY_DISPLAY_INTROTEXT', "Introtext anzeigen?", '');