ALTER TABLE `quotes` 
CHANGE `text` `text` text COLLATE 'utf8_general_ci' NOT NULL AFTER `index`,
CHANGE `user` `user` varchar(25) COLLATE 'utf8_general_ci' NOT NULL AFTER `text`,
COLLATE 'utf8_general_ci';

ALTER TABLE `rang` 
ADD `maxSongrequest` int NOT NULL COMMENT 'wie viel Songs der Person gleichzeitig in der liste sein dürfen',
COLLATE 'utf8_general_ci';

ALTER TABLE `songrequest`
CHANGE `duration` `duration` varchar(45) COLLATE 'utf8_general_ci' NOT NULL AFTER `title`,
CHANGE `source` `source` enum('youtube') COLLATE 'utf8_general_ci' NOT NULL AFTER `idSongrequest`,
COLLATE 'utf8_general_ci';

ALTER TABLE `admin`
ADD `style` varchar(255) COLLATE 'utf8_general_ci' NOT NULL;