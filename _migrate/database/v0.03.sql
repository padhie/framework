CREATE TABLE `giveaway` (
  `idGiveaway` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `type` enum('raffle','random') NOT NULL,
  `minRangWinner` int(11) NOT NULL,
  `anonym` tinyint(1) NOT NULL,
  `intro` text NOT NULL,
  `profit` text NOT NULL,
  `status` enum('new','current','ended','lock') NOT NULL,
  `creator` varchar(255) NOT NULL,
  `confirmer` varchar(255) DEFAULT NULL,
  `winner` varchar(255) DEFAULT NULL,
  `dateCreate` datetime NOT NULL,
  `dateExecuted` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `giveaway`
  ADD PRIMARY KEY (`idGiveaway`);

ALTER TABLE `giveaway`
  MODIFY `idGiveaway` int(11) NOT NULL AUTO_INCREMENT;