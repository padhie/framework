ALTER TABLE admin
	ADD `master` TINYINT(1) DEFAULT 0;

CREATE TABLE `content_menu` (
  `idContentMenu` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idContentMenu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `content_menu_item` (
  `idContentMenuItem` int(11) NOT NULL AUTO_INCREMENT,
  `idParent` int(11) NOT NULL DEFAULT '0',
  `idContentMenu` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `linkType` enum('custom','modul') NOT NULL DEFAULT 'custom',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `target` enum('_self','_blank') NOT NULL DEFAULT '_self',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `settings` text NOT NULL,
  PRIMARY KEY (`idContentMenuItem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `content_page` (
  `idContentPage` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idContentPage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `plugins_positionen` (
  `idPluginsPosition` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  PRIMARY KEY (`idPluginsPosition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `plugins_positionen` (`idPluginsPosition`, `name`, `key`) VALUES
(1, 'Kopfnavigation', 'navihead'),
(2, 'Headbereich', 'head'),
(3, 'Footer links', 'footerLeft'),
(4, 'Footer Rechts', 'footerRight'),
(5, 'Content Position 1', 'content1'),
(6, 'Content Position 2', 'content2'),
(7, 'Content Position 3', 'content3'),
(8, 'Vor Footer', 'preFooter'),
(9, 'Nach Footer', 'postFooter');

CREATE TABLE `plugins_regist` (
  `idPluginsRegist` int(11) NOT NULL AUTO_INCREMENT,
  `idPluginsPosition` int(11) NOT NULL,
  `modul` varchar(255) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text NOT NULL,
  PRIMARY KEY (`idPluginsRegist`),
  KEY `idPluginsPosition` (`idPluginsPosition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`) VALUES
('de', 'web', 'MODAL_TITLE', "Eintrag löschen?", ''),
('de', 'web', 'MODAL_TEXT', "Soll der Eintrag wirklich gelöscht werden?", '');

UPDATE `language` SET `value` = CONCAT("t = ", `value`) WHERE `key` = "SONGREQUEST_PARAMETERS_T";
UPDATE `language` SET `value` = CONCAT("c = ", `value`) WHERE `key` = "SONGREQUEST_PARAMETERS_C";
UPDATE `language` SET `value` = CONCAT("bg = ", `value`) WHERE `key` = "SONGREQUEST_PARAMETERS_BG";

INSERT INTO `language` (`lang`, `group`, `key`, `value`, `notic`) VALUES
('de', 'web', 'SONGREQUEST_PARAMETERS_TPL', "tpl = Template (z.B.: 2)", ''),


ALTER TABLE `songrequest` 
  ADD `inPlaylist` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Angabe ob dieser Song in der Playlist sein soll';