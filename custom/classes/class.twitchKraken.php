<?php

class twitchKraken {
	protected $sBaseUrl = "https://api.twitch.tv/kraken/";
	protected $sOauth = "";
	protected $sRresponse = "";
	protected $sClientId = "mjz5b2s8sj5vnmjlb8hyrtu55xie1yz";
	protected $sAdditionalString = "";

	public function setOAuth($sOauth) {
		$this->sOaAuth = $sOauth;
	}

	public function setAdditionalString($sAdditional) {
		$this->sAdditionalString = $sAdditional;
	}
	
	public function getAccesTokenUrl() {
		$sUrl = "https://api.twitch.tv/kraken/oauth2/authorize?";
		$sParams = "response_type=token" + "&client_id=" + $this->sClientId + "&redirect_uri=http://padhie.de/request.php" + "&scope=channel_editor";
		return $sUrl.$sParams;
	}

	public function loadData($sUrlextension) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->sBaseUrl.$sUrlextension.$this->sAdditionalString);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl,CURLOPT_HTTPHEADER, array(
			'Client-ID: '.$this->sClientId
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$this->sResponse = curl_exec($curl);
		curl_close($curl);
	}

	public function changeData($sUrlextension, $sData) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->sBaseUrl.$sUrlextension.$this->sAdditionalString."?".$sData);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
		curl_setopt($curl,CURLOPT_HTTPHEADER, array(
			'Client-ID: '.$this->sClientId,
			"Authorization: OAuth ".$this->sOauth,
			"Accept: application/vnd.twitchtv.v2+json"
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$this->sResponse = curl_exec($curl);
		curl_close($curl);
	}

	public function getData() {
		return $this->sResponse;
	}

	public function loadChannelData($sChannel) {
		$this->loadData("channels/".$sChannel);
	}

	public function loadChatData( $sChannel) {
		$this->loadData("chat/" + $sChannel);
	}

	public function loadFollowsData( $sChannel) {
		$this->loadData("channels/" + $sChannel + "/follows");
	}

	public function loadSubscriptionsData( $sChannel) {
		$this->loadData("channels/" + $sChannel + "/subscriptions");
	}

	public function loadUserData( $sUser) {
		$this->loadData("users/" + $sUser);
	}

	public function loadUserFollowChannel( $sUser, $sCchannel) {
		$this->loadData("users/" + $sUser + "/follows/chanels/" + $sChannel);
	}

	public function loadVideoData( $sVideoId) {
		$this->loadData("videos/" + $sVideoId);
	}

	public function loadStreamData( $sChannel) {
		$this->loadData("streams/" + $sChannel);
	}

	public function loadUserFollows( $sUser) {
		$this->loadData("/users/"+$sUser+"/follows/channels");
	}
	
	public function changeTitle( $sChannel,  $sTitle) {
		$sTitle = str_replace(" ", "+", $sTitle);
		$this->changeData("channels/" + $sChannel, "channel[status]=" + $sTitle);
	}

	public function changeGame( $sChannel,  $sGame) {
		$sGame = str_replace(" ", "+", $sGame);
		$this->changeData("channels/" + $sChannel, "channel[game]=" + $sGame);
	}

	public function followUserToChannel($sUser, $sChannel) {
		$this->changeData("/users/" + $sUser + "/follows/channels/" + $sChannel, "");
	}
}