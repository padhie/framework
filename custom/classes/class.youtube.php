<?php

class youtube {
	private $_sBaseUrl = "https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&key=AIzaSyA6hgm1UuzFwx7Y7yjhFQdiFyGzqfIA4J4&id=";
	public $sRespons;
	
	public function getVideoId($sUrl) {
	
		// Protokoll für Prüfung entfernen
		if (strpos($sUrl, "http") == 0) {
			$sUrl = str_replace("https://", "", $sUrl);
			$sUrl = str_replace("http://", "", $sUrl);
		}
		
		
		$sVideoId = null;
		// Wenn normale youtubeurl
		if (strpos($sUrl, "v=") !== false) {

			// Parameter ermitteln
			if (strpos($sUrl, "?") !== false) {
				$sUrl = substr($sUrl, 0, strpos($sUrl, "?") + 1);
			}
				
			// Parameter splitten und durclaufen
			$aParams = explode("&", $sUrl);
			for ($i = 0; $i < count($aParams); $i++) {
	
				// Wenn Parameter v dann youtubeID!
				if (strpos($aParams[$i], "=") > 0) {
					if (strpos($aParams[$i], "v=") !== false) {
						$sVideoId = str_replace($aParams[$i], "v=", "");
					}
						
					// Wenn keine Parameterangabe (rewriterule bei youtube)
				} else {
					$sVideoId = $aParams[$i];
				}
			}
				
			// Wenn gekürzte youtubeURL (youtu.be/VIDEOID)
		} elseif (strpos($sUrl, "/") !== false) {
			$aParams = split("/", $sUrl);
			$sVideoId = $aParams[0];
			if (strpos($aParams[0], "youtu") !== false) {
				$sVideoId = $aParams[1];
			}
				
		// Wenn Video direkt übergeben wurde
		} else {
			$sVideoId = $sUrl;
		}

	
		// Wenn Video gefunden wurde
		if ($sVideoId != null) {
			return $sVideoId;
		}
		return "";
	}
	
	public function loadVideo($sVideoId) {
		$sVideoId = $this->getVideoId($sVideoId);
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_URL, $this->_sBaseUrl.$sVideoId);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		
		$sRespons = curl_exec($ch);
		if (curl_errno($ch) > 0) {
			// Fehlerausgabe
			echo curl_error($ch);
			curl_close($ch);
		} else {
			curl_close($ch);
			if (strpos($sRespons, "{") == 0) {
				$this->sRespons = trim($sRespons);
			}
				
		}
	}
	
	public function videoExist($sVideoId) {
		$sVideoId = $this->getVideoId($sVideoId);
	
		$this->loadVideo($sVideoId);
		$sRespons = $this->sRespons;
		
		try {
			if (strpos($sRespons, "{") == 0) {
				$aResponse = json_decode($sRespons, true);
				if (isset($aResponse["items"])) {
					$aItems = $aResponse["items"];
					if (count($aItems) > 0) {
						for ($i = 0; $i <= count($aItems); $i++) {
							$aItem = $aItems[$i];
							if (isset($aItem["snippet"])) {
								$aSnippet = $aItem["snippet"];
								if (isset($aSnippet["localized"])) {
									$sLocalized = $aSnippet["localized"];
									if (isset($sLocalized["title"])) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}catch (Exception $e) {
			var_dump("Youtube::videoExist", $e);
		}
	
		return false;
	}

	public function getCompactData() {
		$aData = array();
	
		try {
			$aResponse = json_decode($this->sRespons, true);
	
			$aItems = $aResponse["items"];
			$aItem = $aItems[0];
			$aSnippet = $aItem["snippet"];
			$aLocalized = $aSnippet["localized"];
			$aContentDetails = $aItem["contentDetails"];
	
			
			// Ermittel ID
			if (isset($aItem["id"])) $aData["id"] = $aItem["id"];
			else $aData["id"] = "";
	
			// Ermittel Publisher
			if (isset($aItem["channelTitle"])) $aData["publisher"] = $aItem["channelTitle"];
			else $aData["publisher"] = "";
	
			// Ermittel Titel
			if (isset($aSnippet["title"])) $aData["title"] = $aSnippet["title"];
			elseif (isset($aLocalized["title"])) $aData["title"] = $aLocalized["title"];
			else $aData["title"] = "";

			// Ermittel Länge
			if (isset($aContentDetails["duration"])) {
				$aDuration = $aContentDetails["duration"];
				$aDuration = str_replace("PT", "", $aDuration);
				$sHours = "0";
				$sMinutes = "0";
				$sSeconds = "0";
				
				if (strpos($aDuration, "H") !== false) {
					$sHours = substr($aDuration, 0, strpos($aDuration, "H"));
					$aDuration = substr($aDuration, strpos($aDuration, "H") + 1);
				}
				if (strpos($aDuration, "M") !== false) {
					$sMinutes = substr($aDuration, 0, strpos($aDuration, "M"));
					$aDuration = substr($aDuration, strpos($aDuration, "M") + 1);
				}
				if (strpos($aDuration, "S") !== false) {
					$sSeconds = substr($aDuration, 0, strpos($aDuration, "S"));
					$aDuration = substr($aDuration, strpos($aDuration, "S") + 1);
				}
	
				$sOnlyMinutes = "" + ((intval($sHours) * 60) + intval($sMinutes));
				$aData["duration"] = $sHours . ":" . $sMinutes . ":" . $sSeconds;
				$aData["durationMinutes"] = $sOnlyMinutes;
			} else {
				$aData["duration"] = "";
				$aData["durationMinutes"] = "";
			}
	
			// Ermittel Link
			$aData["link"] = "https://www.youtube.com/watch?v=" . $aData["id"];
	
		} catch (Exception $e) {
			var_dump("Youtube::getCompactData()",$e." | ".$this->sRespons);
		}

		return $aData;
	}
}