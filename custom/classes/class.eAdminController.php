<?php
include_once Config::getDocumentRoot()."/lib/php/pattern/extend.controller.php";

class eAdminController extends eController {
	private $sAdminLogin = "backendLogin";
	private $sAdminLoginKey;
	
	public function start() {
		$this->sSubfolder = "admin";
		
		// Alle Notwendigen Modulclassen laden
		parent::loadModulClasses("index", "admin");
		parent::loadModulClasses("admins", "admin");
		parent::loadModulClasses("settings", "admin");
		parent::loadModulClasses("log", "admin");
		
		// Sessionkey erstellen
		$this->sAdminLoginKey = md5(date("Y-m-d")."#".Config::$sAdminLoginKey);
		
		// Wurde sich eingeloggt
		$oAdmin = Factory::getModel("Admin", "admins");
		if (Params::postParams("action") == "login") {
			if ($oAdmin->checkLoginData(Params::postParams("admin_user"), Params::postParams("admin_pass"))) {
				Params::setCookie($this->sAdminLogin, $this->sAdminLoginKey);
				Params::setCookie("username", Params::postParams("admin_user"));
				LogModelLog::addLog("admin", "login", Params::cookieParams("username"), "login");
			}
		}
		
		// Wurde sich ausgeloggt
		if (Params::getParams("action") == "logout") {
			Params::removeCookie($this->sAdminLogin);
			Params::removeCookie("username");
			Params::setCookie($this->sAdminLogin, "invalid");
			Params::setCookie("username", "invalid");
		}
		
		// Sind logindaten korrekt
		if (Params::cookieParams($this->sAdminLogin) === false || Params::cookieParams($this->sAdminLogin) !== $this->sAdminLoginKey) {
 			header("Location: ".Config::getServerHost()."/admin/login");
 			die;
		}

 		// Bootstrap für nur Adminbereich laden
 		Config::adminBootstrap();

		$this->blExecuteMainBootstrap = false;
		parent::start();
		
		// Setze Admintemplate
		View::username(Params::cookieParams("username"));
		View::adminRights( $oAdmin->getRightsByUsername(Params::cookieParams("username")) );
		$mStyle = $oAdmin->getTemplate(Params::cookieParams("username"));
		if ($mStyle === false || empty($mStyle) || $mStyle == "") {
			$oSettings = Factory::getModel("settings", "settings");
			$mStyle = $oSettings->getSetting("style_admin_default");
			unset($oSettings);
		}
 		View::popTemplatePath("templates/".$mStyle);
	}
}