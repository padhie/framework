<?php
class language extends DbExtent {
	protected $_sTable = "language";
	protected $_sIdField = "idLanguage";
	
	public function reduceGui($sLang) {
		$this->_oQueryBuilder->addWhere("group", "web");
		$this->_oQueryBuilder->addWhere("lang", $sLang);
	}
}