<?php
class GamewispApi {
	// Base Variablen
	public $_sBaseUrl 			= "https://api.gamewisp.com/pub/v1/";
	
	// Channel Accesdaten
	private $_sClientId 		= "5b66a50ba38ab9e437c7f19fcbfa610c9ad729c";
	private $_sSecret 			= "302c92f9bf949c52b65244bdbc10aedcc7d38fb";
	private $_sRedirectUri 		= "http://xoneteam:9ghpidv_@xoneris.padhie.de/custom/api/?action=gamewispSub";
	
	// Api Daten
	public $sAuthCode			= "";
	public $sAuthToken			= "";
	public $sRefreshToken		= "";
	
	public $oAuthResponse;
	public $oTokenResponse;
	public $oChannelSubList;
	
	
	public function getCliendId() {
		return $this->_sClientId;
	}
	
	public function getRedirektUri() {
		return $this->_sRedirectUri;
	}
	
	public function getAutorisationUrl() {
		return $this->_sBaseUrl."oauth/authorize?".
				"client_id=".$this->_sClientId."&".
				"response_type=code&".
				"scope=read_only,subscriber_read_limited&".
				"state=ASKDLFJsisisks23k&".
				"redirect_uri=".$this->_sRedirectUri;
	}
	
	public function getAutorisation() {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->getAutorisationUrl());
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$this->oAuthResponse = json_decode(curl_exec($ch), true);
		curl_close($ch);
		
		if (isset($this->oAuthResponse["code"]))  {
			$this->sAuthCode	= $this->oAuthResponse["code"];
			return true;
		} else {
			return false;
		}
		
	}
	
	public function getToken() {
		$aFields = array(
			'grant_type' 	=> "authorization_code",
			'client_id' 	=> $this->_sClientId,
			'client_secret' => $this->_sSecret,
			'code' 			=> $this->sAuthCode,
			'redirect_uri' 	=> $this->_sRedirectUri,
		);
		
		if (isset($this->sRefreshToken)) {
			$aFields["refresh_token"]	= $this->sRefreshToken;
		} elseif (isset($this->oTokenResponse)) {
			$aFields["refresh_token"]	= $this->oTokenResponse["refresh_token"];
			$this->sRefreshToken		= $this->oTokenResponse["refresh_token"];
		}
		
		$sFieldString = "";
		foreach
		($aFields as $key=>$value) { 
			$sFieldString .= $key.'='.$value.'&'; 
		}
		rtrim($sFieldString, '&');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->_sBaseUrl."oauth/token");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, count($aFields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $sFieldString);
// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
// 			"Content-Type: text/html"
// // 			"Content-Type: application/json"
// 		));
		
		if( ($sResult=curl_exec($ch)) == false) {
			trigger_error(curl_error($ch));
		}
		
		$this->oTokenResponse = json_decode($sResult, true);
		curl_close($ch);
		
		if (isset($this->oTokenResponse["access_token"])) {
			$this->sAuthToken		= $this->oTokenResponse["access_token"];
			if (isset($this->oTokenResponse["refresh_token"])) {
				$this->sRefreshToken	= $this->oTokenResponse["refresh_token"];
			}
			return true;
		} else {
			return false;
		}		
	}
	
	public function getSubList($iLimit=10, $sOrder="desc") {
		$sParams	= "";	
		$sParams	.= "access_token=".$this->sAuthToken."&";
		$sParams	.= "include=user,tier&";
		$sParams	.= "limit=".$iLimit."&";
		$sParams	.= "order=".$sOrder."&";
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $this->_sBaseUrl."channel/subscribers?".$sParams);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$this->oChannelSubList = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if (isset($this->oChannelSubList["error"])) {
			return false;
		} else {
			return true;
		}
	}
	
	public function getUserInfo($iUserId=0, $sUsername="") {
		$sParams	= "";
		$sParams	.= "access_token=".$this->sAuthToken."&";
		$sParams	.= "scope=read_only&";
		$sParams	.= "include=user,tier&";
		$sParams	.= "type=twitch&";
		
		if ($iUserId >= 1) {
			$sParams	.= "user_id=".$iUserId."&";
		} elseif ($sUsername != "") {
			$sParams	.= "user_name=".$sUsername."&";
		} else {
			return false;
		}
		
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $this->_sBaseUrl."channel/subscriber-for-channel?".$sParams);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$aResult = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if (isset($aResult["error"])) {
			return false;
		} else {
			return $aResult;
		}
	}
}