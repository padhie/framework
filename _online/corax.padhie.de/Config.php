<?php

class Config {
	// DATABASE CONNECTION INFORMATIONS
	public static $sDbHost 			= "localhost";
	public static $sDbUser 			= "ni181510_2sql6";
	public static $sDbPassword 		= "6f19e30c";
	public static $sDbDatabase 		= "ni181510_2sql6";
	public static $sDbCollation 	= "UTF-8";

	
	// CUSTOM DEVELOP
	public static $sCustomBase 			= "/custom";
	public static $sCustomClasses 		= "/classes";
	public static $sCustomVendorClasses = "/vendor";
	public static $sCustomFiles 		= "/data";
	public static $sAdminLoginKey 		= "fY6EHmtpCP3K1p7XlKU1";


	// METHODES
	public static function bootstrap() {
		View::project("Corax");
		Translate::setLang("de");
		//Translate::setCsv(self::getDocumentRoot().self::$sCustomBase."/data/lang_de.csv");
		
		$oLang = new language();
		$oLang->reduceGui(Translate::getLang());
		Translate::setClass($oLang, "key", "value", "lang");
		
		LinkHelper::$sPattern = "{BASE}{MODUL}{CONTROLLER}{CUSTOM}";
		View::naviMenu(array(
			array("key"=>"home", 		"name"=>Translate::get("MENU_HOME"), 		"link"=>LinkHelper::_()),
			array("key"=>"commands", 	"name"=>Translate::get("MENU_COMMANDS"), 	"link"=>LinkHelper::modul("commands")),
			array("key"=>"quotes", 		"name"=>Translate::get("MENU_QUOTES"),	 	"link"=>LinkHelper::modul("quotes")),
			array("key"=>"songrequest", "name"=>Translate::get("MENU_SONGREQUEST"), "link"=>LinkHelper::modul("songrequest")),
			array("key"=>"rangs", 		"name"=>Translate::get("MENU_RANGS"), 		"link"=>LinkHelper::modul("rangs")),
			array("key"=>"user", 		"name"=>Translate::get("MENU_USERS"), 		"link"=>LinkHelper::modul("user")),
// 			array("key"=>"giveaway", 	"name"=>Translate::get("MENU_GIVEAWAY"), 	"link"=>LinkHelper::modul("giveaway")),
			array("key"=>"emotes", 		"name"=>Translate::get("MENU_EMOTES"), 		"link"=>LinkHelper::modul("emotes")),
		));
	}
	
	public static function adminBootstrap() {
		LinkHelper::setTmpPattern("{BASE}admin/{MODUL}{CONTROLLER}{CUSTOM}");
		View::adminNaviMenu(array(
			array("key"=>"home", 		"name"=>Translate::get("MENU_ADMIN_HOME"), 			"link"=>LinkHelper::_(),					"color"=>"danger",	"icon"=>"fa-dashboard"),
			array("key"=>"commands", 	"name"=>Translate::get("MENU_ADMIN_COMMANDS"), 		"link"=>LinkHelper::modul("commands"),		"color"=>"orange",	"icon"=>"fa-terminal"),
			array("key"=>"quotes", 		"name"=>Translate::get("MENU_ADMIN_QUOTES"),	 	"link"=>LinkHelper::modul("quotes"),		"color"=>"warning",	"icon"=>"fa-comment"),
			array("key"=>"songrequest", "name"=>Translate::get("MENU_ADMIN_SONGREQUEST"), 	"link"=>LinkHelper::modul("songrequest"),	"color"=>"success",	"icon"=>"fa-play"),
			array("key"=>"rangs", 		"name"=>Translate::get("MENU_ADMIN_RANGS"), 		"link"=>LinkHelper::modul("rangs"),			"color"=>"cyan",	"icon"=>"fa-trophy"),
			array("key"=>"admins", 		"name"=>Translate::get("MENU_ADMIN_ADMINS"), 		"link"=>LinkHelper::modul("admins"),		"color"=>"info",	"icon"=>"fa-user"),
			array("key"=>"user", 		"name"=>Translate::get("MENU_ADMIN_USERS"), 		"link"=>LinkHelper::modul("user"),			"color"=>"primary",	"icon"=>"fa-users"),
			array("key"=>"hosting", 	"name"=>Translate::get("MENU_ADMIN_HOSTING"), 		"link"=>LinkHelper::modul("hosting"),		"color"=>"violet",	"icon"=>"fa-video-camera"),
			array("key"=>"giveaway", 	"name"=>Translate::get("MENU_ADMIN_GIVEAWAY"), 		"link"=>LinkHelper::modul("giveaway"),		"color"=>"primary",	"icon"=>"fa-gift"),
			array("key"=>"templates", 	"name"=>Translate::get("MENU_ADMIN_TEMPLATES"), 	"link"=>LinkHelper::modul("templates"),		"color"=>"info",	"icon"=>"fa-table"),
			array("key"=>"carousel", 	"name"=>Translate::get("MENU_ADMIN_CAROUSEL"), 		"link"=>LinkHelper::modul("carousel"),		"color"=>"cyan",	"icon"=>"fa-repeat"),
			array("key"=>"counter", 	"name"=>Translate::get("MENU_ADMIN_COUNTER"), 		"link"=>LinkHelper::modul("counter"),		"color"=>"success",	"icon"=>"fa-calendar"),
			array("key"=>"language", 	"name"=>Translate::get("MENU_ADMIN_LANGUAGE"), 		"link"=>LinkHelper::modul("language"),		"color"=>"orange",	"icon"=>"fa-comment"),
			array("key"=>"settings", 	"name"=>Translate::get("MENU_ADMIN_SETTINGS"), 		"link"=>LinkHelper::modul("settings"),		"color"=>"danger",	"icon"=>"fa-gear"),
		));
	}
	
 	public static function getDocumentRoot () {
		return $_SERVER["DOCUMENT_ROOT"];		
	}
	
	public static function getServerHost ()	{
		$_sProtokoll = "http://";
		if (strstr($_SERVER["SERVER_PROTOCOL"], "HTTPS") !== false)
		{
			$_sProtokoll = "https://";
		} 
		
		return $_sProtokoll.$_SERVER["SERVER_NAME"];
	}
}