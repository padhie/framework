<?php

class Config {
	// DATABASE CONNECTION INFORMATIONS
	public static $sDbHost 			= "localhost";
	public static $sDbUser 			= "root";
	public static $sDbPassword 		= "";
	public static $sDbDatabase 		= "xonebot";
	public static $sDbCollation 	= "UTF-8";

	
	// CUSTOM DEVELOP
	public static $sCustomBase 		= "/custom";
	public static $sCustomClasses 	= "/classes";
	public static $sCustomVentorClasses = "/vendor";
	public static $sCustomFiles 	= "/data";
	public static $sAdminLoginKey 	= "fY6EHmtpCP3K1p7XlKUg";
	
	
	// METHODES
	public static function bootstrap() {
		Translate::setLang("de");
		Translate::setCsv(self::getDocumentRoot().self::$sCustomBase."/data/lang_de.csv");
	}
	
 	public static function getDocumentRoot () {
		return $_SERVER["DOCUMENT_ROOT"];		
	}
	
	public static function getServerHost ()	{
		$_sProtokoll = "http://";
		if (strstr($_SERVER["SERVER_PROTOCOL"], "HTTPS") !== false)
		{
			$_sProtokoll = "https://";
		} 
		
		return $_sProtokoll.$_SERVER["SERVER_NAME"];
	}
}