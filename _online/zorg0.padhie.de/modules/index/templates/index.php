<?php View::render("header"); ?>

<div class="col-md-9 stream">
	<iframe id="stream" src="http://twitch.tv/zorg0/embed" style="width:100%; height:753px;" scrolling="no" frameborder="0" allowfullscreen="true" autoplay="true"></iframe>
	<div class="stream-close"><i class="fa fa-times"></i></div>
</div>

<div class="col-md-3 chat">
	<iframe id="chat" src="http://www.twitch.tv/zorg0/chat" style="width:100%; height:753px;" scrolling="no" frameborder="0"></iframe>
	<div class="chat-close"><i class="fa fa-times"></i></div>
</div>


<script>
$(".chat-close").each(function() {
	$(this).on("click", function() {
		$($('.chat')[0]).remove();
		$($('.stream')[0])
			.removeClass("col-md-9")
			.addClass("col-md-12");
	});
});
$(".stream-close").each(function() {
	$(this).on("click", function() {
		$($('.stream')[0]).remove();
		$($('.chat')[0])
			.removeClass("col-md-3")
			.addClass("col-md-12");
	});
});
</script>


<?php View::render("footer"); ?>