<?php

class Config {
	// DATABASE CONNECTION INFORMATIONS
// 	public static $sDbHost 			= "localhost";
// 	public static $sDbUser 			= "ni181510_2sql4";
// 	public static $sDbPassword 		= "1lJLMBDNybK8RP25RKRr";
// 	public static $sDbDatabase 		= "ni181510_2sql4";
// 	public static $sDbCollation 	= "UTF-8";
	public static $sDbHost 			= "padhie.de.w01513bb.kasserver.com";
	public static $sDbUser 			= "d024df03";
	public static $sDbPassword 		= "UZu3pooNzpWt8T4p";
	public static $sDbDatabase 		= "d024df03";
	public static $sDbCollation 	= "UTF-8";

	
	// CUSTOM DEVELOP
	public static $sCustomBase 			= "/custom";
	public static $sCustomClasses 		= "/classes";
	public static $sCustomVendorClasses = "/vendor";
	public static $sCustomFiles 		= "/data";
	public static $sAdminLoginKey 		= "fY6EHmtpCP3K1p7XlKU1";

	// TEMPLATE CONFIGS
	public static $sTemplateFolder		= "/templates";

	// METHODES
	public static function bootstrap() {
		View::project("Kanja1");
		Translate::setLang("de");
		
		$oLang = new language();
		$oLang->reduceGui(Translate::getLang());
		Translate::setClass($oLang, "key", "value", "lang");
		
		LinkHelper::$sPattern = "{BASE}{MODUL}{CONTROLLER}{CUSTOM}";

		requireFileInFolder(Config::getDocumentRoot()."/module/cms/classes/");
		$oCmsMenu = Factory::getModel("menu", "cms");
		View::naviMenu($oCmsMenu->getMenuArray(1));
		View::footerMenu($oCmsMenu->getMenuArray(2));
		View::footerSocial($oCmsMenu->getMenuArray(3));
		unset($oCmsMenu, $aMenu);

		
		// Für Frontend Tabellenliste
		View::rangDisplay(array("zeit", "posts"));
		View::rangIcon(array(
			"viewer" => array(
				"subscriber"=>"<i class='fa fa-eur' style='color:green' aria-hidden='true'></i>",
			),
			"special" => array(
				"mod"=>"<i class='fa fa-space-shuttle' style='color:red' aria-hidden='true'></i>",
				"team"=>"<i class='fa fa-user' style='color:blue' aria-hidden='true'></i>",
				"subscriber"=>"<i class='fa fa-eur' style='color:green' aria-hidden='true'></i>",
				"tester"=>"<i class='fa fa-american-sign-language-interpreting' style='color:magenta' aria-hidden='true'></i>",
			)
		));
	}
	
	public static function adminBootstrap() {
		LinkHelper::setTmpPattern("{BASE}admin/{MODUL}{CONTROLLER}{CUSTOM}");
		View::adminNaviMenu(array(
			array("key"=>"home", 		"name"=>Translate::get("MENU_ADMIN_HOME"), 			"link"=>LinkHelper::_(),					"color"=>"danger",	"icon"=>"fa-dashboard"),
			array("key"=>"commands", 	"name"=>Translate::get("MENU_ADMIN_COMMANDS"), 		"link"=>LinkHelper::modul("commands"),		"color"=>"orange",	"icon"=>"fa-terminal"),
			array("key"=>"quotes", 		"name"=>Translate::get("MENU_ADMIN_QUOTES"),	 	"link"=>LinkHelper::modul("quotes"),		"color"=>"warning",	"icon"=>"fa-comment"),
			array("key"=>"songrequest", "name"=>Translate::get("MENU_ADMIN_SONGREQUEST"), 	"link"=>LinkHelper::modul("songrequest"),	"color"=>"success",	"icon"=>"fa-play"),
			array("key"=>"rangs", 		"name"=>Translate::get("MENU_ADMIN_RANGS"), 		"link"=>LinkHelper::modul("rangs"),			"color"=>"cyan",	"icon"=>"fa-trophy"),
			array("key"=>"admins", 		"name"=>Translate::get("MENU_ADMIN_ADMINS"), 		"color"=>"info",	"icon"=>"fa-user",			"subnavi"=>array(
					array("key"=>"alladmins", 	"name"=>Translate::get("MENU_ADMIN_ALL"), 		"link"=>LinkHelper::controller("admins", "all"),		"color"=>"info",	"icon"=>"fa-circle"),
					array("key"=>"ownadmins", 	"name"=>Translate::get("MENU_ADMIN_OWN"), 		"link"=>LinkHelper::controller("admins", "own"),		"color"=>"info",	"icon"=>"fa-circle"),
			)),	
			array("key"=>"user", 		"name"=>Translate::get("MENU_ADMIN_USERS"), 		"link"=>LinkHelper::modul("user"),			"color"=>"primary",	"icon"=>"fa-users"),
			// array("key"=>"giveaway", 	"name"=>Translate::get("MENU_ADMIN_GIVEAWAY"), 		"link"=>LinkHelper::modul("giveaway"),		"color"=>"primary",	"icon"=>"fa-gift",			"betaflag"=>true),
			// array("key"=>"templates", 	"name"=>Translate::get("MENU_ADMIN_TEMPLATES"), 	"link"=>LinkHelper::modul("templates"),		"color"=>"info",	"icon"=>"fa-table"),
			// array("key"=>"carousel", 	"name"=>Translate::get("MENU_ADMIN_CAROUSEL"), 		"link"=>LinkHelper::modul("carousel"),		"color"=>"cyan",	"icon"=>"fa-repeat"),
			// array("key"=>"counter", 	"name"=>Translate::get("MENU_ADMIN_COUNTER"), 		"link"=>LinkHelper::modul("counter"),		"color"=>"success",	"icon"=>"fa-calendar"),
			array("key"=>"language", 	"name"=>Translate::get("MENU_ADMIN_LANGUAGE"), 		"link"=>LinkHelper::modul("language"),		"color"=>"orange",	"icon"=>"fa-comment"),
			// array("key"=>"log", 		"name"=>Translate::get("MENU_ADMIN_LOG"), 			"color"=>"danger",	"icon"=>"fa-file-text-o",	"subnavi"=>array(
			// 		array("key"=>"commandlog", 	"name"=>Translate::get("MENU_ADMIN_COMMANDLOG"),	"link"=>LinkHelper::controller("log", "command"),	"color"=>"info",	"icon"=>"fa-circle"),
			// 		array("key"=>"chatlog",		"name"=>Translate::get("MENU_ADMIN_CHATLOG"), 		"link"=>LinkHelper::controller("log", "chat"),		"color"=>"info",	"icon"=>"fa-circle"),
			// 		array("key"=>"modlog",		"name"=>Translate::get("MENU_ADMIN_MODLOG"), 		"link"=>LinkHelper::controller("log", "mod"),		"color"=>"info",	"icon"=>"fa-circle"),
			// 		array("key"=>"adminlog", 	"name"=>Translate::get("MENU_ADMIN_ADMINLOG"), 		"link"=>LinkHelper::controller("log", "admin"),		"color"=>"info",	"icon"=>"fa-circle"),
			// )),
			// array("key"=>"lists", 		"name"=>Translate::get("MENU_ADMIN_LISTS"), 		"color"=>"orange",	"icon"=>"fa-list",			"betaflag"=>true,	"subnavi"=>array(
			// 		array("key"=>"wordlists", 	"name"=>Translate::get("MENU_ADMIN_WORDLIST"),		"link"=>LinkHelper::controller("lists", "word"),	"color"=>"info",	"icon"=>"fa-circle"),
			// 		array("key"=>"linklists",	"name"=>Translate::get("MENU_ADMIN_LINKLIST"), 		"link"=>LinkHelper::controller("lists", "link"),	"color"=>"info",	"icon"=>"fa-circle"),
			// )),
			array("key"=>"cms", 		"name"=>Translate::get("MENU_ADMIN_CMS"), 			"color"=>"orange",	"icon"=>"fa-file-text",		"subnavi"=>array(
				array("key"=>"cmsmenu", 	"name"=>Translate::get("MENU_ADMIN_CMS_MENU"),		"link"=>LinkHelper::controller("cms", "menu"),	"color"=>"info",	"icon"=>"fa-circle"),
				array("key"=>"cmspage",		"name"=>Translate::get("MENU_ADMIN_CMS_PAGES"), 	"link"=>LinkHelper::controller("cms", "page"),	"color"=>"info",	"icon"=>"fa-circle"),
			)),
			array("key"=>"settings", 	"name"=>Translate::get("MENU_ADMIN_SETTINGS"), 		"link"=>LinkHelper::modul("settings"),		"color"=>"success",	"icon"=>"fa-gear"),
		));
	}
	
 	public static function getDocumentRoot () {
		return $_SERVER["DOCUMENT_ROOT"];		
	}
	
	public static function getServerHost ()	{
		$_sProtokoll = "http://";
		if (strstr($_SERVER["SERVER_PROTOCOL"], "HTTPS") !== false)
		{
			$_sProtokoll = "https://";
		} 
		
		return $_sProtokoll.$_SERVER["SERVER_NAME"];
	}
}