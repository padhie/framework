<?php
include_once __DIR__."/lib/php/Core/ConfigCore.php";
class Config extends ConfigCore {
	// DATABASE CONNECTION INFORMATIONS
	public static $sDbHost 			= "padhie.de.w01513bb.kasserver.com";
	public static $sDbUser 			= "d02474dd";
	public static $sDbPassword 		= "RRvv3p5MDNLKL5os";
	public static $sDbDatabase 		= "d02474dd";
	public static $sDbCollation 	= "UTF-8";
	
	// MODUL-CONFIGS
	public static $aModules 		= array();

	// PATH
	public static $sModulFolder			= "/module";
	public static $sCustomBase 			= "/custom";
	public static $sCustomClasses 		= "/classes";
	public static $sCustomVendorClasses = "/vendor";
	public static $sCustomFiles 		= "/data";
	public static $sTemplateFolder		= "/templates";

	// ADMIN-CONFIGS
	public static $sAdminLoginKey 		= "fY6EHmtpCP3K1p7XlKUg";


	
	// METHODES
	public static function bootstrap() {
		View::init();
		View::addTemplatePath("base");
		View::project("Zorg0");
		Translate::setLang("de");

		$oLang = new language();
		$oLang->reduceGui(Translate::getLang());
		Translate::setClass($oLang, "key", "value", "lang");
		
		LinkHelper::$sPattern = "{BASE}/{MODUL}/{CONTROLLER}/{CUSTOM}/";

		// Navis
		// $oCmsMenu = Factory::getModel("menu", "cms");
		// View::footerMenu($oCmsMenu->getMenuArray(2));
		// View::footerSocial($oCmsMenu->getMenuArray(3));
		// unset($oCmsMenu, $aMenu);

	}
	
	public static function adminBootstrap() {
		// ##### LINKPATTERN #####
		// Linkbasis für alle durch LinkHelper erstellten Links 
		LinkHelper::setTmpPattern("{BASE}/{SUBFOLDER}/{MODUL}/{CONTROLLER}/{CUSTOM}/");
		LinkHelper::$sSubfolder = "admin";

		// ##### NAVIGATION #####
		// Nur beim ersten mal ausführen
		if (View::adminNaviMenu() === false || View::adminNaviMenu() === null) {
			$aModullist = ModulHelper::getModullist("admin");
			$aNavi = array();

			// Alle Adminmodule laden und durchlaufen (um Config auszuführen)
			foreach ($aModullist as $aModul) {

				// Ist eine Config vorhanden?
				if ($aModul["settings"] == true) {

					// Alle Gefundene Module in Configarray speichern
					array_push(self::$aModules, $aModul["name"]);

					// Modulconfig und MenüArray Laden
					require_once $aModul["path"]."/config.php";
					$sModulConfig = ModulHelper::getConfigClassname($aModul["name"]);
					$oModulConfig = new $sModulConfig();

					// Index/Hauptmodul (im Adminbereich das Dashboard) an erste stelle setzen
					if ($aModul["name"] == "index") {
						array_unshift($aNavi, $oModulConfig->getMenuConfig());
					} else {
						array_push($aNavi, $oModulConfig->getMenuConfig());
					}
				}
			}
 				View::adminNaviMenu($aNavi);
		}
	}
}